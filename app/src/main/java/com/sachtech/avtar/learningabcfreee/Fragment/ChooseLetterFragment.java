package com.sachtech.avtar.learningabcfreee.Fragment;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Constants.Config;
import com.sachtech.avtar.learningabcfreee.Constants.DataHolder;
import com.sachtech.avtar.learningabcfreee.FragmentContainerActivity;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;

public class ChooseLetterFragment extends Fragment implements View.OnClickListener {

    private RelativeLayout relativeLayoutsmall, relativeLayoutcaps;
    private Button textsmall_button, textCaps_button;
    private Button mButtonA, mButtonB, mButtonC, mButtonD, mButtonE, mButtonF, mButtonG, mButtonH, mButtonI, mButtonJ, mButtonK, mButtonL, mButtonM, mButtonN, mButtonO, mButtonP, mButtonQ, mButtonR, mButtonS, mButtonT, mButtonU, mButtonV, mButtonW, mButtonX, mButtonY, mButtonZ;
    private Button mButtona, mButtonb, mButtonc, mButtond, mButtone, mButtonf, mButtong, mButtonh, mButtoni, mButtonj, mButtonk, mButtonl, mButtonm, mButtonn, mButtono, mButtonp, mButtonq, mButtonr, mButtons, mButtont, mButtonu, mButtonv, mButtonw, mButtonx, mButtony, mButtonz;

    private static ChooseLetterFragment chooseLetterFrag;

    private ObjectAnimator anim = null;

    public static ChooseLetterFragment getInstance() {
        return chooseLetterFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_choose_letter);

        chooseLetterFrag = this;


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_choose_letter, container, false);
        initView(view);
        FragmentContainerActivity.getInstance().homeButton.setVisibility(View.VISIBLE);
        return view;
    }

    private void initView(View view) {
        relativeLayoutsmall = (RelativeLayout) view.findViewById(R.id.relativelayoutsmall);
        relativeLayoutcaps = (RelativeLayout) view.findViewById(R.id.relativelayoutCaps);
        textsmall_button = (Button) view.findViewById(R.id.buttonSmallA);
        textCaps_button = (Button) view.findViewById(R.id.buttonCapsA);

        mButtonA = (Button) view.findViewById(R.id.button_A);
        mButtona = (Button) view.findViewById(R.id.button_a);
        mButtonA.setOnClickListener(this);
        mButtona.setOnClickListener(this);

        mButtonB = (Button) view.findViewById(R.id.button_B);
        mButtonb = (Button) view.findViewById(R.id.button_b);
        mButtonB.setOnClickListener(this);
        mButtonb.setOnClickListener(this);

        mButtonC = (Button) view.findViewById(R.id.button_C);
        mButtonc = (Button) view.findViewById(R.id.button_c);
        mButtonC.setOnClickListener(this);
        mButtonc.setOnClickListener(this);

        mButtonD = (Button) view.findViewById(R.id.button_D);
        mButtond = (Button) view.findViewById(R.id.button_d);
        mButtonD.setOnClickListener(this);
        mButtond.setOnClickListener(this);

        mButtonE = (Button) view.findViewById(R.id.button_E);
        mButtone = (Button) view.findViewById(R.id.button_e);
        mButtonE.setOnClickListener(this);
        mButtone.setOnClickListener(this);

        mButtonF = (Button) view.findViewById(R.id.button_F);
        mButtonf = (Button) view.findViewById(R.id.button_f);
        mButtonF.setOnClickListener(this);
        mButtonf.setOnClickListener(this);

        mButtonG = (Button) view.findViewById(R.id.button_G);
        mButtong = (Button) view.findViewById(R.id.button_g);
        mButtonG.setOnClickListener(this);
        mButtong.setOnClickListener(this);

        mButtonH = (Button) view.findViewById(R.id.button_H);
        mButtonh = (Button) view.findViewById(R.id.button_h);
        mButtonH.setOnClickListener(this);
        mButtonh.setOnClickListener(this);

        mButtonI = (Button) view.findViewById(R.id.button_I);
        mButtoni = (Button) view.findViewById(R.id.button_i);
        mButtonI.setOnClickListener(this);
        mButtoni.setOnClickListener(this);

        mButtonJ = (Button) view.findViewById(R.id.button_J);
        mButtonj = (Button) view.findViewById(R.id.button_j);
        mButtonJ.setOnClickListener(this);
        mButtonj.setOnClickListener(this);

        mButtonK = (Button) view.findViewById(R.id.button_K);
        mButtonk = (Button) view.findViewById(R.id.button_k);
        mButtonK.setOnClickListener(this);
        mButtonk.setOnClickListener(this);

        mButtonL = (Button) view.findViewById(R.id.button_L);
        mButtonl = (Button) view.findViewById(R.id.button_l);
        mButtonL.setOnClickListener(this);
        mButtonl.setOnClickListener(this);

        mButtonM = (Button) view.findViewById(R.id.button_M);
        mButtonm = (Button) view.findViewById(R.id.button_m);
        mButtonM.setOnClickListener(this);
        mButtonm.setOnClickListener(this);

        mButtonN = (Button) view.findViewById(R.id.button_N);
        mButtonn = (Button) view.findViewById(R.id.button_n);
        mButtonN.setOnClickListener(this);
        mButtonn.setOnClickListener(this);

        mButtonO = (Button) view.findViewById(R.id.button_O);
        mButtono = (Button) view.findViewById(R.id.button_o);
        mButtonO.setOnClickListener(this);
        mButtono.setOnClickListener(this);

        mButtonP = (Button) view.findViewById(R.id.button_P);
        mButtonp = (Button) view.findViewById(R.id.button_p);
        mButtonP.setOnClickListener(this);
        mButtonp.setOnClickListener(this);

        mButtonQ = (Button) view.findViewById(R.id.button_Q);
        mButtonq = (Button) view.findViewById(R.id.button_q);
        mButtonQ.setOnClickListener(this);
        mButtonq.setOnClickListener(this);

        mButtonR = (Button) view.findViewById(R.id.button_R);
        mButtonr = (Button) view.findViewById(R.id.button_r);
        mButtonR.setOnClickListener(this);
        mButtonr.setOnClickListener(this);

        mButtonS = (Button) view.findViewById(R.id.button_S);
        mButtons = (Button) view.findViewById(R.id.button_s);
        mButtonS.setOnClickListener(this);
        mButtons.setOnClickListener(this);

        mButtonT = (Button) view.findViewById(R.id.button_T);
        mButtont = (Button) view.findViewById(R.id.button_t);
        mButtonT.setOnClickListener(this);
        mButtont.setOnClickListener(this);

        mButtonU = (Button) view.findViewById(R.id.button_U);
        mButtonu = (Button) view.findViewById(R.id.button_u);
        mButtonU.setOnClickListener(this);
        mButtonu.setOnClickListener(this);

        mButtonV = (Button) view.findViewById(R.id.button_V);
        mButtonv = (Button) view.findViewById(R.id.button_v);
        mButtonV.setOnClickListener(this);
        mButtonv.setOnClickListener(this);

        mButtonW = (Button) view.findViewById(R.id.button_W);
        mButtonw = (Button) view.findViewById(R.id.button_w);
        mButtonW.setOnClickListener(this);
        mButtonw.setOnClickListener(this);

        mButtonX = (Button) view.findViewById(R.id.button_X);
        mButtonx = (Button) view.findViewById(R.id.button_x);
        mButtonX.setOnClickListener(this);
        mButtonx.setOnClickListener(this);

        mButtonY = (Button) view.findViewById(R.id.button_Y);
        mButtony = (Button) view.findViewById(R.id.button_y);
        mButtonY.setOnClickListener(this);
        mButtony.setOnClickListener(this);

        mButtonZ = (Button) view.findViewById(R.id.button_Z);
        mButtonz = (Button) view.findViewById(R.id.button_z);
        mButtonZ.setOnClickListener(this);
        mButtonz.setOnClickListener(this);

        textCaps_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  startAnimationA(mButtonA,4000);
                startAnimationB(mButtonB,4000);
                startAnimationC(mButtonC,4000);
                startAnimationD(mButtonD,4000);*/
                startAnimation(relativeLayoutcaps, 3000);
                Sound.playButtonClick(getActivity());

                textCaps_button.setBackgroundResource(R.drawable.button_caps_select);
                textsmall_button.setBackgroundResource(R.drawable.button_small_unselect);
                DataHolder.LETTER_TYPE = Config.CAPS;
                relativeLayoutsmall.setVisibility(View.GONE);
                relativeLayoutcaps.setVisibility(View.VISIBLE);
            }
        });
        textsmall_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* startAnimationa(mButtona,4000);
                startAnimationb(mButtonb,4000);
                startAnimationc(mButtonc,4000);
                startAnimationd(mButtond,4000);*/
                Sound.playButtonClick(getActivity());
                startAnimation(relativeLayoutsmall, 3000);
                DataHolder.LETTER_TYPE = Config.SMALL;
                textCaps_button.setBackgroundResource(R.drawable.button_caps_unselect);
                textsmall_button.setBackgroundResource(R.drawable.button_small_select);
                relativeLayoutcaps.setVisibility(View.GONE);
                relativeLayoutsmall.setVisibility(View.VISIBLE);
            }
        });


       /* startAnimationA(mButtonA, 3000);
        startAnimationB(mButtonB, 4000);
        startAnimationC(mButtonC, 4000);
        startAnimationD(mButtonD, 4000);*/

        switch (DataHolder.LETTER_TYPE) {
            case Config.CAPS:
                relativeLayoutcaps.setVisibility(View.VISIBLE);
                relativeLayoutsmall.setVisibility(View.GONE);
                textCaps_button.setBackgroundResource(R.drawable.button_caps_select);
                textsmall_button.setBackgroundResource(R.drawable.button_small_unselect);
                startAnimation(relativeLayoutcaps, 3000);
                break;
            case Config.SMALL:
                relativeLayoutcaps.setVisibility(View.GONE);
                relativeLayoutsmall.setVisibility(View.VISIBLE);
                textCaps_button.setBackgroundResource(R.drawable.button_caps_unselect);
                textsmall_button.setBackgroundResource(R.drawable.button_small_select);
                startAnimation(relativeLayoutsmall, 3000);

                break;
        }

    }

    private synchronized void startAnimation(View view, int time) {

        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.bounce_in_down);
        anim.setTarget(view);
        anim.setDuration(time);
        anim.start();
    }

    private void startAnimationB(Button image, int time) {
        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.flip_anim);
        anim.setTarget(image);
        anim.setDuration(time);
        anim.start();
    }

    private void startAnimationC(Button image, int time) {
        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.flip_anim);
        anim.setTarget(image);
        anim.setDuration(time);
        anim.start();
    }

    private void startAnimationD(Button image, int time) {
        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.flip_anim);
        anim.setTarget(image);
        anim.setDuration(time);
        anim.start();
    }

    private synchronized void startAnimationa(Button image, int time) {


        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.flip_anim);
        anim.setTarget(image);
        anim.setDuration(time);
        anim.start();
    }

    private void startAnimationb(Button image, int time) {
        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.flip_anim);
        anim.setTarget(image);
        anim.setDuration(time);
        anim.start();
    }

    private void startAnimationc(Button image, int time) {
        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.flip_anim);
        anim.setTarget(image);
        anim.setDuration(time);
        anim.start();
    }

    private void startAnimationd(Button image, int time) {
        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.flip_anim);
        anim.setTarget(image);
        anim.setDuration(time);
        anim.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_A:
                Sound.playButtonClick(getActivity());
              /*  startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                .putExtra(Config.Letter,Config.A));*/
                DataHolder.LETTER = Config.A;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_a:
                Sound.playButtonClick(getActivity());
               /* startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.a));*/
                DataHolder.LETTER = Config.a;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_B:
                Sound.playButtonClick(getActivity());
                DataHolder.LETTER = Config.B;
               /* startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.B));*/
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_b:
                Sound.playButtonClick(getActivity());
               /* startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.b));*/
                DataHolder.LETTER = Config.b;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_D:
                Sound.playButtonClick(getActivity());
               /* startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                    .putExtra(Config.Letter,Config.C));*/
                DataHolder.LETTER = Config.D;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_d:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.c));*/
                DataHolder.LETTER = Config.d;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_C:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.D));*/
                DataHolder.LETTER = Config.C;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_c:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.c;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_E:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.E;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_e:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.e;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_F:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.F;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_f:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.f;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_G:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.G;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_g:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.g;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_H:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.H;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_h:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.h;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_I:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));*/
                DataHolder.LETTER = Config.I;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_i:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.i;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_J:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.J;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_j:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.j;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_K:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.K;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_k:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.k;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_L:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.L;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_l:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.l;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_M:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.M;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_m:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.m;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_N:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.N;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_n:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.n;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_O:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.O;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_o:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.o;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_P:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.P;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_p:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.p;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_Q:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.Q;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_q:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.q;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_R:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.R;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_r:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.r;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_S:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.S;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_s:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.s;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_T:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.T;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_t:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.t;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_U:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.U;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_u:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.u;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_V:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.V;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_v:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.v;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_W:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.W;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_w:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.w;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_X:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.X;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_x:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.x;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_Y:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.Y;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_y:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.y;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_Z:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.Z;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;
            case R.id.button_z:
                Sound.playButtonClick(getActivity());
                /*startActivity(new Intent(getActivity(),DrawLetterActivity.class)
                        .putExtra(Config.Letter,Config.d));/*/
                DataHolder.LETTER = Config.z;
                ((FragmentContainerActivity) getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                break;


        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_letter_type, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
