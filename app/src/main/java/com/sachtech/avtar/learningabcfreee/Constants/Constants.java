package com.sachtech.avtar.learningabcfreee.Constants;

public class Constants {
	public static final int MAX_DELAY = 100;
	public static final int ANIM_DURATION = 2000;
	public static final int EMPTY_MESSAGE_WHAT = 0x001;
}
