package com.sachtech.avtar.learningabcfreee.Constants;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nardeep Sandhu on 3/14/2016.
 */
public class LetterPointsConfig {

    public static float scale = 0;

    ///////////////////Lettr A?///////////////////


    public static int E_TOTAL_STEPS = 4;
    public static int F_TOTAL_STEPS = 3;
    public static int H_TOTAL_STEPS = 3;
    public static int T_TOTAL_STEPS = 2;
    public static int V_TOTAL_STEPS = 2;
    public static int X_TOTAL_STEPS = 2;
    public static int K_TOTAL_STEPS = 4;
    public static int N_TOTAL_STEPS =3;
    public static int Y_TOTAL_STEPS = 3;
    public static int W_TOTAL_STEPS =4;
    public static int Z_TOTAL_STEPS = 3;
    public static int I_TOTAL_STEPS = 1;
    public static int A_TOTAL_STEPS = 3;
    public static int M_TOTAL_STEPS = 4;
    public static int L_TOTAL_STEPS = 2;

    public static List<Point>[] point_L_Array = new List[L_TOTAL_STEPS];
    public static List<Point>[] point_M_Array = new List[M_TOTAL_STEPS];
    public static List<Point>[] point_A_Array = new List[A_TOTAL_STEPS];
    public static List<Point>[] point_I_Array = new List[I_TOTAL_STEPS];
    public static List<Point>[] point_Z_Array = new List[Z_TOTAL_STEPS];
    public static List<Point>[] point_W_Array = new List[W_TOTAL_STEPS];
    public static List<Point>[] point_Y_Array = new List[Y_TOTAL_STEPS];
    public static List<Point>[] point_N_Array = new List[N_TOTAL_STEPS];
    public static List<Point>[] point_K_Array = new List[K_TOTAL_STEPS];
    public static List<Point>[] point_E_Array = new List[E_TOTAL_STEPS];
    public static List<Point>[] point_F_Array = new List[F_TOTAL_STEPS];
    public static List<Point>[] point_H_Array = new List[H_TOTAL_STEPS];
    public static List<Point>[] point_V_Array = new List[V_TOTAL_STEPS];
    public static List<Point>[] point_T_Array = new List[T_TOTAL_STEPS];
    public static List<Point>[] point_X_Array = new List[X_TOTAL_STEPS];











////////////////////////////////////////////////// A /////////////////////////////////////////////

    public static void getADetails(){



        List<Point> pt_A_0 = new ArrayList<Point>();
        List<Point> pt_A_1 = new ArrayList<Point>();
        List<Point> pt_A_2 = new ArrayList<Point>();

        int xA1 = (int) (scale * 150);
        int yA1 = (int) (scale * 60);

        int xA2 = (int) (scale * 80);
        int yA2 = (int) (scale * 240);

        int xA3 = (int) (scale * 210);
        int yA3 = (int) (scale * 240);

        int xA4 = (int) (scale * 110);
        int yA4 = (int) (scale * 195);

        int xA5 = (int) (scale * 190);
        int yA5 = (int) (scale * 195);

        Point pA1 = new Point(xA1, yA1);
        Point pA2 = new Point(xA2, yA2);
        Point pA3 = new Point(xA3, yA3);
        Point pA4 = new Point(xA4, yA4);
        Point pA5 = new Point(xA5, yA5);


        pt_A_0.add(pA1);
        pt_A_0.add(pA2);
        pt_A_1.add(pA1);
        pt_A_1.add(pA3);
        pt_A_2.add(pA4);
        pt_A_2.add(pA5);

        point_A_Array[0] = pt_A_0;
        point_A_Array[1] =pt_A_1;
        point_A_Array[2] = pt_A_2;

    }


//////////////////////////////////////////////////////////////////////////




    ////////////////////////////// B //////////////////////////////

    public static int B_TOTAL_STEPS = 8;

    public static List<Point>[] point_B_Array = new List[B_TOTAL_STEPS];
    public static void getBDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();
        List<Point> pt_C_8 = new ArrayList<Point>();

        // List<Point> pt_C_13 = new ArrayList<Point>();
        //List<Point> pt_C_14 = new ArrayList<Point>();

        int x1 = (int) (scale * 85);
        int y1 = (int) (scale * 60);

       /* int x3 = (int) (scale * 60);
        int y3 = (int) (scale * 125);*/
        int x3 = (int) (scale * 85);
        int y3 = (int) (scale * 240);

        int x4 = (int) (scale * 90);
        int y4 = (int) (scale * 60);



        int x6 = (int) (scale * 180);
        int y6 = (int) (scale * 65);



        int x8 = (int) (scale * 190);
        int y8 = (int) (scale * 120);

        int x15 = (int) (scale * 150);
        int y15 = (int) (scale * 150);

        int x9 = (int) (scale * 105);
        int y9= (int) (scale * 150);

        int x10 = (int) (scale * 180);
        int y10 = (int) (scale * 150);

        int x11 = (int) (scale * 205);
        int y11 = (int) (scale * 195);



        int x13 = (int) (scale * 165);
        int y13 = (int) (scale * 240);

        int x14 = (int) (scale * 95);
        int y14 = (int) (scale * 240);

      /*  int x14 = (int) (scale * 65);
        int y14 = (int) (scale * 195);
*/

        Point p1 = new Point(x1, y1);


        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        Point p6 = new Point(x6, y6);

        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
        Point p10 = new Point(x10, y10);
        Point p11 = new Point(x11, y11);

        Point p13 = new Point(x13, y13);
        Point p14 = new Point(x14, y14);
        Point p15 = new Point(x15, y15);


        pt_C_0.add(p1);
        pt_C_0.add(p3);

        pt_C_1.add(p4);
        pt_C_1.add(p6);

        pt_C_2.add(p6);
        pt_C_2.add(p8);

        pt_C_3.add(p8);
        pt_C_3.add(p15);

        pt_C_4.add(p15);
        pt_C_4.add(p9);

        pt_C_5.add(p10);
        pt_C_5.add(p11);

        pt_C_6.add(p11);
        pt_C_6.add(p13);

        pt_C_7.add(p13);
        pt_C_7.add(p14);

        point_B_Array[0] = pt_C_0;
        point_B_Array[1] =pt_C_1;
        point_B_Array[2] = pt_C_2;
        point_B_Array[3] = pt_C_3;
        point_B_Array[4] = pt_C_4;
        point_B_Array[5] = pt_C_5;
        point_B_Array[6] = pt_C_6;
        point_B_Array[7] = pt_C_7;


        //  point_B_Array[13] = pt_C_13;
        // point_B_Array[14] = pt_C_14;

    }

    ///////////////////////////////////////////////////////////
    ////////////////////////////// C //////////////////////////////
    public static int C_TOTAL_STEPS = 7;

    public static List<Point>[] point_C_Array = new List[C_TOTAL_STEPS];
    public static void getCDetails(){

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();


        int x1 = (int) (scale * 210);
        int y1 = (int) (scale * 95);

        int x6 = (int) (scale * 190);
        int y6 = (int) (scale * 60);

        int x2 = (int) (scale * 130);
        int y2 = (int) (scale * 60);

        int x7 = (int) (scale * 95);
        int y7 = (int) (scale * 100);


        int x3 = (int) (scale * 95);
        int y3 = (int) (scale * 195);

        int x4 = (int) (scale * 130);
        int y4 = (int) (scale * 240);

        int x5 = (int) (scale * 210);
        int y5 = (int) (scale * 200);

        int x8= (int) (scale * 190);
        int y8 = (int) (scale * 240);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);

        pt_C_0.add(p1);
        pt_C_0.add(p6);
        pt_C_1.add(p6);
        pt_C_1.add(p2);

        pt_C_2.add(p2);
        pt_C_2.add(p7);

        pt_C_3.add(p7);
        pt_C_3.add(p3);

        pt_C_4.add(p3);
        pt_C_4.add(p4);

        pt_C_5.add(p4);
        pt_C_5.add(p8);

        pt_C_6.add(p8);
        pt_C_6.add(p5);

        point_C_Array[0] = pt_C_0;
        point_C_Array[1] =pt_C_1;
        point_C_Array[2] = pt_C_2;
        point_C_Array[3] = pt_C_3;
        point_C_Array[4] = pt_C_4;
        point_C_Array[5] = pt_C_5;
        point_C_Array[6] = pt_C_6;


    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

   ///////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////D/////////////////////////////////
    public static int D_TOTAL_STEPS = 6;

    public static List<Point>[] point_D_Array = new List[D_TOTAL_STEPS];
    public static void getDDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 90);
        int y1 = (int) (scale * 60);



        int x3 = (int) (scale * 90);
        int y3 = (int) (scale * 235);

        int x4 = (int) (scale * 100);
        int y4 = (int) (scale * 60);

        int x5 = (int) (scale * 180);
        int y5 = (int) (scale * 70);

        int x6 = (int) (scale * 210);
        int y6 = (int) (scale * 120);

        int x7 = (int) (scale * 210);
        int y7 = (int) (scale * 190);



        int x9 = (int) (scale * 160);
        int y9= (int) (scale * 235);

        int x10 = (int) (scale * 100);
        int y10= (int) (scale * 235);

        Point p1 = new Point(x1, y1);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);

        Point p9 = new Point(x9, y9);
        Point p10= new Point(x10, y10);

        pt_C_0.add(p1);
        pt_C_0.add(p3);


        pt_C_2.add(p4);
        pt_C_2.add(p5);

        pt_C_3.add(p5);
        pt_C_3.add(p6);

        pt_C_4.add(p6);
        pt_C_4.add(p7);

        pt_C_5.add(p7);
        pt_C_5.add(p9);

        pt_C_6.add(p9);
        pt_C_6.add(p10);



        point_D_Array[0] = pt_C_0;
        point_D_Array[1] = pt_C_2;
        point_D_Array[2] = pt_C_3;
        point_D_Array[3] = pt_C_4;
        point_D_Array[4] = pt_C_5;
        point_D_Array[5] = pt_C_6;


    }
////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////  E ///////////////////////////////////////////
    public static void getEDetails(){

        List<Point> pt_E_0 = new ArrayList<Point>();
        List<Point> pt_E_1 = new ArrayList<Point>();
        List<Point> pt_E_2 = new ArrayList<Point>();
        List<Point> pt_E_3 = new ArrayList<Point>();
        int xE1 = (int) (scale * 105);
        int yE1 = (int) (scale * 60);

        int xE2 = (int) (scale * 105);
        int yE2 = (int) (scale * 240);

        int xE3 = (int) (scale * 205);
        int yE3 = (int) (scale * 60);

        int xE4 = (int) (scale * 118);
        int yE4 = (int) (scale * 150);

        int xE5 = (int) (scale * 180);
        int yE5 = (int) (scale * 150);

        int xE6 = (int) (scale * 205);
        int yE6 = (int) (scale * 240);
        Point pE1 = new Point(xE1, yE1);
        Point pE2 = new Point(xE2, yE2);
        Point pE3 = new Point(xE3, yE3);
        Point pE4 = new Point(xE4, yE4);
        Point pE5 = new Point(xE5, yE5);
        Point pE6 = new Point(xE6, yE6);


        pt_E_0.add(pE1);
        pt_E_0.add(pE2);
        pt_E_1.add(pE1);
        pt_E_1.add(pE3);
        pt_E_2.add(pE4);
        pt_E_2.add(pE5);
        pt_E_3.add(pE2);
        pt_E_3.add(pE6);

        point_E_Array[0] = pt_E_0;
        point_E_Array[1] =pt_E_1;
        point_E_Array[2] = pt_E_2;
        point_E_Array[3] = pt_E_3;

    }
    ///////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////// F /////////////////////////////////////////////
    public static void getFDetails(){

        List<Point> pt_F_0 = new ArrayList<Point>();
        List<Point> pt_F_1 = new ArrayList<Point>();
        List<Point> pt_F_2 = new ArrayList<Point>();
        int xF1 = (int) (scale * 105);
        int yF1 = (int) (scale * 60);

        int xF2 = (int) (scale * 105);
        int yF2 = (int) (scale * 240);

        int xF3 = (int) (scale * 205);
        int yF3 = (int) (scale * 60);

        int xF4 = (int) (scale * 115);
        int yF4 = (int) (scale * 150);

        int xF5 = (int) (scale * 180);
        int yF5 = (int) (scale * 150);
        Point pF1 = new Point(xF1, yF1);
        Point pF2 = new Point(xF2, yF2);
        Point pF3 = new Point(xF3, yF3);
        Point pF4 = new Point(xF4, yF4);
        Point pF5 = new Point(xF5, yF5);


        pt_F_0.add(pF1);
        pt_F_0.add(pF2);
        pt_F_1.add(pF1);
        pt_F_1.add(pF3);
        pt_F_2.add(pF4);
        pt_F_2.add(pF5);

        point_F_Array[0] = pt_F_0;
        point_F_Array[1] =pt_F_1;
        point_F_Array[2] = pt_F_2;

    }

    ////////////////////////////////////////////////////////////////////////////////



    ////////////////////////////// G//////////////////////////////
    public static int G_TOTAL_STEPS = 7;

    public static List<Point>[] point_G_Array = new List[G_TOTAL_STEPS];
    public static void getGDetails(){

        List<Point> pt_G_0 = new ArrayList<Point>();
        List<Point> pt_G_1 = new ArrayList<Point>();
        List<Point> pt_G_2 = new ArrayList<Point>();
        List<Point> pt_G_3 = new ArrayList<Point>();
        List<Point> pt_G_4 = new ArrayList<Point>();
        List<Point> pt_G_5 = new ArrayList<Point>();
        List<Point> pt_G_6 = new ArrayList<Point>();

        int x1 = (int) (scale * 200);
        int y1 = (int) (scale * 90);

        int x2 = (int) (scale * 150);
        int y2= (int) (scale * 55);

        int x3 = (int) (scale * 90);
        int y3 = (int) (scale * 85);

        int x4 = (int) (scale * 80);
        int y4 = (int) (scale * 200);

        int x5 = (int) (scale * 160);
        int y5 = (int) (scale * 240);

        int x6 = (int) (scale * 200);
        int y6 = (int) (scale * 200);

        int x7 = (int) (scale * 200);
        int y7 = (int) (scale * 160);

        int x8 = (int) (scale * 160);
        int y8= (int) (scale * 160);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);



        pt_G_0.add(p1);
        pt_G_0.add(p2);

        pt_G_1.add(p2);
        pt_G_1.add(p3);

        pt_G_2.add(p3);
        pt_G_2.add(p4);

        pt_G_3.add(p4);
        pt_G_3.add(p5);

        pt_G_4.add(p5);
        pt_G_4.add(p6);

        pt_G_5.add(p6);
        pt_G_5.add(p7);

        pt_G_6.add(p7);
        pt_G_6.add(p8);



        point_G_Array[0] = pt_G_0;
        point_G_Array[1] =pt_G_1;
        point_G_Array[2] = pt_G_2;
        point_G_Array[3] = pt_G_3;
        point_G_Array[4] = pt_G_4;
        point_G_Array[5] = pt_G_5;
        point_G_Array[6] = pt_G_6;


    }
    ///////////////////////////////////////////////////////////





    /////////////////////////////// H//////////////////////////////////////////////
    public static void getHDetails(){

        List<Point> pt_F_0 = new ArrayList<Point>();
        List<Point> pt_F_1 = new ArrayList<Point>();
        List<Point> pt_F_2 = new ArrayList<Point>();
        int xF1 = (int) (scale * 90);
        int yF1 = (int) (scale * 60);

        int xF2 = (int) (scale * 90);
        int yF2 = (int) (scale * 240);

        int xF3 = (int) (scale * 220);
        int yF3 = (int) (scale * 60);

        int xF4 = (int) (scale * 220);
        int yF4 = (int) (scale * 240);

        int xF5 = (int) (scale * 100 );
        int yF5 = (int) (scale * 150);

        int xF6 = (int) (scale * 210);
        int yF6 = (int) (scale * 150);

        Point pF1 = new Point(xF1, yF1);
        Point pF2 = new Point(xF2, yF2);
        Point pF3 = new Point(xF3, yF3);
        Point pF4 = new Point(xF4, yF4);
        Point pF5 = new Point(xF5, yF5);
        Point pF6 = new Point(xF6, yF6);


        pt_F_0.add(pF1);
        pt_F_0.add(pF2);
        pt_F_1.add(pF3);
        pt_F_1.add(pF4);
        pt_F_2.add(pF5);
        pt_F_2.add(pF6);

        point_H_Array[0] = pt_F_0;
        point_H_Array[1] =pt_F_1;
        point_H_Array[2] = pt_F_2;

    }

///////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////// Letter I //////////////////////////////


    public static void getIDetails(){



        List<Point> pt_I_0 = new ArrayList<Point>();

        int xI1 = (int) (scale * 150);
        int yI1 = (int) (scale * 60);
        int xI2 = (int) (scale * 150);
        int yI2 = (int) (scale * 240);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);

        pt_I_0.add(pI1);
        pt_I_0.add(pI2);

        point_I_Array[0] = pt_I_0;

    }


    ///////////////////////////////////////////////////////////
////////////////////////////// J//////////////////////////////
    public static int J_TOTAL_STEPS = 4;

    public static List<Point>[] point_J_Array = new List[J_TOTAL_STEPS];
    public static void getJDetails(){



        List<Point> pt_J_0 = new ArrayList<Point>();
        List<Point> pt_J_1 = new ArrayList<Point>();
        List<Point> pt_J_2 = new ArrayList<Point>();
        List<Point> pt_J_3 = new ArrayList<Point>();



        int x1 = (int) (scale * 200);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 200);
        int y2= (int) (scale * 210);

        int x3 = (int) (scale * 170);
        int y3 = (int) (scale * 235);

        int x4 = (int) (scale * 110);
        int y4 = (int) (scale * 235);

        int x5 = (int) (scale * 100);
        int y5 = (int) (scale * 210);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);





        pt_J_0.add(p1);
        pt_J_0.add(p2);

        pt_J_1.add(p2);
        pt_J_1.add(p3);

        pt_J_2.add(p3);
        pt_J_2.add(p4);

        pt_J_3.add(p4);
        pt_J_3.add(p5);





        point_J_Array[0] = pt_J_0;
        point_J_Array[1] =pt_J_1;
        point_J_Array[2] = pt_J_2;
        point_J_Array[3] = pt_J_3;



    }
    ///////////////////////////////////////////////////////////

    /////////////////////////////////// K ///////////////////////////////////////////////////////////




    public static void getKDetails(){



        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();


        int xK1 = (int) (scale * 80);
        int yK1 = (int) (scale * 60);

        int xK2 = (int) (scale * 80);
        int yK2= (int) (scale * 240);

        int xK3= (int) (scale * 90);
        int yK3= (int) (scale * 150);

        int xK4= (int) (scale * 125);
        int yK4= (int) (scale * 150);

        int xK5= (int) (scale * 130);
        int yK5= (int) (scale * 150);

        int xK6= (int) (scale * 190);
        int yK6= (int) (scale * 60);

        int xK7= (int) (scale * 190);
        int yK7= (int) (scale * 240);


        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3= new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(xK7, yK7);



        pt_K_0.add(pK1);
        pt_K_0.add(pK2);
        pt_K_1.add(pK3);
        pt_K_1.add(pK4);
        pt_K_2.add(pK5);
        pt_K_2.add(pK6);
        pt_K_3.add(pK5);
        pt_K_3.add(pK7);




        point_K_Array[0] = pt_K_0;
        point_K_Array[1] = pt_K_1;
        point_K_Array[2] = pt_K_2;
        point_K_Array[3] = pt_K_3;

    }




//////////////////////////////////////////////////////////////////////////////////////////////////


    ///////////////////Letter L/////////////////////////////////////////////////////////////



    public static void getLDetails(){
        List<Point> pt_L_0 = new ArrayList<Point>();
        List<Point> pt_L_1 = new ArrayList<Point>();

        int xL1 = (int) (scale * 99);
        int yL1 = (int) (scale *60);

        int xL2 = (int) (scale * 99);
        int yL2 = (int) (scale * 240);

        int xL3 = (int) (scale * 205);
        int yL3 = (int) (scale * 240);


        Point pL1 = new Point(xL1, yL1);
        Point pL2 = new Point(xL2, yL2);
        Point pL3 = new Point(xL3, yL3);


        pt_L_0.add(pL1);
        pt_L_0.add(pL2);
        pt_L_1.add(pL2);
        pt_L_1.add(pL3);

        point_L_Array[0] = pt_L_0;
        point_L_Array[1] =pt_L_1;


    }


//////////////////////////////////////////////////////////////////////////


    ///////////////////Letter M///////////////////




    public static void getMDetails(){



        List<Point> pt_M_0 = new ArrayList<Point>();
        List<Point> pt_M_1 = new ArrayList<Point>();
        List<Point> pt_M_2 = new ArrayList<Point>();
        List<Point> pt_M_3 = new ArrayList<Point>();

        int xM1 = (int) (scale * 65);
        int yM1 = (int) (scale * 70);

        int xM2 = (int) (scale * 65);
        int yM2 = (int) (scale * 240);

        int xM3 = (int) (scale * 145);
        int yM3 = (int) (scale * 240);

        int xM4 = (int) (scale * 235);
        int yM4 = (int) (scale * 70);

        int xM5 = (int) (scale * 235);
        int yM5 = (int) (scale * 240);

        int xM6 = (int) (scale * 75);
        int yM6 = (int) (scale * 70);

        int xM8 = (int) (scale * 230);
        int yM8 = (int) (scale * 70);

        int xM7 = (int) (scale * 145);
        int yM7 = (int) (scale * 240);
        Point pA1 = new Point(xM1, yM1);
        Point pA2 = new Point(xM2, yM2);
        Point pA3 = new Point(xM3, yM3);
        Point pA4 = new Point(xM4, yM4);
        Point pA5 = new Point(xM5, yM5);
        Point pA6 = new Point(xM6, yM6);
        Point pA7 = new Point(xM7, yM7);
        Point pA8 = new Point(xM8, yM8);


        pt_M_0.add(pA1);
        pt_M_0.add(pA2);

        pt_M_1.add(pA6);
        pt_M_1.add(pA3);

        pt_M_2.add(pA7);
        pt_M_2.add(pA8);

        pt_M_3.add(pA4);
        pt_M_3.add(pA5);


        point_M_Array[0] = pt_M_0;
        point_M_Array[1] =pt_M_1;
        point_M_Array[2] = pt_M_2;
        point_M_Array[3] = pt_M_3;

    }


//////////////////////////////////////////////////////////////////////////




    /////////////////////////////////// N ///////////////////////////////////////////////////////////




    public static void getNDetails(){



        List<Point> pt_N_0 = new ArrayList<Point>();
        List<Point> pt_N_1 = new ArrayList<Point>();
        List<Point> pt_N_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 84);
        int y1 = (int) (scale * 240);

        int x2 = (int) (scale * 84);
        int y2= (int) (scale * 60);

        int x3 = (int) (scale * 85);
        int y3= (int) (scale * 60);

        int x4= (int) (scale * 210);
        int y4= (int) (scale * 240);

        int x5= (int) (scale * 215);
        int y5= (int) (scale * 240);

        int x6= (int) (scale * 215);
        int y6= (int) (scale * 60);

        Point pK1 = new Point(x1, y1);
        Point pK2 = new Point(x2, y2);

        Point pK3= new Point(x3, y3);
        Point pK4 = new Point(x4, y4);

        Point pK5 = new Point(x5, y5);
        Point pK6 = new Point(x6, y6);

        pt_N_0.add(pK2);
        pt_N_0.add(pK1);

        pt_N_1.add(pK3);
        pt_N_1.add(pK4);

        pt_N_2.add(pK5);
        pt_N_2.add(pK6);


        point_N_Array[0] = pt_N_0;
        point_N_Array[1] = pt_N_1;
        point_N_Array[2] = pt_N_2;




    }

//////////////////////////////////////////////////////




    ////////////////////////////// O//////////////////////////////
    public static int O_TOTAL_STEPS = 6;

    public static List<Point>[] point_O_Array = new List[O_TOTAL_STEPS];
    public static void getODetails(){



        List<Point> pt_O_0 = new ArrayList<Point>();
        List<Point> pt_O_1 = new ArrayList<Point>();
        List<Point> pt_O_2 = new ArrayList<Point>();
        List<Point> pt_O_3 = new ArrayList<Point>();
        List<Point> pt_O_4 = new ArrayList<Point>();
        List<Point> pt_O_5 = new ArrayList<Point>();




        int x1 = (int) (scale * 130);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 90);
        int y2= (int) (scale *90);

        int x3 = (int) (scale * 90);
        int y3 = (int) (scale * 200);

        int x4 = (int) (scale * 160);
        int y4 = (int) (scale * 240);

        int x5 = (int) (scale * 210);
        int y5 = (int) (scale * 200);

        int x6 = (int) (scale * 210);
        int y6 = (int) (scale * 90);

        int x7= (int) (scale * 150);
        int y7= (int) (scale * 60);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);






        pt_O_0.add(p1);
        pt_O_0.add(p2);

        pt_O_1.add(p2);
        pt_O_1.add(p3);

        pt_O_2.add(p3);
        pt_O_2.add(p4);

        pt_O_3.add(p4);
        pt_O_3.add(p5);

        pt_O_4.add(p5);
        pt_O_4.add(p6);

        pt_O_5.add(p6);
        pt_O_5.add(p7);





        point_O_Array[0] = pt_O_0;
        point_O_Array[1] =pt_O_1;
        point_O_Array[2] = pt_O_2;
        point_O_Array[3] = pt_O_3;
        point_O_Array[4] = pt_O_4;
        point_O_Array[5] = pt_O_5;




    }
    ///////////////////////////////////////////////////////////


    ////////////////////////////// P//////////////////////////////
    public static int P_TOTAL_STEPS = 5;

    public static List<Point>[] point_P_Array = new List[P_TOTAL_STEPS];
    public static void getPDetails(){
        List<Point> pt_P_0 = new ArrayList<Point>();
        List<Point> pt_P_1 = new ArrayList<Point>();
        List<Point> pt_P_2 = new ArrayList<Point>();
        List<Point> pt_P_3 = new ArrayList<Point>();
        List<Point> pt_P_4 = new ArrayList<Point>();

        int x1 = (int) (scale * 90);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 90);
        int y2= (int) (scale *240);

        int x3 = (int) (scale * 100);
        int y3 = (int) (scale * 60);

        int x4 = (int) (scale * 180);
        int y4 = (int) (scale * 65);

        int x5 = (int) (scale * 210);
        int y5 = (int) (scale * 120);

        int x6 = (int) (scale * 180);
        int y6 = (int) (scale * 160);

        int x7= (int) (scale * 110);
        int y7= (int) (scale * 160);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);






        pt_P_0.add(p1);
        pt_P_0.add(p2);

        pt_P_1.add(p3);
        pt_P_1.add(p4);

        pt_P_2.add(p4);
        pt_P_2.add(p5);

        pt_P_3.add(p5);
        pt_P_3.add(p6);

        pt_P_4.add(p6);
        pt_P_4.add(p7);




        point_P_Array[0] = pt_P_0;
        point_P_Array[1] =pt_P_1;
        point_P_Array[2] = pt_P_2;
        point_P_Array[3] = pt_P_3;
        point_P_Array[4] = pt_P_4;




    }
    ///////////////////////////////////////////////////////////

    ////////////////////////////// Q//////////////////////////////
    public static int Q_TOTAL_STEPS = 7;

    public static List<Point>[] point_Q_Array = new List[Q_TOTAL_STEPS];
    public static void getQDetails(){



        List<Point> pt_Q_0 = new ArrayList<Point>();
        List<Point> pt_Q_1 = new ArrayList<Point>();
        List<Point> pt_Q_2 = new ArrayList<Point>();
        List<Point> pt_Q_3 = new ArrayList<Point>();
        List<Point> pt_Q_4 = new ArrayList<Point>();
        List<Point> pt_Q_5 = new ArrayList<Point>();

        List<Point> pt_Q_6= new ArrayList<Point>();



        int x1 = (int) (scale * 130);
        int y1 = (int) (scale * 50);

        int x2 = (int) (scale * 80);
        int y2= (int) (scale *90);

        int x3 = (int) (scale * 80);
        int y3 = (int) (scale * 210);

        int x4 = (int) (scale * 140);
        int y4 = (int) (scale * 230);

        int x5 = (int) (scale * 200);
        int y5 = (int) (scale * 200);

        int x6 = (int) (scale * 200);
        int y6 = (int) (scale * 90);

        int x7= (int) (scale * 145);
        int y7= (int) (scale * 50);



        int x9= (int) (scale * 180);
        int y9= (int) (scale * 210);

        int x10= (int) (scale * 225);
        int y10= (int) (scale * 245);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);

        Point p9= new Point(x9, y9);
        Point p10= new Point(x10, y10);





        pt_Q_0.add(p1);
        pt_Q_0.add(p2);

        pt_Q_1.add(p2);
        pt_Q_1.add(p3);

        pt_Q_2.add(p3);
        pt_Q_2.add(p4);

        pt_Q_3.add(p4);
        pt_Q_3.add(p5);

        pt_Q_4.add(p5);
        pt_Q_4.add(p6);

        pt_Q_5.add(p6);
        pt_Q_5.add(p7);



        pt_Q_6.add(p9);
        pt_Q_6.add(p10);


        point_Q_Array[0] = pt_Q_0;
        point_Q_Array[1] =pt_Q_1;
        point_Q_Array[2] = pt_Q_2;
        point_Q_Array[3] = pt_Q_3;
        point_Q_Array[4] = pt_Q_4;
        point_Q_Array[5] = pt_Q_5;
        point_Q_Array[6] = pt_Q_6;




    }
    ///////////////////////////////////////////////////////////




    ////////////////////////////// R //////////////////////////////

    public static int R_TOTAL_STEPS = 6;

    public static List<Point>[] point_R_Array = new List[R_TOTAL_STEPS];
    public static void getRDetails(){


        List<Point> pt_R_0 = new ArrayList<Point>();
        List<Point> pt_R_1 = new ArrayList<Point>();
        List<Point> pt_R_2 = new ArrayList<Point>();
        List<Point> pt_R_3 = new ArrayList<Point>();
        List<Point> pt_R_4 = new ArrayList<Point>();
        List<Point> pt_R_5 = new ArrayList<Point>();
        List<Point> pt_R_6 = new ArrayList<Point>();
        List<Point> pt_R_7 = new ArrayList<Point>();
        int x1 = (int) (scale * 90);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 90);
        int y2= (int) (scale *240);

        int x3 = (int) (scale * 100);
        int y3 = (int) (scale * 60);

        int x4 = (int) (scale * 180);
        int y4 = (int) (scale * 65);

        int x5 = (int) (scale * 195);
        int y5 = (int) (scale * 120);

        int x6 = (int) (scale * 160);
        int y6 = (int) (scale * 150);

        int x7= (int) (scale * 110);
        int y7= (int) (scale * 155);

        int x8= (int) (scale * 170);
        int y8= (int) (scale * 165);

        int x9= (int) (scale * 210);
        int y9= (int) (scale * 240);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);
        Point p8= new Point(x8, y8);
        Point p9= new Point(x9, y9);



        pt_R_0.add(p1);
        pt_R_0.add(p2);

        pt_R_1.add(p3);
        pt_R_1.add(p4);

        pt_R_2.add(p4);
        pt_R_2.add(p5);

        pt_R_3.add(p5);
        pt_R_3.add(p6);

        pt_R_4.add(p6);
        pt_R_4.add(p7);

        pt_R_5.add(p8);
        pt_R_5.add(p9);



        point_R_Array[0] = pt_R_0;
        point_R_Array[1] =pt_R_1;
        point_R_Array[2] = pt_R_2;
        point_R_Array[3] = pt_R_3;
        point_R_Array[4] = pt_R_4;
        point_R_Array[5] = pt_R_5;


    }
//////////////////////////////////////////////////////////////////////////


    ////////////////////////////// S//////////////////////////////
    public static int S_TOTAL_STEPS = 8;

    public static List<Point>[] point_S_Array = new List[S_TOTAL_STEPS];
    public static void getSDetails(){


        List<Point> pt_S_0 = new ArrayList<Point>();
        List<Point> pt_S_1 = new ArrayList<Point>();
        List<Point> pt_S_2 = new ArrayList<Point>();
        List<Point> pt_S_3 = new ArrayList<Point>();
        List<Point> pt_S_4 = new ArrayList<Point>();
        List<Point> pt_S_5 = new ArrayList<Point>();
        List<Point> pt_S_6 = new ArrayList<Point>();
        List<Point> pt_S_7 = new ArrayList<Point>();
        List<Point> pt_S_8 = new ArrayList<Point>();
        List<Point> pt_S_9 = new ArrayList<Point>();


        int x1 = (int) (scale * 210);
        int y1 = (int) (scale * 90);

        int x2 = (int) (scale * 180);
        int y2= (int) (scale *60);

        int x3 = (int) (scale * 135);
        int y3= (int) (scale * 60);

        int x4 = (int) (scale * 100);
        int y4= (int) (scale * 90);

        int x5 = (int) (scale * 130);
        int y5= (int) (scale * 140);

        int x6 = (int) (scale * 195);
        int y6= (int) (scale * 170);

        int x7 = (int) (scale * 200);
        int y7= (int) (scale * 215);

        int x8 = (int) (scale * 195);
        int y8= (int) (scale * 235);


        int x10= (int) (scale * 145);
        int y10= (int) (scale * 240);

        int x11= (int) (scale * 90);
        int y11= (int) (scale *210);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        // Point p9 = new Point(x9, y9);
        Point p10 = new Point(x10, y10);
        Point p11 = new Point(x11, y11);

        pt_S_0.add(p1);
        pt_S_0.add(p2);

        pt_S_1.add(p2);
        pt_S_1.add(p3);

        pt_S_2.add(p3);
        pt_S_2.add(p4);

        pt_S_3.add(p4);
        pt_S_3.add(p5);

        pt_S_4.add(p5);
        pt_S_4.add(p6);

        pt_S_5.add(p6);
        pt_S_5.add(p8);


/*

        pt_S_7.add(p8);
        pt_S_7.add(p9);
*/

        pt_S_8.add(p8);
        pt_S_8.add(p10);

        pt_S_9.add(p10);
        pt_S_9.add(p11);

        point_S_Array[0] = pt_S_0;
        point_S_Array[1] = pt_S_1;
        point_S_Array[2] = pt_S_2;
        point_S_Array[3] = pt_S_3;
        point_S_Array[4] = pt_S_4;
        point_S_Array[5] = pt_S_5;
        point_S_Array[6] = pt_S_8;
        //  point_S_Array[7] = pt_S_7;
        point_S_Array[7] = pt_S_9;




    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////// T/////////////////////////////////////////////////////////////////////////////
    public static void getTDetails(){

        List<Point> pt_F_0 = new ArrayList<Point>();
        List<Point> pt_F_1 = new ArrayList<Point>();

        int xF1 = (int) (scale * 80);
        int yF1 = (int) (scale * 60);
        int xF2 = (int) (scale * 150);
        int yF2 = (int) (scale * 70);
        int xF3 = (int) (scale * 215);
        int yF3 = (int) (scale * 60);
        int xF4 = (int) (scale * 150);
        int yF4 = (int) (scale * 240);

        Point pF1 = new Point(xF1, yF1);
        Point pF3 = new Point(xF3, yF3);
        Point pF4 = new Point(xF4, yF4);
        Point pF2 = new Point(xF2, yF2);



        pt_F_0.add(pF1);
        pt_F_0.add(pF3);
        pt_F_1.add(pF2);
        pt_F_1.add(pF4);

        point_T_Array[0] = pt_F_0;
        point_T_Array[1] =pt_F_1;


    }
    //////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////U///////////////////
    public static int U_TOTAL_STEPS = 4;

    public static List<Point>[] point_U_Array = new List[U_TOTAL_STEPS];
    public static void getUDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();

        int x1 = (int) (scale * 86);
        int y1 = (int) (scale * 61);

        int x2 = (int) (scale * 86);
        int y2 = (int) (scale * 180);

        int x3 = (int) (scale * 75);
        int y3 = (int) (scale * 155);

      /*  int x4 = (int) (scale * 100);
        int y4 = (int) (scale * 160);*/

        int x5 = (int) (scale * 160);
        int y5 = (int) (scale * 240);

        int x6 = (int) (scale * 210);
        int y6 = (int) (scale * 180);

        int x7 = (int) (scale * 215);
        int y7 = (int) (scale * 65);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);

        //Point p3 = new Point(x3, y3);
        //Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);



        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p5);

      /*  pt_C_2.add(p3);
        pt_C_2.add(p5);*/

        pt_C_3.add(p5);
        pt_C_3.add(p6);

        pt_C_4.add(p6);
        pt_C_4.add(p7);




        point_U_Array[0] = pt_C_0;
        point_U_Array[1] = pt_C_1;
        // point_U_Array[2] = pt_C_2;
        point_U_Array[2] = pt_C_3;
        point_U_Array[3] = pt_C_4;


    }
    ///////////////////////////////////////////////////



    /////////////////////////////////////////// V ///////////////////////////////////////////////////
    public static void getVDetails(){

        List<Point> pt_F_0 = new ArrayList<Point>();
        List<Point> pt_F_1 = new ArrayList<Point>();

        int xF1 = (int) (scale * 80);
        int yF1 = (int) (scale * 65);
        int xF2 = (int) (scale * 145);
        int yF2 = (int) (scale * 240);
        int xF3 = (int) (scale * 150);
        int yF3 = (int) (scale * 240);
        int xF4 = (int) (scale * 215);
        int yF4 = (int) (scale * 65);


        Point pF1 = new Point(xF1, yF1);
        Point pF3 = new Point(xF3, yF3);
        Point pF4 = new Point(xF4, yF4);
        Point pF2 = new Point(xF2, yF2);



        pt_F_0.add(pF1);
        pt_F_0.add(pF2);
        pt_F_1.add(pF3);
        pt_F_1.add(pF4);

        point_V_Array[0] = pt_F_0;
        point_V_Array[1] =pt_F_1;


    }
    ////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////// W ///////////////////////////////////////////////////////////

    public static void getWDetails(){
        List<Point> pt_W_0 = new ArrayList<Point>();
        List<Point> pt_W_1 = new ArrayList<Point>();
        List<Point> pt_W_2 = new ArrayList<Point>();
        List<Point> pt_W_3 = new ArrayList<Point>();

        int x1 = (int) (scale * 50);
        int y1 = (int) (scale * 65);

        int x2 = (int) (scale * 90);
        int y2= (int) (scale * 240);

        int x3= (int) (scale * 150);
        int y3= (int) (scale * 70);

        int x4= (int) (scale * 210);
        int y4= (int) (scale * 240);

        int x5= (int) (scale * 260);
        int y5= (int) (scale * 65);

        Point pK1 = new Point(x1, y1);
        Point pK2 = new Point(x2, y2);
        Point pK3 = new Point(x3, y3);
        Point pK4 = new Point(x4, y4);
        Point pK5 = new Point(x5, y5);

        pt_W_0.add(pK1);
        pt_W_0.add(pK2);

        pt_W_1.add(pK2);
        pt_W_1.add(pK3);

        pt_W_2.add(pK3);
        pt_W_2.add(pK4);

        pt_W_3.add(pK4);
        pt_W_3.add(pK5);

        point_W_Array[0] = pt_W_0;
        point_W_Array[1] = pt_W_1;
        point_W_Array[2] = pt_W_2;
        point_W_Array[3] = pt_W_3;



    }




//////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////  X //////////////////////////////////////////////////
    public static void getXDetails(){

        List<Point> pt_F_0 = new ArrayList<Point>();
        List<Point> pt_F_1 = new ArrayList<Point>();

        int xF1 = (int) (scale * 90);
        int yF1 = (int) (scale * 58);

        int xF2 = (int) (scale * 200);
        int yF2 = (int) (scale * 58);

        int xF3 = (int) (scale * 90);
        int yF3 = (int) (scale * 240);

        int xF4 = (int) (scale * 200);
        int yF4 = (int) (scale * 240);


        Point pF1 = new Point(xF1, yF1);
        Point pF3 = new Point(xF3, yF3);
        Point pF4 = new Point(xF4, yF4);
        Point pF2 = new Point(xF2, yF2);


        pt_F_0.add(pF1);
        pt_F_0.add(pF4);
        pt_F_1.add(pF2);
        pt_F_1.add(pF3);

        point_X_Array[0] = pt_F_0;
        point_X_Array[1] =pt_F_1;

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////




    ///////////////////Letter Y///////////////////




    public static void getYDetails(){



        List<Point> pt_Y_0 = new ArrayList<Point>();
        List<Point> pt_Y_1 = new ArrayList<Point>();
        List<Point> pt_Y_2 = new ArrayList<Point>();

        int xY1 = (int) (scale * 90);
        int yY1 = (int) (scale * 60);
        int xY2 = (int) (scale * 150);
        int yY2 = (int) (scale * 180);
        int xY3 = (int) (scale * 210);
        int yY3 = (int) (scale * 60);
        int xY4 = (int) (scale * 150);
        int yY4 = (int) (scale * 190);
        int xA5 = (int) (scale * 150);
        int yA5 = (int) (scale * 240);

        Point pY1 = new Point(xY1, yY1);
        Point pY2 = new Point(xY2, yY2);
        Point pY3 = new Point(xY3, yY3);
        Point pY4 = new Point(xY4, yY4);
        Point pA5 = new Point(xA5, yA5);


        pt_Y_0.add(pY1);
        pt_Y_0.add(pY2);
        pt_Y_1.add(pY2);
        pt_Y_1.add(pY3);
        pt_Y_2.add(pY4);
        pt_Y_2.add(pA5);

        point_Y_Array[0] = pt_Y_0;
        point_Y_Array[1] =pt_Y_1;
        point_Y_Array[2] = pt_Y_2;

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////Letter Z///////////////////



    public static void getZDetails(){

        List<Point> pt_Z_0 = new ArrayList<Point>();
        List<Point> pt_Z_1 = new ArrayList<Point>();
        List<Point> pt_Z_2 = new ArrayList<Point>();

        int xZ1 = (int) (scale * 100);
        int yZ1 = (int) (scale * 58);
        int xZ2 = (int) (scale * 200);
        int yZ2 = (int) (scale * 58);
        int xZ3 = (int) (scale * 100);
        int yZ3 = (int) (scale * 240);
        int xZ4 = (int) (scale * 195);
        int yZ4 = (int) (scale * 240);
        /*int xA5 = (int) (scale * 128);
        int yA5 = (int) (scale * 130);
*/
        Point pZ1 = new Point(xZ1, yZ1);
        Point pZ2 = new Point(xZ2, yZ2);
        Point pZ3 = new Point(xZ3, yZ3);
        Point pZ4 = new Point(xZ4, yZ4);
        /*Point pA5 = new Point(xZ5, yA5);*/

        pt_Z_0.add(pZ1);
        pt_Z_0.add(pZ2);
        pt_Z_1.add(pZ2);
        pt_Z_1.add(pZ3);
        pt_Z_2.add(pZ3);
        pt_Z_2.add(pZ4);

        point_Z_Array[0] = pt_Z_0;
        point_Z_Array[1] =pt_Z_1;
        point_Z_Array[2] = pt_Z_2;

    }


//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////


    //////////////////////////////// SMALL LETTERS/////////////////////////////////////////////////////////
//////////////////////////////////// a////////////////////////////////////////////////////
    public static int a_TOTAL_STEPS =5;

    public static List<Point>[] point_a_Array = new List[a_TOTAL_STEPS];
    public static void getaDetails(){


        List<Point> pt_a_0 = new ArrayList<Point>();
        List<Point> pt_a_1 = new ArrayList<Point>();
        List<Point> pt_a_2 = new ArrayList<Point>();
        List<Point> pt_a_3 = new ArrayList<Point>();
        List<Point> pt_a_4 = new ArrayList<Point>();
        List<Point> pt_a_5 = new ArrayList<Point>();
        List<Point> pt_a_6 = new ArrayList<Point>();

        int x1 = (int) (scale * 190);
        int y1 = (int) (scale * 100);

        int x2 = (int) (scale * 110);
        int y2 = (int) (scale * 108);

        /*int x3 = (int) (scale * 105);
        int y3 = (int) (scale * 86);
*/
        int x4 = (int) (scale * 85);
        int y4 = (int) (scale * 190);

        int x5 = (int) (scale * 175);
        int y5 = (int) (scale * 185);

        int x6 = (int) (scale * 205);
        int y6 = (int) (scale * 90);

        int x7 = (int) (scale * 180);
        int y7 = (int) (scale * 200);

        int x8= (int) (scale * 230);
        int y8= (int) (scale * 200);

      /*  int x7 = (int) (scale * 115);
        int y7 = (int) (scale * 160);

        int x8 = (int) (scale * 185);
        int y8 = (int) (scale * 145);

        int x9 = (int) (scale * 135);
        int y9 = (int) (scale * 220);
*/
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
       // Point p3 = new Point(x4, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
     //   Point p9 = new Point(x9, y9);

        pt_a_0.add(p1);
        pt_a_0.add(p2);

        pt_a_1.add(p2);
        pt_a_1.add(p4);

      /*  pt_a_2.add(p3);
        pt_a_2.add(p4);
*/

        pt_a_3.add(p4);
        pt_a_3.add(p5);

        pt_a_4.add(p6);
        pt_a_4.add(p7);

        pt_a_5.add(p7);
        pt_a_5.add(p8);

    /*    pt_a_3.add(p9);
        pt_a_3.add(p6);
*/

        point_a_Array[0] = pt_a_0;
        point_a_Array[1] = pt_a_1;
        point_a_Array[2] = pt_a_3;
        point_a_Array[3] = pt_a_4;
        point_a_Array[4] = pt_a_5;
      //  point_a_Array[5] = pt_a_5;
    //    point_a_Array[6] = pt_a_3;


    }





/////////////////////////////////// small b ///////////////////////////////////////////////////////////

    public static int b_TOTAL_STEPS = 6;

    public static List<Point>[] point_b_Array = new List[b_TOTAL_STEPS];

    public static void getbDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();
        List<Point> pt_K_4 = new ArrayList<Point>();
        List<Point> pt_K_5 = new ArrayList<Point>();

        int xK1 = (int) (scale * 100);
        int yK1 = (int) (scale * 55);

        int xK2 = (int) (scale * 100);
        int yK2= (int) (scale * 240);

        int xK3= (int) (scale * 106);
        int yK3= (int) (scale * 140);

        int xK4= (int) (scale * 150);
        int yK4= (int) (scale * 120);

        int xK5= (int) (scale * 192);
        int yK5= (int) (scale * 230);

        int xK51= (int) (scale * 190);
        int yK51= (int) (scale * 140);

        int xK6= (int) (scale * 106);
        int yK6= (int) (scale * 230);

        int xK61= (int) (scale * 160);
        int yK61= (int) (scale * 250);
        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK51 = new Point(xK51, yK51);
        Point pK61 = new Point(xK61, yK61);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(pK4);

        pt_K_2.add(pK4);
        pt_K_2.add(pK51);

        pt_K_3.add(pK51);
        pt_K_3.add(pK5);
        pt_K_4.add(pK5);
        pt_K_4.add(pK61);
        pt_K_5.add(pK61);
        pt_K_5.add(pK6);
        point_b_Array[0] = pt_K_0;
        point_b_Array[1] = pt_K_1;
        point_b_Array[2] = pt_K_2;
        point_b_Array[3] = pt_K_3;
        point_b_Array[4] = pt_K_4;
        point_b_Array[5] = pt_K_5;

    }
////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////small c ///////////////////
    public static int c_TOTAL_STEPS = 6;

    public static List<Point>[] point_c_Array = new List[c_TOTAL_STEPS];
    public static void getcDetails(){

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();

        int x1 = (int) (scale * 205);
        int y1 = (int) (scale * 110);

        int x3 = (int) (scale * 150);
        int y3 = (int) (scale * 80);
        int x31 = (int) (scale * 105);
        int y31 = (int) (scale * 105);
        int x4 = (int) (scale * 95);
        int y4 = (int) (scale * 155);
        int x41 = (int) (scale * 105);
        int y41 = (int) (scale * 200);
        int x5 = (int) (scale * 145);
        int y5 = (int) (scale * 230);

        int x6 = (int) (scale * 210);
        int y6 = (int) (scale * 200);


        Point p1 = new Point(x1, y1);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p31 = new Point(x31, y31);
        Point p41 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p31);

        pt_C_3.add(p31);
        pt_C_3.add(p4);

        pt_C_4.add(p4);
        pt_C_4.add(p41);

        pt_C_5.add(p41);
        pt_C_5.add(p5);

        pt_C_6.add(p5);
        pt_C_6.add(p6);

        point_c_Array[0] = pt_C_0;
        point_c_Array[1] = pt_C_2;
        point_c_Array[2] = pt_C_3;
        point_c_Array[3] = pt_C_4;
        point_c_Array[4] = pt_C_5;
        point_c_Array[5] = pt_C_6;

    }
///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////small d ///////////////////
public static int d_TOTAL_STEPS = 6;

    public static List<Point>[] point_d_Array = new List[d_TOTAL_STEPS];
    public static void getdDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();

        int x1 = (int) (scale * 190);
        int y1 = (int) (scale * 140);

        int x2 = (int) (scale * 145);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 100);
        int y3 = (int) (scale * 155);

        int x4 = (int) (scale * 105);
        int y4 = (int) (scale * 220);

        int x5 = (int) (scale * 145);
        int y5 = (int) (scale * 250);

        int x6 = (int) (scale * 180);
        int y6 = (int) (scale * 230);

        int x7 = (int) (scale * 200);
        int y7 = (int) (scale * 55);

        int x8 = (int) (scale * 200);
        int y8 = (int) (scale * 250);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p5);

        pt_C_4.add(p5);
        pt_C_4.add(p6);

        pt_C_5.add(p7);
        pt_C_5.add(p8);


        point_d_Array[0] = pt_C_0;
        point_d_Array[1] = pt_C_1;
        point_d_Array[2] = pt_C_2;
        point_d_Array[3] = pt_C_3;
        point_d_Array[4] = pt_C_4;
        point_d_Array[5] = pt_C_5;
    }
    ///////////////////////////////////////////////////

    /////////////////////////////////////////small e ///////////////////
    public static int e_TOTAL_STEPS = 8;

    public static List<Point>[] point_e_Array = new List[e_TOTAL_STEPS];
    public static void geteDetails(){

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 120);
        int y1 = (int) (scale * 147);

        int x2 = (int) (scale * 203);
        int y2 = (int) (scale * 147);

        int x3 = (int) (scale * 195);
        int y3 = (int) (scale * 110);

        int x4 = (int) (scale * 150);
        int y4 = (int) (scale * 90);

        int x41 = (int) (scale * 113);
        int y41= (int) (scale * 115);

        int x5 = (int) (scale * 100);
        int y5 = (int) (scale * 145);

        int x6 = (int) (scale * 110);
        int y6 = (int) (scale * 190);

        int x7 = (int) (scale * 155);
        int y7 = (int) (scale * 215);

        int x8 = (int) (scale * 190);
        int y8 = (int) (scale * 210);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p9);

        pt_C_4.add(p9);
        pt_C_4.add(p5);

        pt_C_5.add(p5);
        pt_C_5.add(p6);

        pt_C_6.add(p6);
        pt_C_6.add(p7);

        pt_C_7.add(p7);
        pt_C_7.add(p8);

        point_e_Array[0] = pt_C_0;
        point_e_Array[1] = pt_C_1;
        point_e_Array[2] = pt_C_2;
        point_e_Array[3] = pt_C_3;
        point_e_Array[4] = pt_C_4;
        point_e_Array[5] = pt_C_5;
        point_e_Array[6] = pt_C_6;
        point_e_Array[7] = pt_C_7;

    }
    ///////////////////////////////////////////////////

    /////////////////////////////////////////small f ///////////////////


    public static int f_TOTAL_STEPS = 3;

    public static List<Point>[] point_f_Array = new List[f_TOTAL_STEPS];
    public static void getfDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 185);
        int y1 = (int) (scale * 50);

        int x2 = (int) (scale * 149);
        int y2 = (int) (scale * 63);

        int x3 = (int) (scale * 145);
        int y3 = (int) (scale * 245);

        int x4 = (int) (scale * 110);
        int y4 = (int) (scale * 125);

        int x5 = (int) (scale * 180);
        int y5 = (int) (scale * 125);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p4);
        pt_C_2.add(p5);

        point_f_Array[0] = pt_C_0;
        point_f_Array[1] = pt_C_1;
        point_f_Array[2] = pt_C_2;

    }
    ///////////////////////////////////////////////////

    /////////////////////////////////////////small g ///////////////////
    public static int g_TOTAL_STEPS = 8;

    public static List<Point>[] point_g_Array = new List[g_TOTAL_STEPS];
    public static void getgDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 186);
        int y1 = (int) (scale * 74);

        int x2 = (int) (scale * 135);
        int y2 = (int) (scale * 60);

        int x21 = (int) (scale * 100);
        int y21 = (int) (scale * 100);

        int x3 = (int) (scale * 100);
        int y3 = (int) (scale * 155);

        int x4 = (int) (scale * 135);
        int y4 = (int) (scale * 194);

        int x5 = (int) (scale * 180);
        int y5 = (int) (scale * 170);

        int x6 = (int) (scale * 200);
        int y6 = (int) (scale * 60);

        int x7 = (int) (scale * 200);
        int y7 = (int) (scale * 210);

        int x8 = (int) (scale * 160);
        int y8 = (int) (scale * 252);

        int x9 = (int) (scale * 114);
        int y9 = (int) (scale * 240);

      /*  int x10 = (int) (scale * 85);
        int y10 = (int) (scale * 163);*/

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
        Point p10 = new Point(x21, y21);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p10);

        pt_C_2.add(p10);
        pt_C_2.add(p3);

        pt_C_3.add(p3);
        pt_C_3.add(p4);

        pt_C_4.add(p4);
        pt_C_4.add(p5);

        pt_C_5.add(p6);
        pt_C_5.add(p7);

        pt_C_6.add(p7);
        pt_C_6.add(p8);

        pt_C_7.add(p8);
        pt_C_7.add(p9);

        // pt_C_7.add(p9);
        // pt_C_7.add(p10);

        point_g_Array[0] = pt_C_0;
        point_g_Array[1] = pt_C_1;
        point_g_Array[2] = pt_C_2;
        point_g_Array[3] = pt_C_3;
        point_g_Array[4] = pt_C_4;
        point_g_Array[5] = pt_C_5;
        point_g_Array[6] = pt_C_6;
        point_g_Array[7] = pt_C_7;
        //  point_g_Array[7] = pt_C_7;

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////// small h ///////////////////////////////////////////////////////////

    public static int h_TOTAL_STEPS = 4;

    public static List<Point>[] point_h_Array = new List[h_TOTAL_STEPS];


    public static void gethDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 100);
        int yK1 = (int) (scale * 58);

        int xK2 = (int) (scale * 100);
        int yK2= (int) (scale * 245);

        int xK3= (int) (scale * 108);
        int yK3= (int) (scale * 145);

        int xK4= (int) (scale * 170);
        int yK4= (int) (scale * 120);

        int xK5= (int) (scale * 195);
        int yK5= (int) (scale * 140);

        int xK6= (int) (scale * 195);
        int yK6= (int) (scale * 245);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3= new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);
        pt_K_1.add(pK3);
        pt_K_1.add(pK4);
        pt_K_2.add(pK4);
        pt_K_2.add(pK5);
        pt_K_3.add(pK5);
        pt_K_3.add(pK6);

        point_h_Array[0] = pt_K_0;
        point_h_Array[1] = pt_K_1;
        point_h_Array[2] = pt_K_2;
        point_h_Array[3] = pt_K_3;

    }
////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////// small i //////////////////////////////

    public static int i_TOTAL_STEPS = 2;

    public static List<Point>[] point_i_Array = new List[i_TOTAL_STEPS];
    public static void getiDetails(){



        List<Point> pt_l_0 = new ArrayList<Point>();
        List<Point> pt_l_1 = new ArrayList<Point>();


        int xI1 = (int) (scale * 152);
        int yI1 = (int) (scale * 30);

        int xI2 = (int) (scale * 152);
        int yI2 = (int) (scale * 34);

        int xI3 = (int) (scale * 152);
        int yI3 = (int) (scale * 115);

        int xI4 = (int) (scale * 152);
        int yI4 = (int) (scale * 250);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        pt_l_1.add(pI3);
        pt_l_1.add(pI4);



        point_i_Array[0] = pt_l_1;
        point_i_Array[1] = pt_l_0;

    }

//////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////// small j //////////////////////////////

    public static int j_TOTAL_STEPS = 3;

    public static List<Point>[] point_j_Array = new List[j_TOTAL_STEPS];
    public static void getjDetails(){



        List<Point> pt_l_0 = new ArrayList<Point>();
        List<Point> pt_l_1 = new ArrayList<Point>();
        List<Point> pt_l_2 = new ArrayList<Point>();


        int xI1 = (int) (scale * 164);
        int yI1 = (int) (scale * 20);

        int xI2 = (int) (scale * 164);
        int yI2 = (int) (scale * 24);

        int xI3 = (int) (scale * 164);
        int yI3 = (int) (scale * 90);

        int xI4 = (int) (scale * 164);
        int yI4 = (int) (scale * 255);

        int xI5 = (int) (scale * 130);
        int yI5 = (int) (scale * 275);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);
        Point pI5 = new Point(xI5, yI5);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        pt_l_1.add(pI3);
        pt_l_1.add(pI4);

        pt_l_2.add(pI4);
        pt_l_2.add(pI5);

        point_j_Array[0] = pt_l_1;
        point_j_Array[1] = pt_l_2;
        point_j_Array[2] = pt_l_0;

    }
    ////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////// small k ///////////////////////////////////////////////////////////

    public static int k_TOTAL_STEPS = 4;

    public static List<Point>[] point_k_Array = new List[k_TOTAL_STEPS];


    public static void getkDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();


        int xK1 = (int) (scale * 95);
        int yK1 = (int) (scale * 55);

        int xK2 = (int) (scale * 95);
        int yK2= (int) (scale * 245);

        int xK3= (int) (scale * 100);
        int yK3= (int) (scale * 179);

        int xK4= (int) (scale * 134);
        int yK4= (int) (scale * 179);

        int xK5= (int) (scale * 135);
        int yK5= (int) (scale * 179);

        int xK6= (int) (scale * 185);
        int yK6= (int) (scale * 120);

        int xK7= (int) (scale * 190);
        int yK7= (int) (scale * 245);


        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3= new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(xK7, yK7);



        pt_K_0.add(pK1);
        pt_K_0.add(pK2);
        pt_K_1.add(pK3);
        pt_K_1.add(pK4);
        pt_K_2.add(pK5);
        pt_K_2.add(pK6);
        pt_K_3.add(pK5);
        pt_K_3.add(pK7);

        point_k_Array[0] = pt_K_0;
        point_k_Array[1] = pt_K_1;
        point_k_Array[2] = pt_K_2;
        point_k_Array[3] = pt_K_3;

    }
//////////////////////////////////////////////////

///////////////////////////// small l //////////////////////////////

    public static int l_TOTAL_STEPS = 1;

    public static List<Point>[] point_l_Array = new List[l_TOTAL_STEPS];
    public static void getlDetails(){



        List<Point> pt_l_0 = new ArrayList<Point>();

        int xI1 = (int) (scale * 150);
        int yI1 = (int) (scale * 35);
        int xI2 = (int) (scale * 150);
        int yI2 = (int) (scale * 250);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        point_l_Array[0] = pt_l_0;

    }

////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////// small m ///////////////////////////////////////////////////////////

    public static int m_TOTAL_STEPS = 7;

    public static List<Point>[] point_m_Array = new List[m_TOTAL_STEPS];


    public static void getmDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();
        List<Point> pt_K_4 = new ArrayList<Point>();
        List<Point> pt_K_5 = new ArrayList<Point>();
        List<Point> pt_K_6 = new ArrayList<Point>();

        int xK1 = (int) (scale * 60);
        int yK1 = (int) (scale * 90);

        int xK2 = (int) (scale * 60);
        int yK2= (int) (scale * 210);

        int xK3= (int) (scale * 64);
        int yK3= (int) (scale * 120);

        int x3= (int) (scale * 119);
        int y3= (int) (scale * 90);

        int xK4= (int) (scale * 147);
        int yK4= (int) (scale * 120);

        int xK5= (int) (scale * 147);
        int yK5= (int) (scale * 210);

        int xK6= (int) (scale * 153);
        int yK6= (int) (scale * 120);

        int x6= (int) (scale * 209);
        int y6= (int) (scale * 90);

        int xK7= (int) (scale * 240);
        int yK7= (int) (scale * 120);

        int xK8= (int) (scale * 240);
        int yK8= (int) (scale * 210);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point p3 = new Point(x3, y3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point p6 = new Point(x6, y6);
        Point pK7 = new Point(xK7, yK7);
        Point pK8 = new Point(xK8, yK8);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(p3);

        pt_K_2.add(p3);
        pt_K_2.add(pK4);

        pt_K_3.add(pK4);
        pt_K_3.add(pK5);

        pt_K_4.add(pK6);
        pt_K_4.add(p6);

        pt_K_5.add(p6);
        pt_K_5.add(pK7);

        pt_K_6.add(pK7);
        pt_K_6.add(pK8);

        point_m_Array[0] = pt_K_0;
        point_m_Array[1] = pt_K_1;
        point_m_Array[2] = pt_K_2;
        point_m_Array[3] = pt_K_3;
        point_m_Array[4] = pt_K_4;
        point_m_Array[5] = pt_K_5;
        point_m_Array[6] = pt_K_6;

    }
////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////// small n ///////////////////////////////////////////////////////////

    public static int n_TOTAL_STEPS = 4;

    public static List<Point>[] point_n_Array = new List[n_TOTAL_STEPS];


    public static void getnDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 103);
        int yK1 = (int) (scale * 95);

        int xK2 = (int) (scale * 103);
        int yK2= (int) (scale * 220);

        int xK3= (int) (scale * 108);
        int yK3= (int) (scale * 110);

        int x3= (int) (scale * 155);
        int y3= (int) (scale * 95);

        int xK4= (int) (scale * 196);
        int yK4= (int) (scale * 110);

        int xK5= (int) (scale * 196);
        int yK5= (int) (scale * 220);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(x3, y3);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(pK6);

        pt_K_2.add(pK6);
        pt_K_2.add(pK4);

        pt_K_3.add(pK4);
        pt_K_3.add(pK5);

        point_n_Array[0] = pt_K_0;
        point_n_Array[1] = pt_K_1;
        point_n_Array[2] = pt_K_2;
        point_n_Array[3] = pt_K_3;

    }
////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////// o ////////////////////////////////////////////////////////////////////////

    public static int o_TOTAL_STEPS =6;

    public static List<Point>[] point_o_Array = new List[o_TOTAL_STEPS];
    public static void getoDetails(){


        List<Point> pt_o_0 = new ArrayList<Point>();
        List<Point> pt_o_1 = new ArrayList<Point>();
        List<Point> pt_o_2 = new ArrayList<Point>();
        List<Point> pt_o_3 = new ArrayList<Point>();
        List<Point> pt_o_4 = new ArrayList<Point>();
        List<Point> pt_o_5 = new ArrayList<Point>();

        int x1 = (int) (scale * 150);
        int y1 = (int) (scale * 90);

        int x2 = (int) (scale * 100);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 100);
        int y3 = (int) (scale * 190);

        int x4 = (int) (scale * 155);
        int y4 = (int) (scale * 215);

        int x5 = (int) (scale * 200);
        int y5 = (int) (scale * 190);

        int x6 = (int) (scale * 200);
        int y6 = (int) (scale * 120);

        int x7 = (int) (scale * 155);
        int y7 = (int) (scale * 90);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);


        pt_o_0.add(p1);
        pt_o_0.add(p2);

        pt_o_1.add(p2);
        pt_o_1.add(p3);

        pt_o_2.add(p3);
        pt_o_2.add(p4);

        pt_o_3.add(p4);
        pt_o_3.add(p5);

        pt_o_4.add(p5);
        pt_o_4.add(p6);

        pt_o_5.add(p6);
        pt_o_5.add(p7);

        point_o_Array[0] = pt_o_0;
        point_o_Array[1] = pt_o_1;
        point_o_Array[2] = pt_o_2;
        point_o_Array[3] = pt_o_3;
        point_o_Array[4] = pt_o_4;
        point_o_Array[5] = pt_o_5;


    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////// small p ///////////////////////////////////////////////////////////

    public static int p_TOTAL_STEPS = 7;

    public static List<Point>[] point_p_Array = new List[p_TOTAL_STEPS];

    public static void getpDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();
        List<Point> pt_K_4 = new ArrayList<Point>();
        List<Point> pt_K_5 = new ArrayList<Point>();
        List<Point> pt_K_6 = new ArrayList<Point>();

        int xK1 = (int) (scale * 100);
        int yK1 = (int) (scale * 58);

        int xK2 = (int) (scale * 100);
        int yK2= (int) (scale * 238);

        int xK3= (int) (scale * 110);
        int yK3= (int) (scale * 78);

        int x3= (int) (scale * 140);
        int y3= (int) (scale * 60);

        int xK4= (int) (scale * 180);
        int yK4= (int) (scale * 60);

        int x4= (int) (scale * 200);
        int y4= (int) (scale * 110);

        int xK5= (int) (scale * 190);
        int yK5= (int) (scale * 160);

        int x5= (int) (scale * 147);
        int y5= (int) (scale * 180);

        int xK6= (int) (scale * 110);
        int yK6= (int) (scale * 165);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(x3, y3);
        Point pK8 = new Point(x4, y4);
        Point pK9 = new Point(x5, y5);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(pK7);

        pt_K_2.add(pK7);
        pt_K_2.add(pK4);

        pt_K_3.add(pK4);
        pt_K_3.add(pK8);

        pt_K_4.add(pK8);
        pt_K_4.add(pK5);

        pt_K_5.add(pK5);
        pt_K_5.add(pK9);

        pt_K_6.add(pK9);
        pt_K_6.add(pK6);

        point_p_Array[0] = pt_K_0;
        point_p_Array[1] = pt_K_1;
        point_p_Array[2] = pt_K_2;
        point_p_Array[3] = pt_K_3;
        point_p_Array[4] = pt_K_4;
        point_p_Array[5] = pt_K_5;
        point_p_Array[6] = pt_K_6;

    }
    ////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////small q ///////////////////
    public static int q_TOTAL_STEPS = 7;

    public static List<Point>[] point_q_Array = new List[q_TOTAL_STEPS];
    public static void getqDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();

        int x1 = (int) (scale * 190);
        int y1 = (int) (scale * 64);

        int x2 = (int) (scale * 140);
        int y2 = (int) (scale * 40);

        int x9 = (int) (scale * 107);
        int y9 = (int) (scale * 65);

        int x3 = (int) (scale * 90);
        int y3 = (int) (scale * 100);

        int x4 = (int) (scale * 100);
        int y4 = (int) (scale * 160);

        int x5 = (int) (scale * 130);
        int y5 = (int) (scale * 200);

        int x6 = (int) (scale * 190);
        int y6 = (int) (scale * 180);

        int x7 = (int) (scale * 210);
        int y7 = (int) (scale * 55);

        int x8 = (int) (scale * 210);
        int y8 = (int) (scale * 248);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p9);

        pt_C_6.add(p9);
        pt_C_6.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p5);

        pt_C_4.add(p5);
        pt_C_4.add(p6);

        pt_C_5.add(p7);
        pt_C_5.add(p8);

        point_q_Array[0] = pt_C_0;
        point_q_Array[1] = pt_C_1;
        point_q_Array[2] = pt_C_6;
        point_q_Array[3] = pt_C_2;
        point_q_Array[4] = pt_C_3;
        point_q_Array[5] = pt_C_4;
        point_q_Array[6] = pt_C_5;

    }
    //////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////small r ///////////////////
    public static int r_TOTAL_STEPS = 2;

    public static List<Point>[] point_r_Array = new List[r_TOTAL_STEPS];
    public static void getrDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();

        int x1 = (int) (scale * 124);
        int y1 = (int) (scale * 75);

        int x2 = (int) (scale * 124);
        int y2 = (int) (scale * 224);

        int x3 = (int) (scale * 126);
        int y3 = (int) (scale * 104);

        int x4 = (int) (scale * 186);
        int y4 = (int) (scale * 74);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p3);
        pt_C_1.add(p4);

        point_r_Array[0] = pt_C_0;
        point_r_Array[1] = pt_C_1;
    }
    ///////////////////////////////////////////////////
    //////////////////////////////////// s //////////////////////////////////////////////////////////////////

    public static int s_TOTAL_STEPS =8;

    public static List<Point>[] point_s_Array = new List[s_TOTAL_STEPS];
    public static void getsDetails(){


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();




        int x1 = (int) (scale * 190);
        int y1 = (int) (scale * 107);

        int x2 = (int) (scale * 165);
        int y2= (int) (scale * 85);

        int x3 = (int) (scale * 127);
        int y3= (int) (scale * 89);

        int x4 = (int) (scale * 110);
        int y4= (int) (scale * 135);

        int x5 = (int) (scale * 170);
        int y5= (int) (scale *157);

        int x6 = (int) (scale * 198);
        int y6= (int) (scale *190);

        int x7 = (int) (scale * 170);
        int y7= (int) (scale *217);

        int x8 = (int) (scale * 145);
        int y8= (int) (scale *220);

        int x9= (int) (scale * 110);
        int y9= (int) (scale *200);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);



        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);

        pt_s_4.add(p5);
        pt_s_4.add(p6);

        pt_s_5.add(p6);
        pt_s_5.add(p7);

        pt_s_6.add(p7);
        pt_s_6.add(p8);

        pt_s_7.add(p8);
        pt_s_7.add(p9);

        point_s_Array[0] = pt_s_0;
        point_s_Array[1] = pt_s_1;
        point_s_Array[2] = pt_s_2;
        point_s_Array[3] = pt_s_3;
        point_s_Array[4] = pt_s_4;
        point_s_Array[5] = pt_s_5;
        point_s_Array[6] = pt_s_6;
        point_s_Array[7] = pt_s_7;


    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////small t ///////////////////
    public static int t_TOTAL_STEPS = 3;

    public static List<Point>[] point_t_Array = new List[t_TOTAL_STEPS];
    public static void gettDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 150);
        int y1 = (int) (scale * 55);

        int x2 = (int) (scale * 150);
        int y2 = (int) (scale * 230);

        int x3 = (int) (scale * 190);
        int y3 = (int) (scale * 250);

        int x4 = (int) (scale * 110);
        int y4 = (int) (scale * 100);

        int x5 = (int) (scale * 190);
        int y5 = (int) (scale * 100);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p4);
        pt_C_2.add(p5);

        point_t_Array[0] = pt_C_0;
        point_t_Array[1] = pt_C_1;
        point_t_Array[2] = pt_C_2;

    }
    ///////////////////////////////////////////////////

    /////////////////////////////////////////small u ///////////////////
    public static int u_TOTAL_STEPS = 4;

    public static List<Point>[] point_u_Array = new List[u_TOTAL_STEPS];
    public static void getuDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();

        int x1 = (int) (scale * 100);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 100);
        int y2 = (int) (scale * 180);

        int x3 = (int) (scale * 140);
        int y3 = (int) (scale * 210);

        int x4 = (int) (scale * 180);
        int y4 = (int) (scale * 190);

        int x5 = (int) (scale * 200);
        int y5 = (int) (scale * 85);

        int x6 = (int) (scale * 200);
        int y6 = (int) (scale * 215);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);

        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);



        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p5);
        pt_C_3.add(p6);



        point_u_Array[0] = pt_C_0;
        point_u_Array[1] = pt_C_1;
        point_u_Array[2] = pt_C_2;
        point_u_Array[3] = pt_C_3;


    }
    ///////////////////////////////////////////////////

    /////////////////////////////////////////small v ///////////////////
    public static int v_TOTAL_STEPS = 2;

    public static List<Point>[] point_v_Array = new List[v_TOTAL_STEPS];
    public static void getvDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();


        int x1 = (int) (scale * 100);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 150);
        int y2 = (int) (scale * 220);

        int x3 = (int) (scale * 200);
        int y3 = (int) (scale * 80);

        Point p1 = new Point(x1, y1);

        Point p2 = new Point(x2, y2);

        Point p3 = new Point(x3, y3);



        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);


        point_v_Array[0] = pt_C_0;
        point_v_Array[1] = pt_C_1;


    }
    ///////////////////////////////////////////////////

    /////////////////////////////////////////small w ///////////////////
    public static int w_TOTAL_STEPS = 4;

    public static List<Point>[] point_w_Array = new List[w_TOTAL_STEPS];
    public static void getwDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();


        int x1 = (int) (scale * 65);
        int y1 = (int) (scale * 85);

        int x2 = (int) (scale * 105);
        int y2 = (int) (scale * 210);

        int x3 = (int) (scale * 150);
        int y3 = (int) (scale * 85);

        int x4 = (int) (scale * 190);
        int y4 = (int) (scale * 210);

        int x5 = (int) (scale * 230);
        int y5 = (int) (scale * 85);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);

        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);


        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p5);



        point_w_Array[0] = pt_C_0;
        point_w_Array[1] = pt_C_1;
        point_w_Array[2] = pt_C_2;
        point_w_Array[3] = pt_C_3;


    }
    ///////////////////////////////////////////////////


    /////////////////////////////////////////small x ///////////////////
    public static int x_TOTAL_STEPS = 2;

    public static List<Point>[] point_x_Array = new List[x_TOTAL_STEPS];
    public static void getxDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();


        int x1 = (int) (scale * 110);
        int y1 = (int) (scale * 90);

        int x2 = (int) (scale * 190);
        int y2 = (int) (scale * 210);

        int x3 = (int) (scale * 190);
        int y3 = (int) (scale * 90);

        int x4 = (int) (scale * 110);
        int y4 = (int) (scale * 210);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);


        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p3);
        pt_C_1.add(p4);





        point_x_Array[0] = pt_C_0;
        point_x_Array[1] = pt_C_1;


    }
    ///////////////////////////////////////////////////

    /////////////////////////////////////////small y ///////////////////
    public static int y_TOTAL_STEPS = 3;

    public static List<Point>[] point_y_Array = new List[y_TOTAL_STEPS];
    public static void getyDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();



        int x1 = (int) (scale * 105);
        int y1 = (int) (scale * 55);

        int x2 = (int) (scale * 145);
        int y2 = (int) (scale * 170);

        int x3 = (int) (scale * 200);
        int y3 = (int) (scale * 55);

        int x4 = (int) (scale * 130);
        int y4 = (int) (scale * 235);

        int x5 = (int) (scale * 110);
        int y5 = (int) (scale * 240);





        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);



        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p3);
        pt_C_1.add(p4);

        pt_C_2.add(p4);
        pt_C_2.add(p5);


        point_y_Array[0] = pt_C_0;
        point_y_Array[1] = pt_C_1;
        point_y_Array[2] = pt_C_2;



    }
    ///////////////////////////////////////////////////

    /////////////////////////////////////////small z ///////////////////
    public static int z_TOTAL_STEPS = 3;

    public static List<Point>[] point_z_Array = new List[z_TOTAL_STEPS];
    public static void getzDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 90);
        int y1 = (int) (scale * 75);

        int x4 = (int) (scale * 210);
        int y4 = (int) (scale * 220);

        int x2 = (int) (scale * 205);
        int y2 = (int) (scale * 75);

        int x3 = (int) (scale * 100);
        int y3 = (int) (scale * 220);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);


        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        point_z_Array[0] = pt_C_0;
        point_z_Array[1] = pt_C_1;
        point_z_Array[2] = pt_C_2;

    }
    ///////////////////////////////////////////////////






}
