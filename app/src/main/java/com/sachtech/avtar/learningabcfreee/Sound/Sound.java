package com.sachtech.avtar.learningabcfreee.Sound;

import android.content.Context;
import android.media.MediaPlayer;

import com.sachtech.avtar.learningabc.R;

/**
 * Created by avtar on 3/5/2016.
 */
public class Sound {

    public static MediaPlayer playButtonClick(Context context) {

        MediaPlayer player = MediaPlayer.create(context, R.raw.button_click);
        player.start();

        return player;
    }

    public static MediaPlayer playLetterSound(Context context, int soundId) {
        MediaPlayer player = MediaPlayer.create(context, soundId);
        player.start();
        return player;
    }

}
