package com.sachtech.avtar.learningabcfreee.Animation;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;

/**
 * Created by avtar on 3/9/2016.
 */
public class ObjectAnim {
    public static void startAnimation(View view, int time,Context context,int animId) {

       ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(context, animId);
        anim.setTarget(view);
        anim.setDuration(time);
        anim.start();
    }
}
