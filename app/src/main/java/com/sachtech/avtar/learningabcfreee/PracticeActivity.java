package com.sachtech.avtar.learningabcfreee;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sachtech.avtar.learningabcfreee.Constants.Config;
import com.sachtech.avtar.learningabcfreee.Constants.DataHolder;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.LayoutComponents.CustomButton;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;
import com.sachtech.avtar.learningabcfreee.Utils.PracticePaint;
import com.sachtech.avtar.learningabcfreee.Utils.PracticeView;

public class PracticeActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = this.getClass().getSimpleName();
    ImageView alphabet_img, dotted_img, simple_imgl, picture, small_alphabet_image;
    ImageView home_button, next_button, back_button,retry_button;
    private Button mButtonA, mButtonB, mButtonC, mButtonD, mButtonE, mButtonF, mButtonG, mButtonH, mButtonI, mButtonJ, mButtonK, mButtonL, mButtonM, mButtonN, mButtonO, mButtonP, mButtonQ, mButtonR, mButtonS, mButtonT, mButtonU, mButtonV, mButtonW, mButtonX, mButtonY, mButtonZ;
    String CURRENT_LETTER;
    private int DOTTED_IMAGE = 0;
    private int picture_alphabet = 0;
    private int OUTLINE_IMAGE = 0;
    private int SMALL_LETTER = 0;
    private int OUTLINE_SMALL = 0;
    LinearLayout linearLayout_a_img;
    RelativeLayout outline_layout, draw_layout;
    PracticeView wordView;
    PracticePaint practicePaint;
    private Handler nextWordHandler = null;
    private Runnable nextWordRunnable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);

        nextWordHandler = new Handler();
        nextWordRunnable = new Runnable() {
            @Override
            public void run() {

                outline_layout.removeAllViewsInLayout();
                draw_layout.removeAllViewsInLayout();
                alphabet_img.setBackgroundResource(0);
                picture.setBackgroundResource(0);

               /* if (CURRENT_LETTER.equalsIgnoreCase("G")){
                   getFullVersionDialog();
                }else {*/
                    initView();
              /*  }*/


            }
        };


        initView();
    }

    public void initView() {
        alphabet_img = (ImageView) findViewById(R.id.practice_a_img);
        picture = (ImageView) findViewById(R.id.practice_apple_img);
        dotted_img = (ImageView) findViewById(R.id.practice_dotted_img);
        simple_imgl = (ImageView) findViewById(R.id.practice_empty);
        home_button = (ImageView) findViewById(R.id.practice_home_button);
        next_button = (ImageView) findViewById(R.id.practice_next_button);
        back_button = (ImageView) findViewById(R.id.practice_back_button);
        linearLayout_a_img = (LinearLayout) findViewById(R.id.linearlayout1);
        outline_layout = (RelativeLayout) findViewById(R.id.outline_layout);
        draw_layout = (RelativeLayout) findViewById(R.id.draw_layout);
        small_alphabet_image = (ImageView) findViewById(R.id.practice_small_img);
        retry_button = (ImageView) findViewById(R.id.practice_redo_button);

        mButtonA = (Button) findViewById(R.id.button_A);
        mButtonB = (Button) findViewById(R.id.button_B);
        mButtonC = (Button) findViewById(R.id.button_C);
        mButtonD = (Button) findViewById(R.id.button_D);
        mButtonE = (Button) findViewById(R.id.button_E);
        mButtonF = (Button) findViewById(R.id.button_F);
        mButtonG = (Button) findViewById(R.id.button_G);
        mButtonH = (Button) findViewById(R.id.button_H);
        mButtonI = (Button) findViewById(R.id.button_I);
        mButtonJ = (Button) findViewById(R.id.button_J);
        mButtonK = (Button) findViewById(R.id.button_K);
        mButtonL = (Button) findViewById(R.id.button_L);
        mButtonM = (Button) findViewById(R.id.button_M);
        mButtonN = (Button) findViewById(R.id.button_N);
        mButtonO = (Button) findViewById(R.id.button_O);
        mButtonP = (Button) findViewById(R.id.button_P);
        mButtonQ = (Button) findViewById(R.id.button_Q);
        mButtonR = (Button) findViewById(R.id.button_R);
        mButtonS = (Button) findViewById(R.id.button_S);
        mButtonT = (Button) findViewById(R.id.button_T);
        mButtonU = (Button) findViewById(R.id.button_U);
        mButtonV = (Button) findViewById(R.id.button_V);
        mButtonW = (Button) findViewById(R.id.button_W);
        mButtonX = (Button) findViewById(R.id.button_X);
        mButtonY = (Button) findViewById(R.id.button_Y);
        mButtonZ = (Button) findViewById(R.id.button_Z);

        mButtonA.setOnClickListener(this);
        mButtonB.setOnClickListener(this);
        mButtonC.setOnClickListener(this);
        mButtonD.setOnClickListener(this);
        mButtonE.setOnClickListener(this);
        mButtonF.setOnClickListener(this);
        mButtonG.setOnClickListener(this);
        mButtonH.setOnClickListener(this);
        mButtonI.setOnClickListener(this);
        mButtonJ.setOnClickListener(this);
        mButtonK.setOnClickListener(this);
        mButtonL.setOnClickListener(this);
        mButtonM.setOnClickListener(this);
        mButtonN.setOnClickListener(this);
        mButtonO.setOnClickListener(this);
        mButtonP.setOnClickListener(this);
        mButtonQ.setOnClickListener(this);
        mButtonR.setOnClickListener(this);
        mButtonS.setOnClickListener(this);
        mButtonT.setOnClickListener(this);
        mButtonU.setOnClickListener(this);
        mButtonV.setOnClickListener(this);
        mButtonW.setOnClickListener(this);
        mButtonX.setOnClickListener(this);
        mButtonY.setOnClickListener(this);
        mButtonZ.setOnClickListener(this);

        checkComingLetter();

        next_button.setOnClickListener(this);
        back_button.setOnClickListener(this);
        home_button.setOnClickListener(this);
        retry_button.setOnClickListener(this);
    }

    public void checkComingLetter() {
        CURRENT_LETTER = DataHolder.LETTER;
        switch (CURRENT_LETTER) {
            case Config.A:

              new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_a);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);

                DOTTED_IMAGE = Config.Image_A;
                picture_alphabet = Config.APPLE;
                OUTLINE_IMAGE = Config.Word_A_outline;
                SMALL_LETTER = Config.Image_a;

                small_alphabet_image.setImageResource(SMALL_LETTER);
                picture.setImageResource(picture_alphabet);
                alphabet_img.setImageResource(DOTTED_IMAGE);

                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_a_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                // dotted_img.setImageResource(OUTLINE_IMAGE);

                break;

            case Config.B:


                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_b);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
            DOTTED_IMAGE = Config.Image_B;
            alphabet_img.setImageResource(DOTTED_IMAGE);

            picture_alphabet = Config.BUS;
            picture.setImageResource(picture_alphabet);

            OUTLINE_IMAGE = Config.Word_B_outline;
            wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
            outline_layout.addView(wordView);

            OUTLINE_SMALL = Config.Word_b_outline;
            practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
            draw_layout.addView(practicePaint);

            SMALL_LETTER = Config.Image_b;
            small_alphabet_image.setImageResource(SMALL_LETTER);
            break;

            case Config.C:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_c);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_C;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.CAR;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_C_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_c_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_c;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.D:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_d);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_D;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.DOG;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_D_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_d_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_d;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.E:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_e);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_E;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.EGG;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_E_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_e_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_e;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;


            case Config.F:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_f);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_F;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.FISH;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_F_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_f_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_f;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.G:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_g);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_G;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.GOAT;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_G_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_g_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_g;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.H:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_h);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_H;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.HUT;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_H_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_h_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_h;
                small_alphabet_image.setImageResource(SMALL_LETTER);
                break;

            case Config.I:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_i);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_I;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.IGLOO;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_I_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_i_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_i;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.J:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_j);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_J;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.JEEP;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_J_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_j_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_j;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.K:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_k);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_K;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.KITE;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_K_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_k_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_k;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.L:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_l);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_L;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.LAMP;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_L_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_l_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_l;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.M:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_m);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_M;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.MANGO;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_M_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_m_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_m;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.N:
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_n);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_N;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.NET;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_N_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_n_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_n;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.O:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_o);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_O;
                alphabet_img.setImageResource(DOTTED_IMAGE);

                picture_alphabet = Config.OX;
                picture.setImageResource(picture_alphabet);

                OUTLINE_IMAGE = Config.Word_O_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_o_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_o;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.P:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_p);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_P;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.PEACOCK;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_P_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_p_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_p;
                small_alphabet_image.setImageResource(SMALL_LETTER);
                break;

            case Config.Q:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_q);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_Q;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.QUEEN;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_Q_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_q_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_q;
                small_alphabet_image.setImageResource(SMALL_LETTER);
                break;

            case Config.R:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_r);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_R;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.RABBIT;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_R_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_r_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_r;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.S:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_s);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_S;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.SNAKE;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_S_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_s_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_s;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.T:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_t);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_T;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.TRAIN;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_T_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_t_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_t;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.U:
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_u);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_U;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.UMBRELLA;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_U_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_u_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_u;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.V:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_v);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_V;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.VIOLIN;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_V_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_v_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_v;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.W:
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_w);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_W;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.WHALE;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_W_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_w_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_w;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.X:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_x);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_X;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.X_RAY;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_X_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_x_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_x;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.Y:

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_y);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_Y;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.YOGA;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_Y_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_y_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_y;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;

            case Config.Z:
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Sound.playLetterSound(PracticeActivity.this, R.raw.sound_caps_z);
                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }

                    }
                }, 1000);
                DOTTED_IMAGE = Config.Image_Z;
                alphabet_img.setImageResource(DOTTED_IMAGE);
                picture_alphabet = Config.ZOO;
                picture.setImageResource(picture_alphabet);
                OUTLINE_IMAGE = Config.Word_Z_outline;
                wordView = new PracticeView(PracticeActivity.this, OUTLINE_IMAGE);
                outline_layout.addView(wordView);

                OUTLINE_SMALL = Config.Word_z_outline;
                practicePaint = new PracticePaint(PracticeActivity.this, OUTLINE_SMALL);
                draw_layout.addView(practicePaint);

                SMALL_LETTER = Config.Image_z;
                small_alphabet_image.setImageResource(SMALL_LETTER);

                break;
            default:
                DOTTED_IMAGE = Config.Image_A;
                picture_alphabet = Config.APPLE;
                OUTLINE_IMAGE = Config.Word_A_outline;
                SMALL_LETTER = Config.Image_a;
                OUTLINE_SMALL = Config.Word_a_outline;

                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.practice_next_button:
                /*outline_layout.removeAllViewsInLayout();
                draw_layout.removeAllViewsInLayout();
                alphabet_img.setBackgroundResource(0);
                picture.setBackgroundResource(0);*/
                nextWord();
                break;

            case R.id.practice_back_button:
                outline_layout.removeAllViewsInLayout();
                draw_layout.removeAllViewsInLayout();
                alphabet_img.setBackgroundResource(0);
                picture.setBackgroundResource(0);
                previousWord();
                break;

            case R.id.practice_home_button:
                finish();
                break;

            case R.id.practice_redo_button:
                practicePaint.clearPaint();
                practicePaint.clearCanvas();
                wordView.clearCanvas();
                break;

            case R.id.button_A:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.A;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;

            case R.id.button_B:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.B;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;

            case R.id.button_C:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.C;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;

            case R.id.button_D:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.D;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;

            case R.id.button_E:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.E;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;

            case R.id.button_F:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.F;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_G:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.G;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_H:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.H;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_I:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.I;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_J:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.J;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_K:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.K;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_L:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.L;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_M:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.M;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_N:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.N;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_O:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.O;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_P:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.P;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_Q:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.Q;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_R:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.R;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_S:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.S;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_T:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.T;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_U:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.U;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_V:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.V;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_W:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.W;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_X:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.X;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_Y:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.Y;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;
            case R.id.button_Z:
                CurrentLetter(DataHolder.LETTER);
                DataHolder.LETTER=Config.Z;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                break;

            default:
                break;
        }
    }

    public void nextWord() {
        CURRENT_LETTER = DataHolder.LETTER;

                switch (CURRENT_LETTER) {
                    case Config.A:

                        Sound.playLetterSound(PracticeActivity.this, R.raw.a_for_apple);
                        DataHolder.LETTER = Config.B;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);

                        //initView();
                        break;

                    case Config.B:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.b_for_ball);
                        DataHolder.LETTER = Config.C;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                        //initView();
                        break;


                    case Config.C:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.c_for_car);
                        DataHolder.LETTER = Config.D;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                      //  initView();
                        break;

                    case Config.D:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.d_for_dog);
                        DataHolder.LETTER = Config.E;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                       // initView();
                        break;

                    case Config.E:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.eforegg);
                        DataHolder.LETTER = Config.F;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                      //  initView();
                        break;

                    case Config.F:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.fforfish);
                        DataHolder.LETTER = Config.G;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                      //  initView();
                        break;

                    case Config.G:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.g_for_gift);
                        DataHolder.LETTER = Config.A;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);


                       // initView();
                        break;

                    case Config.H:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.h_for_hat);
                        DataHolder.LETTER = Config.I;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                     //   initView();
                        break;

                    case Config.I:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.i_for_ink);
                        DataHolder.LETTER = Config.J;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                     //   initView();
                        break;

                    case Config.J:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.j_for_jug);
                        DataHolder.LETTER = Config.K;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                    //    initView();
                        break;

                    case Config.K:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.kforkites);
                        DataHolder.LETTER = Config.L;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                     //   initView();
                        break;

                    case Config.L:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.lforamp);
                        DataHolder.LETTER = Config.M;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                      //  initView();
                        break;

                    case Config.M:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.mformango);
                        DataHolder.LETTER = Config.N;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                        //initView();
                        break;

                    case Config.N:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.n_for_nest);
                        DataHolder.LETTER = Config.O;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                       // initView();
                        break;

                    case Config.O:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.o_for_owl);
                        DataHolder.LETTER = Config.P;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);

                       // initView();
                        break;

                    case Config.P:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.p_for_pig);
                        DataHolder.LETTER = Config.Q;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                      //  initView();
                        break;

                    case Config.Q:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.qforqueen);
                        DataHolder.LETTER = Config.R;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                     //   initView();
                        break;

                    case Config.R:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.rforrabbit);
                        DataHolder.LETTER = Config.S;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                      //  initView();
                        break;

                    case Config.S:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.sforsnake);
                        DataHolder.LETTER = Config.T;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                        //initView();
                        break;

                    case Config.T:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.tfortrain);
                        DataHolder.LETTER = Config.U;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                      //  initView();
                        break;

                    case Config.U:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.uforumbrella);
                        DataHolder.LETTER = Config.V;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                      //  initView();
                        break;

                    case Config.V:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.violin);
                        DataHolder.LETTER = Config.W;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                     //   initView();
                        break;

                    case Config.W:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.wforwell);
                        DataHolder.LETTER = Config.X;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                    //    initView();
                        break;

                    case Config.X:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.xforxray);
                        DataHolder.LETTER = Config.Y;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                     //   initView();
                        break;

                    case Config.Y:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.y_for_yak);
                        DataHolder.LETTER = Config.Z;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                    //    initView();
                        break;

                    case Config.Z:
                        Sound.playLetterSound(PracticeActivity.this, R.raw.zforzoo);
                        DataHolder.LETTER = Config.A;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                     //   initView();
                        break;


                    default:
                        DataHolder.LETTER = Config.A;
                        nextWordHandler.postDelayed(nextWordRunnable,3500);
                       // initView();
                        break;
                }
            }



    public void previousWord(){
        CURRENT_LETTER=DataHolder.LETTER;

        switch(CURRENT_LETTER){
        case Config.A:
        DataHolder.LETTER=Config.G;
        initView();
        break;

        case Config.B:
        DataHolder.LETTER=Config.A;
        initView();
        break;


        case Config.C:
        DataHolder.LETTER=Config.B;
        initView();
        break;

        case Config.D:
        DataHolder.LETTER=Config.C;
        initView();
        break;

        case Config.E:
        DataHolder.LETTER=Config.D;
        initView();
        break;

        case Config.F:
        DataHolder.LETTER=Config.E;
        initView();
        break;

        case Config.G:
        DataHolder.LETTER=Config.F;
        initView();
        break;

            case Config.H:
                DataHolder.LETTER = Config.G;
                initView();
                break;

            case Config.I:
                DataHolder.LETTER = Config.H;
                initView();
                break;

            case Config.J:
                DataHolder.LETTER = Config.I;
                initView();
                break;

            case Config.K:
                DataHolder.LETTER = Config.J;
                initView();
                break;

            case Config.L:
                DataHolder.LETTER = Config.K;
                initView();
                break;

            case Config.M:
                DataHolder.LETTER = Config.L;
                initView();
                break;

            case Config.N:
                DataHolder.LETTER = Config.M;
                initView();
                break;

            case Config.O:
                DataHolder.LETTER = Config.N;
                initView();
                break;

            case Config.P:
                DataHolder.LETTER = Config.O;
                initView();
                break;

            case Config.Q:
                DataHolder.LETTER = Config.P;
                initView();
                break;

            case Config.R:
                DataHolder.LETTER = Config.Q;
                initView();
                break;

            case Config.S:
                DataHolder.LETTER = Config.R;
                initView();
                break;

            case Config.T:
                DataHolder.LETTER = Config.S;
                initView();
                break;

            case Config.U:
                DataHolder.LETTER = Config.T;
                initView();
                break;
            case Config.V:
                DataHolder.LETTER = Config.U;
                initView();
                break;

            case Config.W:
                DataHolder.LETTER = Config.V;
                initView();
                break;

            case Config.X:
                DataHolder.LETTER = Config.W;
                initView();
                break;

            case Config.Y:
                DataHolder.LETTER = Config.X;
                initView();
                break;

            case Config.Z:
                DataHolder.LETTER = Config.Y;
                initView();
                break;

default:
        DataHolder.LETTER=Config.A;
        initView();
        break;

        }
    }

    public void CurrentLetter(String LETTER)
    {

        switch(LETTER){
            case Config.A:

                Sound.playLetterSound(PracticeActivity.this, R.raw.a_for_apple);
                /*DataHolder.LETTER = Config.B;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
*/
                //initView();
                break;

            case Config.B:
                Sound.playLetterSound(PracticeActivity.this, R.raw.b_for_ball);
                /*DataHolder.LETTER = Config.C;
                nextWordHandler.postDelayed(nextWordRunnable,3500)*/;
                //initView();
                break;


            case Config.C:
                Sound.playLetterSound(PracticeActivity.this, R.raw.c_for_car);
               /* DataHolder.LETTER = Config.D;
                nextWordHandler.postDelayed(nextWordRunnable,3500);*/
                //  initView();
                break;

            case Config.D:
                Sound.playLetterSound(PracticeActivity.this, R.raw.d_for_dog);
                /*DataHolder.LETTER = Config.E;
                nextWordHandler.postDelayed(nextWordRunnable,3500);*/
                // initView();
                break;

            case Config.E:
                Sound.playLetterSound(PracticeActivity.this, R.raw.eforegg);
            /*    DataHolder.LETTER = Config.F;
                nextWordHandler.postDelayed(nextWordRunnable,3500);
                //  initView();*/
                break;

            case Config.F:
                Sound.playLetterSound(PracticeActivity.this, R.raw.fforfish);
              /*  DataHolder.LETTER = Config.G;
                nextWordHandler.postDelayed(nextWordRunnable,3500);*/
                //  initView();
                break;

            case Config.G:
                Sound.playLetterSound(PracticeActivity.this, R.raw.g_for_gift);
               /* DataHolder.LETTER = Config.H;
                nextWordHandler.postDelayed(nextWordRunnable,3500);*/
                // initView();
                break;

            case Config.H:
                Sound.playLetterSound(PracticeActivity.this, R.raw.h_for_hat);
                DataHolder.LETTER = Config.I;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);*//*
                //   initView();
                break;

            case Config.I:
                Sound.playLetterSound(PracticeActivity.this, R.raw.i_for_ink);
            /*   DataHolder.LETTER = Config.J;
                //nextWordHandler.postDelayed(nextWordRunnable,3500);
                //   initView();*/
                break;

            case Config.J:
                Sound.playLetterSound(PracticeActivity.this, R.raw.j_for_jug);
              /* DataHolder.LETTER = Config.K;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);
                //    initView();*/
                break;

            case Config.K:
                Sound.playLetterSound(PracticeActivity.this, R.raw.kforkites);
               /*DataHolder.LETTER = Config.L;
              //  nextWordHandler.postDelayed(nextWordRunnable,3500);
                //   initView();*/
                break;

            case Config.L:
                Sound.playLetterSound(PracticeActivity.this, R.raw.lforamp);
                /*DataHolder.LETTER = Config.M;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);
                //  initView();*/
                break;

            case Config.M:
                Sound.playLetterSound(PracticeActivity.this, R.raw.mformango);
               /*DataHolder.LETTER = Config.N;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);
                //initView();*/
                break;

            case Config.N:
                Sound.playLetterSound(PracticeActivity.this, R.raw.n_for_nest);
//                DataHolder.LETTER = Config.O;
//                nextWordHandler.postDelayed(nextWordRunnable,3500);
//                // initView();
                break;

            case Config.O:
                Sound.playLetterSound(PracticeActivity.this, R.raw.o_for_owl);
              /* DataHolder.LETTER = Config.P;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);

                // initView();*/
                break;

            case Config.P:
                Sound.playLetterSound(PracticeActivity.this, R.raw.p_for_pig);
               /*DataHolder.LETTER = Config.Q;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);
                //  initView();*/
                break;

            case Config.Q:
                Sound.playLetterSound(PracticeActivity.this, R.raw.qforqueen);
                /*DataHolder.LETTER = Config.R;
                //nextWordHandler.postDelayed(nextWordRunnable,3500);
                //   initView();*/
                break;

            case Config.R:
                Sound.playLetterSound(PracticeActivity.this, R.raw.rforrabbit);
                /*DataHolder.LETTER = Config.S;
              //  nextWordHandler.postDelayed(nextWordRunnable,3500);
                //  initView();*/
                break;

            case Config.S:
                Sound.playLetterSound(PracticeActivity.this, R.raw.sforsnake);
                /*DataHolder.LETTER = Config.T;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);
                //initView();*/
                break;

            case Config.T:
                Sound.playLetterSound(PracticeActivity.this, R.raw.tfortrain);
                /*DataHolder.LETTER = Config.U;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);
                //  initView();*/
                break;

            case Config.U:
                Sound.playLetterSound(PracticeActivity.this, R.raw.uforumbrella);
                /*DataHolder.LETTER = Config.V;
              //  nextWordHandler.postDelayed(nextWordRunnable,3500);
                //  initView();*/
                break;

            case Config.V:
                Sound.playLetterSound(PracticeActivity.this, R.raw.violin);
                /*DataHolder.LETTER = Config.W;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);
                //   initView();*/
                break;

            case Config.W:
                Sound.playLetterSound(PracticeActivity.this, R.raw.wforwell);
               /*DataHolder.LETTER = Config.X;
              //  nextWordHandler.postDelayed(nextWordRunnable,3500);
                //    initView();*/
                break;

            case Config.X:
                Sound.playLetterSound(PracticeActivity.this, R.raw.xforxray);
                /*DataHolder.LETTER = Config.Y;
               // nextWordHandler.postDelayed(nextWordRunnable,3500);
                //   initView();*/
                break;

            case Config.Y:
                Sound.playLetterSound(PracticeActivity.this, R.raw.y_for_yak);
                /*DataHolder.LETTER = Config.Z;
                //nextWordHandler.postDelayed(nextWordRunnable,3500);
                //    initView();*/
                break;

            case Config.Z:
                Sound.playLetterSound(PracticeActivity.this, R.raw.zforzoo);
                /*DataHolder.LETTER = Config.A;
              //  nextWordHandler.postDelayed(nextWordRunnable,3500);
                //   initView();*/
                break;
                
            default:
              //  DataHolder.LETTER = Config.A;
              //  nextWordHandler.postDelayed(nextWordRunnable,3500);
                // initView();
                break;
        }
    }
        
    private void getFullVersionDialog(){
        View view = LayoutInflater.from(this).inflate(R.layout.dialog,null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);

        final AlertDialog dialog = builder.create();
        dialog.show();

        CustomButton fullVersionBtn = (CustomButton)dialog.findViewById(R.id.full_version);
        CustomButton mayBeLatterBtn = (CustomButton)dialog.findViewById(R.id.maybe_later);
        ImageView image = (ImageView)dialog.findViewById(R.id.preview_img1);

        fullVersionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.sachtech.avtar.learningabc"));
                startActivity(intent);
            }
        });
        mayBeLatterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "on click work", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                finish();
            }
        });

    }
}
