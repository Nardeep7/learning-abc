package com.sachtech.avtar.learningabcfreee.Fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.sachtech.avtar.learningabcfreee.Constants.Config;
import com.sachtech.avtar.learningabcfreee.Constants.DataHolder;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;

public class DrawShapesFragment extends Fragment {



    private Context m_Context = null;
    private Paint paint, mBitmapPaint;
    private Path path;
    private Bitmap bitmap;
    private RelativeLayout mPaintLayout ;

   // private MediaPlayer mediaPlayer = null;
    private static DrawShapesFragment drawLetterFrag;

    private String TAG = this.getClass().getSimpleName();



    public static DrawShapesFragment getInstance(){
        return drawLetterFrag;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_draw_letter);*/

        drawLetterFrag = this;



      //  playMusic();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_draw_letter,container,false);
        initView(view);
        return view;
    }

    private void initView(View view) {

        checkComingLetter();

        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.triangle);

        mPaintLayout = (RelativeLayout)view.findViewById(R.id.painting_layout);

        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(40);

        paint.setStyle(Paint.Style.STROKE);

        path = new Path();

        path.close();

        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        ;
        MyCustomView v = new MyCustomView(getActivity());
        mPaintLayout.addView(v);
    }

    private void checkComingLetter() {

        String letter = DataHolder.LETTER;

        switch (letter){
            case Config.A:
               // GIF_IMAGE = Config.Gif_A;
            //    DOTTED_IMAGE = Config.Image_A;
                Sound.playLetterSound(getActivity(), R.raw.a);
                break;

            case Config.a:
               // GIF_IMAGE = Config.Gif_a;
           //     DOTTED_IMAGE = Config.Image_a;
                Sound.playLetterSound(getActivity(),R.raw.a);
                break;

            default:
                //GIF_IMAGE = Config.Gif_A;
             //   DOTTED_IMAGE = Config.Image_A;
                break;
        }
    }





    private void playMusic() {
        //mediaPlayer = MediaPlayer.create(getInstance(), R.raw.alphabet_song_sml);
        /*try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            L.e(TAG, "Exception at play music  == " + e);
        }*/
      //  mediaPlayer.start();
    }

    class MyCustomView extends View {

        private Rect m_ImageRect;
        private Rect m_TextRect;

        //you need these constructor
//you can init paint object or anything on them
      /*  public MyCustomView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            m_Context = context;

        }

        public MyCustomView(Context context, AttributeSet attrs) {
            super(context, attrs);
            m_Context = context;

        }*/

        public MyCustomView(Context context) {
            super(context);
            m_Context = context;

        }

        Bitmap mBitmap;
        Canvas mCanvas;

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
        }

        //then override on draw method
        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            //here frist create two rectangle
            //one for your image and two for text you want draw on it
            m_ImageRect = canvas.getClipBounds();
            m_TextRect = canvas.getClipBounds();
            //it gives you an area that can draw on it,
            //the width and height of your rect depend on your screen size device
            canvas.drawBitmap(bitmap, null, m_ImageRect, paint);
            canvas.save();
            canvas.clipRect(m_TextRect);

           // canvas.drawText("your text", 200, 300, paint);

            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

            canvas.drawPath(path, paint);



          //  canvas.drawText("A", 10, 20, paint);

            canvas.restore();
        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        private void touch_start(float x, float y) {
            path.reset();
            path.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_up() {

            path.lineTo(mX, mY);

            // commit the path to our offscreen
            mCanvas.drawPath(path, paint);
            // kill this so we don't double draw
            path.reset();
        }

        private void touch_move(float x, float y) {

            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);

            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                path.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            int bmH = bitmap.getHeight();
            int bmW = bitmap.getWidth();

            /*if (y <= bmH){

            }else return false;
            if (x <=bmW){

            }else return false;

            int pixelColor = bitmap.getPixel((int)x,(int)y);

            if (pixelColor == 0){
                return false;
            }*/

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:

                    touch_start(x, y);
                    invalidate();

                    break;
                case MotionEvent.ACTION_MOVE:

                    touch_move(x, y);
                    invalidate();

                    break;
                case MotionEvent.ACTION_UP:

                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }
    }
}
