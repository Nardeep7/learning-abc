package com.sachtech.avtar.learningabcfreee.Services;

import android.app.ActivityManager;
import android.app.IntentService;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import com.sachtech.avtar.learningabcfreee.Prefrences.AppPref;
import com.sachtech.avtar.learningabc.R;

import java.util.List;

public class BackgroundSoundService extends IntentService {
    private static final String TAG = null;
    public MediaPlayer player;
    private final IBinder myBinder = new MyLocalBinder();

    public static BackgroundSoundService backgroundSoundService = null;
    private Handler handler = null;
    private Runnable runnable = null;

    public BackgroundSoundService() {
        super("BackgroundSoundService");
    }

    public void init(BackgroundSoundService service) {
        backgroundSoundService = service;

    }

    public static BackgroundSoundService getInstance() {
        return backgroundSoundService;
    }

    public IBinder onBind(Intent arg0) {

        return myBinder;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                buildNotification();
                handler.postDelayed(runnable, 1500);
            }
        };

        player = MediaPlayer.create(this, R.raw.back_music);
        player.setLooping(true); // Set looping
        player.setVolume(100, 100);

    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        //player.reset()

       // handler.postDelayed(runnable,1000);

      /*  player = MediaPlayer.create(this, R.raw.back_music);
        player.setLooping(true); // Set looping
        player.setVolume(100, 100);
        ;*/

        player.start();
        if(AppPref.isSoundMute(getApplicationContext())){
            muteMusic();
        }else {
            unMuteMusic();
        }

        return 1;
    }

    public void muteMusic() {
        player.setVolume(0, 0);
    }

    public void unMuteMusic() {
        player.setVolume(1, 1);
    }

    public void onStart(Intent intent, int startId) {
        // TO DO
    }

    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

    public void onStop() {

    }

    public void onPause() {

    }

    @Override
    public void onDestroy() {
        player.stop();
        player.release();
        backgroundSoundService = null;
    }

    public void stopMusic() {
        player.stop();


    }

 /*   public void startMusic() {


        player.start();
    }*/

    @Override
    public void onLowMemory() {

    }

    public class MyLocalBinder extends Binder {
        public BackgroundSoundService getService() {
            return BackgroundSoundService.this;
        }
    }

    public boolean buildNotification() {

       /* ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> services = activityManager
                .getRunningTasks(Integer.MAX_VALUE);
        boolean isActivityFound = false;

        if (services.get(0).topActivity.getPackageName().toString()
                .equalsIgnoreCase(getApplicationContext().getPackageName().toString())) {
            isActivityFound = true;
        }

        if (isActivityFound) {
            return true;
        } else {
            // write your code to build a notification.
            // return the notification you built here
        }*/
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals("com.sachtech.avtar.learningabc")) {
              //  Toast.makeText(getApplicationContext(), "Abc is running", Toast.LENGTH_LONG).show();
            }else {
               // Toast.makeText(getApplicationContext(), "Abc is stop", Toast.LENGTH_LONG).show();
            }
        }
        return false;
    }
}