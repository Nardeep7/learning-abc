package com.sachtech.avtar.learningabcfreee;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageButton;

import com.sachtech.avtar.learningabcfreee.Constants.Config;
import com.sachtech.avtar.learningabcfreee.Fragment.ChooseLetterFragment;
import com.sachtech.avtar.learningabcfreee.Fragment.ChooseOptionFragment;
import com.sachtech.avtar.learningabcfreee.Fragment.DrawLetterFragment;
import com.sachtech.avtar.learningabcfreee.Fragment.DrawShapesFragment;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Services.BackgroundSoundService;
import com.sachtech.avtar.learningabcfreee.Utils.GifMovieView;

public class FragmentContainerActivity extends FragmentActivity implements View.OnClickListener {

    private static FragmentContainerActivity activity = null;
    String fragmentTag;
    private ImageButton backButton;
    FragmentManager manager;
    public static ImageButton homeButton;
    BackgroundSoundService backgroundSoundService;
    boolean isBound = false;
    public static FragmentContainerActivity getInstance(){
        return activity;
    }

    private GifMovieView gifMovieView;


    private ServiceConnection myConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            BackgroundSoundService.MyLocalBinder binder = (BackgroundSoundService.MyLocalBinder) service;
            backgroundSoundService = binder.getService();
            isBound = true;
        }

        public void onServiceDisconnected(ComponentName arg0) {
            isBound = false;
        }


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        activity = this;

        //gifMovieView = (GifMovieView) findViewById(R.id.gif_movie_view);
        backButton = (ImageButton)findViewById(R.id.back_button);
        homeButton=(ImageButton)findViewById(R.id.home_button);
        homeButton.setOnClickListener(this);

        /*// TODO: 18/12/18 replace new1 here reset back image
        gifMovieView.setMovieResource(R.drawable.ic_back);*/

        displayFrag(Config.CHOOSE_OPTION,Config.ChooseOptionFrag);

        backButton.setOnClickListener(this);
    }

    public void displayFrag(int fragCode,String tag){
        Object obj = null;

        switch (fragCode){
            case Config.CHOOSE_LETTER:
                obj = new ChooseLetterFragment();
                break;
            case Config.DRAW_LETTER:
                obj = new DrawLetterFragment();
                break;
            case Config.CHOOSE_OPTION:
                obj = new ChooseOptionFragment();
                break;

            case Config.DRAW_SHAPE:
                obj = new DrawShapesFragment();
                break;
        }

        fragTransaction((Fragment) obj, R.id.fragment_container, tag);
    }

    private void fragTransaction(Fragment fragment,int id,String tag){

   manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(id, fragment, "");
        boolean sd=checkFromBackStack(tag);
        if (!sd){

            transaction.addToBackStack(tag);
        }


        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_right);
        transaction.commit();



    }

public boolean checkFromBackStack(String tag)
{
     Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
    if (fragment != null){
        return true;
    }
    return false;

}
    public void removeFrag(String Tag){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment fragment=(manager.findFragmentByTag(Tag));

        transaction.remove(fragment);
        transaction.commitAllowingStateLoss();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount()==0)
            finish();

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.home_button:
                manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                displayFrag(Config.CHOOSE_OPTION,Config.ChooseOptionFrag);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isBound) {
            backgroundSoundService.stopMusic();
            unbindService(myConnection);
            isBound = false;
            Intent svc = new Intent(this, BackgroundSoundService.class);
            stopService(svc);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isBound) {
            Intent svc = new Intent(this, BackgroundSoundService.class);
            bindService(svc, myConnection, BIND_AUTO_CREATE);
            startService(svc);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent svc = new Intent(this, BackgroundSoundService.class);
        if (isBound) {
            //   unbindService(myConnection);
        }
        stopService(svc);

    }

    public void clearBackStack(){
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

}
