package com.sachtech.avtar.learningabcfreee;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sachtech.avtar.learningabcfreee.Constants.Config;
import com.sachtech.avtar.learningabcfreee.Constants.DataHolder;
import com.sachtech.avtar.learningabcfreee.Constants.WordsPointConfig;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.LayoutComponents.CustomButton;
import com.sachtech.avtar.learningabcfreee.Services.BackgroundSoundService;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;
import com.sachtech.avtar.learningabcfreee.Utils.LetterCompleteListner;
import com.sachtech.avtar.learningabcfreee.Utils.PaintView;
import com.sachtech.avtar.learningabcfreee.Utils.WordView2;

import tyrantgit.explosionfield.ExplosionField;

public class WordsActivity extends AppCompatActivity implements LetterCompleteListner, Animation.AnimationListener, View.OnClickListener {

    private ExplosionField mExplosionField;


    private RelativeLayout mPaintLayout;
    private ImageView preview_image_demo,preview_image1;
    private Animation mAnim;
    boolean Explode = false;
    private String TAG = this.getClass().getSimpleName();
    BackgroundSoundService backgroundSoundService;
    boolean isBound = false;
    private int GIF_IMAGE = 0;
    private int DOTTED_IMAGE = 0;
    PaintView letterView;
  private WordView2 wordView;
    Button next_button, back_button, home_button, retry_btn;
    String CURRENT_LETTER;
    Button full_version,maybe_later;
    private Button mButtonA, mButtonB, mButtonC, mButtonD, mButtonE, mButtonF, mButtonG, mButtonH, mButtonI, mButtonJ, mButtonK, mButtonL, mButtonM, mButtonN, mButtonO, mButtonP, mButtonQ, mButtonR, mButtonS, mButtonT, mButtonU, mButtonV, mButtonW, mButtonX, mButtonY, mButtonZ;
    private ServiceConnection myConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            BackgroundSoundService.MyLocalBinder binder = (BackgroundSoundService.MyLocalBinder) service;
            backgroundSoundService = binder.getService();
            isBound = true;
        }

        public void onServiceDisconnected(ComponentName arg0) {
            isBound = false;
        }


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words);

        initView();

    }

    private void initView() {
        DataHolder.Word_Count = 0;
        WordsPointConfig.scale = getResources().getDisplayMetrics().density;
        Log.e("Scale Word", "Scale" + WordsPointConfig.scale);
        mPaintLayout = (RelativeLayout) findViewById(R.id.painting_word_layout);
        preview_image_demo = (ImageView) findViewById(R.id.preview_word_img);
        next_button = (Button) findViewById(R.id.Next_word);
        back_button = (Button) findViewById(R.id.Back_word);
        home_button = (Button) findViewById(R.id.homw_word_btn);
        retry_btn = (Button) findViewById(R.id.retry_btn_2);
        mButtonA = (Button) findViewById(R.id.button_A);
        mButtonB = (Button) findViewById(R.id.button_B);


        preview_image1 = (ImageView)findViewById(R.id.preview_img1);
        full_version= (Button)findViewById(R.id.full_version);
        maybe_later = (Button)findViewById(R.id.maybe_later);

        mButtonC = (Button) findViewById(R.id.button_C);
        mButtonD = (Button) findViewById(R.id.button_D);
        mButtonE = (Button) findViewById(R.id.button_E);
        mButtonF = (Button) findViewById(R.id.button_F);
        mButtonG = (Button) findViewById(R.id.button_G);
        mButtonH = (Button) findViewById(R.id.button_H);
        mButtonI = (Button) findViewById(R.id.button_I);
        mButtonJ = (Button) findViewById(R.id.button_J);
        mButtonK = (Button) findViewById(R.id.button_K);
        mButtonL = (Button) findViewById(R.id.button_L);
        mButtonM = (Button) findViewById(R.id.button_M);
        mButtonN = (Button) findViewById(R.id.button_N);
        mButtonO = (Button) findViewById(R.id.button_O);
        mButtonP = (Button) findViewById(R.id.button_P);
        mButtonQ = (Button) findViewById(R.id.button_Q);
        mButtonR = (Button) findViewById(R.id.button_R);
        mButtonS = (Button) findViewById(R.id.button_S);
        mButtonT = (Button) findViewById(R.id.button_T);
        mButtonU = (Button) findViewById(R.id.button_U);
        mButtonV = (Button) findViewById(R.id.button_V);
        mButtonW = (Button) findViewById(R.id.button_W);
        mButtonX = (Button) findViewById(R.id.button_X);
        mButtonY = (Button) findViewById(R.id.button_Y);
        mButtonZ = (Button) findViewById(R.id.button_Z);

        mButtonA.setOnClickListener(this);
        mButtonB.setOnClickListener(this);
        mButtonC.setOnClickListener(this);
        mButtonD.setOnClickListener(this);
        mButtonE.setOnClickListener(this);
        mButtonF.setOnClickListener(this);
        mButtonG.setOnClickListener(this);
        mButtonH.setOnClickListener(this);
        mButtonI.setOnClickListener(this);
        mButtonJ.setOnClickListener(this);
        mButtonK.setOnClickListener(this);
        mButtonL.setOnClickListener(this);
        mButtonM.setOnClickListener(this);
        mButtonN.setOnClickListener(this);
        mButtonO.setOnClickListener(this);
        mButtonP.setOnClickListener(this);
        mButtonQ.setOnClickListener(this);
        mButtonR.setOnClickListener(this);
        mButtonS.setOnClickListener(this);
        mButtonT.setOnClickListener(this);
        mButtonU.setOnClickListener(this);
        mButtonV.setOnClickListener(this);
        mButtonW.setOnClickListener(this);
        mButtonX.setOnClickListener(this);
        mButtonY.setOnClickListener(this);
        mButtonZ.setOnClickListener(this);

        mPaintLayout.removeAllViewsInLayout();
        preview_image_demo.setBackgroundResource(0);

        checkComingLetter();

        if (Explode) {
            reset(mPaintLayout);
            mExplosionField.clear();

        }
        next_button.setOnClickListener(this);
        back_button.setOnClickListener(this);
        home_button.setOnClickListener(this);
        retry_btn.setOnClickListener(this);


    }


    ////////////////////////////////// reset explode layout/////////////////////////////////////////////
    private void reset(View root) {
        if (root instanceof ViewGroup) {
            ViewGroup parent = (ViewGroup) root;
            Log.e("Reset_if", "Called");
            root.setScaleX(1);
            root.setScaleY(1);
            root.setAlpha(1);
        } else {
            Log.e("Reset_if_else", "Called");

        }
    }


    private void checkComingLetter() {

        CURRENT_LETTER = DataHolder.LETTER;
        preview_image_demo.setImageResource(0);
        preview_image_demo.setVisibility(View.GONE);
        preview_image1.setImageResource(0);
        preview_image1.setVisibility(View.GONE);
        full_version.setVisibility(View.GONE);
        maybe_later.setVisibility(View.GONE);
        mPaintLayout.setVisibility(View.VISIBLE);
        switch (CURRENT_LETTER) {
            case Config.A:
                DOTTED_IMAGE = Config.Word_A;
                WordsPointConfig.getWord_Apple_ADetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Apple_A_TOTAL_STEPS, WordsPointConfig.point_Word_Apple_A_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);

                break;

            case Config.B:
                DOTTED_IMAGE = Config.Word_B;
                WordsPointConfig.getWord_Ball_BDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Ball_B_TOTAL_STEPS, WordsPointConfig.point_Word_Ball_B_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.C:
                DOTTED_IMAGE = Config.Word_C;
                WordsPointConfig.getCarCDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Car_C_TOTAL_STEPS, WordsPointConfig.point_Car_C_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.D:
                DOTTED_IMAGE = Config.Word_D;
                WordsPointConfig.getWord_DogDDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Dog_D_TOTAL_STEPS, WordsPointConfig.point_Dog_D_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.E:
                DOTTED_IMAGE = Config.Word_E;
                WordsPointConfig.getWord_Egg_EDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Egg_E_TOTAL_STEPS, WordsPointConfig.point_Egg_E_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.F:
                DOTTED_IMAGE = Config.Word_F;
                WordsPointConfig.getFish_F_Details();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Fish_F_TOTAL_STEPS, WordsPointConfig.point_Fish_F_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;
            case Config.G:
                DOTTED_IMAGE = Config.Word_G;
                WordsPointConfig.getWord_Gift_GDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Gift_G_TOTAL_STEPS, WordsPointConfig.point_Word_Gift_G_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.H:
                DOTTED_IMAGE = Config.Word_H;
                WordsPointConfig.getHut_H_Details();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Hut_H_TOTAL_STEPS, WordsPointConfig.point_Hut_H_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;
            case Config.I:

                DOTTED_IMAGE = Config.Word_I;
                WordsPointConfig.getInk_IDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Ink_I_TOTAL_STEPS, WordsPointConfig.point_Word_Ink_I_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.J:
                DOTTED_IMAGE = Config.Word_J;
                WordsPointConfig.getJug_JDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Jug_J_TOTAL_STEPS, WordsPointConfig.point_Word_Jug_J_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.K:
                DOTTED_IMAGE = Config.Word_K;
                WordsPointConfig.getWord_kite_k_Details();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_kite_k_TOTAL_STEPS, WordsPointConfig.point_Word_kite_k_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.L:
                DOTTED_IMAGE = Config.Word_L;
                WordsPointConfig.getWord_Lamp_LDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Lamp_L_TOTAL_STEPS, WordsPointConfig.point_Word_Lamp_L_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.M:
                DOTTED_IMAGE = Config.Word_M;
                WordsPointConfig.getWord_Mango_MDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Mango_M_TOTAL_STEPS, WordsPointConfig.point_Word_Mango_M_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.N:
                DOTTED_IMAGE = Config.Word_N;
                WordsPointConfig.getNDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Nest_N_TOTAL_STEPS, WordsPointConfig.point_Word_Nest_N_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.O:
                DOTTED_IMAGE = Config.Word_O;
                WordsPointConfig.getODetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.O_TOTAL_STEPS, WordsPointConfig.point_O_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.P:
                DOTTED_IMAGE = Config.Word_P;
                WordsPointConfig.getWord_Pig_P_Details();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Pig_P_TOTAL_STEPS, WordsPointConfig.point_Word_Pig_P_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.Q:
                DOTTED_IMAGE = Config.Word_Q;
                WordsPointConfig.getQueenQDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Queen_Q_TOTAL_STEPS, WordsPointConfig.point_Queen_Q_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.R:
                DOTTED_IMAGE = Config.Word_R;
                WordsPointConfig.getRabbit_RDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Rabbit_R_TOTAL_STEPS, WordsPointConfig.point_Word_Rabbit_R_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.S:
                DOTTED_IMAGE = Config.Word_S;
                WordsPointConfig.getSnakesDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Snake_S_TOTAL_STEPS, WordsPointConfig.point_Snake_S_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            case Config.T:
                DOTTED_IMAGE = Config.Word_T;
                WordsPointConfig.getWord_Train_TDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Train_T_TOTAL_STEPS, WordsPointConfig.point_Word_Train_T_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;
            case Config.U:
                DOTTED_IMAGE = Config.Word_U;
                WordsPointConfig.getWord_Umbrella_UDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Umbrella_U_TOTAL_STEPS, WordsPointConfig.point_Word_Umbrella_U_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;
            case Config.V:
                DOTTED_IMAGE = Config.Word_V;
                WordsPointConfig.getWord_Violin_VDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Violin_V_TOTAL_STEPS, WordsPointConfig.point_Word_Violin_V_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;
            case Config.W:
                DOTTED_IMAGE = Config.Word_W;
                WordsPointConfig.getWord_Whale_WDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Whale_W_TOTAL_STEPS, WordsPointConfig.point_Word_Whale_W_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;
            case Config.X:
                DOTTED_IMAGE = Config.Word_X;
                WordsPointConfig.getWord_XRay_XDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_XRay_X_TOTAL_STEPS, WordsPointConfig.point_Word_XRay_X_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;
            case Config.Y:
                DOTTED_IMAGE = Config.Word_Y;
                WordsPointConfig.getWord_Yak_YDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Yak_Y_TOTAL_STEPS, WordsPointConfig.point_Word_Yak_Y_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;
            case Config.Z:
                DOTTED_IMAGE = Config.Word_Z;
                WordsPointConfig.getWord_Zoo_ZDetails();
                wordView = new WordView2(WordsActivity.this, WordsPointConfig.Word_Zoo_Z_TOTAL_STEPS, WordsPointConfig.point_Word_Zoo_Z_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(wordView);
                break;

            default:
                DOTTED_IMAGE = Config.Image_A;
                break;
        }
    }

    @Override
    public void OnLetterComplete() {

        switch (CURRENT_LETTER) {

            case Config.A:
                switch (DataHolder.Word_Count) {

                    case 1:

                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                        WordsPointConfig.getWord_Apple_P_1Details();
                        wordView.nextLetter(WordsPointConfig.point_Word_Apple_P_1_Array, WordsPointConfig.Word_Apple_P_1_TOTAL_STEPS);
                        break;


                    case 2:

                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_p);
                        WordsPointConfig.getWord_Apple_P_2Details();
                        wordView.nextLetter(WordsPointConfig.point_Word_Apple_P_2_Array, WordsPointConfig.Word_Apple_P_2_TOTAL_STEPS);

                        break;

                    case 3:

                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_p);
                        WordsPointConfig.getWord_Apple_LDetails();
                        wordView.nextLetter(WordsPointConfig.point_Word_Apple_L_Array, WordsPointConfig.Word_Apple_L_TOTAL_STEPS);

                        break;


                    case 4:

                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);
                        WordsPointConfig.getWord_Apple_EDetails();
                        wordView.nextLetter(WordsPointConfig.point_Word_Apple_E_Array, WordsPointConfig.Word_Apple_E_TOTAL_STEPS);


                        break;


                    case 5:
                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);

                        DataHolder.LETTER = Config.B;
                        DataHolder.Word_Count = 0;

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    Sound.playLetterSound(WordsActivity.this, R.raw.a_for_apple);
                                    previewImage(R.drawable.apple_img);

                                } catch (Exception e) {
                                    Log.e(TAG, "" + e);
                                }


                            }
                        }, 1000);


                        break;
                    default:
                        break;
                }

                break;



            case Config.B:
                switch (DataHolder.Word_Count) {
                    case 1:
                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_b);
                        WordsPointConfig.getWord_Ball_aDetails();
                        wordView.nextLetter(WordsPointConfig.point_Word_Ball_a_Array, WordsPointConfig.Word_Ball_a_TOTAL_STEPS);
                        break;

                    case 2:
                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                        WordsPointConfig.getWord_Ball_LDetails();
                        wordView.nextLetter(WordsPointConfig.point_Word_Ball_L_Array, WordsPointConfig.Word_Ball_L_TOTAL_STEPS);

                        break;

                    case 3:
                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);
                        WordsPointConfig.getWord_Ball_L2Details();
                        wordView.nextLetter(WordsPointConfig.point_Word_Ball_L2_Array, WordsPointConfig.Word_Ball_L2_TOTAL_STEPS);

                        break;

                    case 4:

                        Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);

                        DataHolder.LETTER = Config.A;
                        DataHolder.Word_Count = 0;

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    Sound.playLetterSound(WordsActivity.this, R.raw.b_for_ball);
                                    previewImage(R.drawable.ball_img);
/*
                                    new Handler().postDelayed(new Runnable() {

                                        @Override
                                        public void run() {


                                         //   preview_image_demo.setBackgroundResource(0);

                                            try {
                                                //((FragmentContainerActivity)getActivity()).removeFrag(Config.DrawLetterFrag);
                                                // ((FragmentContainerActivity)getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);
                                                mPaintLayout.removeAllViewsInLayout();
                                            //    mPaintLayout.setVisibility(View.GONE);
                                                preview_image_demo.setBackgroundResource(0);

                                              //  previewImage1(R.drawable.gift_img);
                                                getFullVersionDialog();
                                                //   initView(view);


                                            } catch (Exception e) {
                                                Log.e(TAG, "" + e);
                                            }


                                        }
                                    }, 5000);*/


                                } catch (Exception e) {
                                    Log.e(TAG, "" + e);
                                }


                            }
                        }, 1000);
                }
                        break;


                    ////////////////////////////////////////////
                    //////////////////////////////////////
                    //////////////////////////////////////
                    //////////////////////////////////////


                    case Config.D:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_d);
                                WordsPointConfig.getWord_DogODetails();
                                wordView.nextLetter(WordsPointConfig.point_Dog_O_Array, WordsPointConfig.Word_Dog_O_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_o);
                                WordsPointConfig.getWord_DogGDetails();
                                wordView.nextLetter(WordsPointConfig.point_Dog_G_Array, WordsPointConfig.Word_Dog_G_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_g);

                                DataHolder.LETTER = Config.E;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.d_for_dog);
                                            previewImage(R.drawable.dog_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.E:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);
                                WordsPointConfig.getWord_Egg_G1EDetails();
                                wordView.nextLetter(WordsPointConfig.point_Egg_G_1_Array, WordsPointConfig.Word_Egg_G_1TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_g);
                                WordsPointConfig.getWord_Egg_G2Details();
                                wordView.nextLetter(WordsPointConfig.point_Egg_G_2_Array, WordsPointConfig.Word_Egg_G_2_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_g);

                                DataHolder.LETTER = Config.F;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.eforegg);
                                            previewImage(R.drawable.egg_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.G:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_g);
                                WordsPointConfig.getWord_Gift_iDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Gift_i_Array, WordsPointConfig.Word_Gift_i_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getWord_Gift_fDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Gift_f_Array, WordsPointConfig.Word_Gift_f_TOTAL_STEPS);

                                break;
                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_f);
                                WordsPointConfig.getWord_Gift_TDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Gift_T_Array, WordsPointConfig.Word_Gift_T_TOTAL_STEPS);

                                break;

                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_t);

                                DataHolder.LETTER = Config.H;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.g_for_gift);
                                            previewImage(R.drawable.gift_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.H:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_h);
                                WordsPointConfig.getHutuDetails();
                                wordView.nextLetter(WordsPointConfig.point_Hut_u_Array, WordsPointConfig.Hut_u_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getHuttDetails();
                                wordView.nextLetter(WordsPointConfig.point_Hut_t_Array, WordsPointConfig.Hut_t_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_t);

                                DataHolder.LETTER = Config.I;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.h_for_hat);
                                            previewImage(R.drawable.hat_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }

                        break;
                    case Config.C:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_c);
                                WordsPointConfig.getCaraDetails();
                                wordView.nextLetter(WordsPointConfig.point_Car_a_Array, WordsPointConfig.Car_a_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getCarrDetails();
                                wordView.nextLetter(WordsPointConfig.point_Car_r_Array, WordsPointConfig.Car_r_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_r);

                                DataHolder.LETTER = Config.D;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.c_for_car);
                                            previewImage(R.drawable.car_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }

                        break;
                    case Config.Q:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_q);
                                WordsPointConfig.getQueenuDetails();
                                wordView.nextLetter(WordsPointConfig.point_Queen_u_Array, WordsPointConfig.Queen_u_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_u);
                                WordsPointConfig.getQueeneDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Queen_e_Array, WordsPointConfig.Word_Queen_e_TOTAL_STEPS);

                                break;
                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);
                                WordsPointConfig.getQueene1Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Queen_e1_Array, WordsPointConfig.Word_Queen_e1_TOTAL_STEPS);

                                break;
                            case 4:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);
                                WordsPointConfig.getQueennDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Queen_n_Array, WordsPointConfig.Word_Queen_n_TOTAL_STEPS);

                                break;
                            case 5:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_n);

                                DataHolder.LETTER = Config.R;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.qforqueen);
                                            previewImage(R.drawable.queen_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.S:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_s);
                                WordsPointConfig.getSnakenDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Snake_n_Array, WordsPointConfig.Word_Snake_n_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_n);
                                WordsPointConfig.getSnakeaDetails();
                                wordView.nextLetter(WordsPointConfig.point_Snake_a_Array, WordsPointConfig.Snake_a_TOTAL_STEPS);

                                break;
                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getSnakekDetails();
                                wordView.nextLetter(WordsPointConfig.point_Snake_k_Array, WordsPointConfig.Snake_k_TOTAL_STEPS);

                                break;
                            case 4:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_k);
                                WordsPointConfig.getSnakeeDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Snake_e_Array, WordsPointConfig.Word_Snake_e_TOTAL_STEPS);

                                break;
                            case 5:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);

                                DataHolder.LETTER = Config.T;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.sforsnake);
                                            previewImage(R.drawable.snake_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.F:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_f);
                                WordsPointConfig.getFish_i_Details();
                                wordView.nextLetter(WordsPointConfig.point_Fish_i_Array, WordsPointConfig.Fish_i_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getFish_s_Details();
                                wordView.nextLetter(WordsPointConfig.point_Fish_s_Array, WordsPointConfig.Fish_s_TOTAL_STEPS);

                                break;

                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_s);
                                WordsPointConfig.getFish_h_Details();
                                wordView.nextLetter(WordsPointConfig.point_Fish_h_Array, WordsPointConfig.Fish_h_TOTAL_STEPS);

                                break;

                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_h);

                                DataHolder.LETTER = Config.G;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.fforfish);
                                            previewImage(R.drawable.fish_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.I:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getInk_nDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Ink_n_Array, WordsPointConfig.Word_Ink_n_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_n);
                                WordsPointConfig.getInk_kDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Ink_k_Array, WordsPointConfig.Word_Ink_k_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_k);

                                DataHolder.LETTER = Config.J;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.i_for_ink);
                                            previewImage(R.drawable.ink_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.J:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_j);
                                WordsPointConfig.getJug_u_Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Jug_u_Array, WordsPointConfig.Word_Jug_u_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_u);
                                WordsPointConfig.getJug_g_Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Jug_g_Array, WordsPointConfig.Word_Jug_g_TOTAL_STEPS);
                                break;

                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_g);

                                DataHolder.LETTER = Config.K;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.j_for_jug);
                                            previewImage(R.drawable.jug_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.K:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_k);
                                WordsPointConfig.getWord_kite_i_Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_kite_i_Array, WordsPointConfig.Word_kite_i_TOTAL_STEPS);

                                break;
                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getWord_kite_t_Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_kite_t_Array, WordsPointConfig.Word_kite_t_TOTAL_STEPS);

                                break;
                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_t);
                                WordsPointConfig.getWord_kite_e_Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_kite_e_Array, WordsPointConfig.Word_kite_e_TOTAL_STEPS);

                                break;
                            case 4:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);
                                DataHolder.LETTER = Config.L;
                                DataHolder.Word_Count = 0;
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.kforkites);
                                            previewImage(R.drawable.kite_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 2000);
                                break;
                        }
                        break;


                    case Config.L:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);
                                WordsPointConfig.getWord_Lamp_ADetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Lamp_A_Array, WordsPointConfig.Word_Lamp_A_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getWord_Lamp_MDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Lamp_M_Array, WordsPointConfig.Word_Lamp_M_TOTAL_STEPS);

                                break;
                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_m);
                                WordsPointConfig.getWord_Lamp_PDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Lamp_P_Array, WordsPointConfig.Word_Lamp_P_TOTAL_STEPS);

                                break;

                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_p);

                                DataHolder.LETTER = Config.M;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.lforamp);
                                            previewImage(R.drawable.lamp_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;

                    case Config.M:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_m);
                                WordsPointConfig.getWord_Mango_ADetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Mango_A_Array, WordsPointConfig.Word_Mango_A_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getWord_Mango_NDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Mango_N_Array, WordsPointConfig.Word_Mango_N_TOTAL_STEPS);

                                break;
                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_n);
                                WordsPointConfig.getWord_Mango_GDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Mango_G_Array, WordsPointConfig.Word_Mango_G_TOTAL_STEPS);

                                break;
                            case 4:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_g);
                                WordsPointConfig.getWord_Mango_ODetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Mango_O_Array, WordsPointConfig.Word_Mango_O_TOTAL_STEPS);

                                break;
                            case 5:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_o);

                                DataHolder.LETTER = Config.N;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.mformango);
                                            previewImage(R.drawable.mango_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    case Config.N:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_n);
                                WordsPointConfig.geteDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Nest_e_Array, WordsPointConfig.Word_Nest_e_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);
                                WordsPointConfig.getWord_Nest_sDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Nest_s_Array, WordsPointConfig.Word_Nest_s_TOTAL_STEPS);
                                break;

                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_s);
                                WordsPointConfig.gettDetails();
                                wordView.nextLetter(WordsPointConfig.point_t_Array, WordsPointConfig.t_TOTAL_STEPS);

                                break;

                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_t);

                                DataHolder.LETTER = Config.O;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.n_for_nest);
                                            previewImage(R.drawable.nest_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;

                        }
                        break;
                    case Config.O:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_o);
                                WordsPointConfig.getwDetails();
                                wordView.nextLetter(WordsPointConfig.point_w_Array, WordsPointConfig.w_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_w);
                                WordsPointConfig.getWord_Owl_LDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Owl_L_Array, WordsPointConfig.Word_Owl_L_TOTAL_STEPS);
                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);

                                DataHolder.LETTER = Config.P;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.o_for_owl);
                                            previewImage(R.drawable.owl_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;

                        }
                        break;


                    case Config.P:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                //p
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_p);
                                WordsPointConfig.getWord_Pig_i_Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Pig_i_Array, WordsPointConfig.Word_Pig_i_TOTAL_STEPS);

                                break;
                            case 2:
                                //e.
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getWord_Pig_g_Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Pig_g_Array, WordsPointConfig.Word_Pig_g_TOTAL_STEPS);

                                break;
                            case 3:
                                //k
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_g);

                                DataHolder.LETTER = Config.Q;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.p_for_pig);
                                            previewImage(R.drawable.pig_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 2000);
                                break;
                        }
                        break;


                    case Config.R:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_r);
                                WordsPointConfig.getRabbit_ADetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Rabbit_A_Array, WordsPointConfig.Word_Rabbit_A_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getRabbit_B_1Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Rabbit_B_1_Array, WordsPointConfig.Word_Rabbit_B_1_TOTAL_STEPS);
                                break;
                            case 3:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_b);
                                WordsPointConfig.getRabbit_B_2Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Rabbit_B_2_Array, WordsPointConfig.Word_Rabbit_B_2_TOTAL_STEPS);
                                break;
                            case 4:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_b);
                                WordsPointConfig.getRabbit_IDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Rabbit_I_Array, WordsPointConfig.Word_Rabbit_I_TOTAL_STEPS);
                                break;
                            case 5:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getRabbit_TDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Rabbit_T_Array, WordsPointConfig.Word_Rabbit_T_TOTAL_STEPS);
                                break;

                            case 6:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_t);

                                DataHolder.LETTER = Config.S;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.rforrabbit);
                                            previewImage(R.drawable.rabbit_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;

                        }
                        break;


                    case Config.T:
                        switch (DataHolder.Word_Count) {

                            case 1:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_t);
                                WordsPointConfig.getWord_Train_RDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Train_R_Array, WordsPointConfig.Word_Train_R_TOTAL_STEPS);
                                break;


                            case 2:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_r);
                                WordsPointConfig.getWord_Train_ADetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Train_A_Array, WordsPointConfig.Word_Train_A_TOTAL_STEPS);
                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getWord_Train_IDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Train_I_Array, WordsPointConfig.Word_Train_I_TOTAL_STEPS);
                                break;


                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getWord_Train_NDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Train_N_Array, WordsPointConfig.Word_Train_N_TOTAL_STEPS);

                                break;


                            case 5:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_n);

                                DataHolder.LETTER = Config.U;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.tfortrain);
                                            previewImage(R.drawable.train_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);


                                break;
                            default:
                                break;
                        }

                        break;

                    /////////////////////
                    //////////////////////////////
                    case Config.U:
                        switch (DataHolder.Word_Count) {

                            case 1:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_u);
                                WordsPointConfig.getWord_Umbrella_MDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Umbrella_M_Array, WordsPointConfig.Word_Umbrella_M_TOTAL_STEPS);
                                break;


                            case 2:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_m);
                                WordsPointConfig.getWord_Umbrella_BDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Umbrella_B_Array, WordsPointConfig.Word_Umbrella_B_TOTAL_STEPS);
                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_b);
                                WordsPointConfig.getWord_Umbrella_RDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Umbrella_R_Array, WordsPointConfig.Word_Umbrella_R_TOTAL_STEPS);
                                break;


                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_r);
                                WordsPointConfig.getWord_Umbrella_EDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Umbrella_E_Array, WordsPointConfig.Word_Umbrella_E_TOTAL_STEPS);

                                break;

                            case 5:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);
                                WordsPointConfig.getWord_Umbrella_L1Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Umbrella_L1_Array, WordsPointConfig.Word_Umbrella_L1_TOTAL_STEPS);

                                break;
                            case 6:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);
                                WordsPointConfig.getWord_Umbrella_L2Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Umbrella_L2_Array, WordsPointConfig.Word_Umbrella_L2_TOTAL_STEPS);

                                break;
                            case 7:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);
                                WordsPointConfig.getWord_Umbrella_ADetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Umbrella_A_Array, WordsPointConfig.Word_Umbrella_A_TOTAL_STEPS);

                                break;


                            case 8:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);

                                DataHolder.LETTER = Config.V;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.uforumbrella);
                                            previewImage(R.drawable.umbrella_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);


                                break;
                            default:
                                break;
                        }

                        break;

//////////////////////////////
                    /////////////////////////
                    case Config.V:
                        switch (DataHolder.Word_Count) {

                            case 1:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_v);
                                WordsPointConfig.getWord_Violin_iDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Violin_i_Array, WordsPointConfig.Word_Violin_i_TOTAL_STEPS);
                                break;


                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getWord_Violin_oDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Violin_o_Array, WordsPointConfig.Word_Violin_o_TOTAL_STEPS);
                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_o);
                                WordsPointConfig.getWord_Violin_lDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Violin_l_Array, WordsPointConfig.Word_Violin_l_TOTAL_STEPS);
                                break;


                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);
                                WordsPointConfig.getWord_Violin_i2Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Violin_i2_Array, WordsPointConfig.Word_Violin_i2_TOTAL_STEPS);
                                break;

                            case 5:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_i);
                                WordsPointConfig.getWord_Violin_nDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Violin_n_Array, WordsPointConfig.Word_Violin_n_TOTAL_STEPS);
                                break;


                            case 6:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_n);

                                DataHolder.LETTER = Config.W;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.violin);
                                            previewImage(R.drawable.vilion_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);


                                break;
                            default:
                                break;
                        }

                        break;
//////////////////////////////

                    case Config.W:
                        switch (DataHolder.Word_Count) {

                            case 1:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_w);
                                WordsPointConfig.getWord_Whale_hDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Whale_h_Array, WordsPointConfig.Word_Whale_h_TOTAL_STEPS);
                                break;


                            case 2:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_h);
                                WordsPointConfig.getWord_Whale_aDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Whale_a_Array, WordsPointConfig.Word_Whale_a_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getWord_Whale_lDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Whale_l_Array, WordsPointConfig.Word_Whale_l_TOTAL_STEPS);

                                break;


                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_l);
                                WordsPointConfig.getWord_Whale_eDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Whale_e_Array, WordsPointConfig.Word_Whale_e_TOTAL_STEPS);


                                break;


                            case 5:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_e);

                                DataHolder.LETTER = Config.X;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.wforwell);
                                            previewImage(R.drawable.whale_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);


                                break;
                            default:
                                break;
                        }

                        break;


////////////////////////////

                    case Config.X:
                        switch (DataHolder.Word_Count) {

                            case 1:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_x);
                                WordsPointConfig.getWord_XRay_spaceDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_XRay_space_Array, WordsPointConfig.Word_XRay_space_TOTAL_STEPS);
                                break;


                            case 2:

                                //   Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_p);
                                WordsPointConfig.getWord_XRay_RDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_XRay_R_Array, WordsPointConfig.Word_XRay_R_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_r);
                                WordsPointConfig.getWord_XRay_aDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_XRay_a_Array, WordsPointConfig.Word_XRay_a_TOTAL_STEPS);

                                break;
                            case 4:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getWord_Xray_yDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_XRay_y__Array, WordsPointConfig.Word_XRay_y_TOTAL_STEPS);

                                break;
                            case 5:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_y);

                                DataHolder.LETTER = Config.Y;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.xforxray);
                                            previewImage(R.drawable.xray_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;
                            default:
                                break;
                        }

                        break;

//////////////////////////////
                    case Config.Y:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_y);
                                WordsPointConfig.getWord_Yak_aDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Yak_a_Array, WordsPointConfig.Word_Yak_a_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_a);
                                WordsPointConfig.getWord_Yak_kDetails();
                                wordView.nextLetter(WordsPointConfig.point_Word_Yak_k_Array, WordsPointConfig.Word_Yak_k_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_k);

                                DataHolder.LETTER = Config.Z;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.y_for_yak);
                                            previewImage(R.drawable.yak_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;


                    /////////////////////////////
                    ////////////////////////////
                    case Config.Z:
                        switch (DataHolder.Word_Count) {
                            case 1:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_z);
                                WordsPointConfig.getWord_Zoo_o1Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Zoo_o1_Array, WordsPointConfig.Word_Zoo_o1_TOTAL_STEPS);
                                break;

                            case 2:
                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_o);
                                WordsPointConfig.getWord_Zoo_o2Details();
                                wordView.nextLetter(WordsPointConfig.point_Word_Zoo_o2_Array, WordsPointConfig.Word_Zoo_o2_TOTAL_STEPS);

                                break;

                            case 3:

                                Sound.playLetterSound(WordsActivity.this, R.raw.sound_caps_o);

                                DataHolder.LETTER = Config.A;
                                DataHolder.Word_Count = 0;

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            Sound.playLetterSound(WordsActivity.this, R.raw.zforzoo);
                                            previewImage(R.drawable.zoo_img);

                                        } catch (Exception e) {
                                            Log.e(TAG, "" + e);
                                        }


                                    }
                                }, 1000);

                                break;


                            default:
                                break;
                        }
                        break;

                    ///////////////////

                    default:
                        break;

                }

    }
        public void previewImage ( int image_id){


            mExplosionField = ExplosionField.attach2Window(this);
            mExplosionField.explode(mPaintLayout);
            Explode = true;
            preview_image_demo.setVisibility(View.VISIBLE);

            preview_image_demo.setBackgroundResource(image_id);
            mAnim = AnimationUtils.loadAnimation(this, R.anim.together);
            mAnim.setAnimationListener(WordsActivity.this);
            preview_image_demo.clearAnimation();
            preview_image_demo.setAnimation(mAnim);

            preview_image_demo.startAnimation(mAnim);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    try {

                        mPaintLayout.removeAllViewsInLayout();
                        preview_image_demo.setBackgroundResource(0);


                        initView();


                    } catch (Exception e) {
                        Log.e(TAG, "" + e);
                    }


                }
            }, 5000);

        }



    public void previewImage1(int image_id1) {
///starAnimation();


        mPaintLayout.setVisibility(View.GONE);
        preview_image_demo.setBackgroundResource(0);
        preview_image1.setVisibility(View.VISIBLE);
        full_version.setVisibility(View.VISIBLE);
        maybe_later.setVisibility(View.VISIBLE);
        preview_image1.setBackgroundResource(image_id1);
        mAnim = AnimationUtils.loadAnimation(WordsActivity.this, R.anim.together);
        mAnim.setAnimationListener(WordsActivity.this);
        preview_image1.clearAnimation();
        preview_image1.setAnimation(mAnim);
        preview_image1.startAnimation(mAnim);


        full_version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.sachtech.avtar.learningabc"));
                startActivity(intent);
            }
        });
        maybe_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "on click work", Toast.LENGTH_SHORT).show();
             //   FragmentContainerActivity.getInstance().displayFrag(Config.CHOOSE_LETTER, Config.ChooseLetterFrag);
                Intent intent=new Intent(WordsActivity.this,FragmentContainerActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.Next_word:
                /*mPaintLayout.removeAllViewsInLayout();
                preview_image_demo.setBackgroundResource(0);*/
                nextWord();
                break;

            case R.id.Back_word:
                /*mPaintLayout.removeAllViewsInLayout();
                preview_image_demo.setBackgroundResource(0);*/
                previousWord();
                break;

            case R.id.homw_word_btn:
                finish();
                break;

            case R.id.retry_btn_2:
             //   wordView.clearCanvas();
                initView();
                break;

            case R.id.button_A:
                DataHolder.LETTER=Config.A;
                initView();
               
                break;
            case R.id.button_B:
                DataHolder.LETTER=Config.B;
                initView();

                break;

            case R.id.button_C:
                DataHolder.LETTER=Config.C;
                initView();

                break;
            case R.id.button_D:
                DataHolder.LETTER=Config.D;
                initView();

                break;
            case R.id.button_E:
                DataHolder.LETTER=Config.E;
                initView();

                break;
            case R.id.button_F:
                DataHolder.LETTER=Config.F;
                initView();

                break;
            case R.id.button_G:
                DataHolder.LETTER=Config.G;
                initView();
                 
                break;
            case R.id.button_H:
                DataHolder.LETTER=Config.H;
                initView();
                 
                break;
            case R.id.button_I:
                DataHolder.LETTER=Config.I;
                initView();
                 
                break;
            case R.id.button_J:
                DataHolder.LETTER=Config.J;
                initView();
                 
                break;
            case R.id.button_K:
                DataHolder.LETTER=Config.K;
                initView();
                 
                break;
            case R.id.button_L:
                DataHolder.LETTER=Config.L;
                initView();
                 
                break;
            case R.id.button_M:
                DataHolder.LETTER=Config.M;
                initView();
                 
                break;
            case R.id.button_N:
                DataHolder.LETTER=Config.N;
                initView();
                 
                break;
            case R.id.button_O:
                DataHolder.LETTER=Config.O;
                initView();
                 
                break;
            case R.id.button_P:
                DataHolder.LETTER=Config.P;
                initView();
                 
                break;
            case R.id.button_Q:
                DataHolder.LETTER=Config.Q;
                initView();
                 
                break;
            case R.id.button_R:
                DataHolder.LETTER=Config.R;
                initView();
                 
                break;
            case R.id.button_S:
                DataHolder.LETTER=Config.S;
                initView();
                 
                break;
            case R.id.button_T:
                DataHolder.LETTER=Config.T;
                initView();
                 
                break;
            case R.id.button_U:
                DataHolder.LETTER=Config.U;
                initView();
                 
                break;
            case R.id.button_V:
                DataHolder.LETTER=Config.V;
                initView();
                 
                break;
            case R.id.button_W:
                DataHolder.LETTER=Config.W;
                initView();
                 
                break;
            case R.id.button_X:
                DataHolder.LETTER=Config.X;
                initView();
                 
                break;
            case R.id.button_Y:
                DataHolder.LETTER=Config.Y;
                initView();
                 
                break;
            case R.id.button_Z:
                DataHolder.LETTER=Config.Z;
                initView();
                 
                break;
            default:
                break;
        }
    }


    public void nextWord() {
        CURRENT_LETTER = DataHolder.LETTER;

        switch (DataHolder.LETTER) {
            case Config.A:
                DataHolder.LETTER = Config.B;
                initView();
                break;

            case Config.B:

              //  mPaintLayout.setVisibility(View.GONE);
                getFullVersionDialog();
                DataHolder.LETTER = Config.A;
                initView();
                break;



            case Config.C:
                DataHolder.LETTER = Config.D;
                initView();
                break;

            case Config.D:
                DataHolder.LETTER = Config.E;
                initView();
                break;

            case Config.E:
                DataHolder.LETTER = Config.F;
                initView();
                break;

            case Config.F:
                DataHolder.LETTER = Config.G;
                initView();
                break;

            case Config.G:
                DataHolder.LETTER = Config.H;
                initView();
                break;

            case Config.H:
                DataHolder.LETTER = Config.I;
                initView();
                break;

            case Config.I:
                DataHolder.LETTER = Config.J;
                initView();
                break;

            case Config.J:
                DataHolder.LETTER = Config.K;
                initView();
                break;

            case Config.K:
                DataHolder.LETTER = Config.L;
                initView();
                break;

            case Config.L:
                DataHolder.LETTER = Config.M;
                initView();
                break;

            case Config.M:
                DataHolder.LETTER = Config.N;
                initView();
                break;

            case Config.N:
                DataHolder.LETTER = Config.O;
                initView();
                break;

            case Config.O:
                DataHolder.LETTER = Config.P;
                initView();
                break;

            case Config.P:
                DataHolder.LETTER = Config.Q;
                initView();
                break;

            case Config.Q:
                DataHolder.LETTER = Config.R;
                initView();
                break;

            case Config.R:
                DataHolder.LETTER = Config.S;
                initView();
                break;

            case Config.S:
                DataHolder.LETTER = Config.T;
                initView();
                break;

            case Config.T:
                DataHolder.LETTER = Config.U;
                initView();
                break;

            case Config.U:
                DataHolder.LETTER = Config.V;
                initView();
                break;


            case Config.V:
                DataHolder.LETTER = Config.W;
                initView();
                break;

            case Config.W:
                DataHolder.LETTER = Config.X;
                initView();
                break;

            case Config.X:
                DataHolder.LETTER = Config.Y;
                initView();
                break;

            case Config.Y:
                DataHolder.LETTER = Config.Z;
                initView();
                break;

            case Config.Z:
                DataHolder.LETTER = Config.A;
                initView();
                break;



            default:
                break;
        }
    }

    public void previousWord() {
        CURRENT_LETTER = DataHolder.LETTER;

        switch (DataHolder.LETTER) {
            case Config.A:
                DataHolder.LETTER = Config.B;
                initView();

                break;

            case Config.B:
                DataHolder.LETTER = Config.A;
                initView();
                break;


            case Config.C:
                DataHolder.LETTER = Config.B;
                initView();
                break;

            case Config.D:
                DataHolder.LETTER = Config.C;
                initView();
                break;

            case Config.E:
                DataHolder.LETTER = Config.D;
                initView();
                break;

            case Config.F:
                DataHolder.LETTER = Config.E;
                initView();
                break;

            case Config.G:
                DataHolder.LETTER = Config.F;
                initView();
                break;

            case Config.H:
                DataHolder.LETTER = Config.G;
                initView();
                break;

            case Config.I:
                DataHolder.LETTER = Config.H;
                initView();
                break;

            case Config.J:
                DataHolder.LETTER = Config.I;
                initView();
                break;

            case Config.K:
                DataHolder.LETTER = Config.J;
                initView();
                break;

            case Config.L:
                DataHolder.LETTER = Config.K;
                initView();
                break;

            case Config.M:
                DataHolder.LETTER = Config.L;
                initView();
                break;

            case Config.N:
                DataHolder.LETTER = Config.M;
                initView();
                break;

            case Config.O:
                DataHolder.LETTER = Config.N;
                initView();
                break;

            case Config.P:
                DataHolder.LETTER = Config.O;
                initView();
                break;

            case Config.Q:
                DataHolder.LETTER = Config.P;
                initView();
                break;

            case Config.R:
                DataHolder.LETTER = Config.Q;
                initView();
                break;

            case Config.S:
                DataHolder.LETTER = Config.R;
                initView();
                break;

            case Config.T:
                DataHolder.LETTER = Config.S;
                initView();
                break;

            case Config.U:
                DataHolder.LETTER = Config.T;
                initView();
                break;
            case Config.V:
                DataHolder.LETTER = Config.U;
                initView();
                break;

            case Config.W:
                DataHolder.LETTER = Config.V;
                initView();
                break;

            case Config.X:
                DataHolder.LETTER = Config.W;
                initView();
                break;

            case Config.Y:
                DataHolder.LETTER = Config.X;
                initView();
                break;

            case Config.Z:
                DataHolder.LETTER = Config.Y;
                initView();
                break;

            default:
                break;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (isBound) {
            backgroundSoundService.stopMusic();
            unbindService(myConnection);
            isBound = false;
            Intent svc = new Intent(this, BackgroundSoundService.class);
            stopService(svc);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isBound) {
            Intent svc = new Intent(this, BackgroundSoundService.class);
            bindService(svc, myConnection, BIND_AUTO_CREATE);
            startService(svc);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent svc = new Intent(this, BackgroundSoundService.class);
        if (isBound) {
            //   unbindService(myConnection);
        }
        stopService(svc);

    }
    private void getFullVersionDialog(){
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_2,null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);

        final AlertDialog dialog = builder.create();
        dialog.show();

        CustomButton fullVersionBtn = (CustomButton)dialog.findViewById(R.id.full_version);
        CustomButton mayBeLatterBtn = (CustomButton)dialog.findViewById(R.id.maybe_later);
        ImageView image = (ImageView)dialog.findViewById(R.id.preview_img1);

        fullVersionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.sachtech.avtar.learningabc"));
                startActivity(intent);

            }
        });
        mayBeLatterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "on click work", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                finish();

            }
        });
       dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }
}
