package com.sachtech.avtar.learningabcfreee;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Animation.ObjectAnim;
import com.sachtech.avtar.learningabcfreee.Constants.LetterPointsConfig;
import com.sachtech.avtar.learningabcfreee.Constants.WordsPointConfig;
import com.sachtech.avtar.learningabcfreee.Prefrences.AppPref;
import com.sachtech.avtar.learningabcfreee.Services.BackgroundSoundService;
import com.sachtech.avtar.learningabcfreee.Utils.GifMovieView;


public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    private Handler handler = null;
    private Handler handlerTimer = null;
    private Runnable runnable = null;
    private Runnable runnableTimer = null;
    Button mMuteButton;
    ImageView playButton;
    int gifDuration;
    int timer = 1000;
    boolean Ismute = false;
    BackgroundSoundService backgroundSoundService;
    boolean isBound = false;

    private ServiceConnection myConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            BackgroundSoundService.MyLocalBinder binder = (BackgroundSoundService.MyLocalBinder) service;
            backgroundSoundService = binder.getService();
            isBound = true;
        }

        public void onServiceDisconnected(ComponentName arg0) {
            isBound = false;
        }


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // new LetterPointsConfig(this);
        LetterPointsConfig.scale = getResources().getDisplayMetrics().density;
        WordsPointConfig.scale = getResources().getDisplayMetrics().density;
        // Toast.makeText(SplashActivity.this, "Scale "+LetterPointsConfig.scale, Toast.LENGTH_SHORT).show();
        Log.e(">>>>>>", "Scale = " + LetterPointsConfig.scale);
        playButton = findViewById(R.id.play_button);
        mMuteButton = (Button) findViewById(R.id.mute_button);

        mMuteButton.setOnClickListener(this);
        playButton.setOnClickListener(this);
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(display.getWidth(), display.getHeight());

        //gifDuration = gifMovieView.getMovie().duration();
        Log.e(">>>>>>", "Duration = " + gifDuration);


        Intent svc = new Intent(this, BackgroundSoundService.class);
        bindService(svc, myConnection, BIND_AUTO_CREATE);
        startService(svc);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                LinearLayout layout=findViewById(R.id.layoutText);
                startAnimation(layout,5000);

            }
        });

        handler = new Handler();

        runnable = new Runnable() {
            @Override
            public void run() {
                ObjectAnim.startAnimation(playButton, 2000, SplashActivity.this, R.animator.button_bounce);
                handler.postDelayed(runnable, 3000);
            }
        };

        handler.postDelayed(runnable, 3000);

        ObjectAnim.startAnimation(playButton, 2000, SplashActivity.this, R.animator.button_bounce);

      /*  handlerTimer = new Handler();
        runnableTimer = new Runnable() {
            @Override
            public void run() {

                if (timer !=0)
                timer = timer+1000;


                if (timer !=0) {
                    if (timer > gifDuration) {
                        gifMovieView.setMovieResource(R.drawable.splash_part_2);
                        handlerTimer.removeCallbacks(runnableTimer);
                        timer=0;
                    }
                    if (timer!=0)
                    handlerTimer.postDelayed(runnableTimer,1000);
                }



            }
        };

        handlerTimer.postDelayed(runnableTimer,1000);
*/
    }

    private synchronized void startAnimation(View view, int time) {
     ObjectAnimator anim;
        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.bounce_in_down);
        anim.setTarget(view);
        anim.setDuration(time);
        anim.start();
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.play_button:
                startActivity(new Intent(SplashActivity.this, FragmentContainerActivity.class));
                break;


            case R.id.mute_button:
                if (!Ismute) {
                    backgroundSoundService.muteMusic();
                    mMuteButton.setBackgroundResource(R.drawable.music_off);
                    AppPref.soundMute(getApplicationContext());
                    Ismute = true;
                } else if (Ismute) {
                    backgroundSoundService.unMuteMusic();
                    mMuteButton.setBackgroundResource(R.drawable.music_on);
                    AppPref.soundUnMute(getApplicationContext());
                    Ismute = false;
                }
                break;


            default:
                break;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        handler.removeCallbacks(runnable);
        //handlerTimer.removeCallbacks(runnableTimer);
        //finish();

        //L.e(TAG, "onPause call here");
        if (isBound) {
            backgroundSoundService.stopMusic();
            unbindService(myConnection);
            isBound = false;
            Intent svc = new Intent(this, BackgroundSoundService.class);
            stopService(svc);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, 3000);

        // L.e(TAG, "onResume call here");
        if (!isBound) {
            Intent svc = new Intent(this, BackgroundSoundService.class);
            bindService(svc, myConnection, BIND_AUTO_CREATE);
            startService(svc);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // L.e(TAG, "onDestroy call here");
        Intent svc = new Intent(this, BackgroundSoundService.class);
        stopService(svc);
        //unbindService(myConnection);
    }


}
