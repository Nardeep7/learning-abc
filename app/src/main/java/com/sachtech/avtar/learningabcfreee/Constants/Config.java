package com.sachtech.avtar.learningabcfreee.Constants;

/**
 * Created by avtar on 3/5/2016.
 */
public interface Config {
    String A = "A";
    String a = "a";
    String B = "B";
    String b = "b";
    String C = "C";
    String c = "c";
    String D = "D";
    String d = "d";
    String E = "E";
    String e = "e";
    String F = "F";
    String f = "f";
    String G = "G";
    String g = "g";
    String H = "H";
    String h = "h";
    String I = "I";
    String i = "i";
    String J = "J";
    String j = "j";
    String K = "K";
    String k = "k";
    String L = "L";
    String l = "l";
    String M = "M";
    String m = "m";
    String N = "N";
    String n = "n";
    String O = "O";
    String o = "o";
    String P = "P";
    String p = "p";
    String Q = "Q";
    String q = "q";
    String R = "R";
    String r = "r";
    String S = "S";
    String s = "s";
    String T = "T";
    String t = "t";
    String U = "U";
    String u = "u";
    String V = "V";
    String v = "v";
    String W = "W";
    String w = "w";
    String X = "X";
    String x = "x";
    String Y = "Y";
    String y = "y";
    String Z = "Z";
    String z = "z";
    String AG = "AG";

    String Letter = "letter";

    int Image_a = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_a_image;
    int Image_A = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_a;

    int Image_B = com.sachtech.avtar.learningabc.R.drawable.alphabet_capsb;
    int Image_b = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_b;

    int Image_C = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_c;
    int Image_c = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_c;

    int Image_D = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_d;
    int Image_d = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_d;

    int Image_E = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_e;
    int Image_e = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_e;

    int Image_F = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_f;
    int Image_f = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_f;

    int Image_G = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_g;
    int Image_g = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_g;

    int Image_H = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_h;
    int Image_h = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_h;

    int Image_I = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_i;
    int Image_i = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_i;

    int Image_J = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_j;
    int Image_j = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_j;

    int Image_K = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_k;
    int Image_k = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_k;

    int Image_L = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_l;
    int Image_l = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_l;

    int Image_M = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_m;
    int Image_m = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_m;

    int Image_N = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_n;
    int Image_n = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_n;

    int Image_O = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_o;
    int Image_o = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_o;

    int Image_P = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_p;
    int Image_p = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_p;

    int Image_Q = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_q;
    int Image_q = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_q;

    int Image_R = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_r;
    int Image_r = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_r;

    int Image_S = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_s;
    int Image_s = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_s;

    int Image_T = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_t;
    int Image_t = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_t;

    int Image_U = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_u;
    int Image_u = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_u;

    int Image_V = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_v;
    int Image_v = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_v;

    int Image_W = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_w;
    int Image_w = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_w;

    int Image_X = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_x;
    int Image_x = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_x;

    int Image_Y = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_y;
    int Image_y = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_y;

    int Image_Z = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_z;
    int Image_z = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_z;


    int Gif_A = com.sachtech.avtar.learningabc.R.drawable.alphabet_caps_a;
    int Gif_a = com.sachtech.avtar.learningabc.R.drawable.alphabet_small_a_image;


    int Word_A = com.sachtech.avtar.learningabc.R.drawable.appple;
    int Word_B = com.sachtech.avtar.learningabc.R.drawable.ball;

    int Word_C = com.sachtech.avtar.learningabc.R.drawable.car;
    int Word_D= com.sachtech.avtar.learningabc.R.drawable.dog;

    int Word_E = com.sachtech.avtar.learningabc.R.drawable.egg;
    int Word_F= com.sachtech.avtar.learningabc.R.drawable.fish;

    int Word_G= com.sachtech.avtar.learningabc.R.drawable.gift;
    int Word_H = com.sachtech.avtar.learningabc.R.drawable.hat;

    int Word_I = com.sachtech.avtar.learningabc.R.drawable.ink;
    int Word_J = com.sachtech.avtar.learningabc.R.drawable.jug;

    int Word_K = com.sachtech.avtar.learningabc.R.drawable.kite;
    int Word_L = com.sachtech.avtar.learningabc.R.drawable.lamp;

    int Word_M = com.sachtech.avtar.learningabc.R.drawable.mango;
    int Word_N = com.sachtech.avtar.learningabc.R.drawable.nest;

    int Word_O = com.sachtech.avtar.learningabc.R.drawable.owl;
    int Word_P = com.sachtech.avtar.learningabc.R.drawable.pig;

    int Word_Q = com.sachtech.avtar.learningabc.R.drawable.queen;
    int Word_R= com.sachtech.avtar.learningabc.R.drawable.rabbit;

    int Word_S = com.sachtech.avtar.learningabc.R.drawable.snake;
    int Word_T = com.sachtech.avtar.learningabc.R.drawable.train;

    int Word_U = com.sachtech.avtar.learningabc.R.drawable.umbrella;
    int Word_V = com.sachtech.avtar.learningabc.R.drawable.violin;

    int Word_W = com.sachtech.avtar.learningabc.R.drawable.whale;
    int Word_X = com.sachtech.avtar.learningabc.R.drawable.x_ray;

    int Word_Y = com.sachtech.avtar.learningabc.R.drawable.yak;
    int Word_Z = com.sachtech.avtar.learningabc.R.drawable.zoo;

    int APPLE = com.sachtech.avtar.learningabc.R.drawable.apple_img;
    int BUS = com.sachtech.avtar.learningabc.R.drawable.ball_img;
    int CAR = com.sachtech.avtar.learningabc.R.drawable.car_img;
    int DOG = com.sachtech.avtar.learningabc.R.drawable.dog_img;
    int EGG = com.sachtech.avtar.learningabc.R.drawable.egg_img;
    int FISH = com.sachtech.avtar.learningabc.R.drawable.fish_img;
    int GOAT = com.sachtech.avtar.learningabc.R.drawable.gift_img;
    int HUT = com.sachtech.avtar.learningabc.R.drawable.hat_img;
    int IGLOO = com.sachtech.avtar.learningabc.R.drawable.ink_img;
    int JEEP = com.sachtech.avtar.learningabc.R.drawable.jug_img;
    int KITE = com.sachtech.avtar.learningabc.R.drawable.kite_img;
    int LAMP = com.sachtech.avtar.learningabc.R.drawable.lamp_img;
    int MANGO = com.sachtech.avtar.learningabc.R.drawable.mango_img;
    int NET = com.sachtech.avtar.learningabc.R.drawable.nest_img;
    int OX = com.sachtech.avtar.learningabc.R.drawable.owl_img;
    int PEACOCK = com.sachtech.avtar.learningabc.R.drawable.pig_img;
    int QUEEN = com.sachtech.avtar.learningabc.R.drawable.queen_img;
    int RABBIT = com.sachtech.avtar.learningabc.R.drawable.rabbit_img;
    int SNAKE = com.sachtech.avtar.learningabc.R.drawable.snake_img;
    int TRAIN = com.sachtech.avtar.learningabc.R.drawable.train_img;
    int UMBRELLA = com.sachtech.avtar.learningabc.R.drawable.umbrella_img;
    int VIOLIN = com.sachtech.avtar.learningabc.R.drawable.vilion_img;
    int WHALE = com.sachtech.avtar.learningabc.R.drawable.whale_img;
    int X_RAY = com.sachtech.avtar.learningabc.R.drawable.xray_img;
    int YOGA = com.sachtech.avtar.learningabc.R.drawable.yak_img;
    int ZOO = com.sachtech.avtar.learningabc.R.drawable.zoo_img;


    int Word_A_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_a;
    int Word_B_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_b;
    int Word_C_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_c;
    int Word_D_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_d;
    int Word_E_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_e;
    int Word_F_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_f;
    int Word_G_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_g;
    int Word_H_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_h;
    int Word_I_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_i;
    int Word_J_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_j;
    int Word_K_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_k;
    int Word_L_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_l;
    int Word_M_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_m;
    int Word_N_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_n;
    int Word_O_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_o;
    int Word_P_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_p;
    int Word_Q_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_q;
    int Word_R_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_r;
    int Word_S_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_s;
    int Word_T_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_t;
    int Word_U_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_u;
    int Word_V_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_v;
    int Word_W_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_w;
    int Word_X_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_x;
    int Word_Y_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_y;
    int Word_Z_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_z;

    int Word_a_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_a;
    int Word_b_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_b;
    int Word_c_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_c;
    int Word_d_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_d;
    int Word_e_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_e;
    int Word_f_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_f;
    int Word_g_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_g;
    int Word_h_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_h;
    int Word_i_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_i;
    int Word_j_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_j;
    int Word_k_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_k;
    int Word_l_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_l;
    int Word_m_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_m;
    int Word_n_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_n;
    int Word_o_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_o;
    int Word_p_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_p;
    int Word_q_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_q;
    int Word_r_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_r;
    int Word_s_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_s;
    int Word_t_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_t;
    int Word_u_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_u;
    int Word_v_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_v;
    int Word_w_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_w;
    int Word_x_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_x;
    int Word_y_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_y;
    int Word_z_outline = com.sachtech.avtar.learningabc.R.drawable.alphabet_outline_small_z;



    int CHOOSE_LETTER = 1;
    String ChooseLetterFrag = "CHOOSE_LETTER";
    int DRAW_LETTER = 2;
    String DrawLetterFrag = "DRAW_LETTER";
    int CHOOSE_OPTION = 3;
    String ChooseOptionFrag = "CHOOSE_OPTION";
    int DRAW_SHAPE =4;
    String DrawShapreFrag = "DRAW_SHAPE";



    int CAPS =1;
    int SMALL=2;









}
