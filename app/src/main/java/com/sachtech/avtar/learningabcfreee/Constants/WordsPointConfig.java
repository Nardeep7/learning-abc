package com.sachtech.avtar.learningabcfreee.Constants;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nardeep Sandhu on 3/18/2016.
 */
public class WordsPointConfig {
    public static float scale = 0;
    ///////////////////////////////////////////////////// Words Start /////////////////////////////////////
///////////////////////////////////////////////////////Word APPLE /////////////////////////////////////


    public static int Word_Apple_A_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Apple_A_Array = new List[Word_Apple_A_TOTAL_STEPS];

    public static void getWord_Apple_ADetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();


        int x1 = (int) (scale * 110);
        int y1 = (int) (scale * 55);

        int x2 = (int) (scale * 80);
        int y2 = (int) (scale * 165);

        int x3 = (int) (scale * 145);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 95);
        int y4 = (int) (scale * 140);

        int x5 = (int) (scale * 130);
        int y5 = (int) (scale * 140);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);


        pt_0.add(p1);
        pt_0.add(p2);
        pt_1.add(p1);
        pt_1.add(p3);
        pt_2.add(p4);
        pt_2.add(p5);

        point_Word_Apple_A_Array[0] = pt_0;
        point_Word_Apple_A_Array[1] = pt_1;
        point_Word_Apple_A_Array[2] = pt_2;


    }

    //////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Apple_P_1_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Apple_P_1_Array = new List[Word_Apple_P_1_TOTAL_STEPS];

    public static void getWord_Apple_P_1Details() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();


        int x1 = (int) (scale * 173);
        int y1 = (int) (scale * 90);

        int x2 = (int) (scale * 173);
        int y2 = (int) (scale * 202);

        int x3 = (int) (scale * 187);
        int y3 = (int) (scale * 95);

        int x4 = (int) (scale * 207);
        int y4 = (int) (scale * 90);

        int x5 = (int) (scale * 220);
        int y5 = (int) (scale * 100);

        int x6 = (int) (scale * 225);
        int y6 = (int) (scale * 140);

        int x7 = (int) (scale * 211);
        int y7 = (int) (scale * 165);

        int x8 = (int) (scale * 185);
        int y8 = (int) (scale * 163);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        pt_2.add(p4);
        pt_2.add(p5);

        pt_3.add(p5);
        pt_3.add(p6);

        pt_4.add(p6);
        pt_4.add(p7);

        pt_5.add(p7);
        pt_5.add(p8);


        point_Word_Apple_P_1_Array[0] = pt_0;
        point_Word_Apple_P_1_Array[1] = pt_1;
        point_Word_Apple_P_1_Array[2] = pt_2;
        point_Word_Apple_P_1_Array[3] = pt_3;
        point_Word_Apple_P_1_Array[4] = pt_4;
        point_Word_Apple_P_1_Array[5] = pt_5;


    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Apple_P_2_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Apple_P_2_Array = new List[Word_Apple_P_2_TOTAL_STEPS];

    public static void getWord_Apple_P_2Details() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();


        int x1 = (int) (scale * 257);
        int y1 = (int) (scale * 90);

        int x2 = (int) (scale * 257);
        int y2 = (int) (scale * 202);

        int x3 = (int) (scale * 265);
        int y3 = (int) (scale * 95);

        int x4 = (int) (scale * 285);
        int y4 = (int) (scale * 92);

        int x5 = (int) (scale * 305);
        int y5 = (int) (scale * 100);

        int x6 = (int) (scale * 307);
        int y6 = (int) (scale * 140);

        int x7 = (int) (scale * 295);
        int y7 = (int) (scale * 170);

        int x8 = (int) (scale * 265);
        int y8 = (int) (scale * 165);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        pt_2.add(p4);
        pt_2.add(p5);

        pt_3.add(p5);
        pt_3.add(p6);

        pt_4.add(p6);
        pt_4.add(p7);

        pt_5.add(p7);
        pt_5.add(p8);

        point_Word_Apple_P_2_Array[0] = pt_0;
        point_Word_Apple_P_2_Array[1] = pt_1;
        point_Word_Apple_P_2_Array[2] = pt_2;
        point_Word_Apple_P_2_Array[3] = pt_3;
        point_Word_Apple_P_2_Array[4] = pt_4;
        point_Word_Apple_P_2_Array[5] = pt_5;


    }

////////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Apple_L_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Apple_L_Array = new List[Word_Apple_L_TOTAL_STEPS];

    public static void getWord_Apple_LDetails() {


        List<Point> pt_0 = new ArrayList<Point>();


        int x1 = (int) (scale * 345);
        int y1 = (int) (scale * 50);

        int x2 = (int) (scale * 345);
        int y2 = (int) (scale * 160);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);


        pt_0.add(p1);
        pt_0.add(p2);


        point_Word_Apple_L_Array[0] = pt_0;


    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Apple_E_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Apple_E_Array = new List[Word_Apple_E_TOTAL_STEPS];

    public static void getWord_Apple_EDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();


        int x1 = (int) (scale * 387);
        int y1 = (int) (scale * 125);

        int x2 = (int) (scale * 423);
        int y2 = (int) (scale * 125);

        int x3 = (int) (scale * 423);
        int y3 = (int) (scale * 103);

        int x4 = (int) (scale * 395);
        int y4 = (int) (scale * 90);

        int x5 = (int) (scale * 378);
        int y5 = (int) (scale * 125);

        int x6 = (int) (scale * 385);
        int y6 = (int) (scale * 160);

        int x7 = (int) (scale * 400);
        int y7 = (int) (scale * 170);

        int x8 = (int) (scale * 420);
        int y8 = (int) (scale * 165);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);


        pt_4.add(p5);
        pt_4.add(p6);

        pt_5.add(p6);
        pt_5.add(p7);

        pt_6.add(p7);
        pt_6.add(p8);


        point_Word_Apple_E_Array[0] = pt_0;
        point_Word_Apple_E_Array[1] = pt_1;
        point_Word_Apple_E_Array[2] = pt_2;
        point_Word_Apple_E_Array[3] = pt_3;
        point_Word_Apple_E_Array[4] = pt_4;
        point_Word_Apple_E_Array[5] = pt_5;
        point_Word_Apple_E_Array[6] = pt_6;


    }

///////////////////////////////////////////////////////////////////////
  /*////////////////////////////////Word Bus//////////////////////////////////

    public static int Word_Bus_B_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Bus_B_Array = new List[Word_Bus_B_TOTAL_STEPS];

    public static void getWord_Bus_BDetails(){



        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();



        int x1 = (int) (scale * 136);
        int y1 = (int) (scale * 70);

        int x2 = (int) (scale * 136);
        int y2 = (int) (scale * 180);

        int x3 = (int) (scale * 145);
        int y3 = (int) (scale * 70);

        int x4 = (int) (scale * 180);
        int y4 = (int) (scale * 70);

        int x5 = (int) (scale * 190);
        int y5 = (int) (scale * 100);

        int x10 = (int) (scale * 170);
        int y10 = (int) (scale * 125);

        int x6 = (int) (scale * 145);
        int y6 = (int) (scale * 125);

        int x7 = (int) (scale * 185);
        int y7 = (int) (scale * 128);

        int x8 = (int) (scale * 198);
        int y8 = (int) (scale * 155);

        int x11= (int) (scale * 180);
        int y11 = (int) (scale * 180);

        int x9 = (int) (scale * 147);
        int y9 = (int) (scale * 180);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
        Point p10 = new Point(x10, y10);
        Point p11 = new Point(x11, y11);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        pt_2.add(p4);
        pt_2.add(p5);

        pt_3.add(p5);
        pt_3.add(p10);

        pt_4.add(p10);
        pt_4.add(p6);

        pt_5.add(p7);
        pt_5.add(p8);

        pt_6.add(p8);
        pt_6.add(p11);

        pt_7.add(p11);
        pt_7.add(p9);
        point_Word_Bus_B_Array[0] = pt_0;
        point_Word_Bus_B_Array[1] = pt_1;
        point_Word_Bus_B_Array[2] = pt_2;
        point_Word_Bus_B_Array[3] = pt_3;
        point_Word_Bus_B_Array[4] = pt_4;
        point_Word_Bus_B_Array[5] = pt_5;
        point_Word_Bus_B_Array[6] = pt_6;
        point_Word_Bus_B_Array[7] = pt_7;




    }
//////////////////////////////////////////////

    public static int Word_Bus_u_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Bus_u_Array = new List[Word_Bus_u_TOTAL_STEPS];

    public static void getWord_Bus_uDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();


        int x1 = (int) (scale * 235);
        int y1 = (int) (scale * 100);

        int x2 = (int) (scale * 235);
        int y2 = (int) (scale * 170);

        int x3 = (int) (scale * 250);
        int y3 = (int) (scale * 185);

        int x6 = (int) (scale * 274);
        int y6 = (int) (scale * 170);

        int x4 = (int) (scale * 283);
        int y4 = (int) (scale * 100);

        int x5 = (int) (scale * 283);
        int y5 = (int) (scale * 185);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p6);

        pt_3.add(p5);
        pt_3.add(p4);


        point_Word_Bus_u_Array[0] = pt_0;
        point_Word_Bus_u_Array[1] = pt_1;
        point_Word_Bus_u_Array[2] = pt_2;
        point_Word_Bus_u_Array[3] = pt_3;
      *//*  point_Word_Bus_B_Array[4] = pt_4;
        point_Word_Bus_B_Array[5] = pt_5;
*//*

    }

    /////////////////////////////////////////////////////////////////
    public static int Word_Bus_s_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Bus_s_Array = new List[Word_Bus_s_TOTAL_STEPS];

    public static void getWord_Bus_sDetails(){


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        int x1 = (int) (scale * 360);
        int y1 = (int) (scale * 115);

        int x2 = (int) (scale * 340);
        int y2= (int) (scale * 100);

        int x3 = (int) (scale * 320);
        int y3= (int) (scale * 110);

        int x9= (int) (scale * 325);
        int y9= (int) (scale *140);

        int x4 = (int) (scale * 350);
        int y4= (int) (scale *145);

        int x5 = (int) (scale * 360);
        int y5= (int) (scale *170);

        int x7 = (int) (scale * 340);
        int y7= (int) (scale *185);

        int x8 = (int) (scale * 320);
        int y8= (int) (scale *170);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p5 = new Point(x5, y5);
        Point p9 = new Point(x9, y9);



        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p9);

        pt_s_3.add(p9);
        pt_s_3.add(p4);

        pt_s_4.add(p4);
        pt_s_4.add(p5);

        pt_s_5.add(p5);
        pt_s_5.add(p7);

        pt_s_6.add(p7);
        pt_s_6.add(p8);

        point_Word_Bus_s_Array[0] = pt_s_0;
        point_Word_Bus_s_Array[1] = pt_s_1;
        point_Word_Bus_s_Array[2] = pt_s_2;
        point_Word_Bus_s_Array[3] = pt_s_3;
        point_Word_Bus_s_Array[4] = pt_s_4;
        point_Word_Bus_s_Array[5] = pt_s_5;
        point_Word_Bus_s_Array[6] = pt_s_6;


    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

    ////////////////////////////////Word Ball//////////////////////////////////

    public static int Word_Ball_B_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Ball_B_Array = new List[Word_Ball_B_TOTAL_STEPS];

    public static void getWord_Ball_BDetails(){



        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();



        int x1 = (int) (scale * 138);
        int y1 = (int) (scale * 70);

        int x2 = (int) (scale * 138);
        int y2 = (int) (scale * 180);

        int x3 = (int) (scale * 145);
        int y3 = (int) (scale * 70);

        int x4 = (int) (scale * 180);
        int y4 = (int) (scale * 70);

        int x5 = (int) (scale * 193);
        int y5 = (int) (scale * 100);

        int x10 = (int) (scale * 170);
        int y10 = (int) (scale * 125);

        int x6 = (int) (scale * 145);
        int y6 = (int) (scale * 125);

        int x7 = (int) (scale * 185);
        int y7 = (int) (scale * 128);

        int x8 = (int) (scale * 198);
        int y8 = (int) (scale * 155);

        int x11= (int) (scale * 180);
        int y11 = (int) (scale * 184);

        int x9 = (int) (scale * 147);
        int y9 = (int) (scale * 184);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
        Point p10 = new Point(x10, y10);
        Point p11 = new Point(x11, y11);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        pt_2.add(p4);
        pt_2.add(p5);

        pt_3.add(p5);
        pt_3.add(p10);

        pt_4.add(p10);
        pt_4.add(p6);

        pt_5.add(p7);
        pt_5.add(p8);

        pt_6.add(p8);
        pt_6.add(p11);

        pt_7.add(p11);
        pt_7.add(p9);
        point_Word_Ball_B_Array[0] = pt_0;
        point_Word_Ball_B_Array[1] = pt_1;
        point_Word_Ball_B_Array[2] = pt_2;
        point_Word_Ball_B_Array[3] = pt_3;
        point_Word_Ball_B_Array[4] = pt_4;
        point_Word_Ball_B_Array[5] = pt_5;
        point_Word_Ball_B_Array[6] = pt_6;
        point_Word_Ball_B_Array[7] = pt_7;




    }
//////////////////////////////////////////////

    public static int Word_Ball_a_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Ball_a_Array = new List[Word_Ball_a_TOTAL_STEPS];

    public static void getWord_Ball_aDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();


        int x1 = (int) (scale * 270);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 245);
        int y2 = (int) (scale * 115);

        int x3 = (int) (scale * 225);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 245);
        int y4 = (int) (scale * 182);

        int x5 = (int) (scale * 270);
        int y5 = (int) (scale * 167);


        int x6 = (int) (scale * 280);
        int y6 = (int) (scale * 105);

        int x7 = (int) (scale * 272);
        int y7 = (int) (scale * 180);

     /*   int x8 = (int) (scale * 255);
        int y8 = (int) (scale * 182);

        int x9 = (int) (scale * 285);
        int y9 = (int) (scale * 167);*/


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);
       /* Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);*/


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);



        point_Word_Ball_a_Array[0] = pt_0;
        point_Word_Ball_a_Array[1] = pt_1;
        point_Word_Ball_a_Array[2] = pt_2;
        point_Word_Ball_a_Array[3] = pt_3;
        point_Word_Ball_a_Array[4] = pt_4;
       // point_Word_Bus_B_Array[5] = pt_5;


    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Ball_L_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Ball_L_Array = new List[Word_Ball_L_TOTAL_STEPS];

    public static void getWord_Ball_LDetails() {


        List<Point> pt_0 = new ArrayList<Point>();


        int x1 = (int) (scale * 317);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 317);
        int y2 = (int) (scale * 185);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);


        pt_0.add(p1);
        pt_0.add(p2);


        point_Word_Ball_L_Array[0] = pt_0;


    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Ball_L2_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Ball_L2_Array = new List[Word_Ball_L2_TOTAL_STEPS];

    public static void getWord_Ball_L2Details() {


        List<Point> pt_0 = new ArrayList<Point>();


        int x1 = (int) (scale * 360);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 360);
        int y2 = (int) (scale * 185);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);


        pt_0.add(p1);
        pt_0.add(p2);


        point_Word_Ball_L2_Array[0] = pt_0;


    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////DOG/////////////////////////////////////////////////////////

    public static int Word_Dog_D_TOTAL_STEPS = 6;

    public static List<Point>[] point_Dog_D_Array = new List[Word_Dog_D_TOTAL_STEPS];

    public static void getWord_DogDDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();

        int xd1 = (int) (scale * 135);
        int yd1 = (int) (scale * 50);


        int xd2 = (int) (scale * 195);
        int yd2 = (int) (scale * 65);


        int xd3 = (int) (scale * 135);
        int yd3 = (int) (scale * 160);

        int xd4 = (int) (scale * 170);
        int yd4 = (int) (scale * 50);


        int xd5 = (int) (scale * 195);
        int yd5 = (int) (scale * 140);


        int xd6 = (int) (scale * 170);
        int yd6 = (int) (scale * 160);







        Point pd1 = new Point(xd1, yd1);
        Point pd2 = new Point(xd2, yd2);
        Point pd3 = new Point(xd3, yd3);
        Point pd4 = new Point(xd4, yd4);
        Point pd5 = new Point(xd5, yd5);
        Point pd6 = new Point(xd6, yd6);



        pt_0.add(pd1);
        pt_0.add(pd3);

        pt_1.add(pd1);
        pt_1.add(pd4);

        pt_2.add(pd4);
        pt_2.add(pd2);

        pt_3.add(pd2);
        pt_3.add(pd5);
        pt_4.add(pd5);
        pt_4.add(pd6);
        pt_5.add(pd6);
        pt_5.add(pd3);


        //pt_0.add(pd7);


        point_Dog_D_Array[0] = pt_0;
        point_Dog_D_Array[1] = pt_1;
        point_Dog_D_Array[2] = pt_2;
        point_Dog_D_Array[3] = pt_3;
        point_Dog_D_Array[4] = pt_4;
        point_Dog_D_Array[5] = pt_5;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Dog_O_TOTAL_STEPS = 6;

    public static List<Point>[] point_Dog_O_Array = new List[Word_Dog_O_TOTAL_STEPS];

    public static void getWord_DogODetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();

        int xo6 = (int) (scale * 232);
        int yo6 = (int) (scale * 100);

        int xo1 = (int) (scale * 232);
        int yo1 = (int) (scale * 140);

        int xo3 = (int) (scale * 255);
        int yo3 = (int) (scale * 162);

        int xo2 = (int) (scale * 282);
        int yo2 = (int) (scale * 140);

        int xo5 = (int) (scale * 282);
        int yo5 = (int) (scale * 100);
        int xo4 = (int) (scale * 255);
        int yo4 = (int) (scale * 80);



        Point po1 = new Point(xo1, yo1);
        Point po2 = new Point(xo2, yo2);
        Point po3 = new Point(xo3, yo3);
        Point po4 = new Point(xo4, yo4);
        Point po5 = new Point(xo5, yo5);
        Point po6 = new Point(xo6, yo6);





        pt_0.add(po4);
        pt_0.add(po6);

        pt_1.add(po6);
        pt_1.add(po1);

        pt_2.add(po1);
        pt_2.add(po3);

        pt_3.add(po3);
        pt_3.add(po2);
        pt_4.add(po2);
        pt_4.add(po5);

        pt_5.add(po5);
        pt_5.add(po4);

        //pt_0.add(pd7);


        point_Dog_O_Array[0] = pt_0;
        point_Dog_O_Array[1] = pt_1;
        point_Dog_O_Array[2] = pt_2;
        point_Dog_O_Array[3] = pt_3;
        point_Dog_O_Array[4] = pt_4;
        point_Dog_O_Array[5] = pt_5;
    }


//////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Dog_G_TOTAL_STEPS = 8;

    public static List<Point>[] point_Dog_G_Array = new List[Word_Dog_G_TOTAL_STEPS];

    public static void getWord_DogGDetails() {

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3= new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6= new ArrayList<Point>();
        List<Point> pt_7= new ArrayList<Point>();

        int xgg1 = (int) (scale * 355);
        int ygg1 = (int) (scale * 93);

        int xgg2 = (int) (scale * 330);
        int ygg2 = (int) (scale * 80);

        int xgg8 = (int) (scale * 315);
        int ygg8 = (int) (scale * 100);

        int xgg9 = (int) (scale * 315);
        int ygg9 = (int) (scale * 150);



        int xgg3 = (int) (scale * 330);
        int ygg3 = (int) (scale * 160);

        int xgg4 = (int) (scale * 355);
        int ygg4 = (int) (scale * 150);

        int xgg5 = (int) (scale * 365);
        int ygg5 = (int) (scale * 80);

        int xgg6 = (int) (scale * 365);
        int ygg6 = (int) (scale * 180);


        int xgg10 = (int) (scale * 345);
        int ygg10 = (int) (scale * 200);

        int xgg7 = (int) (scale * 325);
        int ygg7 = (int) (scale * 195);




        Point pgg1=new Point(xgg1,ygg1);
        Point pgg2=new Point(xgg2,ygg2);
        Point pgg3=new Point(xgg3,ygg3);
        Point pgg4=new Point(xgg4,ygg4);
        Point pgg5=new Point(xgg5,ygg5);
        Point pgg6=new Point(xgg6,ygg6);
        Point pgg7=new Point(xgg7,ygg7);
        Point pgg8=new Point(xgg8,ygg8);
        Point pgg9=new Point(xgg9,ygg9);
        Point pgg10=new Point(xgg10,ygg10);

        pt_0.add(pgg1);
        pt_0.add(pgg2);

        pt_1.add(pgg2);
        pt_1.add(pgg8);

        pt_2.add(pgg8);
        pt_2.add(pgg9);

        pt_3.add(pgg9);
        pt_3.add(pgg3);

        pt_4.add(pgg3);
        pt_4.add(pgg4);

        pt_5.add(pgg5);
        pt_5.add(pgg6);
        pt_6.add(pgg6);
        pt_6.add(pgg10);

        pt_7.add(pgg10);
        pt_7.add(pgg7);


        point_Dog_G_Array[0] = pt_0;
        point_Dog_G_Array[1] = pt_1;
        point_Dog_G_Array[2] = pt_2;
        point_Dog_G_Array[3] = pt_3;
        point_Dog_G_Array[4] = pt_4;
        point_Dog_G_Array[5] = pt_5;
        point_Dog_G_Array[6] = pt_6;
        point_Dog_G_Array[7] = pt_7;
    }


//////////////////////////////////////////////////


//////////////////////////////////////////////////

    //////////////////////////////////////////////Word Egg///////////////////////////////////////////////////

    public static int Word_Egg_E_TOTAL_STEPS = 4;

    public static List<Point>[] point_Egg_E_Array = new List[Word_Egg_E_TOTAL_STEPS];

    public static void getWord_Egg_EDetails(){



        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        int xe1 = (int) (scale * 145);
        int ye1 = (int) (scale * 50);

        int xe2 = (int) (scale * 145);
        int ye2 = (int) (scale * 160);

        int xe3 = (int) (scale * 150);
        int ye3 = (int) (scale * 50);

        int xe4 = (int) (scale * 190);
        int ye4 = (int) (scale * 50);

        int xe5 = (int) (scale * 150);
        int ye5 = (int) (scale * 105);

        int xe6 = (int) (scale * 185);
        int ye6 = (int) (scale * 105);

        int xe7 = (int) (scale * 150);
        int ye7 = (int) (scale * 160);

        int xe8 = (int) (scale * 190);
        int ye8 = (int) (scale * 160);

        Point pe1 = new Point(xe1, ye1);
        Point pe2 = new Point(xe2, ye2);
        Point pe3 = new Point(xe3, ye3);
        Point pe4 = new Point(xe4, ye4);
        Point pe5 = new Point(xe5, ye5);
        Point pe6 = new Point(xe6, ye6);
        Point pe7 = new Point(xe7, ye7);
        Point pe8 = new Point(xe8, ye8);


        pt_0.add(pe1);
        pt_0.add(pe2);

        pt_1.add(pe3);
        pt_1.add(pe4);

        pt_2.add(pe5);
        pt_2.add(pe6);

        pt_3.add(pe7);
        pt_3.add(pe8);

        point_Egg_E_Array[0] = pt_0;
        point_Egg_E_Array[1] = pt_1;
        point_Egg_E_Array[2] = pt_2;
        point_Egg_E_Array[3] = pt_3;
    }


//////////////////////////////////////////////////
public static int Word_Egg_G_1TOTAL_STEPS = 8;

    public static List<Point>[] point_Egg_G_1_Array = new List[Word_Egg_G_1TOTAL_STEPS];

    public static void getWord_Egg_G1EDetails(){



        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3= new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6= new ArrayList<Point>();
        List<Point> pt_7= new ArrayList<Point>();



        int xg1 = (int) (scale * 265);
        int yg1 = (int) (scale * 95);

        int xg2 = (int) (scale * 240);
        int yg2 = (int) (scale * 80);

        int xg8 = (int) (scale * 220);
        int yg8 = (int) (scale * 100);

        int xg9 = (int) (scale * 225);
        int yg9 = (int) (scale * 150);

        int xg3 = (int) (scale * 245);
        int yg3 = (int) (scale * 160);

        int xg4 = (int) (scale * 270);
        int yg4 = (int) (scale * 150);

        int xg5 = (int) (scale * 270);
        int yg5 = (int) (scale * 80);

        int xg6 = (int) (scale * 270);
        int yg6 = (int) (scale * 190);

        int xg10 = (int) (scale * 255);
        int yg10 = (int) (scale * 205);

        int xg7 = (int) (scale * 230);
        int yg7 = (int) (scale * 200);




        Point pg1=new Point(xg1,yg1);
        Point pg2=new Point(xg2,yg2);
        Point pg3=new Point(xg3,yg3);
        Point pg4=new Point(xg4,yg4);
        Point pg5=new Point(xg5,yg5);
        Point pg6=new Point(xg6,yg6);
        Point pg7=new Point(xg7,yg7);
        Point pg8=new Point(xg8,yg8);
        Point pg9=new Point(xg9,yg9);
        Point pg10=new Point(xg10,yg10);

        pt_0.add(pg1);
        pt_0.add(pg2);

        pt_1.add(pg2);
        pt_1.add(pg8);

        pt_2.add(pg8);
        pt_2.add(pg9);

        pt_3.add(pg9);
        pt_3.add(pg3);

        pt_4.add(pg3);
        pt_4.add(pg4);

        pt_5.add(pg5);
        pt_5.add(pg6);
        pt_6.add(pg6);
        pt_6.add(pg10);

        pt_7.add(pg10);
        pt_7.add(pg7);



        point_Egg_G_1_Array[0] = pt_0;
        point_Egg_G_1_Array[1] = pt_1;
        point_Egg_G_1_Array[2] = pt_2;
        point_Egg_G_1_Array[3] = pt_3;
        point_Egg_G_1_Array[4] = pt_4;
        point_Egg_G_1_Array[5] = pt_5;
        point_Egg_G_1_Array[6] = pt_6;
        point_Egg_G_1_Array[7] = pt_7;

    }


//////////////////////////////////////////////////
public static int Word_Egg_G_2_TOTAL_STEPS = 8;

    public static List<Point>[] point_Egg_G_2_Array = new List[Word_Egg_G_2_TOTAL_STEPS];

    public static void getWord_Egg_G2Details(){



        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3= new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6= new ArrayList<Point>();
        List<Point> pt_7= new ArrayList<Point>();

        int xgg1 = (int) (scale * 350);
        int ygg1 = (int) (scale * 93);

        int xgg2 = (int) (scale * 325);
        int ygg2 = (int) (scale * 80);

        int xgg8 = (int) (scale * 310);
        int ygg8 = (int) (scale * 100);

        int xgg9 = (int) (scale * 310);
        int ygg9 = (int) (scale * 150);



        int xgg3 = (int) (scale * 325);
        int ygg3 = (int) (scale * 165);

        int xgg4 = (int) (scale * 350);
        int ygg4 = (int) (scale * 150);

        int xgg5 = (int) (scale * 355);
        int ygg5 = (int) (scale * 80);

        int xgg6 = (int) (scale * 355);
        int ygg6 = (int) (scale * 180);


        int xgg10 = (int) (scale * 335);
        int ygg10 = (int) (scale * 200);

        int xgg7 = (int) (scale * 315);
        int ygg7 = (int) (scale * 195);




        Point pgg1=new Point(xgg1,ygg1);
        Point pgg2=new Point(xgg2,ygg2);
        Point pgg3=new Point(xgg3,ygg3);
        Point pgg4=new Point(xgg4,ygg4);
        Point pgg5=new Point(xgg5,ygg5);
        Point pgg6=new Point(xgg6,ygg6);
        Point pgg7=new Point(xgg7,ygg7);
        Point pgg8=new Point(xgg8,ygg8);
        Point pgg9=new Point(xgg9,ygg9);
        Point pgg10=new Point(xgg10,ygg10);

        pt_0.add(pgg1);
        pt_0.add(pgg2);

        pt_1.add(pgg2);
        pt_1.add(pgg8);

        pt_2.add(pgg8);
        pt_2.add(pgg9);

        pt_3.add(pgg9);
        pt_3.add(pgg3);

        pt_4.add(pgg3);
        pt_4.add(pgg4);

        pt_5.add(pgg5);
        pt_5.add(pgg6);
        pt_6.add(pgg6);
        pt_6.add(pgg10);

        pt_7.add(pgg10);
        pt_7.add(pgg7);


        point_Egg_G_2_Array[0] = pt_0;
        point_Egg_G_2_Array[1] = pt_1;
        point_Egg_G_2_Array[2] = pt_2;
        point_Egg_G_2_Array[3] = pt_3;
        point_Egg_G_2_Array[4] = pt_4;
        point_Egg_G_2_Array[5] = pt_5;
        point_Egg_G_2_Array[6] = pt_6;
        point_Egg_G_2_Array[7] = pt_7;
    }


//////////////////////////////////////////////////
/*
////////////////////////////////////////////  Word Goat ////////////////////////////////////////////////////////////
    public static int Word_Goat_G_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Goat_G_Array = new List[Word_Goat_G_TOTAL_STEPS];

    public static void getWord_Goat_GDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();

        int x1 = (int) (scale * 170);
        int y1 = (int) (scale * 90);

        int x2 = (int) (scale * 145);
        int y2= (int) (scale * 65);

        int x3 = (int) (scale * 110);
        int y3 = (int) (scale * 85);

        int x4 = (int) (scale * 110);
        int y4 = (int) (scale * 160);

        int x5 = (int) (scale * 140);
        int y5 = (int) (scale * 185);

        int x6 = (int) (scale * 170);
        int y6 = (int) (scale * 160);

        int x7 = (int) (scale * 170);
        int y7 = (int) (scale * 130);

        int x8 = (int) (scale * 150);
        int y8= (int) (scale * 130);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);



        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        pt_4.add(p5);
        pt_4.add(p6);

        pt_5.add(p6);
        pt_5.add(p7);

        pt_6.add(p7);
        pt_6.add(p8);


        point_Word_Goat_G_Array[0] = pt_0;
        point_Word_Goat_G_Array[1] = pt_1;
        point_Word_Goat_G_Array[2] = pt_2;
        point_Word_Goat_G_Array[3] = pt_3;
        point_Word_Goat_G_Array[4] = pt_4;
        point_Word_Goat_G_Array[5] = pt_5;
        point_Word_Goat_G_Array[6] = pt_6;


    }

    /////////////////////////////////////////////////////////////////



    public static int Word_Goat_O_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Goat_O_Array = new List[Word_Goat_O_TOTAL_STEPS];

    public static void getWord_Goat_ODetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();



        int xo4 = (int) (scale * 230);
        int yo4 = (int) (scale * 100);

        int xo6 = (int) (scale * 210);
        int yo6 = (int) (scale * 115);

        int xo1 = (int) (scale * 210);
        int yo1 = (int) (scale * 165);

        int xo3 = (int) (scale * 230);
        int yo3 = (int) (scale * 180);

        int xo2 = (int) (scale * 255);
        int yo2 = (int) (scale * 165);

        int xo5 = (int) (scale * 255);
        int yo5 = (int) (scale * 115);

        Point po1 = new Point(xo1, yo1);
        Point po2 = new Point(xo2, yo2);
        Point po3 = new Point(xo3, yo3);
        Point po4 = new Point(xo4, yo4);
        Point po5 = new Point(xo5, yo5);
        Point po6 = new Point(xo6, yo6);

        pt_0.add(po4);
        pt_0.add(po6);

        pt_1.add(po6);
        pt_1.add(po1);

        pt_2.add(po1);
        pt_2.add(po3);

        pt_3.add(po3);
        pt_3.add(po2);

        pt_4.add(po2);
        pt_4.add(po5);

        pt_5.add(po5);
        pt_5.add(po4);

        point_Word_Goat_O_Array[0] = pt_0;
        point_Word_Goat_O_Array[1] = pt_1;
        point_Word_Goat_O_Array[2] = pt_2;
        point_Word_Goat_O_Array[3] = pt_3;
        point_Word_Goat_O_Array[4] = pt_4;
        point_Word_Goat_O_Array[5] = pt_5;
      *//*
*/
/*  point_Word_Bus_B_Array[4] = pt_4;
        point_Word_Bus_B_Array[5] = pt_5;
*//*
*/
/*

    }






///////////////////////////////////////////////////////////////////////////////
public static int Word_Goat_A_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Goat_A_Array = new List[Word_Goat_A_TOTAL_STEPS];

    public static void getWord_Goat_ADetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 337);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 305);
        int y2= (int) (scale * 120);

        int x3 = (int) (scale * 290);
        int y3= (int) (scale * 160);

        int x4 = (int) (scale * 300);
        int y4= (int) (scale * 180);

        int x5 = (int) (scale * 335);
        int y5= (int) (scale * 160);


        int x6 = (int) (scale * 345);
        int y6= (int) (scale * 100);

        int x7 = (int) (scale * 340);
        int y7= (int) (scale *183);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);



        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);




        point_Word_Goat_A_Array[0] = pt_0;
        point_Word_Goat_A_Array[1] = pt_1;
        point_Word_Goat_A_Array[2] = pt_2;
        point_Word_Goat_A_Array[3] = pt_3;
        point_Word_Goat_A_Array[4] = pt_4;



    }


////////////////////////////////////////////////////////////////////////////
public static int Word_Goat_T_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Goat_T_Array = new List[Word_Goat_T_TOTAL_STEPS];

    public static void getWord_Goat_TDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 390);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 390);
        int y2 = (int) (scale * 170);

        int x3 = (int) (scale * 405);
        int y3 = (int) (scale * 180);

        int x4 = (int) (scale * 375);
        int y4 = (int) (scale * 102);

        int x5 = (int) (scale * 405);
        int y5 = (int) (scale * 102);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p4);
        pt_2.add(p5);

        point_Word_Goat_T_Array[0] = pt_0;
        point_Word_Goat_T_Array[1] = pt_1;
        point_Word_Goat_T_Array[2] = pt_2;



    }

*/



    ////////////////////////////////////////////////////////////////////////////////////////////////////*/

    ////////////////////////////////////////////  Word Gift ////////////////////////////////////////////////////////////
    public static int Word_Gift_G_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Gift_G_Array = new List[Word_Gift_G_TOTAL_STEPS];

    public static void getWord_Gift_GDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();

        int x1 = (int) (scale * 206);
        int y1 = (int) (scale * 94);

        int x2 = (int) (scale * 185);
        int y2= (int) (scale * 70);

        int x3 = (int) (scale * 150);
        int y3 = (int) (scale * 85);

        int x4 = (int) (scale * 147);
        int y4 = (int) (scale * 160);

        int x5 = (int) (scale * 180);
        int y5 = (int) (scale * 185);

        int x6 = (int) (scale * 206);
        int y6 = (int) (scale * 160);

        int x7 = (int) (scale * 206);
        int y7 = (int) (scale * 135);

        int x8 = (int) (scale * 190);
        int y8= (int) (scale * 135);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);



        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        pt_4.add(p5);
        pt_4.add(p6);

        pt_5.add(p6);
        pt_5.add(p7);

        pt_6.add(p7);
        pt_6.add(p8);


        point_Word_Gift_G_Array[0] = pt_0;
        point_Word_Gift_G_Array[1] = pt_1;
        point_Word_Gift_G_Array[2] = pt_2;
        point_Word_Gift_G_Array[3] = pt_3;
        point_Word_Gift_G_Array[4] = pt_4;
        point_Word_Gift_G_Array[5] = pt_5;
        point_Word_Gift_G_Array[6] = pt_6;


    }

    /////////////////////////////////////////////////////////////////



    public static int Word_Gift_i_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Gift_i_Array = new List[Word_Gift_i_TOTAL_STEPS];

    public static void getWord_Gift_iDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();



        int xI1 = (int) (scale * 246);
        int yI1 = (int) (scale * 60);

        int xI2 = (int) (scale * 246);
        int yI2 = (int) (scale * 65);

        int xI3 = (int) (scale * 246);
        int yI3 = (int) (scale * 110);

        int xI4 = (int) (scale * 246);
        int yI4 = (int) (scale * 187);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_1.add(pI1);
        pt_1.add(pI2);

        pt_0.add(pI3);
        pt_0.add(pI4);

        point_Word_Gift_i_Array[0] = pt_0;
        point_Word_Gift_i_Array[1] = pt_1;
      /*  point_Word_Gift_i_Array[2] = pt_2;
        point_Word_Gift_i_Array[3] = pt_3;
        point_Word_Gift_i_Array[4] = pt_4;
        point_Word_Gift_i_Array[5] = pt_5;*/
      /*  point_Word_Bus_B_Array[4] = pt_4;
        point_Word_Bus_B_Array[5] = pt_5;
*/

    }






    ///////////////////////////////////////////////////////////////////////////////
    public static int Word_Gift_f_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Gift_f_Array = new List[Word_Gift_f_TOTAL_STEPS];

    public static void getWord_Gift_fDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 310);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 290);
        int y2 = (int) (scale * 73);

        int x3 = (int) (scale * 290);
        int y3 = (int) (scale * 187);

        int x4 = (int) (scale * 272);
        int y4 = (int) (scale * 107);

        int x5 = (int) (scale * 310);
        int y5 = (int) (scale * 107);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p4);
        pt_2.add(p5);

        point_Word_Gift_f_Array[0] = pt_0;
        point_Word_Gift_f_Array[1] = pt_1;
        point_Word_Gift_f_Array[2] = pt_2;
        /*point_Word_Gift_f_Array[3] = pt_3;
        point_Word_Gift_f_Array[4] = pt_4;
*/
    }


    ////////////////////////////////////////////////////////////////////////////
    public static int Word_Gift_T_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Gift_T_Array = new List[Word_Gift_T_TOTAL_STEPS];

    public static void getWord_Gift_TDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 343);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 343);
        int y2 = (int) (scale * 170);

        int x3 = (int) (scale * 360);
        int y3 = (int) (scale * 184);

        int x4 = (int) (scale * 325);
        int y4 = (int) (scale * 106);

        int x5 = (int) (scale * 360);
        int y5 = (int) (scale * 106);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p4);
        pt_2.add(p5);

        point_Word_Gift_T_Array[0] = pt_0;
        point_Word_Gift_T_Array[1] = pt_1;
        point_Word_Gift_T_Array[2] = pt_2;

    }




    ////////////////////////////////////////////////////////////////////////////////////////////////////
/*

   /////////////////////////////////////////////  word Igloo  ///////////////////////////////////////////////////////////////////////

    public static int Word_Igloo_I_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Igloo_I_Array = new List[Word_Igloo_I_TOTAL_STEPS];

    public static void getIgloo_IDetails(){

        List<Point> pt_I_0 = new ArrayList<Point>();

        int xI1 = (int) (scale * 100);
        int yI1 = (int) (scale * 55);
        int xI2 = (int) (scale * 100);
        int yI2 = (int) (scale * 165);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);

        pt_I_0.add(pI1);
        pt_I_0.add(pI2);

        point_Word_Igloo_I_Array[0] = pt_I_0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    public static int Word_Igloo_G_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Igloo_G_Array = new List[Word_Igloo_G_TOTAL_STEPS];

    public static void getIgloo_GDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3= new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6= new ArrayList<Point>();
        List<Point> pt_7= new ArrayList<Point>();

        int xgg1 = (int) (scale *180);
        int ygg1 = (int) (scale * 98);

        int xgg2 = (int) (scale * 155);
        int ygg2 = (int) (scale * 85);

        int xgg8 = (int) (scale * 140);
        int ygg8 = (int) (scale * 105);

        int xgg9 = (int) (scale * 140);
        int ygg9 = (int) (scale * 155);



        int xgg3 = (int) (scale * 155);
        int ygg3 = (int) (scale * 165);

        int xgg4 = (int) (scale * 180);
        int ygg4 = (int) (scale * 155);

        int xgg5 = (int) (scale * 190);
        int ygg5 = (int) (scale * 85);

        int xgg6 = (int) (scale * 190);
        int ygg6 = (int) (scale * 185);


        int xgg10 = (int) (scale * 170);
        int ygg10 = (int) (scale * 205);

        int xgg7 = (int) (scale * 150);
        int ygg7 = (int) (scale * 200);




        Point pgg1=new Point(xgg1,ygg1);
        Point pgg2=new Point(xgg2,ygg2);
        Point pgg3=new Point(xgg3,ygg3);
        Point pgg4=new Point(xgg4,ygg4);
        Point pgg5=new Point(xgg5,ygg5);
        Point pgg6=new Point(xgg6,ygg6);
        Point pgg7=new Point(xgg7,ygg7);
        Point pgg8=new Point(xgg8,ygg8);
        Point pgg9=new Point(xgg9,ygg9);
        Point pgg10=new Point(xgg10,ygg10);

        pt_0.add(pgg1);
        pt_0.add(pgg2);

        pt_1.add(pgg2);
        pt_1.add(pgg8);

        pt_2.add(pgg8);
        pt_2.add(pgg9);

        pt_3.add(pgg9);
        pt_3.add(pgg3);

        pt_4.add(pgg3);
        pt_4.add(pgg4);

        pt_5.add(pgg5);
        pt_5.add(pgg6);
        pt_6.add(pgg6);
        pt_6.add(pgg10);

        pt_7.add(pgg10);
        pt_7.add(pgg7);

        point_Word_Igloo_G_Array[0] = pt_0;
        point_Word_Igloo_G_Array[1] = pt_1;
        point_Word_Igloo_G_Array[2] = pt_2;
        point_Word_Igloo_G_Array[3] = pt_3;
        point_Word_Igloo_G_Array[4] = pt_4;
        point_Word_Igloo_G_Array[5] = pt_5;
        point_Word_Igloo_G_Array[6] = pt_6;
        point_Word_Igloo_G_Array[7] = pt_7;


    }



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    public static int Word_Igloo_L_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Igloo_L_Array = new List[Word_Igloo_L_TOTAL_STEPS];

    public static void getIgloo_LDetails(){

        List<Point> pt_0 = new ArrayList<Point>();


        int xgg1 = (int) (scale * 228);
        int ygg1 = (int) (scale * 50);

        int xgg2 = (int) (scale * 228);
        int ygg2 = (int) (scale * 165);



        Point pgg1=new Point(xgg1,ygg1);
        Point pgg2=new Point(xgg2,ygg2);


        pt_0.add(pgg1);
        pt_0.add(pgg2);



        point_Word_Igloo_L_Array[0] = pt_0;


    }


/////////////////////////////////////////////////////////////////////////////////////



    public static int Word_Igloo_O_1_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Igloo_O_1_Array = new List[Word_Igloo_O_1_TOTAL_STEPS];

    public static void getIgloo_O_1Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 285);
        int y1 = (int) (scale * 85);

        int x2 = (int) (scale * 263);
        int y2= (int) (scale * 105);

        int x3 = (int) (scale * 263);
        int y3= (int) (scale * 145);

        int x4 = (int) (scale * 285);
        int y4= (int) (scale *170);

        int x5 = (int) (scale * 315);
        int y5= (int) (scale *145);

        int x7 = (int) (scale * 315);
        int y7= (int) (scale *105);

        int x8 = (int) (scale * 290);
        int y8= (int) (scale *85);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p5 = new Point(x5, y5);
        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        pt_4.add(p5);
        pt_4.add(p7);

        pt_5.add(p7);
        pt_5.add(p8);


        point_Word_Igloo_O_1_Array[0] = pt_0;
        point_Word_Igloo_O_1_Array[1] = pt_1;
        point_Word_Igloo_O_1_Array[2] = pt_2;
        point_Word_Igloo_O_1_Array[3] = pt_3;
        point_Word_Igloo_O_1_Array[4] = pt_4;
        point_Word_Igloo_O_1_Array[5] = pt_5;

    }



//////////////////////////////////////////////////////////////////////////////////
public static int Word_Igloo_O_2_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Igloo_O_2_Array = new List[Word_Igloo_O_2_TOTAL_STEPS];

    public static void getIgloo_O_2Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 370);
        int y1 = (int) (scale * 85);

        int x2 = (int) (scale * 350);
        int y2= (int) (scale * 105);

        int x3 = (int) (scale * 350);
        int y3= (int) (scale * 145);

        int x4 = (int) (scale * 375);
        int y4= (int) (scale *170);

        int x5 = (int) (scale * 400);
        int y5= (int) (scale *145);

        int x7 = (int) (scale * 400);
        int y7= (int) (scale *105);

        int x8 = (int) (scale * 375);
        int y8= (int) (scale *85);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p5 = new Point(x5, y5);
        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        pt_4.add(p5);
        pt_4.add(p7);

        pt_5.add(p7);
        pt_5.add(p8);


        point_Word_Igloo_O_2_Array[0] = pt_0;
        point_Word_Igloo_O_2_Array[1] = pt_1;
        point_Word_Igloo_O_2_Array[2] = pt_2;
        point_Word_Igloo_O_2_Array[3] = pt_3;
        point_Word_Igloo_O_2_Array[4] = pt_4;
        point_Word_Igloo_O_2_Array[5] = pt_5;

    }


*/

    /////////////////////////////////////////////  word Ink  ///////////////////////////////////////////////////////////////////////

    public static int Word_Ink_I_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Ink_I_Array = new List[Word_Ink_I_TOTAL_STEPS];

    public static void getInk_IDetails(){

        List<Point> pt_I_0 = new ArrayList<Point>();

        int xI1 = (int) (scale * 166);
        int yI1 = (int) (scale * 70);
        int xI2 = (int) (scale * 166);
        int yI2 = (int) (scale * 185);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);

        pt_I_0.add(pI1);
        pt_I_0.add(pI2);

        point_Word_Ink_I_Array[0] = pt_I_0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    public static int Word_Ink_n_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Ink_n_Array = new List[Word_Ink_n_TOTAL_STEPS];

    public static void getInk_nDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3= new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6= new ArrayList<Point>();
        List<Point> pt_7= new ArrayList<Point>();

        int x1 = (int) (scale * 200);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 200);
        int y2= (int) (scale * 187);

        int x3= (int) (scale * 210);
        int y3= (int) (scale * 116);

        int x6= (int) (scale * 235);
        int y6= (int) (scale * 105);

        int x4= (int) (scale * 245);
        int y4= (int) (scale * 115);

        int x5= (int) (scale * 250);
        int y5= (int) (scale * 187);

        Point pK1 = new Point(x1, y1);
        Point pK2 = new Point(x2, y2);
        Point pK3 = new Point(x3, y3);
        Point pK4 = new Point(x4, y4);
        Point pK5 = new Point(x5, y5);
        Point pK6 = new Point(x6, y6);

        pt_0.add(pK1);
        pt_0.add(pK2);

        pt_1.add(pK3);
        pt_1.add(pK6);

        pt_2.add(pK6);
        pt_2.add(pK4);

        pt_3.add(pK4);
        pt_3.add(pK5);
        point_Word_Ink_n_Array[0] = pt_0;
        point_Word_Ink_n_Array[1] = pt_1;
        point_Word_Ink_n_Array[2] = pt_2;
        point_Word_Ink_n_Array[3] = pt_3;
       /* point_Word_Ink_n_Array[4] = pt_4;
        point_Word_Ink_n_Array[5] = pt_5;
        point_Word_Ink_n_Array[6] = pt_6;
        point_Word_Ink_n_Array[7] = pt_7;*/


    }



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    public static int Word_Ink_k_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Ink_k_Array = new List[Word_Ink_k_TOTAL_STEPS];

    public static void getInk_kDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 280);
        int yK1 = (int) (scale * 60);

        int xK2 = (int) (scale * 280);
        int yK2= (int) (scale * 187);

        int xK3= (int) (scale * 286);
        int yK3= (int) (scale * 142);

        int xK4= (int) (scale * 299);
        int yK4= (int) (scale * 142);

        int xK5= (int) (scale * 302);
        int yK5= (int) (scale * 142);

        int xK6= (int) (scale * 320);
        int yK6= (int) (scale * 104);

        int xK7= (int) (scale * 325);
        int yK7= (int) (scale * 187);


        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3= new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(xK7, yK7);



        pt_0.add(pK1);
        pt_0.add(pK2);
        pt_1.add(pK3);
        pt_1.add(pK4);
        pt_2.add(pK5);
        pt_2.add(pK6);
        pt_3.add(pK5);
        pt_3.add(pK7);


        point_Word_Ink_k_Array[0] = pt_0;
        point_Word_Ink_k_Array[1] = pt_1;
        point_Word_Ink_k_Array[2] = pt_2;
        point_Word_Ink_k_Array[3] = pt_3;


    }


/*    //////////////////////////////////word jeep///////////////////////////////////////////////////////////

    public static int Word_Jeep_J_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Jeep_J_Array = new List[Word_Jeep_J_TOTAL_STEPS];

    public static void getJeep_JDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();



        int x1 = (int) (scale * 150);
        int y1 = (int) (scale * 50);

        int x2 = (int) (scale * 150);
        int y2= (int) (scale * 140);

        int x3 = (int) (scale * 135);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 110);
        int y4 = (int) (scale * 165);

        int x5 = (int) (scale * 100);
        int y5 = (int) (scale * 140);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);



        point_Word_Jeep_J_Array[0] = pt_0;
        point_Word_Jeep_J_Array[1] = pt_1;
        point_Word_Jeep_J_Array[2] = pt_2;
        point_Word_Jeep_J_Array[3] = pt_3;

    }




////////////////////////////////////////////////////////////////////////////////////////////
public static int Word_Jeep_E_1_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Jeep_E_1_Array = new List[Word_Jeep_E_1_TOTAL_STEPS];

    public static void getJeep_E_1Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 200);
        int y1 = (int) (scale * 120);

        int x2 = (int) (scale * 235);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 230);
        int y3 = (int) (scale * 85);

        int x4 = (int) (scale * 203);
        int y4 = (int) (scale * 85);



        int x5 = (int) (scale * 185);
        int y5 = (int) (scale * 110);

        int x6 = (int) (scale * 190);
        int y6 = (int) (scale * 135);

        int x7 = (int) (scale * 200);
        int y7 = (int) (scale * 160);

        int x8 = (int) (scale * 230);
        int y8 = (int) (scale * 160);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        pt_5.add(p5);
        pt_5.add(p6);

        pt_6.add(p6);
        pt_6.add(p7);

        pt_7.add(p7);
        pt_7.add(p8);

        point_Word_Jeep_E_1_Array[0] = pt_0;
        point_Word_Jeep_E_1_Array[1] = pt_1;
        point_Word_Jeep_E_1_Array[2] = pt_2;
        point_Word_Jeep_E_1_Array[3] = pt_3;
        point_Word_Jeep_E_1_Array[4] = pt_5;
        point_Word_Jeep_E_1_Array[5] = pt_6;
        point_Word_Jeep_E_1_Array[6] = pt_7;


    }


////////////////////////////////////////////////////////////////////////////////////////////////
public static int Word_Jeep_E_2_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Jeep_E_2_Array = new List[Word_Jeep_E_2_TOTAL_STEPS];

    public static void getJeep_E_2Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        int x1 = (int) (scale * 280);
        int y1 = (int) (scale * 120);

        int x2 = (int) (scale * 310);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 305);
        int y3 = (int) (scale * 85);

        int x4 = (int) (scale * 275);
        int y4 = (int) (scale * 85);



        int x5 = (int) (scale * 265);
        int y5 = (int) (scale * 110);

        int x6 = (int) (scale * 270);
        int y6 = (int) (scale * 135);

        int x7 = (int) (scale * 280);
        int y7 = (int) (scale * 160);

        int x8 = (int) (scale * 310);
        int y8 = (int) (scale * 160);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        pt_5.add(p5);
        pt_5.add(p6);

        pt_6.add(p6);
        pt_6.add(p7);

        pt_7.add(p7);
        pt_7.add(p8);

        point_Word_Jeep_E_2_Array[0] = pt_0;
        point_Word_Jeep_E_2_Array[1] = pt_1;
        point_Word_Jeep_E_2_Array[2] = pt_2;
        point_Word_Jeep_E_2_Array[3] = pt_3;
        point_Word_Jeep_E_2_Array[4] = pt_5;
        point_Word_Jeep_E_2_Array[5] = pt_6;
        point_Word_Jeep_E_2_Array[6] = pt_7;


    }


/////////////////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Jeep_P_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Jeep_P_Array = new List[Word_Jeep_P_TOTAL_STEPS];

    public static void getJeep_PDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();

        int xp1 = (int) (scale * 345);
        int yp1 = (int) (scale * 85);

        int xp2 = (int) (scale * 345);
        int yp2 = (int) (scale * 190);

        int xp3 = (int) (scale * 350);
        int yp3 = (int) (scale * 100);

        int xp4 = (int) (scale * 375);
        int yp4 = (int) (scale * 80);

        int xp5 = (int) (scale * 395);
        int yp5 = (int) (scale * 100);

        int xp6 = (int) (scale * 395);
        int yp6 = (int) (scale * 130);

        int xp7 = (int) (scale * 380);
        int yp7 = (int) (scale * 160);

        int xp8 = (int) (scale * 355);
        int yp8 = (int) (scale * 150);




        Point p1 = new Point(xp1, yp1);
        Point p2 = new Point(xp2, yp2);
        Point p3 = new Point(xp3, yp3);
        Point p4 = new Point(xp4, yp4);
        Point p5 = new Point(xp5, yp5);
        Point p6 = new Point(xp6, yp6);
        Point p7 = new Point(xp7, yp7);
        Point p8 = new Point(xp8, yp8);



        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        pt_2.add(p4);
        pt_2.add(p5);

        pt_3.add(p5);
        pt_3.add(p6);
/////
        pt_4.add(p6);
        pt_4.add(p7);

        pt_5.add(p7);
        pt_5.add(p8);

        point_Word_Jeep_P_Array[0] = pt_0;
        point_Word_Jeep_P_Array[1] = pt_1;
        point_Word_Jeep_P_Array[2] = pt_2;
        point_Word_Jeep_P_Array[3] = pt_3;
        point_Word_Jeep_P_Array[4] = pt_4;
        point_Word_Jeep_P_Array[5] = pt_5;


    }*/


    //////////////////////////////////word jug///////////////////////////////////////////////////////////

    public static int Word_Jug_J_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Jug_J_Array = new List[Word_Jug_J_TOTAL_STEPS];

    public static void getJug_JDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();



        int x1 = (int) (scale * 194);
        int y1 = (int) (scale * 46);

        int x2 = (int) (scale * 194);
        int y2= (int) (scale * 140);

        int x3 = (int) (scale * 185);
        int y3 = (int) (scale * 160);

        int x4 = (int) (scale * 153);
        int y4 = (int) (scale * 157);

        int x5 = (int) (scale * 150);
        int y5 = (int) (scale * 140);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);



        point_Word_Jug_J_Array[0] = pt_0;
        point_Word_Jug_J_Array[1] = pt_1;
        point_Word_Jug_J_Array[2] = pt_2;
        point_Word_Jug_J_Array[3] = pt_3;

    }




    ////////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Jug_u_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Jug_u_Array = new List[Word_Jug_u_TOTAL_STEPS];

    public static void getJug_u_Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();


        int x1 = (int) (scale * 228);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 228);
        int y2 = (int) (scale * 150);

        int x3 = (int) (scale * 240);
        int y3 = (int) (scale * 165);

        int x6 = (int) (scale * 264);
        int y6 = (int) (scale * 150);

        int x4 = (int) (scale * 275);
        int y4 = (int) (scale * 80);

        int x5 = (int) (scale * 275);
        int y5 = (int) (scale * 165);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p6);

        pt_3.add(p4);
        pt_3.add(p5);


        point_Word_Jug_u_Array[0] = pt_0;
        point_Word_Jug_u_Array[1] = pt_1;
        point_Word_Jug_u_Array[2] = pt_2;
        point_Word_Jug_u_Array[3] = pt_3;

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Jug_g_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Jug_g_Array = new List[Word_Jug_g_TOTAL_STEPS];

    public static void getJug_g_Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();

        int xgg1 = (int) (scale * 344);
        int ygg1 = (int) (scale * 90);

        int xgg2 = (int) (scale * 315);
        int ygg2 = (int) (scale * 88);

        int xgg8 = (int) (scale * 303);
        int ygg8 = (int) (scale * 106);

        int xgg9 = (int) (scale * 310);
        int ygg9 = (int) (scale * 150);

        int xgg3 = (int) (scale * 325);
        int ygg3 = (int) (scale * 166);

        int xgg4 = (int) (scale * 344);
        int ygg4 = (int) (scale * 156);

        int xgg5 = (int) (scale * 353);
        int ygg5 = (int) (scale * 90);

        int xgg6 = (int) (scale * 353);
        int ygg6 = (int) (scale * 186);

        int xgg10 = (int) (scale * 330);
        int ygg10 = (int) (scale * 200);

        int xgg7 = (int) (scale * 310);
        int ygg7 = (int) (scale * 195);

        Point pgg1=new Point(xgg1,ygg1);
        Point pgg2=new Point(xgg2,ygg2);
        Point pgg3=new Point(xgg3,ygg3);
        Point pgg4=new Point(xgg4,ygg4);
        Point pgg5=new Point(xgg5,ygg5);
        Point pgg6=new Point(xgg6,ygg6);
        Point pgg7=new Point(xgg7,ygg7);
        Point pgg8=new Point(xgg8,ygg8);
        Point pgg9=new Point(xgg9,ygg9);
        Point pgg10=new Point(xgg10,ygg10);

        pt_0.add(pgg1);
        pt_0.add(pgg2);

        pt_1.add(pgg2);
        pt_1.add(pgg8);

        pt_2.add(pgg8);
        pt_2.add(pgg9);

        pt_3.add(pgg9);
        pt_3.add(pgg3);

        pt_4.add(pgg3);
        pt_4.add(pgg4);

        pt_5.add(pgg5);
        pt_5.add(pgg6);
        pt_6.add(pgg6);
        pt_6.add(pgg10);

        pt_7.add(pgg10);
        pt_7.add(pgg7);


        point_Word_Jug_g_Array[0] = pt_0;
        point_Word_Jug_g_Array[1] = pt_1;
        point_Word_Jug_g_Array[2] = pt_2;
        point_Word_Jug_g_Array[3] = pt_3;
        point_Word_Jug_g_Array[4] = pt_4;
        point_Word_Jug_g_Array[5] = pt_5;
        point_Word_Jug_g_Array[6] = pt_6;
        point_Word_Jug_g_Array[7] = pt_7;


    }


///

    //////////////////////////////////////////////Word Kite///////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////K////////////////////////////////////////
    public static int Word_kite_k_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_kite_k_Array = new List[Word_kite_k_TOTAL_STEPS];


    public static void getWord_kite_k_Details(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();


        int xK1 = (int) (scale * 135);
        int yK1 = (int) (scale * 70);

        int xK2 = (int) (scale * 135);
        int yK2= (int) (scale * 185);

        int xK3= (int) (scale * 137);
        int yK3= (int) (scale * 132);

        int xK4= (int) (scale * 160);
        int yK4= (int) (scale * 132);

        int xK5= (int) (scale * 160);
        int yK5= (int) (scale * 132);

        int xK6= (int) (scale * 190);
        int yK6= (int) (scale * 70);

        int xK7= (int) (scale * 195);
        int yK7= (int) (scale * 185);


        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3= new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(xK7, yK7);



        pt_K_0.add(pK1);
        pt_K_0.add(pK2);
        pt_K_1.add(pK3);
        pt_K_1.add(pK4);
        pt_K_2.add(pK5);
        pt_K_2.add(pK6);
        pt_K_3.add(pK5);
        pt_K_3.add(pK7);




        point_Word_kite_k_Array[0] = pt_K_0;
        point_Word_kite_k_Array[1] = pt_K_1;
        point_Word_kite_k_Array[2] = pt_K_2;
        point_Word_kite_k_Array[3] = pt_K_3;

    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// i /////////////////////////////////////////////////////
    public static int Word_kite_i_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_kite_i_Array = new List[Word_kite_i_TOTAL_STEPS];

    public static void getWord_kite_i_Details() {


        List<Point> pt_l_0 = new ArrayList<Point>();
        List<Point> pt_l_1 = new ArrayList<Point>();


        int xI1 = (int) (scale * 230);
        int yI1 = (int) (scale * 60);

        int xI2 = (int) (scale * 230);
        int yI2 = (int) (scale * 63);

        int xI3 = (int) (scale * 230);
        int yI3 = (int) (scale * 108);

        int xI4 = (int) (scale * 230);
        int yI4 = (int) (scale * 187);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        pt_l_1.add(pI3);
        pt_l_1.add(pI4);


        point_Word_kite_i_Array[0] = pt_l_1;
        point_Word_kite_i_Array[1] = pt_l_0;

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////  t ///////////////////////////////////////////////////
    public static int Word_kite_t_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_kite_t_Array = new List[Word_kite_t_TOTAL_STEPS];
    public static void getWord_kite_t_Details(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 275);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 275);
        int y2 = (int) (scale * 177);

        int x3 = (int) (scale * 290);
        int y3 = (int) (scale * 187);

        int x4 = (int) (scale * 260);
        int y4 = (int) (scale * 105);

        int x5 = (int) (scale * 290);
        int y5 = (int) (scale * 105);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p4);
        pt_C_2.add(p5);

        point_Word_kite_t_Array[0] = pt_C_0;
        point_Word_kite_t_Array[1] = pt_C_1;
        point_Word_kite_t_Array[2] = pt_C_2;

    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////  e  /////////////////////////////////////////////////////////////
    public static int Word_kite_e_TOTAL_STEPS =7;

    public static List<Point>[] point_Word_kite_e_Array = new List[Word_kite_e_TOTAL_STEPS];
    public static void getWord_kite_e_Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();



        int x1 = (int) (scale * 325);
        int y1 = (int) (scale * 145);

        int x2 = (int) (scale * 365);
        int y2 = (int) (scale *  145);

        int x3 = (int) (scale * 360);
        int y3 = (int) (scale *  115);



        int x4 = (int) (scale * 333);
        int y4 = (int) (scale *  110);

        int x5 = (int) (scale * 318);
        int y5 = (int) (scale * 145);

        int x6 = (int) (scale * 327);
        int y6 = (int) (scale * 180);

        int x7 = (int) (scale * 340);
        int y7 = (int) (scale * 190);

        int x8 = (int) (scale * 360);
        int y8 = (int) (scale * 185);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);


        pt_4.add(p5);
        pt_4.add(p6);

        pt_5.add(p6);
        pt_5.add(p7);

        pt_6.add(p7);
        pt_6.add(p8);


        point_Word_kite_e_Array[0] = pt_0;
        point_Word_kite_e_Array[1] = pt_1;
        point_Word_kite_e_Array[2] = pt_2;
        point_Word_kite_e_Array[3] = pt_3;
        point_Word_kite_e_Array[4] = pt_4;
        point_Word_kite_e_Array[5] = pt_5;
        point_Word_kite_e_Array[6] = pt_6;


    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////       Word Rabbit           /////////////////////////////////////////////////////////////////////////////////



    public static int Word_Rabbit_R_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Rabbit_R_Array = new List[Word_Rabbit_R_TOTAL_STEPS];

    public static void getRabbit_RDetails(){


        List<Point> pt_R_0 = new ArrayList<Point>();
        List<Point> pt_R_1 = new ArrayList<Point>();
        List<Point> pt_R_2 = new ArrayList<Point>();
        List<Point> pt_R_3 = new ArrayList<Point>();
        List<Point> pt_R_4 = new ArrayList<Point>();
        List<Point> pt_R_5 = new ArrayList<Point>();
        List<Point> pt_R_6 = new ArrayList<Point>();
        List<Point> pt_R_7 = new ArrayList<Point>();
        int x1 = (int) (scale * 50);
        int y1 = (int) (scale * 75);

        int x2 = (int) (scale * 50);
        int y2= (int) (scale *180);

        int x3 = (int) (scale * 55);
        int y3 = (int) (scale * 75);

        int x4 = (int) (scale * 90);
        int y4 = (int) (scale * 74);

        int x5 = (int) (scale * 105);
        int y5 = (int) (scale * 105);

        int x6 = (int) (scale * 90);
        int y6 = (int) (scale * 135);

        int x7= (int) (scale * 55);
        int y7= (int) (scale * 135);

        int x8= (int) (scale * 95);
        int y8= (int) (scale * 138);

        int x9= (int) (scale * 105);
        int y9= (int) (scale * 180);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);
        Point p8= new Point(x8, y8);
        Point p9= new Point(x9, y9);



        pt_R_0.add(p1);
        pt_R_0.add(p2);

        pt_R_1.add(p3);
        pt_R_1.add(p4);

        pt_R_2.add(p4);
        pt_R_2.add(p5);

        pt_R_3.add(p5);
        pt_R_3.add(p6);

        pt_R_4.add(p6);
        pt_R_4.add(p7);

        pt_R_5.add(p8);
        pt_R_5.add(p9);



        point_Word_Rabbit_R_Array[0] = pt_R_0;
        point_Word_Rabbit_R_Array[1] =pt_R_1;
        point_Word_Rabbit_R_Array[2] = pt_R_2;
        point_Word_Rabbit_R_Array[3] = pt_R_3;
        point_Word_Rabbit_R_Array[4] = pt_R_4;
        point_Word_Rabbit_R_Array[5] = pt_R_5;



    }


////////////////////////////////////////////////////////////////////////////////


    public static int Word_Rabbit_A_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Rabbit_A_Array = new List[Word_Rabbit_A_TOTAL_STEPS];

    public static void getRabbit_ADetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 182);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 147);
        int y2= (int) (scale * 120);

        int x3 = (int) (scale * 137);
        int y3= (int) (scale * 175);

        int x4 = (int) (scale * 155);
        int y4= (int) (scale * 185);

        int x5 = (int) (scale * 175);
        int y5= (int) (scale * 173);


        int x6 = (int) (scale * 190);
        int y6= (int) (scale * 105);

        int x7 = (int) (scale * 184);
        int y7= (int) (scale *185);

       /* int x8 = (int) (scale * 155);
        int y8= (int) (scale *190);

        int x9= (int) (scale * 175);
        int y9= (int) (scale *177);*/


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);
       /* Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
*/


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);

      /*  pt_5.add(p7);
        pt_5.add(p8);

        pt_6.add(p8);
        pt_6.add(p9);*/



        point_Word_Rabbit_A_Array[0] = pt_0;
        point_Word_Rabbit_A_Array[1] = pt_1;
        point_Word_Rabbit_A_Array[2] = pt_2;
        point_Word_Rabbit_A_Array[3] = pt_3;
        point_Word_Rabbit_A_Array[4] = pt_4;
        /*point_Word_Rabbit_A_Array[5] = pt_5;
        point_Word_Rabbit_A_Array[6] = pt_6;*/



    }
//////////////////////////////////////////////////////////////////////////////////////////


    public static int Word_Rabbit_B_1_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Rabbit_B_1_Array = new List[Word_Rabbit_B_1_TOTAL_STEPS];

    public static void getRabbit_B_1Details(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();

        int x1 = (int) (scale * 223);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 223);
        int y2= (int) (scale * 180);

        int x3= (int) (scale * 230);
        int y3= (int) (scale * 110);

        int x4= (int) (scale * 250);
        int y4= (int) (scale * 98);

        int x5= (int) (scale * 270);
        int y5= (int) (scale * 120);

        int x51= (int) (scale * 275);
        int y51= (int) (scale * 160);

        int x6= (int) (scale * 255);
        int y6= (int) (scale * 185);

        int x61= (int) (scale * 230);
        int y61= (int) (scale * 170);
        Point pK1 = new Point(x1, y1);
        Point pK2 = new Point(x2, y2);
        Point pK3 = new Point(x3, y3);
        Point pK4 = new Point(x4, y4);
        Point pK5 = new Point(x5, y5);
        Point pK6 = new Point(x6, y6);
        Point pK51 = new Point(x51, y51);
        Point pK61 = new Point(x61, y61);

        pt_0.add(pK1);
        pt_0.add(pK2);

        pt_1.add(pK3);
        pt_1.add(pK4);

        pt_2.add(pK4);
        pt_2.add(pK5);

        pt_3.add(pK5);
        pt_3.add(pK51);
        pt_4.add(pK51);
        pt_4.add(pK6);
        pt_5.add(pK6);
        pt_5.add(pK61);
        point_Word_Rabbit_B_1_Array[0] = pt_0;
        point_Word_Rabbit_B_1_Array[1] = pt_1;
        point_Word_Rabbit_B_1_Array[2] = pt_2;
        point_Word_Rabbit_B_1_Array[3] = pt_3;
        point_Word_Rabbit_B_1_Array[4] = pt_4;
        point_Word_Rabbit_B_1_Array[5] = pt_5;

    }

    //////////////////////////////////////////////////////////////////////////////////////////


    public static int Word_Rabbit_B_2_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Rabbit_B_2_Array = new List[Word_Rabbit_B_2_TOTAL_STEPS];

    public static void getRabbit_B_2Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();



        int x1 = (int) (scale * 307);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 307);
        int y2= (int) (scale * 180);

        int x3= (int) (scale * 320);
        int y3= (int) (scale * 110);

        int x4= (int) (scale * 340);
        int y4= (int) (scale * 100);

        int x5= (int) (scale * 355);
        int y5= (int) (scale * 120);

        int x51= (int) (scale *360);
        int y51= (int) (scale * 160);

        int x6= (int) (scale * 340);
        int y6= (int) (scale * 185);

        int x61= (int) (scale * 320);
        int y61= (int) (scale * 170);

        Point pK1 = new Point(x1, y1);
        Point pK2 = new Point(x2, y2);
        Point pK3 = new Point(x3, y3);
        Point pK4 = new Point(x4, y4);
        Point pK5 = new Point(x5, y5);
        Point pK6 = new Point(x6, y6);
        Point pK51 = new Point(x51, y51);
        Point pK61 = new Point(x61, y61);

        pt_0.add(pK1);
        pt_0.add(pK2);

        pt_1.add(pK3);
        pt_1.add(pK4);

        pt_2.add(pK4);
        pt_2.add(pK5);

        pt_3.add(pK5);
        pt_3.add(pK51);
        pt_4.add(pK51);
        pt_4.add(pK6);
        pt_5.add(pK6);
        pt_5.add(pK61);

        point_Word_Rabbit_B_2_Array[0] = pt_0;
        point_Word_Rabbit_B_2_Array[1] = pt_1;
        point_Word_Rabbit_B_2_Array[2] = pt_2;
        point_Word_Rabbit_B_2_Array[3] = pt_3;
        point_Word_Rabbit_B_2_Array[4] = pt_4;
        point_Word_Rabbit_B_2_Array[5] = pt_5;






    }

//////////////////////////////////////////////////////////////////////////////////////////


    public static int Word_Rabbit_I_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Rabbit_I_Array = new List[Word_Rabbit_I_TOTAL_STEPS];

    public static void getRabbit_IDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();




        int xI1 = (int) (scale * 392);
        int yI1 = (int) (scale * 110);

        int xI2 = (int) (scale * 392);
        int yI2 = (int) (scale * 180);

        int xI3 = (int) (scale * 392);
        int yI3 = (int) (scale * 63);

        int xI4 = (int) (scale * 392);
        int yI4 = (int) (scale * 67);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_0.add(pI1);
        pt_0.add(pI2);

        pt_1.add(pI3);
        pt_1.add(pI4);




        point_Word_Rabbit_I_Array[0] = pt_0;
        point_Word_Rabbit_I_Array[1] = pt_1;


    }
//////////////////////////////////////////////////////////////////////////////////////////


    public static int Word_Rabbit_T_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Rabbit_T_Array = new List[Word_Rabbit_T_TOTAL_STEPS];

    public static void getRabbit_TDetails(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 437);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 437);
        int y2 = (int) (scale * 170);

        int x3 = (int) (scale * 455);
        int y3 = (int) (scale * 185);

        int x4 = (int) (scale * 425);
        int y4 = (int) (scale * 105);

        int x5 = (int) (scale * 455);
        int y5 = (int) (scale * 105);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p4);
        pt_2.add(p5);

        point_Word_Rabbit_T_Array[0] = pt_0;
        point_Word_Rabbit_T_Array[1] = pt_1;
        point_Word_Rabbit_T_Array[2] = pt_2;


    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////// Word Lamp///////////////////////////////
    public static int Word_Lamp_L_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Lamp_L_Array = new List[Word_Lamp_L_TOTAL_STEPS];

    public static void getWord_Lamp_LDetails(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();



        int xl1 = (int) (scale * 80);
        int yl1 = (int) (scale * 50);

        int xl2 = (int) (scale * 75);
        int yl2 = (int) (scale * 110);

        int xl3 = (int) (scale * 80);
        int yl3 = (int) (scale * 165);

        int xl4 = (int) (scale * 130);
        int yl4 = (int) (scale * 165);

        Point pl1 = new Point(xl1, yl1);
        Point pl2 = new Point(xl2, yl2);
        Point pl3 = new Point(xl3, yl3);
        Point pl4 = new Point(xl4, yl4);

        pt_0.add(pl1);
        pt_0.add(pl3);

        pt_1.add(pl3);
        pt_1.add(pl4);




     /*   pt_s_6.add(p8);
        pt_s_6.add(p9);*/

        point_Word_Lamp_L_Array[0] = pt_0;
        point_Word_Lamp_L_Array[1] = pt_1;






    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Lamp_A_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Lamp_A_Array = new List[Word_Lamp_A_TOTAL_STEPS];

    public static void getWord_Lamp_ADetails(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 200);
        int y1 = (int) (scale * 86);

        int x2 = (int) (scale * 170);
        int y2= (int) (scale * 100);

        int x3 = (int) (scale * 155);
        int y3= (int) (scale * 145);

        int x4 = (int) (scale * 170);
        int y4= (int) (scale * 164);

        int x5 = (int) (scale * 195);
        int y5= (int) (scale * 147);


        int x6 = (int) (scale * 210);
        int y6= (int) (scale * 86);

        int x7 = (int) (scale * 203);
        int y7= (int) (scale *160);

       /* int x8 = (int) (scale * 170);
        int y8= (int) (scale *164);

        int x9= (int) (scale * 195);
        int y9= (int) (scale *147);*/


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);
       /* Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
*/


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);
/*
        pt_5.add(p7);
        pt_5.add(p8);

        pt_6.add(p8);
        pt_6.add(p9);*/



        point_Word_Lamp_A_Array[0] = pt_0;
        point_Word_Lamp_A_Array[1] = pt_1;
        point_Word_Lamp_A_Array[2] = pt_2;
        point_Word_Lamp_A_Array[3] = pt_3;
        point_Word_Lamp_A_Array[4] = pt_4;
       /* point_Word_Lamp_A_Array[5] = pt_5;
        point_Word_Lamp_A_Array[6] = pt_6;*/


    }
////////////////////////////////////////////// Lamp M///////////////////////////////////
    public static int Word_Lamp_M_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Lamp_M_Array = new List[Word_Lamp_M_TOTAL_STEPS];

    public static void getWord_Lamp_MDetails(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int xm1 = (int) (scale * 240);
        int ym1 = (int) (scale * 80);

        int xm2 = (int) (scale * 240);
        int ym2 = (int) (scale * 118);

        int xm3 = (int) (scale * 240);
        int ym3 = (int) (scale * 162);

        //second step cordinate

        int xm4 = (int) (scale * 245);
        int ym4 = (int) (scale * 95);

        int xm5 = (int) (scale * 270);
        int ym5 = (int) (scale * 80);

        int xm6 = (int) (scale * 290);
        int ym6 = (int) (scale * 100);

        int xm7 = (int) (scale * 290);
        int ym7 = (int) (scale * 120);

        int xm8 = (int) (scale * 290);
        int ym8 = (int) (scale * 162);


        //second third cordinate

        int xm9 = (int) (scale * 295);
        int ym9 = (int) (scale * 95);

        int xm10 = (int) (scale * 315);
        int ym10 = (int) (scale * 80);

        int xm11 = (int) (scale * 335);
        int ym11 = (int) (scale * 100);

        int xm12 = (int) (scale * 335);
        int ym12 = (int) (scale * 162);

        int xm13 = (int) (scale * 335);
        int ym13 = (int) (scale * 162);


        Point p1 = new Point(xm1, ym1);
        Point p2 = new Point(xm2, ym2);
        Point p3 = new Point(xm3, ym3);
        Point p4 = new Point(xm4, ym4);
        Point p5 = new Point(xm5, ym5);
        Point p6 = new Point(xm6, ym6);
        Point p7 = new Point(xm7, ym7);
        Point p8 = new Point(xm8, ym8);
        Point p9 = new Point(xm9, ym9);
        Point p10 = new Point(xm10, ym10);
        Point p11 = new Point(xm11, ym11);
        Point p12 = new Point(xm12, ym12);
        Point p13 = new Point(xm13, ym13);



        pt_0.add(p1);
        pt_0.add(p3);

        pt_1.add(p4);
        pt_1.add(p5);

        pt_2.add(p5);
        pt_2.add(p6);

        pt_3.add(p6);
        pt_3.add(p8);
/////
        pt_4.add(p9);
        pt_4.add(p10);

        pt_5.add(p10);
        pt_5.add(p11);

        pt_6.add(p11);
        pt_6.add(p12);



        point_Word_Lamp_M_Array[0] = pt_0;
        point_Word_Lamp_M_Array[1] = pt_1;
        point_Word_Lamp_M_Array[2] = pt_2;
        point_Word_Lamp_M_Array[3] = pt_3;
        point_Word_Lamp_M_Array[4] = pt_4;
        point_Word_Lamp_M_Array[5] = pt_5;
        point_Word_Lamp_M_Array[6] = pt_6;


    }
    /////////////////////////////////////////Lamp P ////////////////////////////////////////////////
    public static int Word_Lamp_P_TOTAL_STEPS =6;

    public static List<Point>[] point_Word_Lamp_P_Array = new List[Word_Lamp_P_TOTAL_STEPS];

    public static void getWord_Lamp_PDetails(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        int xp1 = (int) (scale * 370);
        int yp1 = (int) (scale * 85);

        int xp2 = (int) (scale * 370);
        int yp2 = (int) (scale * 190);

        int xp3 = (int) (scale * 375);
        int yp3 = (int) (scale * 100);

        int xp4 = (int) (scale * 400);
        int yp4 = (int) (scale * 80);

        int xp5 = (int) (scale * 420);
        int yp5 = (int) (scale * 100);

        int xp6 = (int) (scale * 420);
        int yp6 = (int) (scale * 130);

        int xp7 = (int) (scale * 405);
        int yp7 = (int) (scale * 160);

        int xp8 = (int) (scale * 380);
        int yp8 = (int) (scale * 150);




        Point p1 = new Point(xp1, yp1);
        Point p2 = new Point(xp2, yp2);
        Point p3 = new Point(xp3, yp3);
        Point p4 = new Point(xp4, yp4);
        Point p5 = new Point(xp5, yp5);
        Point p6 = new Point(xp6, yp6);
        Point p7 = new Point(xp7, yp7);
        Point p8 = new Point(xp8, yp8);



        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        pt_2.add(p4);
        pt_2.add(p5);

        pt_3.add(p5);
        pt_3.add(p6);
/////
        pt_4.add(p6);
        pt_4.add(p7);

        pt_5.add(p7);
        pt_5.add(p8);



        point_Word_Lamp_P_Array[0] = pt_0;
        point_Word_Lamp_P_Array[1] = pt_1;
        point_Word_Lamp_P_Array[2] = pt_2;
        point_Word_Lamp_P_Array[3] = pt_3;
        point_Word_Lamp_P_Array[4] = pt_4;
        point_Word_Lamp_P_Array[5] = pt_5;




    }



    ///////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////word Mango ///////////////////////////////////////////
    public static int Word_Mango_M_TOTAL_STEPS =4;

    public static List<Point>[] point_Word_Mango_M_Array = new List[Word_Mango_M_TOTAL_STEPS];

    public static void getWord_Mango_MDetails(){



        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();

        int xM1 = (int) (scale * 35);
        int yM1 = (int) (scale * 50);

        int xM2 = (int) (scale * 35);
        int yM2 = (int) (scale * 160);

        int xM3 = (int) (scale * 78);
        int yM3 = (int) (scale * 155);

        int xM4 = (int) (scale * 125);
        int yM4 = (int) (scale * 50);

        int xM5 = (int) (scale * 125);
        int yM5 = (int) (scale * 160);

        int xM6 = (int) (scale * 40);
        int yM6 = (int) (scale * 50);

        int xM8 = (int) (scale * 120);
        int yM8 = (int) (scale * 50);

        int xM7 = (int) (scale * 83);
        int yM7 = (int) (scale * 155);
        Point pA1 = new Point(xM1, yM1);
        Point pA2 = new Point(xM2, yM2);
        Point pA3 = new Point(xM3, yM3);
        Point pA4 = new Point(xM4, yM4);
        Point pA5 = new Point(xM5, yM5);
        Point pA6 = new Point(xM6, yM6);
        Point pA7 = new Point(xM7, yM7);
        Point pA8 = new Point(xM8, yM8);


        pt_0.add(pA1);
        pt_0.add(pA2);

        pt_1.add(pA6);
        pt_1.add(pA3);

        pt_2.add(pA7);
        pt_2.add(pA8);

        pt_3.add(pA4);
        pt_3.add(pA5);

        point_Word_Mango_M_Array[0] = pt_0;
        point_Word_Mango_M_Array[1] = pt_1;
        point_Word_Mango_M_Array[2] = pt_2;
        point_Word_Mango_M_Array[3] = pt_3;






    }
    /////////////////////////////////////////Mango A/////////////////////////////////////////////////////
    public static int Word_Mango_A_TOTAL_STEPS =5;

    public static List<Point>[] point_Word_Mango_A_Array = new List[Word_Mango_A_TOTAL_STEPS];

    public static void getWord_Mango_ADetails(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 200);
        int y1 = (int) (scale * 85);

        int x2 = (int) (scale * 170);
        int y2= (int) (scale * 100);

        int x3 = (int) (scale * 160);
        int y3= (int) (scale * 145);

        int x4 = (int) (scale * 170);
        int y4= (int) (scale * 164);

        int x5 = (int) (scale * 195);
        int y5= (int) (scale * 147);


        int x6 = (int) (scale * 210);
        int y6= (int) (scale * 86);

        int x7 = (int) (scale * 203);
        int y7= (int) (scale *160);

       /* int x8 = (int) (scale * 170);
        int y8= (int) (scale *164);

        int x9= (int) (scale * 195);
        int y9= (int) (scale *147);*/


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);
       /* Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
*/


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);
/*
        pt_5.add(p7);
        pt_5.add(p8);

        pt_6.add(p8);
        pt_6.add(p9);*/



        point_Word_Mango_A_Array[0] = pt_0;
        point_Word_Mango_A_Array[1] = pt_1;
        point_Word_Mango_A_Array[2] = pt_2;
        point_Word_Mango_A_Array[3] = pt_3;
        point_Word_Mango_A_Array[4] = pt_4;
       /* point_Word_Lamp_A_Array[5] = pt_5;
        point_Word_Lamp_A_Array[6] = pt_6;*/




    }

    /////////////////////////////////////////////////Mango N////////////////////////////////////////////////
    public static int Word_Mango_N_TOTAL_STEPS =4;

    public static List<Point>[] point_Word_Mango_N_Array = new List[Word_Mango_N_TOTAL_STEPS];

    public static void getWord_Mango_NDetails(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();

        int x1 = (int) (scale * 241);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 241);
        int y2= (int) (scale * 162);

        int x3= (int) (scale * 250);
        int y3= (int) (scale * 91);

        int x6= (int) (scale * 275);
        int y6= (int) (scale * 80);

        int x4= (int) (scale * 285);
        int y4= (int) (scale * 90);

        int x5= (int) (scale * 290);
        int y5= (int) (scale * 162);

        Point pK1 = new Point(x1, y1);
        Point pK2 = new Point(x2, y2);
        Point pK3 = new Point(x3, y3);
        Point pK4 = new Point(x4, y4);
        Point pK5 = new Point(x5, y5);
        Point pK6 = new Point(x6, y6);

        pt_0.add(pK1);
        pt_0.add(pK2);

        pt_1.add(pK3);
        pt_1.add(pK6);

        pt_2.add(pK6);
        pt_2.add(pK4);

        pt_3.add(pK4);
        pt_3.add(pK5);

        point_Word_Mango_N_Array[0] = pt_0;
        point_Word_Mango_N_Array[1] = pt_1;
        point_Word_Mango_N_Array[2] = pt_2;
        point_Word_Mango_N_Array[3] = pt_3;





    }
    /////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////// Mango G//////////////////////////////////////////
    public static int Word_Mango_G_TOTAL_STEPS =8;

    public static List<Point>[] point_Word_Mango_G_Array = new List[Word_Mango_G_TOTAL_STEPS];

    public static void getWord_Mango_GDetails(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3= new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6= new ArrayList<Point>();
        List<Point> pt_7= new ArrayList<Point>();

        int xgg1 = (int) (scale * 368);
        int ygg1 = (int) (scale * 93);

        int xgg2 = (int) (scale * 345);
        int ygg2 = (int) (scale * 80);

        int xgg8 = (int) (scale * 325);
        int ygg8 = (int) (scale * 100);

        int xgg9 = (int) (scale * 330);
        int ygg9 = (int) (scale * 150);



        int xgg3 = (int) (scale * 345);
        int ygg3 = (int) (scale * 160);

        int xgg4 = (int) (scale * 370);
        int ygg4 = (int) (scale * 150);

        int xgg5 = (int) (scale * 375);
        int ygg5 = (int) (scale * 80);

        int xgg6 = (int) (scale * 375);
        int ygg6 = (int) (scale * 180);


        int xgg10 = (int) (scale * 355);
        int ygg10 = (int) (scale * 200);

        int xgg7 = (int) (scale * 335);
        int ygg7 = (int) (scale * 197);




        Point pgg1=new Point(xgg1,ygg1);
        Point pgg2=new Point(xgg2,ygg2);
        Point pgg3=new Point(xgg3,ygg3);
        Point pgg4=new Point(xgg4,ygg4);
        Point pgg5=new Point(xgg5,ygg5);
        Point pgg6=new Point(xgg6,ygg6);
        Point pgg7=new Point(xgg7,ygg7);
        Point pgg8=new Point(xgg8,ygg8);
        Point pgg9=new Point(xgg9,ygg9);
        Point pgg10=new Point(xgg10,ygg10);

        pt_0.add(pgg1);
        pt_0.add(pgg2);

        pt_1.add(pgg2);
        pt_1.add(pgg8);

        pt_2.add(pgg8);
        pt_2.add(pgg9);

        pt_3.add(pgg9);
        pt_3.add(pgg3);

        pt_4.add(pgg3);
        pt_4.add(pgg4);

        pt_5.add(pgg5);
        pt_5.add(pgg6);
        pt_6.add(pgg6);
        pt_6.add(pgg10);

        pt_7.add(pgg10);
        pt_7.add(pgg7);

        point_Word_Mango_G_Array[0] = pt_0;
        point_Word_Mango_G_Array[1] = pt_1;
        point_Word_Mango_G_Array[2] = pt_2;
        point_Word_Mango_G_Array[3] = pt_3;
        point_Word_Mango_G_Array[4] = pt_4;
        point_Word_Mango_G_Array[5] = pt_5;
        point_Word_Mango_G_Array[6] = pt_6;
        point_Word_Mango_G_Array[7] = pt_7;




    }
    //////////////////////////////////////////////Mango O/////////////////////////////////////
    public static int Word_Mango_O_TOTAL_STEPS =6;

    public static List<Point>[] point_Word_Mango_O_Array = new List[Word_Mango_O_TOTAL_STEPS];

    public static void getWord_Mango_ODetails(){


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 434);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 410);
        int y2= (int) (scale * 100);

        int x3 = (int) (scale * 410);
        int y3= (int) (scale * 140);

        int x4 = (int) (scale * 440);
        int y4= (int) (scale *165);

        int x5 = (int) (scale * 462);
        int y5= (int) (scale *140);

        int x7 = (int) (scale * 462);
        int y7= (int) (scale *100);

        int x8 = (int) (scale * 440);
        int y8= (int) (scale *80);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p5 = new Point(x5, y5);
        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        pt_4.add(p5);
        pt_4.add(p7);

        pt_5.add(p7);
        pt_5.add(p8);


        point_Word_Mango_O_Array[0] = pt_0;
        point_Word_Mango_O_Array[1] = pt_1;
        point_Word_Mango_O_Array[2] = pt_2;
        point_Word_Mango_O_Array[3] = pt_3;
        point_Word_Mango_O_Array[4] = pt_4;
        point_Word_Mango_O_Array[5] = pt_5;

    }
/*
////////////////////////////////Word Net//////////////////////////////////

    public static int Word_Net_N_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Net_N_Array = new List[Word_Net_N_TOTAL_STEPS];

    /////////////////////////////////// N ///////////////////////////////////////////////////////////

    public static void getNDetails(){



        List<Point> pt_N_0 = new ArrayList<Point>();
        List<Point> pt_N_1 = new ArrayList<Point>();
        List<Point> pt_N_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 150);
        int y1 = (int) (scale * 180);

        int x2 = (int) (scale * 150);
        int y2= (int) (scale * 65);

        int x3 = (int) (scale * 155);
        int y3= (int) (scale * 65);

        int x4= (int) (scale * 210);
        int y4= (int) (scale * 180);

        int x5= (int) (scale * 215);
        int y5= (int) (scale * 180);

        int x6= (int) (scale * 215);
        int y6= (int) (scale * 65);

        Point pK1 = new Point(x1, y1);
        Point pK2 = new Point(x2, y2);

        Point pK3= new Point(x3, y3);
        Point pK4 = new Point(x4, y4);

        Point pK5 = new Point(x5, y5);
        Point pK6 = new Point(x6, y6);

        pt_N_0.add(pK1);
        pt_N_0.add(pK2);

        pt_N_1.add(pK3);
        pt_N_1.add(pK4);

        pt_N_2.add(pK5);
        pt_N_2.add(pK6);


        point_Word_Net_N_Array[0] = pt_N_0;
        point_Word_Net_N_Array[1] = pt_N_1;
        point_Word_Net_N_Array[2] = pt_N_2;




    }
//////////////////////////////////////////////

    public static int Word_Net_e_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Net_e_Array = new List[Word_Net_e_TOTAL_STEPS];

    public static void geteDetails(){

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 267);
        int y1 = (int) (scale * 140);

        int x2 = (int) (scale * 300);
        int y2 = (int) (scale * 140);

        int x3 = (int) (scale * 295);
        int y3 = (int) (scale * 105);

        int x4 = (int) (scale * 270);
        int y4 = (int) (scale * 102);

        int x41 = (int) (scale * 253);
        int y41= (int) (scale * 115);

        int x5 = (int) (scale * 250);
        int y5 = (int) (scale * 145);

        int x6 = (int) (scale * 252);
        int y6 = (int) (scale * 160);

        int x7 = (int) (scale * 262);
        int y7 = (int) (scale * 178);

        int x8 = (int) (scale * 292);
        int y8 = (int) (scale * 180);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p9);

        pt_C_4.add(p9);
        pt_C_4.add(p5);

        pt_C_5.add(p5);
        pt_C_5.add(p6);

        pt_C_6.add(p6);
        pt_C_6.add(p7);

        pt_C_7.add(p7);
        pt_C_7.add(p8);

        point_Word_Net_e_Array[0] = pt_C_0;
        point_Word_Net_e_Array[1] = pt_C_1;
        point_Word_Net_e_Array[2] = pt_C_2;
        point_Word_Net_e_Array[3] = pt_C_3;
        point_Word_Net_e_Array[4] = pt_C_4;
        point_Word_Net_e_Array[5] = pt_C_5;
        point_Word_Net_e_Array[6] = pt_C_6;
        point_Word_Net_e_Array[7] = pt_C_7;

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////small t ///////////////////
    public static int t_TOTAL_STEPS = 3;

    public static List<Point>[] point_t_Array = new List[t_TOTAL_STEPS];
    public static void gettDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 337);
        int y1 = (int) (scale * 75);

        int x2 = (int) (scale * 337);
        int y2 = (int) (scale * 177);

        int x3 = (int) (scale * 350);
        int y3 = (int) (scale * 180);

        int x4 = (int) (scale * 317);
        int y4 = (int) (scale * 102);

        int x5 = (int) (scale * 356);
        int y5 = (int) (scale * 102);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p4);
        pt_C_2.add(p5);

        point_t_Array[0] = pt_C_0;
        point_t_Array[1] = pt_C_1;
        point_t_Array[2] = pt_C_2;

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////


*/


////////////////////////////////Word Nest//////////////////////////////////

    public static int Word_Nest_N_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Nest_N_Array = new List[Word_Nest_N_TOTAL_STEPS];

    /////////////////////////////////// N ///////////////////////////////////////////////////////////

    public static void getNDetails(){



        List<Point> pt_N_0 = new ArrayList<Point>();
        List<Point> pt_N_1 = new ArrayList<Point>();
        List<Point> pt_N_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 110);
        int y1 = (int) (scale * 180);

        int x2 = (int) (scale * 110);
        int y2= (int) (scale * 65);

        int x3 = (int) (scale * 115);
        int y3= (int) (scale * 65);

        int x4= (int) (scale * 170);
        int y4= (int) (scale * 180);

        int x5= (int) (scale * 177);
        int y5= (int) (scale * 180);

        int x6= (int) (scale * 175);
        int y6= (int) (scale * 65);

        Point pK1 = new Point(x1, y1);
        Point pK2 = new Point(x2, y2);

        Point pK3= new Point(x3, y3);
        Point pK4 = new Point(x4, y4);

        Point pK5 = new Point(x5, y5);
        Point pK6 = new Point(x6, y6);

        pt_N_0.add(pK2);
        pt_N_0.add(pK1);

        pt_N_1.add(pK3);
        pt_N_1.add(pK4);

        pt_N_2.add(pK5);
        pt_N_2.add(pK6);


        point_Word_Nest_N_Array[0] = pt_N_0;
        point_Word_Nest_N_Array[1] = pt_N_1;
        point_Word_Nest_N_Array[2] = pt_N_2;




    }
//////////////////////////////////////////////

    public static int Word_Nest_e_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Nest_e_Array = new List[Word_Nest_e_TOTAL_STEPS];

    public static void geteDetails(){

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 220);
        int y1 = (int) (scale * 140);

        int x2 = (int) (scale * 260);
        int y2 = (int) (scale * 140);

        int x3 = (int) (scale * 255);
        int y3 = (int) (scale * 105);

        int x4 = (int) (scale * 230);
        int y4 = (int) (scale * 102);

        int x41 = (int) (scale * 213);
        int y41= (int) (scale * 115);

        int x5 = (int) (scale * 210);
        int y5 = (int) (scale * 145);

        int x6 = (int) (scale * 212);
        int y6 = (int) (scale * 160);

        int x7 = (int) (scale * 222);
        int y7 = (int) (scale * 178);

        int x8 = (int) (scale * 254);
        int y8 = (int) (scale * 180);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p9);

        pt_C_4.add(p9);
        pt_C_4.add(p5);

        pt_C_5.add(p5);
        pt_C_5.add(p6);

        pt_C_6.add(p6);
        pt_C_6.add(p7);

        pt_C_7.add(p7);
        pt_C_7.add(p8);

        point_Word_Nest_e_Array[0] = pt_C_0;
        point_Word_Nest_e_Array[1] = pt_C_1;
        point_Word_Nest_e_Array[2] = pt_C_2;
        point_Word_Nest_e_Array[3] = pt_C_3;
        point_Word_Nest_e_Array[4] = pt_C_4;
        point_Word_Nest_e_Array[5] = pt_C_5;
        point_Word_Nest_e_Array[6] = pt_C_6;
        point_Word_Nest_e_Array[7] = pt_C_7;

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
    public static int Word_Nest_s_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Nest_s_Array = new List[Word_Nest_s_TOTAL_STEPS];

    public static void getWord_Nest_sDetails(){


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        int x1 = (int) (scale * 335);
        int y1 = (int) (scale * 115);

        int x2 = (int) (scale * 315);
        int y2= (int) (scale * 100);

        int x3 = (int) (scale * 295);
        int y3= (int) (scale * 110);

        int x9= (int) (scale * 300);
        int y9= (int) (scale *140);

        int x4 = (int) (scale * 325);
        int y4= (int) (scale *145);

        int x5 = (int) (scale * 330);
        int y5= (int) (scale *170);

        int x7 = (int) (scale * 315);
        int y7= (int) (scale *185);

        int x8 = (int) (scale * 295);
        int y8= (int) (scale *170);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p5 = new Point(x5, y5);
        Point p9 = new Point(x9, y9);



        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p9);

        pt_s_3.add(p9);
        pt_s_3.add(p4);

        pt_s_4.add(p4);
        pt_s_4.add(p5);

        pt_s_5.add(p5);
        pt_s_5.add(p7);

        pt_s_6.add(p7);
        pt_s_6.add(p8);

        point_Word_Nest_s_Array[0] = pt_s_0;
        point_Word_Nest_s_Array[1] = pt_s_1;
        point_Word_Nest_s_Array[2] = pt_s_2;
        point_Word_Nest_s_Array[3] = pt_s_3;
        point_Word_Nest_s_Array[4] = pt_s_4;
        point_Word_Nest_s_Array[5] = pt_s_5;
        point_Word_Nest_s_Array[6] = pt_s_6;


    }

    //////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////small t ///////////////////
    public static int t_TOTAL_STEPS = 3;

    public static List<Point>[] point_t_Array = new List[t_TOTAL_STEPS];
    public static void gettDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 374);
        int y1 = (int) (scale * 75);

        int x2 = (int) (scale * 375);
        int y2 = (int) (scale * 177);

        int x3 = (int) (scale * 388);
        int y3 = (int) (scale * 180);

        int x4 = (int) (scale * 355);
        int y4 = (int) (scale * 102);

        int x5 = (int) (scale * 393);
        int y5 = (int) (scale * 102);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p4);
        pt_C_2.add(p5);

        point_t_Array[0] = pt_C_0;
        point_t_Array[1] = pt_C_1;
        point_t_Array[2] = pt_C_2;

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

/*/
/*//*
//////////////////////////////Word Ox//////////////////////////////////

    ////////////////////////////// O//////////////////////////////
    public static int O_TOTAL_STEPS = 6;

    public static List<Point>[] point_O_Array = new List[O_TOTAL_STEPS];
    public static void getODetails(){



        List<Point> pt_O_0 = new ArrayList<Point>();
        List<Point> pt_O_1 = new ArrayList<Point>();
        List<Point> pt_O_2 = new ArrayList<Point>();
        List<Point> pt_O_3 = new ArrayList<Point>();
        List<Point> pt_O_4 = new ArrayList<Point>();
        List<Point> pt_O_5 = new ArrayList<Point>();




        int x1 = (int) (scale * 209);
        int y1 = (int) (scale * 70);

        int x2 = (int) (scale * 177);
        int y2= (int) (scale *88);

        int x3 = (int) (scale * 177);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 209);
        int y4 = (int) (scale * 183);

        int x5 = (int) (scale * 240);
        int y5 = (int) (scale * 160);

        int x6 = (int) (scale * 240);
        int y6 = (int) (scale * 88);

        int x7= (int) (scale * 207);
        int y7= (int) (scale * 70);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);






        pt_O_0.add(p1);
        pt_O_0.add(p2);

        pt_O_1.add(p2);
        pt_O_1.add(p3);

        pt_O_2.add(p3);
        pt_O_2.add(p4);

        pt_O_3.add(p4);
        pt_O_3.add(p5);

        pt_O_4.add(p5);
        pt_O_4.add(p6);

        pt_O_5.add(p6);
        pt_O_5.add(p7);





        point_O_Array[0] = pt_O_0;
        point_O_Array[1] =pt_O_1;
        point_O_Array[2] = pt_O_2;
        point_O_Array[3] = pt_O_3;
        point_O_Array[4] = pt_O_4;
        point_O_Array[5] = pt_O_5;




    }
    /////////////////////////////////////////smallOx x ///////////////////
    public static int x_TOTAL_STEPS = 2;

    public static List<Point>[] point_x_Array = new List[x_TOTAL_STEPS];
    public static void getxDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();


        int x1 = (int) (scale * 277);
        int y1 = (int) (scale * 101);

        int x2 = (int) (scale * 317);
        int y2 = (int) (scale * 174);

        int x3 = (int) (scale * 317);
        int y3 = (int) (scale * 101);

        int x4 = (int) (scale * 279);
        int y4 = (int) (scale * 174);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);


        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p3);
        pt_C_1.add(p4);





        /*point_x_Array[0] = pt_C_0;
        point_x_Array[1] = pt_C_1;
*/

    /*}*/
/////////////////////////////////////////////////////////////////////////////

///////////////////////////////Word OWL//////////////////////////////////

    ////////////////////////////// O//////////////////////////////
    public static int O_TOTAL_STEPS = 6;

    public static List<Point>[] point_O_Array = new List[O_TOTAL_STEPS];
    public static void getODetails(){



        List<Point> pt_O_0 = new ArrayList<Point>();
        List<Point> pt_O_1 = new ArrayList<Point>();
        List<Point> pt_O_2 = new ArrayList<Point>();
        List<Point> pt_O_3 = new ArrayList<Point>();
        List<Point> pt_O_4 = new ArrayList<Point>();
        List<Point> pt_O_5 = new ArrayList<Point>();

        int x1 = (int) (scale * 177);
        int y1 = (int) (scale * 70);

        int x2 = (int) (scale * 145);
        int y2= (int) (scale *88);

        int x3 = (int) (scale * 145);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 177);
        int y4 = (int) (scale * 185);

        int x5 = (int) (scale * 208);
        int y5 = (int) (scale * 160);

        int x6 = (int) (scale * 208);
        int y6 = (int) (scale * 88);

        int x7= (int) (scale * 180);
        int y7= (int) (scale * 70);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);

        pt_O_0.add(p1);
        pt_O_0.add(p2);

        pt_O_1.add(p2);
        pt_O_1.add(p3);

        pt_O_2.add(p3);
        pt_O_2.add(p4);

        pt_O_3.add(p4);
        pt_O_3.add(p5);

        pt_O_4.add(p5);
        pt_O_4.add(p6);

        pt_O_5.add(p6);
        pt_O_5.add(p7);

        point_O_Array[0] = pt_O_0;
        point_O_Array[1] =pt_O_1;
        point_O_Array[2] = pt_O_2;
        point_O_Array[3] = pt_O_3;
        point_O_Array[4] = pt_O_4;
        point_O_Array[5] = pt_O_5;




    }
    /////////////////////////////////////////smallwl w ///////////////////
    public static int w_TOTAL_STEPS = 4;

    public static List<Point>[] point_w_Array = new List[w_TOTAL_STEPS];
    public static void getwDetails(){



        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();



        int x1 = (int) (scale * 240);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 259);
        int y2 = (int) (scale * 187);

        int x3 = (int) (scale * 283);
        int y3 = (int) (scale * 105);

        int x4 = (int) (scale * 307);
        int y4 = (int) (scale * 187);

        int x5 = (int) (scale * 325);
        int y5 = (int) (scale * 105);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);

        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);


        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p5);



        point_w_Array[0] = pt_C_0;
        point_w_Array[1] = pt_C_1;
        point_w_Array[2] = pt_C_2;
        point_w_Array[3] = pt_C_3;


    }
/////////////////////////////////////////////////////////////////////////////

    public static int Word_Owl_L_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Owl_L_Array = new List[Word_Owl_L_TOTAL_STEPS];

    public static void getWord_Owl_LDetails() {


        List<Point> pt_0 = new ArrayList<Point>();


        int x1 = (int) (scale * 358);
        int y1 = (int) (scale * 60);

        int x2 = (int) (scale * 358);
        int y2 = (int) (scale * 185);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);


        pt_0.add(p1);
        pt_0.add(p2);


        point_Word_Owl_L_Array[0] = pt_0;


    }



 /*   /////////////////////////////////////////////Word Peacock///////////////////////////////////////////////

    //////////////////////////////////////////// P ////////////////////////////////////////////
    public static int Word_Peacock_P_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Peacock_P_Array = new List[Word_Peacock_P_TOTAL_STEPS];
    public static void getWord_Peacock_P_Details(){
        List<Point> pt_P_0 = new ArrayList<Point>();
        List<Point> pt_P_1 = new ArrayList<Point>();
        List<Point> pt_P_2 = new ArrayList<Point>();
        List<Point> pt_P_3 = new ArrayList<Point>();
        List<Point> pt_P_4 = new ArrayList<Point>();

        int x1 = (int) (scale * 42);
        int y1 = (int) (scale * 75);

        int x2 = (int) (scale * 42);
        int y2= (int) (scale * 185);

        int x3 = (int) (scale * 50);
        int y3 = (int) (scale * 75);

        int x4 = (int) (scale * 80);
        int y4 = (int) (scale * 82);

        int x5 = (int) (scale * 88);
        int y5 = (int) (scale * 110);

        int x6 = (int) (scale * 75);
        int y6 = (int) (scale * 133);

        int x7= (int) (scale * 50);
        int y7= (int) (scale * 135);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);


        pt_P_0.add(p1);
        pt_P_0.add(p2);

        pt_P_1.add(p3);
        pt_P_1.add(p4);

        pt_P_2.add(p4);
        pt_P_2.add(p5);

        pt_P_3.add(p5);
        pt_P_3.add(p6);

        pt_P_4.add(p6);
        pt_P_4.add(p7);




        point_Word_Peacock_P_Array[0] = pt_P_0;
        point_Word_Peacock_P_Array[1] =pt_P_1;
        point_Word_Peacock_P_Array[2] = pt_P_2;
        point_Word_Peacock_P_Array[3] = pt_P_3;
        point_Word_Peacock_P_Array[4] = pt_P_4;




    }
    ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////  e  /////////////////////////////////////////////////////////////
    public static int Word_Peacock_e_TOTAL_STEPS =7;

    public static List<Point>[] point_Word_Peacock_e_Array = new List[Word_Peacock_e_TOTAL_STEPS];
    public static void getWord_Peacock_e_Details(){

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();



        int x1 = (int) (scale * 110);
        int y1 = (int) (scale * 145);

        int x2 = (int) (scale * 145);
        int y2 = (int) (scale *  145);

        int x3 = (int) (scale * 145);
        int y3 = (int) (scale *  115);

        int x4 = (int) (scale * 123);
        int y4 = (int) (scale *  110);

        int x5 = (int) (scale * 110);
        int y5 = (int) (scale * 145);

        int x6 = (int) (scale * 116);
        int y6 = (int) (scale * 175);

        int x7 = (int) (scale * 125);
        int y7 = (int) (scale * 185);

        int x8 = (int) (scale * 147);
        int y8 = (int) (scale * 185);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);


        pt_4.add(p5);
        pt_4.add(p6);

        pt_5.add(p6);
        pt_5.add(p7);

        pt_6.add(p7);
        pt_6.add(p8);


        point_Word_Peacock_e_Array[0] = pt_0;
        point_Word_Peacock_e_Array[1] = pt_1;
        point_Word_Peacock_e_Array[2] = pt_2;
        point_Word_Peacock_e_Array[3] = pt_3;
        point_Word_Peacock_e_Array[4] = pt_4;
        point_Word_Peacock_e_Array[5] = pt_5;
        point_Word_Peacock_e_Array[6] = pt_6;


    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////  a  //////////////////////////////////////////////////////////////////
    public static int Word_Peacock_a_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Peacock_a_Array = new List[Word_Peacock_a_TOTAL_STEPS];

    public static void getWord_Peacock_a_Details() {


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 205);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 178);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 168);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 175);
        int y4 = (int) (scale * 185);

        int x5 = (int) (scale * 201);
        int y5 = (int) (scale * 168);


        int x6 = (int) (scale * 212);
        int y6 = (int) (scale * 105);

        int x7 = (int) (scale * 205);
        int y7 = (int) (scale * 185);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);

        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);
/////
        pt_s_4.add(p6);
        pt_s_4.add(p7);

        point_Word_Peacock_a_Array[0] = pt_s_0;
        point_Word_Peacock_a_Array[1] = pt_s_1;
        point_Word_Peacock_a_Array[2] = pt_s_2;
        point_Word_Peacock_a_Array[3] = pt_s_3;
        point_Word_Peacock_a_Array[4] = pt_s_4;


    }

    ///////////////////////////////////////// c ///////////////////
    public static int Word_Peacock_c_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Peacock_c_Array = new List[Word_Peacock_c_TOTAL_STEPS];
    public static void getWord_Peacock_c_Details(){

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();



        int x1 = (int) (scale * 270);
        int y1 = (int) (scale * 120);

        int x3 = (int) (scale * 250);
        int y3 = (int) (scale * 105);

        int x31 = (int) (scale * 235);
        int y31 = (int) (scale * 120);

        int x4 = (int) (scale * 232);
        int y4 = (int) (scale * 155);

        int x41 = (int) (scale * 235);
        int y41 = (int) (scale * 170);

        int x5 = (int) (scale * 250);
        int y5 = (int) (scale * 183);

        int x6 = (int) (scale * 267);
        int y6 = (int) (scale * 170);


        Point p1 = new Point(x1, y1);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p31 = new Point(x31, y31);
        Point p41 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p31);

        pt_C_3.add(p31);
        pt_C_3.add(p4);

        pt_C_4.add(p4);
        pt_C_4.add(p41);

        pt_C_5.add(p41);
        pt_C_5.add(p5);

        pt_C_6.add(p5);
        pt_C_6.add(p6);

        point_Word_Peacock_c_Array[0] = pt_C_0;
        point_Word_Peacock_c_Array[1] = pt_C_2;
        point_Word_Peacock_c_Array[2] = pt_C_3;
        point_Word_Peacock_c_Array[3] = pt_C_4;
        point_Word_Peacock_c_Array[4] = pt_C_5;
        point_Word_Peacock_c_Array[5] = pt_C_6;

    }
    ///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// o ///////////////////
    public static int Word_Peacock_o_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Peacock_o_Array = new List[Word_Peacock_o_TOTAL_STEPS];
    public static void getWord_Peacock_o_Details(){

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();



        int x1 = (int) (scale * 330);
        int y1 = (int) (scale * 120);

        int x3 = (int) (scale * 310);
        int y3 = (int) (scale * 105);

        int x31 = (int) (scale * 295);
        int y31 = (int) (scale * 120);

        int x4 = (int) (scale * 295);
        int y4 = (int) (scale * 155);

        int x41 = (int) (scale * 295);
        int y41 = (int) (scale * 170);

        int x5 = (int) (scale * 310);
        int y5 = (int) (scale * 183);

        int x6 = (int) (scale * 330);
        int y6 = (int) (scale * 170);


        Point p1 = new Point(x1, y1);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p31 = new Point(x31, y31);
        Point p41 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p31);

        pt_C_3.add(p31);
        pt_C_3.add(p4);

        pt_C_4.add(p4);
        pt_C_4.add(p41);

        pt_C_5.add(p41);
        pt_C_5.add(p5);

        pt_C_6.add(p5);
        pt_C_6.add(p6);

        pt_C_1.add(p6);
        pt_C_1.add(p1);

        point_Word_Peacock_o_Array[0] = pt_C_0;
        point_Word_Peacock_o_Array[1] = pt_C_2;
        point_Word_Peacock_o_Array[2] = pt_C_3;
        point_Word_Peacock_o_Array[3] = pt_C_4;
        point_Word_Peacock_o_Array[4] = pt_C_5;
        point_Word_Peacock_o_Array[5] = pt_C_6;
        point_Word_Peacock_o_Array[6] = pt_C_1;

    }
    ///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// c2 ///////////////////
    public static int Word_Peacock_c2_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Peacock_c2_Array = new List[Word_Peacock_c2_TOTAL_STEPS];
    public static void getWord_Peacock_c2_Details(){

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();



        int x1 = (int) (scale * 392);
        int y1 = (int) (scale * 120);

        int x3 = (int) (scale * 378);
        int y3 = (int) (scale * 105);

        int x31 = (int) (scale * 360);
        int y31 = (int) (scale * 120);

        int x4 = (int) (scale * 360);
        int y4 = (int) (scale * 155);

        int x41 = (int) (scale * 360);
        int y41 = (int) (scale * 174);

        int x5 = (int) (scale * 375);
        int y5 = (int) (scale * 183);

        int x6 = (int) (scale * 392);
        int y6 = (int) (scale * 170);


        Point p1 = new Point(x1, y1);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p31 = new Point(x31, y31);
        Point p41 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p31);

        pt_C_3.add(p31);
        pt_C_3.add(p4);

        pt_C_4.add(p4);
        pt_C_4.add(p41);

        pt_C_5.add(p41);
        pt_C_5.add(p5);

        pt_C_6.add(p5);
        pt_C_6.add(p6);

        point_Word_Peacock_c2_Array[0] = pt_C_0;
        point_Word_Peacock_c2_Array[1] = pt_C_2;
        point_Word_Peacock_c2_Array[2] = pt_C_3;
        point_Word_Peacock_c2_Array[3] = pt_C_4;
        point_Word_Peacock_c2_Array[4] = pt_C_5;
        point_Word_Peacock_c2_Array[5] = pt_C_6;

    }
///////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////// k ////////////////////////////////////////
    public static int Word_Peacock_k_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Peacock_k_Array = new List[Word_Peacock_k_TOTAL_STEPS];


    public static void getWord_Peacock_k_Details(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();


        int xK1 = (int) (scale * 420);
        int yK1 = (int) (scale * 60);

        int xK2 = (int) (scale * 420);
        int yK2= (int) (scale * 185);

        int xK3= (int) (scale * 423);
        int yK3= (int) (scale * 140);

        int xK4= (int) (scale * 439);
        int yK4= (int) (scale * 140);

        int xK5= (int) (scale * 437);
        int yK5= (int) (scale * 132);

        int xK6= (int) (scale * 455);
        int yK6= (int) (scale * 100);

        int xK7= (int) (scale * 455);
        int yK7= (int) (scale * 185);


        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3= new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(xK7, yK7);



        pt_K_0.add(pK1);
        pt_K_0.add(pK2);
        pt_K_1.add(pK3);
        pt_K_1.add(pK4);
        pt_K_2.add(pK5);
        pt_K_2.add(pK6);
        pt_K_3.add(pK5);
        pt_K_3.add(pK7);




        point_Word_Peacock_k_Array[0] = pt_K_0;
        point_Word_Peacock_k_Array[1] = pt_K_1;
        point_Word_Peacock_k_Array[2] = pt_K_2;
        point_Word_Peacock_k_Array[3] = pt_K_3;

    }
///////////////////////////////////////////////////////////////////////////////////////////////////////


*/

    /////////////////////////////////////////////Word Pig///////////////////////////////////////////////

    //////////////////////////////////////////// P ////////////////////////////////////////////
    public static int Word_Pig_P_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Pig_P_Array = new List[Word_Pig_P_TOTAL_STEPS];
    public static void getWord_Pig_P_Details(){
        List<Point> pt_P_0 = new ArrayList<Point>();
        List<Point> pt_P_1 = new ArrayList<Point>();
        List<Point> pt_P_2 = new ArrayList<Point>();
        List<Point> pt_P_3 = new ArrayList<Point>();
        List<Point> pt_P_4 = new ArrayList<Point>();

        int x1 = (int) (scale * 161);
        int y1 = (int) (scale * 55);

        int x2 = (int) (scale * 161);
        int y2= (int) (scale * 170);

        int x3 = (int) (scale * 165);
        int y3 = (int) (scale * 55);

        int x4 = (int) (scale * 200);
        int y4 = (int) (scale * 55);

        int x5 = (int) (scale * 217);
        int y5 = (int) (scale * 90);

        int x6 = (int) (scale * 210);
        int y6 = (int) (scale * 113);

        int x7= (int) (scale * 170);
        int y7= (int) (scale * 115);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);


        pt_P_0.add(p1);
        pt_P_0.add(p2);

        pt_P_1.add(p3);
        pt_P_1.add(p4);

        pt_P_2.add(p4);
        pt_P_2.add(p5);

        pt_P_3.add(p5);
        pt_P_3.add(p6);

        pt_P_4.add(p6);
        pt_P_4.add(p7);




        point_Word_Pig_P_Array[0] = pt_P_0;
        point_Word_Pig_P_Array[1] =pt_P_1;
        point_Word_Pig_P_Array[2] = pt_P_2;
        point_Word_Pig_P_Array[3] = pt_P_3;
        point_Word_Pig_P_Array[4] = pt_P_4;




    }
    ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////  i  /////////////////////////////////////////////////////////////
    public static int Word_Pig_i_TOTAL_STEPS =2;

    public static List<Point>[] point_Word_Pig_i_Array = new List[Word_Pig_i_TOTAL_STEPS];
    public static void getWord_Pig_i_Details(){

        List<Point> pt_l_0 = new ArrayList<Point>();
        List<Point> pt_l_1 = new ArrayList<Point>();


        int xI1 = (int) (scale * 254);
        int yI1 = (int) (scale * 40);

        int xI2 = (int) (scale * 254);
        int yI2 = (int) (scale * 45);

        int xI3 = (int) (scale * 254);
        int yI3 = (int) (scale * 86);

        int xI4 = (int) (scale * 254);
        int yI4 = (int) (scale * 167);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        pt_l_1.add(pI3);
        pt_l_1.add(pI4);


        point_Word_Pig_i_Array[0] = pt_l_1;
        point_Word_Pig_i_Array[1] = pt_l_0;

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////  g  //////////////////////////////////////////////////////////////////
    public static int Word_Pig_g_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Pig_g_Array = new List[Word_Pig_g_TOTAL_STEPS];

    public static void getWord_Pig_g_Details() {

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3= new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5= new ArrayList<Point>();
        List<Point> pt_6= new ArrayList<Point>();
        List<Point> pt_7= new ArrayList<Point>();

        int xgg1 = (int) (scale * 333);
        int ygg1 = (int) (scale * 100);

        int xgg2 = (int) (scale * 305);
        int ygg2 = (int) (scale * 88);

        int xgg8 = (int) (scale * 290);
        int ygg8 = (int) (scale * 106);

        int xgg9 = (int) (scale * 293);
        int ygg9 = (int) (scale * 156);



        int xgg3 = (int) (scale * 305);
        int ygg3 = (int) (scale * 166);

        int xgg4 = (int) (scale * 333);
        int ygg4 = (int) (scale * 156);

        int xgg5 = (int) (scale * 340);
        int ygg5 = (int) (scale * 86);

        int xgg6 = (int) (scale * 340);
        int ygg6 = (int) (scale * 186);


        int xgg10 = (int) (scale * 326);
        int ygg10 = (int) (scale * 206);

        int xgg7 = (int) (scale * 300);
        int ygg7 = (int) (scale * 201);

        Point pgg1=new Point(xgg1,ygg1);
        Point pgg2=new Point(xgg2,ygg2);
        Point pgg3=new Point(xgg3,ygg3);
        Point pgg4=new Point(xgg4,ygg4);
        Point pgg5=new Point(xgg5,ygg5);
        Point pgg6=new Point(xgg6,ygg6);
        Point pgg7=new Point(xgg7,ygg7);
        Point pgg8=new Point(xgg8,ygg8);
        Point pgg9=new Point(xgg9,ygg9);
        Point pgg10=new Point(xgg10,ygg10);

        pt_0.add(pgg1);
        pt_0.add(pgg2);

        pt_1.add(pgg2);
        pt_1.add(pgg8);

        pt_2.add(pgg8);
        pt_2.add(pgg9);

        pt_3.add(pgg9);
        pt_3.add(pgg3);

        pt_4.add(pgg3);
        pt_4.add(pgg4);

        pt_5.add(pgg5);
        pt_5.add(pgg6);
        pt_6.add(pgg6);
        pt_6.add(pgg10);

        pt_7.add(pgg10);
        pt_7.add(pgg7);


        point_Word_Pig_g_Array[0] = pt_0;
        point_Word_Pig_g_Array[1] = pt_1;
        point_Word_Pig_g_Array[2] = pt_2;
        point_Word_Pig_g_Array[3] = pt_3;
        point_Word_Pig_g_Array[4] = pt_4;
        point_Word_Pig_g_Array[5] = pt_5;
        point_Word_Pig_g_Array[6] = pt_6;
        point_Word_Pig_g_Array[7] = pt_7;
    }



    /////////////////////////////////////////Word Train/////////////////////////////////////////////////

    public static int Word_Train_T_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Train_T_Array = new List[Word_Train_T_TOTAL_STEPS];

    public static void getWord_Train_TDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();


        int x1 = (int) (scale * 95);
        int y1 = (int) (scale * 74);

        int x2 = (int) (scale * 160);
        int y2 = (int) (scale * 74);

        int x3 = (int) (scale * 125);
        int y3 = (int) (scale * 79);

        int x4 = (int) (scale * 125);
        int y4 = (int) (scale * 180);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        point_Word_Train_T_Array[0] = pt_0;
        point_Word_Train_T_Array[1] = pt_1;
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Train_R_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Train_R_Array = new List[Word_Train_R_TOTAL_STEPS];

    public static void getWord_Train_RDetails() {


        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();

        int x1 = (int) (scale * 185);
        int y1 = (int) (scale * 110);

        int x2 = (int) (scale * 185);
        int y2 = (int) (scale * 180);

        int x3 = (int) (scale * 190);
        int y3 = (int) (scale * 125);

        int x4 = (int) (scale * 205);
        int y4 = (int) (scale * 110);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p3);
        pt_C_1.add(p4);

        point_Word_Train_R_Array[0] = pt_C_0;
        point_Word_Train_R_Array[1] = pt_C_1;
    }

    //////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Train_A_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Train_A_Array = new List[Word_Train_A_TOTAL_STEPS];

    public static void getWord_Train_ADetails() {


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 275);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 240);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 230);
        int y3 = (int) (scale * 175);

        int x4 = (int) (scale * 245);
        int y4 = (int) (scale * 185);

        int x5 = (int) (scale * 272);
        int y5 = (int) (scale * 171);


        int x6 = (int) (scale * 285);
        int y6 = (int) (scale * 105);

        int x7 = (int) (scale * 277);
        int y7 = (int) (scale * 185);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);



        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);
/////
        pt_s_4.add(p6);
        pt_s_4.add(p7);




        point_Word_Train_A_Array[0] = pt_s_0;
        point_Word_Train_A_Array[1] = pt_s_1;
        point_Word_Train_A_Array[2] = pt_s_2;
        point_Word_Train_A_Array[3] = pt_s_3;
        point_Word_Train_A_Array[4] = pt_s_4;

    }

    //////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Train_I_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Train_I_Array = new List[Word_Train_I_TOTAL_STEPS];

    public static void getWord_Train_IDetails() {


        List<Point> pt_l_0 = new ArrayList<Point>();
        List<Point> pt_l_1 = new ArrayList<Point>();


        int xI1 = (int) (scale * 315);
        int yI1 = (int) (scale * 60);

        int xI2 = (int) (scale * 315);
        int yI2 = (int) (scale * 64);

        int xI3 = (int) (scale * 315);
        int yI3 = (int) (scale * 108);

        int xI4 = (int) (scale * 315);
        int yI4 = (int) (scale * 185);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        pt_l_1.add(pI3);
        pt_l_1.add(pI4);

        point_Word_Train_I_Array[0] = pt_l_1;
        point_Word_Train_I_Array[1] = pt_l_0;
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Train_N_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Train_N_Array = new List[Word_Train_N_TOTAL_STEPS];

    public static void getWord_Train_NDetails() {


        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 355);
        int yK1 = (int) (scale * 110);

        int xK2 = (int) (scale * 355);
        int yK2 = (int) (scale * 180);

        int xK3 = (int) (scale * 360);
        int yK3 = (int) (scale * 117);

        int x3 = (int) (scale * 390);
        int y3 = (int) (scale * 107);

        int xK4 = (int) (scale * 400);
        int yK4 = (int) (scale * 125);

        int xK5 = (int) (scale * 402);
        int yK5 = (int) (scale * 180);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(x3, y3);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(pK6);

        pt_K_2.add(pK6);
        pt_K_2.add(pK4);

        pt_K_3.add(pK4);
        pt_K_3.add(pK5);

        point_Word_Train_N_Array[0] = pt_K_0;
        point_Word_Train_N_Array[1] = pt_K_1;
        point_Word_Train_N_Array[2] = pt_K_2;
        point_Word_Train_N_Array[3] = pt_K_3;

    }

    //////////////////////////////////////////////////////////////////////////////////////////




    ///////////////////////////////////WORD UMBRELLA/////////////////////////////////////////
    public static int Word_Umbrella_U_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Umbrella_U_Array = new List[Word_Umbrella_U_TOTAL_STEPS];

    public static void getWord_Umbrella_UDetails() {


        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();
        List<Point> pt_K_4 = new ArrayList<Point>();
        List<Point> pt_K_5 = new ArrayList<Point>();

        int xK1 = (int) (scale * 30);
        int yK1 = (int) (scale * 70);

        int xK2 = (int) (scale * 30);
        int yK2 = (int) (scale * 155);

        int xK3 = (int) (scale * 40);
        int yK3 = (int) (scale * 180);

        int x3 = (int) (scale * 55);
        int y3 = (int) (scale * 185);

        int xK4 = (int) (scale * 70);
        int yK4 = (int) (scale * 180);

        int xK5 = (int) (scale * 80);
        int yK5 = (int) (scale * 155);

        int xK6 = (int) (scale * 80);
        int yK6 = (int) (scale * 70);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(x3, y3);
        Point pK7 = new Point(xK6, yK6);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK2);
        pt_K_1.add(pK3);

        pt_K_2.add(pK3);
        pt_K_2.add(pK6);

        pt_K_3.add(pK6);
        pt_K_3.add(pK4);

        pt_K_4.add(pK4);
        pt_K_4.add(pK5);

        pt_K_5.add(pK5);
        pt_K_5.add(pK7);


        point_Word_Umbrella_U_Array[0] = pt_K_0;
        point_Word_Umbrella_U_Array[1] = pt_K_1;
        point_Word_Umbrella_U_Array[2] = pt_K_2;
        point_Word_Umbrella_U_Array[3] = pt_K_3;
        point_Word_Umbrella_U_Array[4] = pt_K_4;
        point_Word_Umbrella_U_Array[5] = pt_K_5;

    }

    //////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Umbrella_M_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Umbrella_M_Array = new List[Word_Umbrella_M_TOTAL_STEPS];

    public static void getWord_Umbrella_MDetails() {


        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();
        List<Point> pt_K_4 = new ArrayList<Point>();
        List<Point> pt_K_5 = new ArrayList<Point>();
        List<Point> pt_K_6 = new ArrayList<Point>();

        int xK1 = (int) (scale * 110);
        int yK1 = (int) (scale * 105);

        int xK2 = (int) (scale * 110);
        int yK2= (int) (scale * 185);

        int xK3= (int) (scale * 119);
        int yK3= (int) (scale * 115);

        int x3= (int) (scale * 130);
        int y3= (int) (scale * 105);

        int xK4= (int) (scale * 144);
        int yK4= (int) (scale * 120);

        int xK5= (int) (scale * 144);
        int yK5= (int) (scale * 185);
/////
        int xK6= (int) (scale * 150);
        int yK6= (int) (scale * 118);

        int x6= (int) (scale * 161);
        int y6= (int) (scale * 105);

        int xK7= (int) (scale * 180);
        int yK7= (int) (scale * 120);

        int xK8= (int) (scale * 180);
        int yK8= (int) (scale * 185);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point p3 = new Point(x3, y3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point p6 = new Point(x6, y6);
        Point pK7 = new Point(xK7, yK7);
        Point pK8 = new Point(xK8, yK8);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(p3);

        pt_K_2.add(p3);
        pt_K_2.add(pK4);

        pt_K_3.add(pK4);
        pt_K_3.add(pK5);

        pt_K_4.add(pK6);
        pt_K_4.add(p6);

        pt_K_5.add(p6);
        pt_K_5.add(pK7);

        pt_K_6.add(pK7);
        pt_K_6.add(pK8);

        point_Word_Umbrella_M_Array[0] = pt_K_0;
        point_Word_Umbrella_M_Array[1] = pt_K_1;
        point_Word_Umbrella_M_Array[2] = pt_K_2;
        point_Word_Umbrella_M_Array[3] = pt_K_3;
        point_Word_Umbrella_M_Array[4] = pt_K_4;
        point_Word_Umbrella_M_Array[5] = pt_K_5;
        point_Word_Umbrella_M_Array[6] = pt_K_6;


    }
///////////////////////////////////////////////////

    public static int Word_Umbrella_B_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Umbrella_B_Array = new List[Word_Umbrella_B_TOTAL_STEPS];

    public static void getWord_Umbrella_BDetails() {

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();
        List<Point> pt_K_4 = new ArrayList<Point>();
        List<Point> pt_K_5 = new ArrayList<Point>();

        int xK1 = (int) (scale * 206);
        int yK1 = (int) (scale * 60);

        int xK2 = (int) (scale * 206);
        int yK2= (int) (scale * 185);

        int xK3= (int) (scale * 220);
        int yK3= (int) (scale * 110);

        int xK4= (int) (scale * 230);
        int yK4= (int) (scale * 105);

        int xK5= (int) (scale * 245);
        int yK5= (int) (scale * 120);

        int xK6= (int) (scale * 245);
        int yK6= (int) (scale * 170);

        int xK7= (int) (scale * 230);
        int yK7= (int) (scale * 185);

        int xK8= (int) (scale * 220);
        int yK8= (int) (scale * 175);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(xK7, yK7);
        Point pK8 = new Point(xK8, yK8);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(pK4);

        pt_K_2.add(pK4);
        pt_K_2.add(pK5);

        pt_K_3.add(pK5);
        pt_K_3.add(pK6);

        pt_K_4.add(pK6);
        pt_K_4.add(pK7);

        pt_K_5.add(pK7);
        pt_K_5.add(pK8);

        point_Word_Umbrella_B_Array[0] = pt_K_0;
        point_Word_Umbrella_B_Array[1] = pt_K_1;
        point_Word_Umbrella_B_Array[2] = pt_K_2;
        point_Word_Umbrella_B_Array[3] = pt_K_3;
        point_Word_Umbrella_B_Array[4] = pt_K_4;
        point_Word_Umbrella_B_Array[5] = pt_K_5;

    }

    ////////////////////////////////////////////////////////
    public static int Word_Umbrella_R_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Umbrella_R_Array = new List[Word_Umbrella_R_TOTAL_STEPS];

    public static void getWord_Umbrella_RDetails() {

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();

        int x1 = (int) (scale * 275);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 275);
        int y2 = (int) (scale * 185);

        int x3 = (int) (scale * 280);
        int y3 = (int) (scale * 110);

        int x4 = (int) (scale * 292);
        int y4 = (int) (scale * 105);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p3);
        pt_C_1.add(p4);

        point_Word_Umbrella_R_Array[0] = pt_C_0;
        point_Word_Umbrella_R_Array[1] = pt_C_1;

    }

    ////////////////////////////////////////////////////////////////////
    public static int Word_Umbrella_E_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Umbrella_E_Array = new List[Word_Umbrella_E_TOTAL_STEPS];

    public static void getWord_Umbrella_EDetails() {

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 317);
        int y1 = (int) (scale * 144);

        int x2 = (int) (scale * 347);
        int y2 = (int) (scale * 144);

        int x3 = (int) (scale * 347);
        int y3 = (int) (scale * 115);

        int x4 = (int) (scale * 330);
        int y4 = (int) (scale * 105);

        int x41 = (int) (scale * 315);
        int y41= (int) (scale * 115);

        int x5 = (int) (scale * 315);
        int y5 = (int) (scale * 170);

        int x6 = (int) (scale * 330);
        int y6 = (int) (scale * 186);

        int x7 = (int) (scale * 347);
        int y7 = (int) (scale * 180);
/*

        int x8 = (int) (scale * 190);
        int y8 = (int) (scale * 210);
*/

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        //    Point p8 = new Point(x8, y8);
        Point p9 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p9);

        pt_C_4.add(p9);
        pt_C_4.add(p5);

        pt_C_5.add(p5);
        pt_C_5.add(p6);

        pt_C_6.add(p6);
        pt_C_6.add(p7);

       /* pt_C_7.add(p7);
        pt_C_7.add(p8);
*/
        point_Word_Umbrella_E_Array[0] = pt_C_0;
        point_Word_Umbrella_E_Array[1] = pt_C_1;
        point_Word_Umbrella_E_Array[2] = pt_C_2;
        point_Word_Umbrella_E_Array[3] = pt_C_3;
        point_Word_Umbrella_E_Array[4] = pt_C_4;
        point_Word_Umbrella_E_Array[5] = pt_C_5;
        point_Word_Umbrella_E_Array[6] = pt_C_6;
        //   point_Word_Umbrella_E_Array[7] = pt_C_7;

    }

    //////////////////////////////////////////////////////////////
    public static int Word_Umbrella_L1_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Umbrella_L1_Array = new List[Word_Umbrella_L1_TOTAL_STEPS];

    public static void getWord_Umbrella_L1Details() {


        List<Point> pt_l_0 = new ArrayList<Point>();

        int xI1 = (int) (scale * 375);
        int yI1 = (int) (scale * 60);
        int xI2 = (int) (scale * 375);
        int yI2 = (int) (scale * 185);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        point_Word_Umbrella_L1_Array[0] = pt_l_0;
    }

    ///////////////////////////////////////////////////////////
    public static int Word_Umbrella_L2_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Umbrella_L2_Array = new List[Word_Umbrella_L2_TOTAL_STEPS];

    public static void getWord_Umbrella_L2Details() {

        List<Point> pt_l_0 = new ArrayList<Point>();

        int xI1 = (int) (scale * 405);
        int yI1 = (int) (scale * 60);
        int xI2 = (int) (scale * 405);
        int yI2 = (int) (scale * 185);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        point_Word_Umbrella_L2_Array[0] = pt_l_0;

    }

    //////////////////////////////////////////////////////////////
    public static int Word_Umbrella_A_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Umbrella_A_Array = new List[Word_Umbrella_A_TOTAL_STEPS];

    public static void getWord_Umbrella_ADetails() {

        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 466);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 440);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 430);
        int y3 = (int) (scale * 175);

        int x4 = (int) (scale * 445);
        int y4 = (int) (scale * 185);

        int x5 = (int) (scale * 460);
        int y5 = (int) (scale * 171);


        int x6 = (int) (scale * 474);
        int y6 = (int) (scale * 105);

        int x7 = (int) (scale * 469);
        int y7 = (int) (scale * 185);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);

        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);
/////
        pt_s_4.add(p6);
        pt_s_4.add(p7);


        point_Word_Umbrella_A_Array[0] = pt_s_0;
        point_Word_Umbrella_A_Array[1] = pt_s_1;
        point_Word_Umbrella_A_Array[2] = pt_s_2;
        point_Word_Umbrella_A_Array[3] = pt_s_3;
        point_Word_Umbrella_A_Array[4] = pt_s_4;


    }
////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////Word Violin /////////////////////////////////////


    public static int Word_Violin_V_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Violin_V_Array = new List[Word_Violin_V_TOTAL_STEPS];

    public static void getWord_Violin_VDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();


        int x1 = (int) (scale * 76);
        int y1 = (int) (scale * 74);

        int x2 = (int) (scale * 107);
        int y2 = (int) (scale * 180);

        int x3 = (int) (scale * 112);
        int y3 = (int) (scale * 180);

        int x4 = (int) (scale * 140);
        int y4 = (int) (scale * 74);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        point_Word_Violin_V_Array[0] = pt_0;
        point_Word_Violin_V_Array[1] = pt_1;
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Violin_i_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Violin_i_Array = new List[Word_Violin_i_TOTAL_STEPS];

    public static void getWord_Violin_iDetails() {


        List<Point> pt_l_0 = new ArrayList<Point>();
        List<Point> pt_l_1 = new ArrayList<Point>();


        int xI1 = (int) (scale * 177);
        int yI1 = (int) (scale * 63);

        int xI2 = (int) (scale * 177);
        int yI2 = (int) (scale * 67);

        int xI3 = (int) (scale * 177);
        int yI3 = (int) (scale * 110);

        int xI4 = (int) (scale * 177);
        int yI4 = (int) (scale * 180);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        pt_l_1.add(pI3);
        pt_l_1.add(pI4);


        point_Word_Violin_i_Array[0] = pt_l_1;
        point_Word_Violin_i_Array[1] = pt_l_0;

    }

    //////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Violin_o_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Violin_o_Array = new List[Word_Violin_o_TOTAL_STEPS];

    public static void getWord_Violin_oDetails() {


        List<Point> pt_o_0 = new ArrayList<Point>();
        List<Point> pt_o_1 = new ArrayList<Point>();
        List<Point> pt_o_2 = new ArrayList<Point>();
        List<Point> pt_o_3 = new ArrayList<Point>();
        List<Point> pt_o_4 = new ArrayList<Point>();
        List<Point> pt_o_5 = new ArrayList<Point>();

        int x1 = (int) (scale * 235);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 215);
        int y2 = (int) (scale * 130);

        int x3 = (int) (scale * 215);
        int y3 = (int) (scale * 170);

        int x4 = (int) (scale * 242);
        int y4 = (int) (scale * 185);

        int x5 = (int) (scale * 265);
        int y5 = (int) (scale * 170);

        int x6 = (int) (scale * 265);
        int y6 = (int) (scale * 130);

        int x7 = (int) (scale * 245);
        int y7 = (int) (scale * 105);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);


        pt_o_0.add(p1);
        pt_o_0.add(p2);

        pt_o_1.add(p2);
        pt_o_1.add(p3);

        pt_o_2.add(p3);
        pt_o_2.add(p4);

        pt_o_3.add(p4);
        pt_o_3.add(p5);

        pt_o_4.add(p5);
        pt_o_4.add(p6);

        pt_o_5.add(p6);
        pt_o_5.add(p7);

        point_Word_Violin_o_Array[0] = pt_o_0;
        point_Word_Violin_o_Array[1] = pt_o_1;
        point_Word_Violin_o_Array[2] = pt_o_2;
        point_Word_Violin_o_Array[3] = pt_o_3;
        point_Word_Violin_o_Array[4] = pt_o_4;
        point_Word_Violin_o_Array[5] = pt_o_5;


    }

    //////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Violin_l_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Violin_l_Array = new List[Word_Violin_l_TOTAL_STEPS];

    public static void getWord_Violin_lDetails() {
        List<Point> pt_l_0 = new ArrayList<Point>();

        int xI1 = (int) (scale * 300);
        int yI1 = (int) (scale * 65);
        int xI2 = (int) (scale * 300);
        int yI2 = (int) (scale * 180);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        point_Word_Violin_l_Array[0] = pt_l_0;
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Violin_i2_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_Violin_i2_Array = new List[Word_Violin_i2_TOTAL_STEPS];

    public static void getWord_Violin_i2Details() {


        List<Point> pt_l_0 = new ArrayList<Point>();
        List<Point> pt_l_1 = new ArrayList<Point>();


        int xI1 = (int) (scale * 342);
        int yI1 = (int) (scale * 62);

        int xI2 = (int) (scale * 342);
        int yI2 = (int) (scale * 65);

        int xI3 = (int) (scale * 342);
        int yI3 = (int) (scale * 110);

        int xI4 = (int) (scale * 342);
        int yI4 = (int) (scale * 180);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        pt_l_1.add(pI3);
        pt_l_1.add(pI4);


        point_Word_Violin_i2_Array[0] = pt_l_1;
        point_Word_Violin_i2_Array[1] = pt_l_0;

    }

    //////////////////////////////////////////////////////////////////////////////////////////

    public static int Word_Violin_n_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Violin_n_Array = new List[Word_Violin_n_TOTAL_STEPS];

    public static void getWord_Violin_nDetails() {


        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 380);
        int yK1 = (int) (scale * 110);

        int xK2 = (int) (scale * 380);
        int yK2 = (int) (scale * 180);

        int xK3 = (int) (scale * 385);
        int yK3 = (int) (scale * 120);

        int x3 = (int) (scale * 415);
        int y3 = (int) (scale * 106);

        int xK4 = (int) (scale * 425);
        int yK4 = (int) (scale * 125);

        int xK5 = (int) (scale * 427);
        int yK5 = (int) (scale * 180);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(x3, y3);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(pK6);

        pt_K_2.add(pK6);
        pt_K_2.add(pK4);

        pt_K_3.add(pK4);
        pt_K_3.add(pK5);

        point_Word_Violin_n_Array[0] = pt_K_0;
        point_Word_Violin_n_Array[1] = pt_K_1;
        point_Word_Violin_n_Array[2] = pt_K_2;
        point_Word_Violin_n_Array[3] = pt_K_3;

    }

    //////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////Word Whale /////////////////////////////////////


    public static int Word_Whale_W_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Whale_W_Array = new List[Word_Whale_W_TOTAL_STEPS];

    public static void getWord_Whale_WDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();


        int x1 = (int) (scale * 57);
        int y1 = (int) (scale * 74);

        int x2 = (int) (scale * 80);
        int y2 = (int) (scale * 180);

        int x3 = (int) (scale * 110);
        int y3 = (int) (scale * 74);

        int x4 = (int) (scale * 139);
        int y4 = (int) (scale * 180);

        int x5 = (int) (scale * 160);
        int y5 = (int) (scale * 74);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);
        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        point_Word_Whale_W_Array[0] = pt_0;
        point_Word_Whale_W_Array[1] = pt_1;
        point_Word_Whale_W_Array[2] = pt_2;
        point_Word_Whale_W_Array[3] = pt_3;

    }

    //////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Whale_h_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Whale_h_Array = new List[Word_Whale_h_TOTAL_STEPS];

    public static void getWord_Whale_hDetails() {


        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 194);
        int yK1 = (int) (scale * 64);

        int xK2 = (int) (scale * 194);
        int yK2 = (int) (scale * 185);

        int xK3 = (int) (scale * 205);
        int yK3 = (int) (scale * 117);

        int xK4 = (int) (scale * 225);
        int yK4 = (int) (scale * 105);

        int xK5 = (int) (scale * 240);
        int yK5 = (int) (scale * 120);

        int xK6 = (int) (scale * 240);
        int yK6 = (int) (scale * 185);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);
        pt_K_1.add(pK3);
        pt_K_1.add(pK4);
        pt_K_2.add(pK4);
        pt_K_2.add(pK5);
        pt_K_3.add(pK5);
        pt_K_3.add(pK6);

        point_Word_Whale_h_Array[0] = pt_K_0;
        point_Word_Whale_h_Array[1] = pt_K_1;
        point_Word_Whale_h_Array[2] = pt_K_2;
        point_Word_Whale_h_Array[3] = pt_K_3;


    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Whale_a_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Whale_a_Array = new List[Word_Whale_a_TOTAL_STEPS];

    public static void getWord_Whale_aDetails() {


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 319);
        int y1 = (int) (scale * 108);

        int x2 = (int) (scale * 282);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 274);
        int y3 = (int) (scale * 185);

        int x4 = (int) (scale * 290);
        int y4 = (int) (scale * 185);

        int x5 = (int) (scale * 308);
        int y5 = (int) (scale * 175);


        int x6 = (int) (scale * 325);
        int y6 = (int) (scale * 108);

        int x7 = (int) (scale * 320);
        int y7 = (int) (scale * 185);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);

        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);
/////
        pt_s_4.add(p6);
        pt_s_4.add(p7);



        point_Word_Whale_a_Array[0] = pt_s_0;
        point_Word_Whale_a_Array[1] = pt_s_1;
        point_Word_Whale_a_Array[2] = pt_s_2;
        point_Word_Whale_a_Array[3] = pt_s_3;
        point_Word_Whale_a_Array[4] = pt_s_4;

    }

////////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Whale_l_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_Whale_l_Array = new List[Word_Whale_l_TOTAL_STEPS];

    public static void getWord_Whale_lDetails() {


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 360);
        int y1 = (int) (scale * 65);

        int x2 = (int) (scale * 360);
        int y2 = (int) (scale * 180);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);

        pt_s_0.add(p1);
        pt_s_0.add(p2);

        point_Word_Whale_l_Array[0] = pt_s_0;


    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Whale_e_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_Whale_e_Array = new List[Word_Whale_e_TOTAL_STEPS];

    public static void getWord_Whale_eDetails() {

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();


        int x1 = (int) (scale * 405);
        int y1 = (int) (scale * 145);

        int x2 = (int) (scale * 445);
        int y2 = (int) (scale * 145);

        int x3 = (int) (scale * 445);
        int y3 = (int) (scale * 115);

        int x4 = (int) (scale * 410);
        int y4 = (int) (scale * 110);

        int x5 = (int) (scale * 396);
        int y5 = (int) (scale * 145);

        int x6 = (int) (scale * 404);
        int y6 = (int) (scale * 180);

        int x7 = (int) (scale * 418);
        int y7 = (int) (scale * 190);

        int x8 = (int) (scale * 438);
        int y8 = (int) (scale * 185);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);


        pt_4.add(p5);
        pt_4.add(p6);

        pt_5.add(p6);
        pt_5.add(p7);

        pt_6.add(p7);
        pt_6.add(p8);


        point_Word_Whale_e_Array[0] = pt_0;
        point_Word_Whale_e_Array[1] = pt_1;
        point_Word_Whale_e_Array[2] = pt_2;
        point_Word_Whale_e_Array[3] = pt_3;
        point_Word_Whale_e_Array[4] = pt_4;
        point_Word_Whale_e_Array[5] = pt_5;
        point_Word_Whale_e_Array[6] = pt_6;


    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////Word X-Ray /////////////////////////////////////


    public static int Word_XRay_X_TOTAL_STEPS = 2;

    public static List<Point>[] point_Word_XRay_X_Array = new List[Word_XRay_X_TOTAL_STEPS];

    public static void getWord_XRay_XDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();


        int x1 = (int) (scale * 73);
        int y1 = (int) (scale * 55);

        int x2 = (int) (scale * 123);
        int y2 = (int) (scale * 160);

        int x3 = (int) (scale * 121);
        int y3 = (int) (scale * 55);

        int x4 = (int) (scale * 72);
        int y4 = (int) (scale * 160);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        pt_0.add(p1);
        pt_0.add(p2);
        pt_1.add(p3);
        pt_1.add(p4);

        point_Word_XRay_X_Array[0] = pt_0;
        point_Word_XRay_X_Array[1] = pt_1;

    }

    //////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_XRay_space_TOTAL_STEPS = 1;

    public static List<Point>[] point_Word_XRay_space_Array = new List[Word_XRay_space_TOTAL_STEPS];

    public static void getWord_XRay_spaceDetails() {


        List<Point> pt_0 = new ArrayList<Point>();


        int x1 = (int) (scale * 155);
        int y1 = (int) (scale * 114);

        int x2 = (int) (scale * 180);
        int y2 = (int) (scale * 114);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);

        pt_0.add(p1);
        pt_0.add(p2);


        point_Word_XRay_space_Array[0] = pt_0;


    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_XRay_R_TOTAL_STEPS = 7;

    public static List<Point>[] point_Word_XRay_R_Array = new List[Word_XRay_R_TOTAL_STEPS];

    public static void getWord_XRay_RDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();


        int x1 = (int) (scale * 220);
        int y1 = (int) (scale * 50);

        int x2 = (int) (scale * 220);
        int y2 = (int) (scale * 160);

        int x3 = (int) (scale * 230);
        int y3 = (int) (scale * 50);

        int x4 = (int) (scale * 255);
        int y4 = (int) (scale * 50);

        int x5 = (int) (scale * 270);
        int y5 = (int) (scale * 80);

        int x6 = (int) (scale * 255);
        int y6 = (int) (scale * 107);

        int x7 = (int) (scale * 230);
        int y7 = (int) (scale * 107);

        int x8 = (int) (scale * 260);
        int y8 = (int) (scale * 112);

        int x9 = (int) (scale * 270);
        int y9 = (int) (scale * 120);

        int x10 = (int) (scale * 274);
        int y10 = (int) (scale * 160);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
        Point p10 = new Point(x10, y10);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p4);

        pt_2.add(p4);
        pt_2.add(p5);

        pt_3.add(p5);
        pt_3.add(p6);

        pt_4.add(p6);
        pt_4.add(p7);

        pt_5.add(p8);
        pt_5.add(p9);

        pt_6.add(p9);
        pt_6.add(p10);

        point_Word_XRay_R_Array[0] = pt_0;
        point_Word_XRay_R_Array[1] = pt_1;
        point_Word_XRay_R_Array[2] = pt_2;
        point_Word_XRay_R_Array[3] = pt_3;
        point_Word_XRay_R_Array[4] = pt_4;
        point_Word_XRay_R_Array[5] = pt_5;
        point_Word_XRay_R_Array[6] = pt_6;


    }

////////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_XRay_a_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_XRay_a_Array = new List[Word_XRay_a_TOTAL_STEPS];

    public static void getWord_XRay_aDetails() {


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 350);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 322);
        int y2 = (int) (scale * 90);

        int x3 = (int) (scale * 309);
        int y3 = (int) (scale * 145);

        int x4 = (int) (scale * 320);
        int y4 = (int) (scale * 164);

        int x5 = (int) (scale * 345);
        int y5 = (int) (scale * 147);


        int x6 = (int) (scale * 360);
        int y6 = (int) (scale * 80);

        int x7 = (int) (scale * 354);
        int y7 = (int) (scale * 160);

       /* int x8 = (int) (scale * 320);
        int y8 = (int) (scale * 164);

        int x9 = (int) (scale * 345);
        int y9 = (int) (scale * 150);
*/

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);
       /* Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);*/


        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);
/////
        pt_s_4.add(p6);
        pt_s_4.add(p7);

       /* pt_s_5.add(p7);
        pt_s_5.add(p8);

        pt_s_6.add(p8);
        pt_s_6.add(p9);*/


        point_Word_XRay_a_Array[0] = pt_s_0;
        point_Word_XRay_a_Array[1] = pt_s_1;
        point_Word_XRay_a_Array[2] = pt_s_2;
        point_Word_XRay_a_Array[3] = pt_s_3;
        point_Word_XRay_a_Array[4] = pt_s_4;
      /*  point_Word_XRay_a_Array[5] = pt_s_5;
        point_Word_XRay_a_Array[6] = pt_s_6;
*/

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_XRay_y_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_XRay_y__Array = new List[Word_XRay_y_TOTAL_STEPS];

    public static void getWord_Xray_yDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();


        int x1 = (int) (scale * 390);
        int y1 = (int) (scale * 80);

        int x2 = (int) (scale * 415);
        int y2 = (int) (scale * 165);

        int x3 = (int) (scale * 440);
        int y3 = (int) (scale * 80);

        int x4 = (int) (scale * 415);
        int y4 = (int) (scale * 170);

        int x5 = (int) (scale * 410);
        int y5 = (int) (scale * 190);

        int x6 = (int) (scale * 390);
        int y6 = (int) (scale * 200);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        // Point p7 = new Point(x7, y7);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p3);
        pt_1.add(p5);

        pt_2.add(p5);
        pt_2.add(p6);

       /* pt_3.add(p5);
        pt_3.add(p6);*/


        point_Word_XRay_y__Array[0] = pt_0;
        point_Word_XRay_y__Array[1] = pt_1;
        point_Word_XRay_y__Array[2] = pt_2;
      //  point_Word_XRay_y__Array[3] = pt_3;
        //point_Word_XRay_y__Array[4] = pt_4;

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////Word Yak//////////////////////////////////

    public static int Word_Yak_Y_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Yak_Y_Array = new List[Word_Yak_Y_TOTAL_STEPS];

    public static void getWord_Yak_YDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();


        int x1 = (int) (scale * 137);
        int y1 = (int) (scale * 67);

        int x2 = (int) (scale * 165);
        int y2 = (int) (scale * 145);

        int x3 = (int) (scale * 195);
        int y3 = (int) (scale * 67);

        int x4 = (int) (scale * 170);
        int y4 = (int) (scale * 150);

        int x5 = (int) (scale * 168);
        int y5 = (int) (scale * 185);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p4);
        pt_2.add(p5);

      /*  pt_3.add(p5);
        pt_3.add(p6);

        pt_4.add(p7);
        pt_4.add(p8);

        pt_5.add(p8);
        pt_5.add(p9);
*/


        point_Word_Yak_Y_Array[0] = pt_0;
        point_Word_Yak_Y_Array[1] = pt_1;
        point_Word_Yak_Y_Array[2] = pt_2;
       /* point_Word_Zoo_Z_Array[3] = pt_3;
        point_Word_Zoo_Z_Array[4] = pt_4;
        point_Word_Zoo_Z_Array[5] = pt_5;*/


    }
//////////////////////////////////////////////

    public static int Word_Yak_a_TOTAL_STEPS = 5;

    public static List<Point>[] point_Word_Yak_a_Array = new List[Word_Yak_a_TOTAL_STEPS];

    public static void getWord_Yak_aDetails() {

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();



        int x1 = (int) (scale * 260);
        int y1 = (int) (scale * 102);

        int x2 = (int) (scale * 232);
        int y2 = (int) (scale * 115);

        int x3 = (int) (scale * 215);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 235);
        int y4 = (int) (scale * 182);

        int x5 = (int) (scale * 255);
        int y5 = (int) (scale * 167);


        int x6 = (int) (scale * 270);
        int y6 = (int) (scale * 105);

        int x7 = (int) (scale * 262);
        int y7 = (int) (scale * 180);

     /*   int x8 = (int) (scale * 255);
        int y8 = (int) (scale * 182);

        int x9 = (int) (scale * 285);
        int y9 = (int) (scale * 167);*/


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);
       /* Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);*/


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);




        point_Word_Yak_a_Array[0] = pt_0;
        point_Word_Yak_a_Array[1] = pt_1;
        point_Word_Yak_a_Array[2] = pt_2;
        point_Word_Yak_a_Array[3] = pt_3;
        point_Word_Yak_a_Array[4] = pt_4;
       // point_Word_Yoga_o_Array[5] = pt_5;


    }

    /////////////////////////////////////////////////////////////////
    public static int Word_Yak_k_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Yak_k_Array = new List[Word_Yak_k_TOTAL_STEPS];

    public static void getWord_Yak_kDetails() {


        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();
        List<Point> pt_C_8 = new ArrayList<Point>();

        int xK1 = (int) (scale * 300);
        int yK1 = (int) (scale * 60);

        int xK2 = (int) (scale * 300);
        int yK2= (int) (scale * 186);

        int xK3= (int) (scale * 306);
        int yK3= (int) (scale * 140);

        int xK4= (int) (scale * 320);
        int yK4= (int) (scale * 140);

        int xK5= (int) (scale * 323);
        int yK5= (int) (scale * 140);

        int xK6= (int) (scale * 342);
        int yK6= (int) (scale * 102);

        int xK7= (int) (scale * 347);
        int yK7= (int) (scale * 185);


        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3= new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(xK7, yK7);



        pt_C_0.add(pK1);
        pt_C_0.add(pK2);
        pt_C_1.add(pK3);
        pt_C_1.add(pK4);
        pt_C_2.add(pK5);
        pt_C_2.add(pK6);
        pt_C_3.add(pK5);
        pt_C_3.add(pK7);


        point_Word_Yak_k_Array[0] = pt_C_0;
        point_Word_Yak_k_Array[1] = pt_C_1;
        point_Word_Yak_k_Array[2] = pt_C_2;
        point_Word_Yak_k_Array[3] = pt_C_3;
        /*point_Word_Yoga_g_Array[4] = pt_C_4;
        point_Word_Yoga_g_Array[5] = pt_C_5;
        point_Word_Yoga_g_Array[6] = pt_C_6;
        point_Word_Yoga_g_Array[7] = pt_C_7;*/
        //point_Word_Yoga_g_Array[8] = pt_C_8;
    }

  /*  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Yoga_a_TOTAL_STEPS =5;

    public static List<Point>[] point_Word_Yoga_a_Array = new List[Word_Yoga_a_TOTAL_STEPS];

    public static void getWord_Yoga_aDetails() {


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 400);
        int y1 = (int) (scale *85);

        int x2 = (int) (scale * 370);
        int y2 = (int) (scale * 100);

        int x3 = (int) (scale * 355);
        int y3 = (int) (scale * 145);

        int x4 = (int) (scale * 370);
        int y4 = (int) (scale * 164);

        int x5 = (int) (scale * 395);
        int y5 = (int) (scale * 147);


        int x6 = (int) (scale * 410);
        int y6 = (int) (scale * 85);

        int x7 = (int) (scale * 403);
        int y7 = (int) (scale * 160);

        int x8 = (int) (scale * 370);
        int y8 = (int) (scale * 164);

        int x9 = (int) (scale * 395);
        int y9 = (int) (scale * 147);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);



        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);
/////
        pt_s_4.add(p6);
        pt_s_4.add(p7);





        point_Word_Yoga_a_Array[0] = pt_s_0;
        point_Word_Yoga_a_Array[1] = pt_s_1;
        point_Word_Yoga_a_Array[2] = pt_s_2;
        point_Word_Yoga_a_Array[3] = pt_s_3;
        point_Word_Yoga_a_Array[4] = pt_s_4;

*/

/*
    }
*/

/////////////////////////////
    ////////////////////////////////Word Zoo//////////////////////////////////

    public static int Word_Zoo_Z_TOTAL_STEPS = 3;

    public static List<Point>[] point_Word_Zoo_Z_Array = new List[Word_Zoo_Z_TOTAL_STEPS];

    public static void getWord_Zoo_ZDetails() {


        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();


        int x1 = (int) (scale * 138);
        int y1 = (int) (scale * 70);

        int x2 = (int) (scale * 192);
        int y2 = (int) (scale * 70);

        int x3 = (int) (scale * 138);
        int y3 = (int) (scale * 180);

        int x4 = (int) (scale * 192);
        int y4 = (int) (scale * 180);
/*
        int x5 = (int) (scale * 192);
        int y5 = (int) (scale * 115);

        int x6 = (int) (scale * 145);
        int y6 = (int) (scale * 120);

        int x7 = (int) (scale * 192);
        int y7 = (int) (scale * 130);

        int x8 = (int) (scale * 192);
        int y8 = (int) (scale * 174);

        int x9 = (int) (scale * 147);
        int y9 = (int) (scale * 177);*/

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
      /*  Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);
*/
        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

      /*  pt_3.add(p5);
        pt_3.add(p6);

        pt_4.add(p7);
        pt_4.add(p8);

        pt_5.add(p8);
        pt_5.add(p9);
*/


        point_Word_Zoo_Z_Array[0] = pt_0;
        point_Word_Zoo_Z_Array[1] = pt_1;
        point_Word_Zoo_Z_Array[2] = pt_2;
       /* point_Word_Zoo_Z_Array[3] = pt_3;
        point_Word_Zoo_Z_Array[4] = pt_4;
        point_Word_Zoo_Z_Array[5] = pt_5;*/


    }
//////////////////////////////////////////////

    public static int Word_Zoo_o1_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Zoo_o1_Array = new List[Word_Zoo_o1_TOTAL_STEPS];

    public static void getWord_Zoo_o1Details() {

        List<Point> pt_0 = new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_7 = new ArrayList<Point>();
        List<Point> pt_8 = new ArrayList<Point>();


        int x1 = (int) (scale * 246);
        int y1 = (int) (scale * 102);

        int x2 = (int) (scale * 224);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 226);
        int y3 = (int) (scale * 170);

        int x4 = (int) (scale * 250);
        int y4 = (int) (scale * 185);

        int x5 = (int) (scale * 274);
        int y5 = (int) (scale * 170);

        int x6 = (int) (scale * 274);
        int y6 = (int) (scale * 120);

        int x7 = (int) (scale * 246);
        int y7 = (int) (scale * 102);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);

        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);

        pt_4.add(p5);
        pt_4.add(p6);

        pt_5.add(p6);
        pt_5.add(p7);


        point_Word_Zoo_o1_Array[0] = pt_0;
        point_Word_Zoo_o1_Array[1] = pt_1;
        point_Word_Zoo_o1_Array[2] = pt_2;
        point_Word_Zoo_o1_Array[3] = pt_3;
        point_Word_Zoo_o1_Array[4] = pt_4;
        point_Word_Zoo_o1_Array[5] = pt_5;


    }

    /////////////////////////////////////////////////////////////////
    public static int Word_Zoo_o2_TOTAL_STEPS = 6;

    public static List<Point>[] point_Word_Zoo_o2_Array = new List[Word_Zoo_o2_TOTAL_STEPS];

    public static void getWord_Zoo_o2Details() {


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 330);
        int y1 = (int) (scale * 100);

        int x2 = (int) (scale * 309);
        int y2 = (int) (scale * 125);

        int x3 = (int) (scale * 309);
        int y3 = (int) (scale * 170);

        int x4 = (int) (scale * 340);
        int y4 = (int) (scale * 180);

        int x5 = (int) (scale * 360);
        int y5 = (int) (scale * 160);

        int x7 = (int) (scale * 360);
        int y7 = (int) (scale * 115);

        int x8 = (int) (scale * 336);
        int y8 = (int) (scale * 100);

        /*int x9= (int) (scale * 110);
        int y9= (int) (scale *200);*/


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p5 = new Point(x5, y5);


        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);

        pt_s_4.add(p5);
        pt_s_4.add(p7);

        pt_s_5.add(p7);
        pt_s_5.add(p8);

     /*   pt_s_6.add(p8);
        pt_s_6.add(p9);*/

        point_Word_Zoo_o2_Array[0] = pt_s_0;
        point_Word_Zoo_o2_Array[1] = pt_s_1;
        point_Word_Zoo_o2_Array[2] = pt_s_2;
        point_Word_Zoo_o2_Array[3] = pt_s_3;
        point_Word_Zoo_o2_Array[4] = pt_s_4;
        point_Word_Zoo_o2_Array[5] = pt_s_5;


    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////Word Hut//////////////////////////////////


    /////////////////////////////// H//////////////////////////////////////////////
    public static int Hut_H_TOTAL_STEPS = 3;
    public static List<Point>[] point_Hut_H_Array = new List[Hut_H_TOTAL_STEPS];

    public static void getHut_H_Details() {

        List<Point> pt_F_0 = new ArrayList<Point>();
        List<Point> pt_F_1 = new ArrayList<Point>();
        List<Point> pt_F_2 = new ArrayList<Point>();
        int xF1 = (int) (scale * 147);
        int yF1 = (int) (scale * 67);

        int xF2 = (int) (scale * 147);
        int yF2 = (int) (scale * 180);

        int xF3 = (int) (scale * 214);
        int yF3 = (int) (scale * 67);

        int xF4 = (int) (scale * 214);
        int yF4 = (int) (scale * 180);

        int xF5 = (int) (scale * 155);
        int yF5 = (int) (scale * 125);

        int xF6 = (int) (scale * 210);
        int yF6 = (int) (scale * 125);

        Point pF1 = new Point(xF1, yF1);
        Point pF2 = new Point(xF2, yF2);
        Point pF3 = new Point(xF3, yF3);
        Point pF4 = new Point(xF4, yF4);
        Point pF5 = new Point(xF5, yF5);
        Point pF6 = new Point(xF6, yF6);


        pt_F_0.add(pF1);
        pt_F_0.add(pF2);
        pt_F_1.add(pF3);
        pt_F_1.add(pF4);
        pt_F_2.add(pF5);
        pt_F_2.add(pF6);

        point_Hut_H_Array[0] = pt_F_0;
        point_Hut_H_Array[1] = pt_F_1;
        point_Hut_H_Array[2] = pt_F_2;

    }

    /////////////////////////////////////////small u ///////////////////
    public static int Hut_u_TOTAL_STEPS = 5;

    public static List<Point>[] point_Hut_u_Array = new List[Hut_u_TOTAL_STEPS];

    public static void getHutuDetails() {


        List <Point> pt_0=new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 284);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 259);
        int y2 = (int) (scale * 115);

        int x3 = (int) (scale * 240);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 255);
        int y4 = (int) (scale * 182);

        int x5 = (int) (scale * 280);
        int y5 = (int) (scale * 167);


        int x6 = (int) (scale * 296);
        int y6 = (int) (scale * 105);

        int x7 = (int) (scale * 290);
        int y7 = (int) (scale * 180);

     /*   int x8 = (int) (scale * 255);
        int y8 = (int) (scale * 182);

        int x9 = (int) (scale * 285);
        int y9 = (int) (scale * 167);*/


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);
       /* Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);*/


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);


        point_Hut_u_Array[0]=pt_0;
        point_Hut_u_Array[1]=pt_1;
        point_Hut_u_Array[2]=pt_2;
        point_Hut_u_Array[3]=pt_3;
        point_Hut_u_Array[4]=pt_4;

    }

    /////////////////////////////////////////small t ///////////////////
    public static int Hut_t_TOTAL_STEPS = 3;

    public static List<Point>[] point_Hut_t_Array = new List[t_TOTAL_STEPS];

    public static void getHuttDetails() {


        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();

        int x1 = (int) (scale * 342);
        int y1 = (int) (scale * 75);

        int x2 = (int) (scale * 342);
        int y2 = (int) (scale * 177);

        int x3 = (int) (scale * 354);
        int y3 = (int) (scale * 180);

        int x4 = (int) (scale * 325);
        int y4 = (int) (scale * 102);

        int x5 = (int) (scale * 356);
        int y5 = (int) (scale * 102);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p4);
        pt_C_2.add(p5);

        point_Hut_t_Array[0] = pt_C_0;
        point_Hut_t_Array[1] = pt_C_1;
        point_Hut_t_Array[2] = pt_C_2;

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////Word Fish//////////////////////////////////

    ///////////////////////////////////// F /////////////////////////////////////////////
    public static int Fish_F_TOTAL_STEPS = 3;
    public static List<Point>[] point_Fish_F_Array = new List[Fish_F_TOTAL_STEPS];

    public static void getFish_F_Details() {

        List<Point> pt_F_0 = new ArrayList<Point>();
        List<Point> pt_F_1 = new ArrayList<Point>();
        List<Point> pt_F_2 = new ArrayList<Point>();
        int xF1 = (int) (scale * 126);
        int yF1 = (int) (scale * 72);

        int xF2 = (int) (scale * 126);
        int yF2 = (int) (scale * 182);

        int xF3 = (int) (scale * 178);
        int yF3 = (int) (scale * 72);

        int xF4 = (int) (scale * 135);
        int yF4 = (int) (scale * 129);

        int xF5 = (int) (scale * 170);
        int yF5 = (int) (scale * 129);

        Point pF1 = new Point(xF1, yF1);
        Point pF2 = new Point(xF2, yF2);
        Point pF3 = new Point(xF3, yF3);
        Point pF4 = new Point(xF4, yF4);
        Point pF5 = new Point(xF5, yF5);


        pt_F_0.add(pF1);
        pt_F_0.add(pF2);
        pt_F_1.add(pF1);
        pt_F_1.add(pF3);
        pt_F_2.add(pF4);
        pt_F_2.add(pF5);

        point_Fish_F_Array[0] = pt_F_0;
        point_Fish_F_Array[1] = pt_F_1;
        point_Fish_F_Array[2] = pt_F_2;

    }
    ////////////////////////////// small i //////////////////////////////

    public static int Fish_i_TOTAL_STEPS = 2;

    public static List<Point>[] point_Fish_i_Array = new List[Fish_i_TOTAL_STEPS];

    public static void getFish_i_Details() {


        List<Point> pt_l_0 = new ArrayList<Point>();
        List<Point> pt_l_1 = new ArrayList<Point>();


        int xI1 = (int) (scale * 209);
        int yI1 = (int) (scale * 58);

        int xI2 = (int) (scale * 209);
        int yI2 = (int) (scale * 60);

        int xI3 = (int) (scale * 209);
        int yI3 = (int) (scale * 110);

        int xI4 = (int) (scale * 209);
        int yI4 = (int) (scale * 187);


        Point pI1 = new Point(xI1, yI1);
        Point pI2 = new Point(xI2, yI2);
        Point pI3 = new Point(xI3, yI3);
        Point pI4 = new Point(xI4, yI4);

        pt_l_0.add(pI1);
        pt_l_0.add(pI2);

        pt_l_1.add(pI3);
        pt_l_1.add(pI4);


        point_Fish_i_Array[0] = pt_l_1;
        point_Fish_i_Array[1] = pt_l_0;

    }

//////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////// s //////////////////////////////////////////////////////////////////

    public static int Fish_s_TOTAL_STEPS = 7;

    public static List<Point>[] point_Fish_s_Array = new List[Fish_s_TOTAL_STEPS];

    public static void getFish_s_Details() {


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();


        int x1 = (int) (scale * 290);
        int y1 = (int) (scale * 118);

        int x2 = (int) (scale * 272);
        int y2 = (int) (scale * 103);

        int x3 = (int) (scale * 250);
        int y3 = (int) (scale * 108);

        int x4 = (int) (scale * 252);
        int y4 = (int) (scale * 137);

        int x5 = (int) (scale * 288);
        int y5 = (int) (scale * 150);

        int x6 = (int) (scale * 290);
        int y6 = (int) (scale * 173);

        int x7 = (int) (scale * 271);
        int y7 = (int) (scale * 184);

        int x8 = (int) (scale * 265);
        int y8 = (int) (scale * 186);

        int x9 = (int) (scale * 245);
        int y9 = (int) (scale * 172);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);


        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);

        pt_s_4.add(p5);
        pt_s_4.add(p6);

        pt_s_5.add(p6);
        pt_s_5.add(p7);

        pt_s_6.add(p7);
        pt_s_6.add(p9);


        point_Fish_s_Array[0] = pt_s_0;
        point_Fish_s_Array[1] = pt_s_1;
        point_Fish_s_Array[2] = pt_s_2;
        point_Fish_s_Array[3] = pt_s_3;
        point_Fish_s_Array[4] = pt_s_4;
        point_Fish_s_Array[5] = pt_s_5;
        point_Fish_s_Array[6] = pt_s_6;


    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// small h ///////////////////////////////////////////////////////////

    public static int Fish_h_TOTAL_STEPS = 4;

    public static List<Point>[] point_Fish_h_Array = new List[Fish_h_TOTAL_STEPS];


    public static void getFish_h_Details() {

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 325);
        int yK1 = (int) (scale * 66);

        int xK2 = (int) (scale * 325);
        int yK2 = (int) (scale * 188);

        int xK3 = (int) (scale * 330);
        int yK3 = (int) (scale * 115);

        int xK4 = (int) (scale * 357);
        int yK4 = (int) (scale * 105);

        int xK5 = (int) (scale * 372);
        int yK5 = (int) (scale * 128);

        int xK6 = (int) (scale * 372);
        int yK6 = (int) (scale * 183);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);
        pt_K_1.add(pK3);
        pt_K_1.add(pK4);
        pt_K_2.add(pK4);
        pt_K_2.add(pK5);
        pt_K_3.add(pK5);
        pt_K_3.add(pK6);

        point_Fish_h_Array[0] = pt_K_0;
        point_Fish_h_Array[1] = pt_K_1;
        point_Fish_h_Array[2] = pt_K_2;
        point_Fish_h_Array[3] = pt_K_3;

    }
////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////Word Car//////////////////////////////////
    public static int Car_C_TOTAL_STEPS = 7;

    public static List<Point>[] point_Car_C_Array = new List[Car_C_TOTAL_STEPS];

    public static void getCarCDetails() {

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();


        int x1 = (int) (scale * 210);
        int y1 = (int) (scale * 95);

        int x6 = (int) (scale * 200);
        int y6 = (int) (scale * 70);

        int x2 = (int) (scale * 170);
        int y2 = (int) (scale * 70);

        int x7 = (int) (scale * 150);
        int y7 = (int) (scale * 100);


        int x3 = (int) (scale * 150);
        int y3 = (int) (scale * 150);

        int x4 = (int) (scale * 170);
        int y4 = (int) (scale * 180);
        int x8 = (int) (scale * 195);
        int y8 = (int) (scale * 180);

        int x5 = (int) (scale * 208);
        int y5 = (int) (scale * 160);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);

        pt_C_0.add(p1);
        pt_C_0.add(p6);
        pt_C_1.add(p6);
        pt_C_1.add(p2);

        pt_C_2.add(p2);
        pt_C_2.add(p7);

        pt_C_3.add(p7);
        pt_C_3.add(p3);

        pt_C_4.add(p3);
        pt_C_4.add(p4);

        pt_C_5.add(p4);
        pt_C_5.add(p8);

        pt_C_6.add(p8);
        pt_C_6.add(p5);

        point_Car_C_Array[0] = pt_C_0;
        point_Car_C_Array[1] = pt_C_1;
        point_Car_C_Array[2] = pt_C_2;
        point_Car_C_Array[3] = pt_C_3;
        point_Car_C_Array[4] = pt_C_4;
        point_Car_C_Array[5] = pt_C_5;
        point_Car_C_Array[6] = pt_C_6;


    }


    //////////////////////////////////// a////////////////////////////////////////////////////
    public static int Car_a_TOTAL_STEPS = 5;

    public static List<Point>[] point_Car_a_Array = new List[Car_a_TOTAL_STEPS];

    public static void getCaraDetails()
    {
        List <Point> pt_0=new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 284);
        int y1 = (int) (scale * 105);

        int x2 = (int) (scale * 255);
        int y2 = (int) (scale * 115);

        int x3 = (int) (scale * 240);
        int y3 = (int) (scale * 165);

        int x4 = (int) (scale * 255);
        int y4 = (int) (scale * 182);

        int x5 = (int) (scale * 280);
        int y5 = (int) (scale * 167);


        int x6 = (int) (scale * 295);
        int y6 = (int) (scale * 105);

        int x7 = (int) (scale * 287);
        int y7 = (int) (scale * 180);

     /*   int x8 = (int) (scale * 255);
        int y8 = (int) (scale * 182);

        int x9 = (int) (scale * 285);
        int y9 = (int) (scale * 167);*/


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);
       /* Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);*/


        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);

/*
        pt_5.add(p7);
        pt_5.add(p8);

        pt_6.add(p8);
        pt_6.add(p9);

*/

        point_Car_a_Array[0]=pt_0;
        point_Car_a_Array[1]=pt_1;
        point_Car_a_Array[2]=pt_2;
        point_Car_a_Array[3]=pt_3;
        point_Car_a_Array[4]=pt_4;
       /* point_Car_a_Array[5]=pt_5;
        point_Car_a_Array[6]=pt_6;*/
    }
    /////////////////////////////////////////small r ///////////////////
    public static int Car_r_TOTAL_STEPS = 2;

    public static List<Point>[] point_Car_r_Array = new List[Car_r_TOTAL_STEPS];
    public static void getCarrDetails() {


        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();

        int x1 = (int) (scale * 326);
        int y1 = (int) (scale * 99);

        int x2 = (int) (scale * 326);
        int y2 = (int) (scale * 180);

        int x3 = (int) (scale * 329);
        int y3 = (int) (scale * 117);

        int x4 = (int) (scale * 350);
        int y4 = (int) (scale * 98);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p3);
        pt_C_1.add(p4);

        point_Car_r_Array[0] = pt_C_0;
        point_Car_r_Array[1] = pt_C_1;
    }
    ////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////Word Queen//////////////////////////////////

    ////////////////////////////// Q//////////////////////////////
    public static int Queen_Q_TOTAL_STEPS = 7;

    public static List<Point>[] point_Queen_Q_Array = new List[Queen_Q_TOTAL_STEPS];
    public static void getQueenQDetails()
    {

        List<Point> pt_Q_0 = new ArrayList<Point>();
        List<Point> pt_Q_1 = new ArrayList<Point>();
        List<Point> pt_Q_2 = new ArrayList<Point>();
        List<Point> pt_Q_3 = new ArrayList<Point>();
        List<Point> pt_Q_4 = new ArrayList<Point>();
        List<Point> pt_Q_5 = new ArrayList<Point>();
        List<Point> pt_Q_6= new ArrayList<Point>();

        int x1 = (int) (scale * 80);
        int y1 = (int) (scale * 65);

        int x2 = (int) (scale * 55);
        int y2= (int) (scale *85);

        int x3 = (int) (scale * 55);
        int y3 = (int) (scale * 155);

        int x4 = (int) (scale * 80);
        int y4 = (int) (scale * 178);

        int x5 = (int) (scale * 117);
        int y5 = (int) (scale * 155);

        int x6 = (int) (scale * 117);
        int y6 = (int) (scale * 85);

        int x7= (int) (scale * 86);
        int y7= (int) (scale * 65);


        int x9= (int) (scale * 114);
        int y9= (int) (scale * 170);

        int x10= (int) (scale * 125);
        int y10= (int) (scale * 182);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7= new Point(x7, y7);

        Point p9= new Point(x9, y9);
        Point p10= new Point(x10, y10);





        pt_Q_0.add(p1);
        pt_Q_0.add(p2);

        pt_Q_1.add(p2);
        pt_Q_1.add(p3);

        pt_Q_2.add(p3);
        pt_Q_2.add(p4);

        pt_Q_3.add(p4);
        pt_Q_3.add(p5);

        pt_Q_4.add(p5);
        pt_Q_4.add(p6);

        pt_Q_5.add(p6);
        pt_Q_5.add(p7);



        pt_Q_6.add(p9);
        pt_Q_6.add(p10);


        point_Queen_Q_Array[0] = pt_Q_0;
        point_Queen_Q_Array[1] =pt_Q_1;
        point_Queen_Q_Array[2] = pt_Q_2;
        point_Queen_Q_Array[3] = pt_Q_3;
        point_Queen_Q_Array[4] = pt_Q_4;
        point_Queen_Q_Array[5] = pt_Q_5;
        point_Queen_Q_Array[6] = pt_Q_6;


    }
    /////////////////////////////////////////small u ///////////////////
    public static int Queen_u_TOTAL_STEPS = 4;

    public static List<Point>[] point_Queen_u_Array = new List[Queen_u_TOTAL_STEPS];

    public static void getQueenuDetails() {


        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();

        int x1 = (int) (scale * 157);
        int y1 = (int) (scale * 98);

        int x2 = (int) (scale * 159);
        int y2 = (int) (scale * 167);

        int x3 = (int) (scale * 176);
        int y3 = (int) (scale * 178);

        int x4 = (int) (scale * 195);
        int y4 = (int) (scale * 172);

        int x5 = (int) (scale * 205);
        int y5 = (int) (scale * 97);

        int x6 = (int) (scale * 205);
        int y6 = (int) (scale * 178);


        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);

        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);


        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p6);
        pt_C_3.add(p5);


        point_Queen_u_Array[0] = pt_C_0;
        point_Queen_u_Array[1] = pt_C_1;
        point_Queen_u_Array[2] = pt_C_2;
        point_Queen_u_Array[3] = pt_C_3;


    }
//////////////////////////////////////////////

    public static int Word_Queen_e_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Queen_e_Array = new List[Word_Queen_e_TOTAL_STEPS];

    public static void getQueeneDetails() {

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 255);
        int y1 = (int) (scale * 133);

        int x2 = (int) (scale * 289);
        int y2 = (int) (scale * 133);

        int x3 = (int) (scale * 280);
        int y3 = (int) (scale * 94);

        int x4 = (int) (scale * 260);
        int y4 = (int) (scale * 93);

        int x41 = (int) (scale * 243);
        int y41 = (int) (scale * 105);

        int x5 = (int) (scale * 240);
        int y5 = (int) (scale * 135);

        int x6 = (int) (scale * 242);
        int y6 = (int) (scale * 150);

        int x7 = (int) (scale * 255);
        int y7 = (int) (scale * 174);

        int x8 = (int) (scale * 284);
        int y8 = (int) (scale * 174);

        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p9);

        pt_C_4.add(p9);
        pt_C_4.add(p5);

        pt_C_5.add(p5);
        pt_C_5.add(p6);

        pt_C_6.add(p6);
        pt_C_6.add(p7);

        pt_C_7.add(p7);
        pt_C_7.add(p8);

        point_Word_Queen_e_Array[0] = pt_C_0;
        point_Word_Queen_e_Array[1] = pt_C_1;
        point_Word_Queen_e_Array[2] = pt_C_2;
        point_Word_Queen_e_Array[3] = pt_C_3;
        point_Word_Queen_e_Array[4] = pt_C_4;
        point_Word_Queen_e_Array[5] = pt_C_5;
        point_Word_Queen_e_Array[6] = pt_C_6;
        point_Word_Queen_e_Array[7] = pt_C_7;

    }
    public static int Word_Queen_e1_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Queen_e1_Array = new List[Word_Queen_e1_TOTAL_STEPS];
    public static void getQueene1Details() {

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();
        int x1 = (int) (scale * 328);
        int y1 = (int) (scale * 133);

        int x2 = (int) (scale * 369);
        int y2 = (int) (scale * 133);

        int x3 = (int) (scale * 358);
        int y3 = (int) (scale * 95);

        int x4 = (int) (scale * 335);
        int y4 = (int) (scale * 95);

        int x41 = (int) (scale * 324);
        int y41 = (int) (scale * 108);

        int x5 = (int) (scale * 317);
        int y5 = (int) (scale * 135);

        int x6 = (int) (scale * 317);
        int y6 = (int) (scale * 150);

        int x7 = (int) (scale * 330);
        int y7 = (int) (scale * 174);

        int x8 = (int) (scale * 359);
        int y8 = (int) (scale * 174);
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p9);

        pt_C_4.add(p9);
        pt_C_4.add(p5);

        pt_C_5.add(p5);
        pt_C_5.add(p6);

        pt_C_6.add(p6);
        pt_C_6.add(p7);

        pt_C_7.add(p7);
        pt_C_7.add(p8);

        point_Word_Queen_e1_Array[0] = pt_C_0;
        point_Word_Queen_e1_Array[1] = pt_C_1;
        point_Word_Queen_e1_Array[2] = pt_C_2;
        point_Word_Queen_e1_Array[3] = pt_C_3;
        point_Word_Queen_e1_Array[4] = pt_C_4;
        point_Word_Queen_e1_Array[5] = pt_C_5;
        point_Word_Queen_e1_Array[6] = pt_C_6;
        point_Word_Queen_e1_Array[7] = pt_C_7;

    }
    /////////////////////////////////// small n ///////////////////////////////////////////////////////////

    public static int Word_Queen_n_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Queen_n_Array = new List[Word_Queen_n_TOTAL_STEPS];


    public static void getQueennDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 400);
        int yK1 = (int) (scale * 95);

        int xK2 = (int) (scale * 400);
        int yK2= (int) (scale * 177);

        int xK3= (int) (scale * 408);
        int yK3= (int) (scale * 108);

        int x3= (int) (scale * 435);
        int y3= (int) (scale * 95);

        int xK4= (int) (scale * 450);
        int yK4= (int) (scale * 110);

        int xK5= (int) (scale * 450);
        int yK5= (int) (scale * 177);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(x3, y3);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(pK6);

        pt_K_2.add(pK6);
        pt_K_2.add(pK4);

        pt_K_3.add(pK4);
        pt_K_3.add(pK5);

        point_Word_Queen_n_Array[0] = pt_K_0;
        point_Word_Queen_n_Array[1] = pt_K_1;
        point_Word_Queen_n_Array[2] = pt_K_2;
        point_Word_Queen_n_Array[3] = pt_K_3;

    }
    ////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////Word Snake//////////////////////////////////

    ////////////////////////////// S//////////////////////////////



    public static int Snake_S_TOTAL_STEPS =8;

    public static List<Point>[] point_Snake_S_Array = new List[Snake_S_TOTAL_STEPS];
    public static void getSnakesDetails(){


        List<Point> pt_s_0 = new ArrayList<Point>();
        List<Point> pt_s_1 = new ArrayList<Point>();
        List<Point> pt_s_2 = new ArrayList<Point>();
        List<Point> pt_s_3 = new ArrayList<Point>();
        List<Point> pt_s_4 = new ArrayList<Point>();
        List<Point> pt_s_5 = new ArrayList<Point>();
        List<Point> pt_s_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();




        int x1 = (int) (scale * 112);
        int y1 = (int) (scale * 90);

        int x2 = (int) (scale * 92);
        int y2= (int) (scale * 73);

        int x3 = (int) (scale * 59);
        int y3= (int) (scale * 87);

        int x4 = (int) (scale * 62);
        int y4= (int) (scale * 112);

        int x5 = (int) (scale * 110);
        int y5= (int) (scale *140);

        int x6 = (int) (scale * 112);
        int y6= (int) (scale *180);

        int x7 = (int) (scale * 82);
        int y7= (int) (scale *190);

        int x8 = (int) (scale * 70);
        int y8= (int) (scale *180);

        int x9= (int) (scale * 58);
        int y9= (int) (scale *160);



        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x9, y9);



        pt_s_0.add(p1);
        pt_s_0.add(p2);

        pt_s_1.add(p2);
        pt_s_1.add(p3);

        pt_s_2.add(p3);
        pt_s_2.add(p4);

        pt_s_3.add(p4);
        pt_s_3.add(p5);

        pt_s_4.add(p5);
        pt_s_4.add(p6);

        pt_s_5.add(p6);
        pt_s_5.add(p7);

        pt_s_6.add(p7);
        pt_s_6.add(p8);

        pt_s_7.add(p8);
        pt_s_7.add(p9);

        point_Snake_S_Array[0] = pt_s_0;
        point_Snake_S_Array[1] = pt_s_1;
        point_Snake_S_Array[2] = pt_s_2;
        point_Snake_S_Array[3] = pt_s_3;
        point_Snake_S_Array[4] = pt_s_4;
        point_Snake_S_Array[5] = pt_s_5;
        point_Snake_S_Array[6] = pt_s_6;
        point_Snake_S_Array[7] = pt_s_7;


    }
    /////////////////////////////////// small n ///////////////////////////////////////////////////////////

    public static int Word_Snake_n_TOTAL_STEPS = 4;

    public static List<Point>[] point_Word_Snake_n_Array = new List[Word_Snake_n_TOTAL_STEPS];


    public static void getSnakenDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();

        int xK1 = (int) (scale * 152);
        int yK1 = (int) (scale * 101);

        int xK2 = (int) (scale * 152);
        int yK2= (int) (scale * 183);

        int xK3= (int) (scale * 158);
        int yK3= (int) (scale * 114);

        int x3= (int) (scale * 185);
        int y3= (int) (scale * 101);

        int xK4= (int) (scale * 200);
        int yK4= (int) (scale * 116);

        int xK5= (int) (scale * 200);
        int yK5= (int) (scale * 183);

        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3 = new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(x3, y3);

        pt_K_0.add(pK1);
        pt_K_0.add(pK2);

        pt_K_1.add(pK3);
        pt_K_1.add(pK6);

        pt_K_2.add(pK6);
        pt_K_2.add(pK4);

        pt_K_3.add(pK4);
        pt_K_3.add(pK5);

        point_Word_Snake_n_Array[0] = pt_K_0;
        point_Word_Snake_n_Array[1] = pt_K_1;
        point_Word_Snake_n_Array[2] = pt_K_2;
        point_Word_Snake_n_Array[3] = pt_K_3;

    }
    //////////////////////////////////// a////////////////////////////////////////////////////
    public static int Snake_a_TOTAL_STEPS = 5;

    public static List<Point>[] point_Snake_a_Array = new List[Snake_a_TOTAL_STEPS];

    public static void getSnakeaDetails()
    {
        List <Point> pt_0=new ArrayList<Point>();
        List<Point> pt_1 = new ArrayList<Point>();
        List<Point> pt_2 = new ArrayList<Point>();
        List<Point> pt_3 = new ArrayList<Point>();
        List<Point> pt_4 = new ArrayList<Point>();
        List<Point> pt_5 = new ArrayList<Point>();
        List<Point> pt_6 = new ArrayList<Point>();
        List<Point> pt_s_7 = new ArrayList<Point>();

        int x1 = (int) (scale * 280);
        int y1 = (int) (scale * 108);

        int x2 = (int) (scale * 250);
        int y2 = (int) (scale * 120);

        int x3 = (int) (scale * 231);
        int y3 = (int) (scale * 160);

        int x4 = (int) (scale * 250);
        int y4 = (int) (scale * 182);

        int x5 = (int) (scale * 276);
        int y5 = (int) (scale * 160);


        int x6 = (int) (scale * 285);
        int y6 = (int) (scale * 105);

        int x7 = (int) (scale * 281);
        int y7 = (int) (scale * 182);




        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);

        Point p7 = new Point(x7, y7);



        pt_0.add(p1);
        pt_0.add(p2);

        pt_1.add(p2);
        pt_1.add(p3);

        pt_2.add(p3);
        pt_2.add(p4);

        pt_3.add(p4);
        pt_3.add(p5);
/////
        pt_4.add(p6);
        pt_4.add(p7);



        point_Snake_a_Array[0]=pt_0;
        point_Snake_a_Array[1]=pt_1;
        point_Snake_a_Array[2]=pt_2;
        point_Snake_a_Array[3]=pt_3;
        point_Snake_a_Array[4]=pt_4;

    }
    ///////////////////////////////////////////////
    public static int Snake_k_TOTAL_STEPS = 4;

    public static List<Point>[] point_Snake_k_Array = new List[Snake_k_TOTAL_STEPS];


    public static void getSnakekDetails(){

        List<Point> pt_K_0 = new ArrayList<Point>();
        List<Point> pt_K_1 = new ArrayList<Point>();
        List<Point> pt_K_2 = new ArrayList<Point>();
        List<Point> pt_K_3 = new ArrayList<Point>();


        int xK1 = (int) (scale * 312);
        int yK1 = (int) (scale * 67);

        int xK2 = (int) (scale * 317);
        int yK2= (int) (scale * 188);

        int xK3= (int) (scale *320);
        int yK3= (int) (scale * 145);

        int xK4= (int) (scale * 338);
        int yK4= (int) (scale * 145);

        int xK5= (int) (scale * 339);
        int yK5= (int) (scale * 145);

        int xK6= (int) (scale * 363);
        int yK6= (int) (scale * 98);

        int xK7= (int) (scale * 365);
        int yK7= (int) (scale * 187);


        Point pK1 = new Point(xK1, yK1);
        Point pK2 = new Point(xK2, yK2);
        Point pK3= new Point(xK3, yK3);
        Point pK4 = new Point(xK4, yK4);
        Point pK5 = new Point(xK5, yK5);
        Point pK6 = new Point(xK6, yK6);
        Point pK7 = new Point(xK7, yK7);



        pt_K_0.add(pK1);
        pt_K_0.add(pK2);
        pt_K_1.add(pK3);
        pt_K_1.add(pK4);
        pt_K_2.add(pK5);
        pt_K_2.add(pK6);
        pt_K_3.add(pK5);
        pt_K_3.add(pK7);

        point_Snake_k_Array[0] = pt_K_0;
        point_Snake_k_Array[1] = pt_K_1;
        point_Snake_k_Array[2] = pt_K_2;
        point_Snake_k_Array[3] = pt_K_3;

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int Word_Snake_e_TOTAL_STEPS = 8;

    public static List<Point>[] point_Word_Snake_e_Array = new List[Word_Snake_e_TOTAL_STEPS];
    public static void getSnakeeDetails() {

        List<Point> pt_C_0 = new ArrayList<Point>();
        List<Point> pt_C_1 = new ArrayList<Point>();
        List<Point> pt_C_2 = new ArrayList<Point>();
        List<Point> pt_C_3 = new ArrayList<Point>();
        List<Point> pt_C_4 = new ArrayList<Point>();
        List<Point> pt_C_5 = new ArrayList<Point>();
        List<Point> pt_C_6 = new ArrayList<Point>();
        List<Point> pt_C_7 = new ArrayList<Point>();
        int x1 = (int) (scale * 400);
        int y1 = (int) (scale * 145);

        int x2 = (int) (scale * 441);
        int y2 = (int) (scale * 145);

        int x3 = (int) (scale * 430);
        int y3 = (int) (scale * 107);

        int x4 = (int) (scale * 407);
        int y4 = (int) (scale * 107);

        int x41 = (int) (scale * 396);
        int y41 = (int) (scale * 120);

        int x5 = (int) (scale * 389);
        int y5 = (int) (scale * 147);

        int x6 = (int) (scale * 389);
        int y6 = (int) (scale * 155);

        int x7 = (int) (scale * 403);
        int y7 = (int) (scale * 182);

        int x8 = (int) (scale * 431);
        int y8 = (int) (scale * 181);
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(x6, y6);
        Point p7 = new Point(x7, y7);
        Point p8 = new Point(x8, y8);
        Point p9 = new Point(x41, y41);

        pt_C_0.add(p1);
        pt_C_0.add(p2);

        pt_C_1.add(p2);
        pt_C_1.add(p3);

        pt_C_2.add(p3);
        pt_C_2.add(p4);

        pt_C_3.add(p4);
        pt_C_3.add(p9);

        pt_C_4.add(p9);
        pt_C_4.add(p5);

        pt_C_5.add(p5);
        pt_C_5.add(p6);

        pt_C_6.add(p6);
        pt_C_6.add(p7);

        pt_C_7.add(p7);
        pt_C_7.add(p8);

        point_Word_Snake_e_Array[0] = pt_C_0;
        point_Word_Snake_e_Array[1] = pt_C_1;
        point_Word_Snake_e_Array[2] = pt_C_2;
        point_Word_Snake_e_Array[3] = pt_C_3;
        point_Word_Snake_e_Array[4] = pt_C_4;
        point_Word_Snake_e_Array[5] = pt_C_5;
        point_Word_Snake_e_Array[6] = pt_C_6;
        point_Word_Snake_e_Array[7] = pt_C_7;

    }

}
