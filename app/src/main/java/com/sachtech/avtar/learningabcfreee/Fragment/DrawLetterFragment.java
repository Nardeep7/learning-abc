package com.sachtech.avtar.learningabcfreee.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Path;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.os.Handler;
import android.widget.Toast;

import com.sachtech.avtar.learningabcfreee.Constants.Config;
import com.sachtech.avtar.learningabcfreee.Constants.DataHolder;
import com.sachtech.avtar.learningabcfreee.Constants.LetterPointsConfig;
import com.sachtech.avtar.learningabcfreee.FragmentContainerActivity;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;
import com.sachtech.avtar.learningabcfreee.Utils.AlertMessages;
import com.sachtech.avtar.learningabcfreee.Utils.LetterCompleteListner;
import com.sachtech.avtar.learningabcfreee.Utils.PaintView2;

import tyrantgit.explosionfield.ExplosionField;


public class DrawLetterFragment extends Fragment implements LetterCompleteListner, Animation.AnimationListener {

    private Context m_Context = null;
    private Paint paint, mBitmapPaint;
    private ExplosionField mExplosionField;
    private Path path;
    private Bitmap bitmap;
    View view;
    private RelativeLayout mPaintLayout;
    private ImageView preview_image_demo,preview_image1;
    private Animation mAnim;
    Button full_version,maybe_later;
    // private MediaPlayer mediaPlayer = null;
    private static DrawLetterFragment drawLetterFrag;
    boolean Explode = false;
    private String TAG = this.getClass().getSimpleName();

    private int GIF_IMAGE = 0;
    private int DOTTED_IMAGE = 0;
    PaintView2 letterView;
    String CURRENT_LETTER;

    public static DrawLetterFragment getInstance() {
        return drawLetterFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawLetterFrag = this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_draw_letter_new, container, false);
        FragmentContainerActivity.getInstance().homeButton.setVisibility(View.VISIBLE);
        initView(view);
        return view;
    }

    private void initView(View view) {
//        for(int i=0;i<view.getCh)

        mPaintLayout = (RelativeLayout) view.findViewById(R.id.painting_layout);
        preview_image_demo = (ImageView) view.findViewById(R.id.preview_img);
        preview_image1 = (ImageView) view.findViewById(R.id.preview_img1);
        full_version= (Button) view.findViewById(R.id.full_version);
        maybe_later = (Button) view.findViewById(R.id.maybe_later);

        checkComingLetter();
        bitmap = BitmapFactory.decodeResource(getResources(), DOTTED_IMAGE);


        if (Explode) {
            reset(mPaintLayout);
            mExplosionField.clear();

        }
    }

    private void checkComingLetter() {

        CURRENT_LETTER = DataHolder.LETTER;
        preview_image_demo.setImageResource(0);
        preview_image_demo.setVisibility(View.GONE);
        mPaintLayout.setVisibility(View.VISIBLE);
        switch (CURRENT_LETTER) {
            case Config.A:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_A;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_a);

                LetterPointsConfig.getADetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.A_TOTAL_STEPS, LetterPointsConfig.point_A_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.a:
                // GIF_IMAGE = Config.Gif_a;
                DOTTED_IMAGE = Config.Image_a;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_a);
                LetterPointsConfig.getaDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.a_TOTAL_STEPS, LetterPointsConfig.point_a_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.B:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_B;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_b);
                LetterPointsConfig.getBDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.B_TOTAL_STEPS, LetterPointsConfig.point_B_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.b:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_b;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_b);
                LetterPointsConfig.getbDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.b_TOTAL_STEPS, LetterPointsConfig.point_b_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.C:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_C;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_c);


                LetterPointsConfig.getCDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.C_TOTAL_STEPS, LetterPointsConfig.point_C_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.c:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_c;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_c);
                LetterPointsConfig.getcDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.c_TOTAL_STEPS, LetterPointsConfig.point_c_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.D:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_D;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_d);
                LetterPointsConfig.getDDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.D_TOTAL_STEPS, LetterPointsConfig.point_D_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.d:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_d;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_d);
                LetterPointsConfig.getdDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.d_TOTAL_STEPS, LetterPointsConfig.point_d_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.E:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_E;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_e);
                LetterPointsConfig.getEDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.E_TOTAL_STEPS, LetterPointsConfig.point_E_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.e:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_e;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_e);
                LetterPointsConfig.geteDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.e_TOTAL_STEPS, LetterPointsConfig.point_e_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.F:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_F;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_f);
                LetterPointsConfig.getFDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.F_TOTAL_STEPS, LetterPointsConfig.point_F_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.f:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_f;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_f);
                LetterPointsConfig.getfDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.f_TOTAL_STEPS, LetterPointsConfig.point_f_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.G:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_G;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_g);
                LetterPointsConfig.getGDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.G_TOTAL_STEPS, LetterPointsConfig.point_G_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);


                break;







            case Config.g:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_g;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_g);
                LetterPointsConfig.getgDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.g_TOTAL_STEPS, LetterPointsConfig.point_g_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.H:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_H;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_h);
                LetterPointsConfig.getHDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.H_TOTAL_STEPS, LetterPointsConfig.point_H_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.h:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_h;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_h);
                LetterPointsConfig.gethDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.h_TOTAL_STEPS, LetterPointsConfig.point_h_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.I:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_I;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_i);
                LetterPointsConfig.getIDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.I_TOTAL_STEPS, LetterPointsConfig.point_I_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.i:
                // GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_i;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_i);
                LetterPointsConfig.getiDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.i_TOTAL_STEPS, LetterPointsConfig.point_i_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.J:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_J;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_j);
                LetterPointsConfig.getJDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.J_TOTAL_STEPS, LetterPointsConfig.point_J_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.j:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_j;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_j);
                LetterPointsConfig.getjDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.j_TOTAL_STEPS, LetterPointsConfig.point_j_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.K:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_K;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_k);
                LetterPointsConfig.getKDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.K_TOTAL_STEPS, LetterPointsConfig.point_K_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.k:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_k;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_k);
                LetterPointsConfig.getkDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.k_TOTAL_STEPS, LetterPointsConfig.point_k_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.L:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_L;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_l);
                LetterPointsConfig.getLDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.L_TOTAL_STEPS, LetterPointsConfig.point_L_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.l:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_l;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_l);
                LetterPointsConfig.getlDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.l_TOTAL_STEPS, LetterPointsConfig.point_l_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.M:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_M;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_m);
                LetterPointsConfig.getMDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.M_TOTAL_STEPS, LetterPointsConfig.point_M_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.m:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_m;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_m);
                LetterPointsConfig.getmDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.m_TOTAL_STEPS, LetterPointsConfig.point_m_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.N:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_N;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_n);
                LetterPointsConfig.getNDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.N_TOTAL_STEPS, LetterPointsConfig.point_N_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.n:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_n;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_n);
                LetterPointsConfig.getnDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.n_TOTAL_STEPS, LetterPointsConfig.point_n_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.O:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_O;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_o);
                LetterPointsConfig.getODetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.O_TOTAL_STEPS, LetterPointsConfig.point_O_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.o:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_o;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_o);
                LetterPointsConfig.getoDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.o_TOTAL_STEPS, LetterPointsConfig.point_o_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.P:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_P;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_p);
                LetterPointsConfig.getPDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.P_TOTAL_STEPS, LetterPointsConfig.point_P_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.p:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_p;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_p);
                LetterPointsConfig.getpDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.p_TOTAL_STEPS, LetterPointsConfig.point_p_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.Q:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_Q;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_q);
                LetterPointsConfig.getQDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.Q_TOTAL_STEPS, LetterPointsConfig.point_Q_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.q:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_q;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_q);
                LetterPointsConfig.getqDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.q_TOTAL_STEPS, LetterPointsConfig.point_q_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.R:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_R;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_r);
                LetterPointsConfig.getRDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.R_TOTAL_STEPS, LetterPointsConfig.point_R_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.r:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_r;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_r);
                LetterPointsConfig.getrDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.r_TOTAL_STEPS, LetterPointsConfig.point_r_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.S:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_S;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_s);
                LetterPointsConfig.getSDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.S_TOTAL_STEPS, LetterPointsConfig.point_S_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.s:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_s;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_s);
                LetterPointsConfig.getsDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.s_TOTAL_STEPS, LetterPointsConfig.point_s_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.T:
                //   GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_T;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_t);
                LetterPointsConfig.getTDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.T_TOTAL_STEPS, LetterPointsConfig.point_T_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.t:
                //   GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_t;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_t);
                LetterPointsConfig.gettDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.t_TOTAL_STEPS, LetterPointsConfig.point_t_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.U:
                //   GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_U;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_u);
                LetterPointsConfig.getUDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.U_TOTAL_STEPS, LetterPointsConfig.point_U_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.u:
                //   GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_u;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_u);
                LetterPointsConfig.getuDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.u_TOTAL_STEPS, LetterPointsConfig.point_u_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.V:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_V;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_v);
                LetterPointsConfig.getVDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.V_TOTAL_STEPS, LetterPointsConfig.point_V_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.v:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_v;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_v);
                // LetterPointsConfig.getvDetails();
                // letterView = new PaintView2(getActivity(), LetterPointsConfig.v_TOTAL_STEPS,LetterPointsConfig.point_v_Array,DOTTED_IMAGE);
                LetterPointsConfig.getvDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.v_TOTAL_STEPS, LetterPointsConfig.point_v_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.W:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_W;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_w);
                LetterPointsConfig.getWDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.W_TOTAL_STEPS, LetterPointsConfig.point_W_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.w:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_w;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_w);
                LetterPointsConfig.getwDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.w_TOTAL_STEPS, LetterPointsConfig.point_w_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.X:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_X;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_x);
                LetterPointsConfig.getXDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.X_TOTAL_STEPS, LetterPointsConfig.point_X_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.x:
                //  GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_x;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_x);
                LetterPointsConfig.getxDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.x_TOTAL_STEPS, LetterPointsConfig.point_x_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.Y:
                //   GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_Y;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_y);
                LetterPointsConfig.getYDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.Y_TOTAL_STEPS, LetterPointsConfig.point_Y_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.y:
                //   GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_y;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_y);
                LetterPointsConfig.getyDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.y_TOTAL_STEPS, LetterPointsConfig.point_y_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            case Config.Z:
                //   GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_Z;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_z);
                LetterPointsConfig.getZDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.Z_TOTAL_STEPS, LetterPointsConfig.point_Z_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);
                break;

            case Config.z:
                //   GIF_IMAGE = Config.Gif_A;

                DOTTED_IMAGE = Config.Image_z;
                Sound.playLetterSound(getActivity(), R.raw.sound_caps_z);
                LetterPointsConfig.getzDetails();
                letterView = new PaintView2(getActivity(), LetterPointsConfig.z_TOTAL_STEPS, LetterPointsConfig.point_z_Array, DOTTED_IMAGE, this);
                mPaintLayout.addView(letterView);

                break;

            default:
                //   GIF_IMAGE = Config.Gif_A;
                DOTTED_IMAGE = Config.Image_A;
                break;
        }
    }

    public void previewImage(int image_id) {
///starAnimation();

        //mPaintLayout.setVisibility(View.GONE);
        mExplosionField = ExplosionField.attach2Window(getActivity());
        mExplosionField.explode(mPaintLayout);
        Explode = true;
        preview_image_demo.setVisibility(View.VISIBLE);

        preview_image_demo.setBackgroundResource(image_id);
        mAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.together);
        mAnim.setAnimationListener(DrawLetterFragment.this);
        preview_image_demo.clearAnimation();
        preview_image_demo.setAnimation(mAnim);

        preview_image_demo.startAnimation(mAnim);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {


                // preview_image_demo.setBackgroundResource(0);

                try {
                    //((FragmentContainerActivity)getActivity()).removeFrag(Config.DrawLetterFrag);
                    // ((FragmentContainerActivity)getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);

                    mPaintLayout.removeAllViewsInLayout();
                    preview_image_demo.setBackgroundResource(0);


                    initView(view);


                } catch (Exception e) {
                    Log.e(TAG, "" + e);
                }


            }
        }, 5000);

    }


    public void previewImage1(int image_id1) {
///starAnimation();


        mPaintLayout.setVisibility(View.GONE);
        preview_image_demo.setBackgroundResource(0);
        preview_image1.setVisibility(View.VISIBLE);
        full_version.setVisibility(View.VISIBLE);
        maybe_later.setVisibility(View.VISIBLE);
        preview_image1.setBackgroundResource(image_id1);
        mAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.together);
        mAnim.setAnimationListener(DrawLetterFragment.this);
        preview_image1.clearAnimation();
        preview_image1.setAnimation(mAnim);
        preview_image1.startAnimation(mAnim);


        full_version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.sachtech.avtar.learningabc"));
                startActivity(intent);
            }
        });
        maybe_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "on click work", Toast.LENGTH_SHORT).show();
                FragmentContainerActivity.getInstance().displayFrag(Config.CHOOSE_LETTER, Config.ChooseLetterFrag);
            }
        });

    }

    private void reset(View root) {
        if (root instanceof ViewGroup) {
            ViewGroup parent = (ViewGroup) root;
            Log.e("Reset_if", "Called");
            root.setScaleX(1);
            root.setScaleY(1);
            root.setAlpha(1);
        } else {
            Log.e("Reset_if_else", "Called");

        }
    }

    private void playMusic() {
        //mediaPlayer = MediaPlayer.create(getInstance(), R.raw.alphabet_song_sml);
        /*try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            L.e(TAG, "Exception at play music  == " + e);
        }*/
        //  mediaPlayer.start();
    }


    @Override
    public void OnLetterComplete() {
        switch (CURRENT_LETTER) {
            case Config.A:
                DataHolder.LETTER = Config.B;
                //starAnimation();
                previewImage(R.drawable.apple_img);
                Sound.playLetterSound(getActivity(), R.raw.a_for_apple);


                break;

            case Config.a:
                // GIF_IMAGE = Config.Gif_a;
                DataHolder.LETTER = Config.b;
                previewImage(R.drawable.apple_img);
                Sound.playLetterSound(getActivity(), R.raw.a_for_apple);
                break;

            case Config.B:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.C;
                previewImage(R.drawable.ball_img);
                Sound.playLetterSound(getActivity(), R.raw.b_for_ball);
                break;

            case Config.b:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.c;
                previewImage(R.drawable.ball_img);
                Sound.playLetterSound(getActivity(), R.raw.b_for_ball);
                break;

            case Config.C:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.D;
                previewImage(R.drawable.car_img);
                Sound.playLetterSound(getActivity(), R.raw.c_for_car);
                break;

            case Config.c:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.d;
                previewImage(R.drawable.car_img);
                Sound.playLetterSound(getActivity(), R.raw.c_for_car);
                break;

            case Config.D:
                DataHolder.LETTER = Config.E;
                previewImage(R.drawable.dog_img);
                Sound.playLetterSound(getActivity(), R.raw.d_for_dog);
                break;

            case Config.d:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.e;
                previewImage(R.drawable.dog_img);
                Sound.playLetterSound(getActivity(), R.raw.d_for_dog);
                break;

            case Config.E:
                // GIF_IMAGE = Config.Gif_A;  DOTTED_IMAGE=Config.Image_E;
                DataHolder.LETTER = Config.F;
                previewImage(R.drawable.egg_img);
                Sound.playLetterSound(getActivity(), R.raw.eforegg);


                break;

            case Config.e:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.f;
                previewImage(R.drawable.egg_img);
                Sound.playLetterSound(getActivity(), R.raw.eforegg);
                break;

            case Config.F:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.G;
                previewImage(R.drawable.fish_img);
                Sound.playLetterSound(getActivity(), R.raw.fforfish);

                break;

            case Config.f:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.g;
                previewImage(R.drawable.fish_img);
                Sound.playLetterSound(getActivity(), R.raw.fforfish);
                break;

            case Config.G:
                 //GIF_IMAGE = Config.Gif_A;
               DataHolder.LETTER = Config.H;
                previewImage(R.drawable.gift_img);
                Sound.playLetterSound(getActivity(), R.raw.g_for_gift);
                /*new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {


                       //  preview_image_demo.setBackgroundResource(0);

                        try {
                            //((FragmentContainerActivity)getActivity()).removeFrag(Config.DrawLetterFrag);
                            // ((FragmentContainerActivity)getActivity()).displayFrag(Config.DRAW_LETTER, Config.DrawLetterFrag);

                         //  mPaintLayout.removeAllViewsInLayout();
                          //  preview_image_demo.setBackgroundResource(0);
                         //  mPaintLayout.setVisibility(View.GONE);
                          //  preview_image_demo.setBackgroundResource(0);

                           // previewImage1(R.drawable.gift_img);

                          // initView(view);
                            AlertMessages.showProVersionDialog(getActivity());


                        } catch (Exception e) {
                            Log.e(TAG, "" + e);
                        }


                    }
                },5000);*/

                break;


            case Config.H:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.I;
                previewImage(R.drawable.hat_img);
                Sound.playLetterSound(getActivity(), R.raw.h_for_hat);

                break;

            case Config.h:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.i;
                previewImage(R.drawable.hat_img);
                Sound.playLetterSound(getActivity(), R.raw.h_for_hat);
                break;

            case Config.I:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.J;
                previewImage(R.drawable.ink_img);
                Sound.playLetterSound(getActivity(), R.raw.i_for_ink);
                break;

            case Config.i:
                // GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.j;
                previewImage(R.drawable.ink_img);
                Sound.playLetterSound(getActivity(), R.raw.i_for_ink);
                break;

            case Config.J:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.K;
                previewImage(R.drawable.jug_img);
                Sound.playLetterSound(getActivity(), R.raw.j_for_jug);
                break;

            case Config.j:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.k;
                previewImage(R.drawable.jug_img);
                Sound.playLetterSound(getActivity(), R.raw.j_for_jug);
                break;

            case Config.K:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.L;
                previewImage(R.drawable.kite_img);
                Sound.playLetterSound(getActivity(), R.raw.kforkites);

                break;

            case Config.k:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.l;
                previewImage(R.drawable.kite_img);
                Sound.playLetterSound(getActivity(), R.raw.kforkites);
                break;

            case Config.L:

                DataHolder.LETTER = Config.M;
                previewImage(R.drawable.lamp_img);
                Sound.playLetterSound(getActivity(), R.raw.lforamp);

                break;

            case Config.l:
                DataHolder.LETTER = Config.m;
                previewImage(R.drawable.lamp_img);
                Sound.playLetterSound(getActivity(), R.raw.lforamp);
                break;

            case Config.M:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.N;
                previewImage(R.drawable.mango_img);
                Sound.playLetterSound(getActivity(), R.raw.mformango);

                break;

            case Config.m:
                DataHolder.LETTER = Config.n;
                previewImage(R.drawable.mango_img);
                Sound.playLetterSound(getActivity(), R.raw.mformango);
                break;

            case Config.N:
                DataHolder.LETTER = Config.O;
                previewImage(R.drawable.nest_img);
                Sound.playLetterSound(getActivity(), R.raw.n_for_nest);

                break;

            case Config.n:
                DataHolder.LETTER = Config.o;
                previewImage(R.drawable.nest_img);
                Sound.playLetterSound(getActivity(), R.raw.n_for_nest);
                break;

            case Config.O:
                DataHolder.LETTER = Config.P;
                previewImage(R.drawable.owl_img);
                Sound.playLetterSound(getActivity(), R.raw.o_for_owl);
                break;

            case Config.o:
                DataHolder.LETTER = Config.p;
                previewImage(R.drawable.owl_img);
                Sound.playLetterSound(getActivity(), R.raw.o_for_owl);
                break;

            case Config.P:
                DataHolder.LETTER = Config.Q;
                previewImage(R.drawable.pig_img);
                Sound.playLetterSound(getActivity(), R.raw.p_for_pig);
                break;

            case Config.p:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.q;
                previewImage(R.drawable.pig_img);
                Sound.playLetterSound(getActivity(), R.raw.p_for_pig);
                break;

            case Config.Q:
                DataHolder.LETTER = Config.R;
                previewImage(R.drawable.queen_img);
                Sound.playLetterSound(getActivity(), R.raw.qforqueen);
                break;

            case Config.q:
                DataHolder.LETTER = Config.r;
                previewImage(R.drawable.queen_img);
                Sound.playLetterSound(getActivity(), R.raw.qforqueen);
                break;

            case Config.R:
                DataHolder.LETTER = Config.S;
                previewImage(R.drawable.rabbit_img);
                Sound.playLetterSound(getActivity(), R.raw.rforrabbit);
                break;

            case Config.r:
                DataHolder.LETTER = Config.s;
                previewImage(R.drawable.rabbit_img);
                Sound.playLetterSound(getActivity(), R.raw.rforrabbit);
                break;

            case Config.S:
                DataHolder.LETTER = Config.T;
                previewImage(R.drawable.snake_img);
                Sound.playLetterSound(getActivity(), R.raw.sforsnake);
                break;

            case Config.s:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.t;
                previewImage(R.drawable.snake_img);
                Sound.playLetterSound(getActivity(), R.raw.sforsnake);
                break;

            case Config.T:
                //   GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.U;

                previewImage(R.drawable.train_img);
                Sound.playLetterSound(getActivity(), R.raw.tfortrain);

                break;

            case Config.t:
                DataHolder.LETTER = Config.u;
                previewImage(R.drawable.train_img);
                Sound.playLetterSound(getActivity(), R.raw.tfortrain);
                break;

            case Config.U:
                DataHolder.LETTER = Config.V;
                previewImage(R.drawable.umbrella_img);
                Sound.playLetterSound(getActivity(), R.raw.uforumbrella);
                break;

            case Config.u:
                DataHolder.LETTER = Config.v;
                previewImage(R.drawable.umbrella_img);
                Sound.playLetterSound(getActivity(), R.raw.uforumbrella);
                break;

            case Config.V:
                DataHolder.LETTER = Config.W;
                previewImage(R.drawable.vilion_img);
                Sound.playLetterSound(getActivity(), R.raw.violin);

                break;

            case Config.v:
                DataHolder.LETTER = Config.w;
                previewImage(R.drawable.vilion_img);
                Sound.playLetterSound(getActivity(), R.raw.violin);
                // LetterPointsConfig.getvDetails();
                // letterView = new PaintView2(getActivity(), LetterPointsConfig.v_TOTAL_STEPS,LetterPointsConfig.point_v_Array,DOTTED_IMAGE);

                break;

            case Config.W:
                //  GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.X;
                previewImage(R.drawable.whale_img);
                Sound.playLetterSound(getActivity(), R.raw.wforwell);

                break;

            case Config.w:
                DataHolder.LETTER = Config.x;
                previewImage(R.drawable.whale_img);
                Sound.playLetterSound(getActivity(), R.raw.wforwell);
                break;

            case Config.X:
                DataHolder.LETTER = Config.Y;
                previewImage(R.drawable.xray_img);
                Sound.playLetterSound(getActivity(), R.raw.xforxray);

                break;

            case Config.x:
                DataHolder.LETTER = Config.y;
                previewImage(R.drawable.xray_img);
                Sound.playLetterSound(getActivity(), R.raw.xforxray);
                break;

            case Config.Y:
                //   GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.Z;
                previewImage(R.drawable.yak_img);
                Sound.playLetterSound(getActivity(), R.raw.y_for_yak);

                break;

            case Config.y:
                DataHolder.LETTER = Config.z;
                previewImage(R.drawable.yak_img);
                Sound.playLetterSound(getActivity(), R.raw.y_for_yak);
                break;

            case Config.Z:
                //   GIF_IMAGE = Config.Gif_A;
                DataHolder.LETTER = Config.A;
                previewImage(R.drawable.zoo_img);
                Sound.playLetterSound(getActivity(), R.raw.zforzoo);

                break;

            case Config.z:
                DataHolder.LETTER = Config.a;
                previewImage(R.drawable.zoo_img);
                Sound.playLetterSound(getActivity(), R.raw.zforzoo);
                break;


            default:
                //   GIF_IMAGE = Config.Gif_A;

                break;
        }

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        animation.setAnimationListener(null);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


}
