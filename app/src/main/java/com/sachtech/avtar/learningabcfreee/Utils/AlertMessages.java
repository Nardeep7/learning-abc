package com.sachtech.avtar.learningabcfreee.Utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Constants.Config;
import com.sachtech.avtar.learningabcfreee.FragmentContainerActivity;
import com.sachtech.avtar.learningabcfreee.LayoutComponents.CustomButton;

/**
 * Created by avtar on 4/5/2016.
 */
public class AlertMessages  {
    public static void showProVersionDialog(final Activity activity){

        View view = LayoutInflater.from(activity).inflate(R.layout.dialog,null);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(view);

        final AlertDialog dialog = builder.create();
        dialog.show();

        CustomButton fullVersionBtn = (CustomButton)dialog.findViewById(R.id.full_version);
        CustomButton mayBeLatterBtn = (CustomButton)dialog.findViewById(R.id.maybe_later);
        ImageView image = (ImageView)dialog.findViewById(R.id.preview_img1);

        fullVersionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.sachtech.avtar.learningabc"));
                activity.startActivity(intent);
            }
        });
        mayBeLatterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //Toast.makeText(getActivity(), "on click work", Toast.LENGTH_SHORT).show();
                FragmentContainerActivity.getInstance().clearBackStack();
                FragmentContainerActivity.getInstance().displayFrag(Config.CHOOSE_OPTION, Config.ChooseOptionFrag);
            }
        });

    }
}
