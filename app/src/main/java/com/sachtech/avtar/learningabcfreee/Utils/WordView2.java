package com.sachtech.avtar.learningabcfreee.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.sachtech.avtar.learningabcfreee.Constants.DataHolder;
import com.sachtech.avtar.learningabcfreee.WordsActivity;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nardeep Sandhu on 3/14/2016.
 */
public class WordView2 extends View {

    public int width;
    public int height;
    private Bitmap mBitmap, bitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Paint mBitmapPaint, mPaint, mPaintNext;
    Context context;
    private Paint circlePaint;
    private Path circlePath;
    private Rect m_ImageRect;
    private Rect m_TextRect;
    private List<Point> mPoints = new ArrayList<Point>();

    private int mLastPointIndex = 0;
    private int mTouchTolerance;
    private int mTouchToleranceNext;
    int STEP_COUNT = 1;
    int TOTAL_LETTTER_STEPS = 0;
    List<Point>[] pointListArray = null;
    LetterCompleteListner letterCompleteListner = null;


    private boolean isPathStarted = false;
    private static final int TOUCH_TOLERANCE_DP = 24;
    private static final int TOUCH_TOLERANCE_DP_NEXT = 10;
    private boolean tryAgainSound = true;

    public WordView2(Context context, int TotalLetterSteps, List<Point>[] pointListArray,
                     int imageResourceId, WordsActivity letterActivity) {
        super(context);
        this.context = context;
        letterCompleteListner = letterActivity;

        this.pointListArray = pointListArray;
        TOTAL_LETTTER_STEPS = TotalLetterSteps;


        bitmap = BitmapFactory.decodeResource(getResources(), imageResourceId);

        mPoints = pointListArray[0];

        mPath = new Path();
        mPaint = new Paint();
        mPaintNext = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);

        mPaintNext.setAntiAlias(true);
        mPaintNext.setDither(true);
        mPaintNext.setColor(Color.RED);
        mPaintNext.setStyle(Paint.Style.STROKE);
        mPaintNext.setStrokeJoin(Paint.Join.ROUND);
        mPaintNext.setStrokeCap(Paint.Cap.ROUND);
        final float scale1 = getResources().getDisplayMetrics().density;
        int Strokewidth1 = (int) (scale1 * 5);
        mPaintNext.setStrokeWidth(Strokewidth1);

        final float scale = getResources().getDisplayMetrics().density;
        int Strokewidth = (int) (scale * 18);
        mPaint.setStrokeWidth(Strokewidth);
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        mBitmapPaint.setFilterBitmap(true);
        mBitmapPaint.setAntiAlias(true);
        mTouchTolerance = dp2px(TOUCH_TOLERANCE_DP);
        mTouchToleranceNext = dp2px(TOUCH_TOLERANCE_DP_NEXT);


    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        m_ImageRect = canvas.getClipBounds();
        m_TextRect = canvas.getClipBounds();
        canvas.drawBitmap(bitmap, null, m_ImageRect, mPaint);
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.drawPath(mPath, mPaint);
        //canvas.drawPath( circlePath,  circlePaint);
        for (Point point : mPoints) {
            mCanvas.drawPoint(point.x, point.y, mPaintNext);
        }
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

   /* private void touch_start(float x, float y) {
        mPath.reset();
        Log.e("mLastPointIndex", ">>" + mLastPointIndex);

        if (checkPoint(x, y, mLastPointIndex)) {

            mPath.moveTo(x, y);
            mX = x;
            mY = y;
            Log.e("Touch_Start_If", "Callled>" + mLastPointIndex);
            isPathStarted = true;
        } else {
            // user starts move from point which doen's belongs to mPinst list
            isPathStarted = false;
            Log.e("Touch_Start_If_else ", "Callled>" + mLastPointIndex);
        }

    }


    private void touch_move(float x, float y) {

        Log.e("Touch_move mx", "" + mX);
        Log.e("Touch_move my", "" + mY);
        Log.e("Touch_move x", "" + x);
        Log.e("Touch_move y", "" + y);

        if (isPathStarted) {

            *//*if (checkPoint(x, y, mLastPointIndex) && isPathStarted) {

                Point p = mPoints.get(mLastPointIndex);
                if (((x - 5) <= p.x && p.x <= (x + 5))) {
                    Log.e("WordView ", "Point found here-in (+)(-) area------xxxxxxx--> ");
                    if ( p.y <= (y + 5)) {

                        Log.e("WordView ", "Point found here-in (+)(-) area------yyyyy--> ");

                    } else {

                        Log.e("WordView ", "Point found here-Outttttttt (+)(-) area------yyyyy--> ");
                        return ;
                    }
                } else {

                    Log.e("WordView ", "Point found here-Outttttttt (+)(-) area------xxxxxxx--> ");
                    return ;
                }
            }*//*



            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            Log.e("Touch_move dx", "" + dx);
            Log.e("Touch_move dy", "" + dy);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                Log.e("Touch_move", "" + TOUCH_TOLERANCE);
                mPath.quadTo(x, y, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }

        }
    }

    private void touch_up(float x, float y) {

        if (checkPoint(x, y, mLastPointIndex + 1) && isPathStarted) {

            Point p = mPoints.get(mLastPointIndex);
            mPath.moveTo(p.x, p.y);
            Log.e("Touch_up", "Callled>" + mLastPointIndex);
            // end point
            p = mPoints.get(mLastPointIndex + 1);
            // mPath.lineTo(mX,mY);
            mCanvas.drawPath(mPath, mPaint);
            mPath.reset();
            // increment point index
            ++mLastPointIndex;
            isPathStarted = false;
            Log.e("Draw done ", ">>>>>>>>>>>done >>>>>>>>>");
            if (STEP_COUNT != TOTAL_LETTTER_STEPS) {
                init(pointListArray[STEP_COUNT]);

                STEP_COUNT++;
                return;
            }
            if (STEP_COUNT == TOTAL_LETTTER_STEPS) {

                DataHolder.Word_Count++;
                //  Toast.makeText(context, "A is completed", Toast.LENGTH_SHORT).show();
                letterCompleteListner.OnLetterComplete();


                return;
            }


        } else {
            mPath.reset();
            if (isPathStarted)
                Sound.playLetterSound(context, R.raw.tryagain);
        }

    }

*/


    private void touch_start(float x, float y) {

        if (checkPoint(x, y, mLastPointIndex)) {
            mPath.reset();
            // user starts from given point so path can beis started
            Log.e("Touch_Start_If ", "Callled>" + mLastPointIndex);
            isPathStarted = true;
            Point p = mPoints.get(mLastPointIndex);
            mPath.moveTo(p.x, p.y);
            tryAgainSound = true;

        } else {
            // user starts move from point which doen's belongs to mPinst list
            isPathStarted = false;

            Log.e("Touch_Start_If_else ", "Callled>" + mLastPointIndex);
        }

    }

    float difx, dify;

    //ADDED WITH LAST EDIT
    private void touch_move(float x, float y) {
        // draw line with finger move
        if (isPathStarted) {
            // mPath.reset();
            if ((mLastPointIndex + 1) >= mPoints.size()) {
                return;
            }
            Point p = mPoints.get(mLastPointIndex);

            Point p2 = mPoints.get(mLastPointIndex + 1);
            Log.e("Touch_move", "Callled>" + mLastPointIndex);
            // mPath.moveTo(p.x, p.y);

            float dif_X = Math.abs(p.x - p2.x);
            float dif_Y = Math.abs(p.y - p2.y);

            difx++;
            dify++;
            Log.e("Points diff", "Diff_X= " + dif_X + " x=" + x + " difx = " + difx + " diff_Y= " + dif_Y + " y=" + y + " dify=" + dify);
            //  Log.e("Point xy", "difx " + difx + " p2x" + p2.x + " dify " + dify + " p2y" + p2.y);

            if ((dif_X) >= difx || (dif_Y) >= dify) {
                mPath.lineTo(x, y);

                nextPoint(x, y);
                Log.e("Point ok", " >>>>>>>>>>>>>>>Point OK>>>>>>>>>>>>>>>>>>");
            }


        }
    }

    private void nextPoint(float x, float y) {
        if (checkNextPoint(x, y, mLastPointIndex + 1) && isPathStarted) {
            mCanvas.drawPath(mPath, mPaint);
            Point p = mPoints.get(mLastPointIndex + 1);
            // mPath.moveTo(p.x,p.y);
            // mPath.reset();
            // increment point index
            mLastPointIndex++;
            tryAgainSound = false;
            if (STEP_COUNT != TOTAL_LETTTER_STEPS) {
                difx = 0;
                dify = 0;
                init(pointListArray[STEP_COUNT]);

                STEP_COUNT++;
                return;
            }
            if (STEP_COUNT == TOTAL_LETTTER_STEPS) {
                difx = 0;
                dify = 0;
                DataHolder.Word_Count++;
                //  Toast.makeText(context, "A is completed", Toast.LENGTH_SHORT).show();
                letterCompleteListner.OnLetterComplete();


                return;
            }
        } else {

        }
    }

    /**
     * Draws line.
     */
    private void touch_up(float x, float y) {

        // mPath.reset();


        if (checkPoint(x, y, mLastPointIndex) && isPathStarted) {
            // move finished at valid point so draw whole line

            // start point
            Point p = mPoints.get(mLastPointIndex);
            // mPath.moveTo(p.x, p.y);
            Log.e("Touch_up", "Callled>" + mLastPointIndex);
            // end point
            p = mPoints.get(mLastPointIndex);
            //  mPath.lineTo(p.x, p.y);
            // mCanvas.drawPath(mPath, mPaint);
            mPath.reset();
            // increment point index
            //   ++mLastPointIndex;

           /* if (mLastPointIndex == mPoints.size()){
                Log.e("init>>>>>>>>>>>>>>>>>>",">init");
                init(mPoints1);
            }*/
            isPathStarted = false;
            tryAgainSound = true;
            Log.e("Draw done ", ">>>>>>>>>>>done >>>>>>>>>");


            /*if (STEP_COUNT != TOTAL_LETTTER_STEPS) {
                init(pointListArray[STEP_COUNT]);

                STEP_COUNT++;
                return;
            }
            if (STEP_COUNT == TOTAL_LETTTER_STEPS) {

                DataHolder.Word_Count++;
                //  Toast.makeText(context, "A is completed", Toast.LENGTH_SHORT).show();
                letterCompleteListner.OnLetterComplete();


                return;
            }*/

        } else {
            if (isPathStarted) {
                if (tryAgainSound) {
                    Sound.playLetterSound(context, R.raw.tryagain);
                }
                difx = 0;
                dify = 0;
                mPath.reset();
            }
        }


        // out of bounds


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        int bmH = mBitmap.getHeight();
        int bmW = mBitmap.getWidth();
        int x1 = (int) x;
        int y1 = (int) y;

        Log.e("DrawingView", " bmH = " + bmH + "  y1=" + y1 + "  bmW = " + bmW + "  x1 = " + x1);


        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up(x, y);
                invalidate();
                break;
        }
        return true;
    }


    public void init(List<Point> pt) {
        mLastPointIndex = 0;

        mPoints = pt;

    }

    public void nextLetter(List<Point>[] pointListArray, int stepCount) {
        this.pointListArray = pointListArray;
        TOTAL_LETTTER_STEPS = stepCount;
        mLastPointIndex = 0;
        STEP_COUNT = 1;
        mPoints = this.pointListArray[0];
        invalidate();
    }

    private boolean checkPoint(float x, float y, int pointIndex) {
        Log.e("checkPoint outside ", "called");

        try {
            Point point = mPoints.get(pointIndex);
            if (x > (point.x - mTouchTolerance) && x < (point.x + mTouchTolerance)) {
                Log.e("mTouch outside ", "mTouch");
                if (y > (point.y - mTouchTolerance) && y < (point.y + mTouchTolerance)) {

                    Log.e("mTouchTolerance done ", ">>>>>>>>>>>mTouchTolerance >>>>>>>>>");
                    return true;
                }
            }
        } catch (Exception ex) {
            Log.e("Exception  checkPoint", ">> " + ex);
        }

        //EDIT changed point.y to poin.x in the first if statement


        return false;
    }

    private boolean checkNextPoint(float x, float y, int pointIndex) {
        Log.e("checkPoint outside ", "called");

        try {
            Point point = mPoints.get(pointIndex);
            if (x > (point.x - mTouchToleranceNext) && x < (point.x + mTouchToleranceNext)) {
                Log.e("mTouch outside ", "mTouch");
                if (y > (point.y - mTouchToleranceNext) && y < (point.y + mTouchToleranceNext)) {

                    Log.e("mTouchTolerance done ", ">>>>>>>>>>>mTouchTolerance >>>>>>>>>");
                    return true;
                }
            }
        } catch (Exception ex) {
            Log.e("Exception  checkPoint", ">> " + ex);
        }

        //EDIT changed point.y to poin.x in the first if statement


        return false;
    }

    private int dp2px(int dp) {
        Resources r = getContext().getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }

    public void clearCanvas() {
        mPath.reset();
        mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
       // TOTAL_LETTTER_STEPS = stepCount;
        mLastPointIndex = 0;
        STEP_COUNT = 1;
        mPoints = this.pointListArray[0];
        invalidate();
    }

}
