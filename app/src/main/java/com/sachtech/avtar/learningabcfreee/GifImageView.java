package com.sachtech.avtar.learningabcfreee;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Utils.GifMovieView;

public class GifImageView extends AppCompatActivity {

    private  GifMovieView gifMovieView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif_image_view);
        gifMovieView = (GifMovieView) findViewById(R.id.gif_movie_view);

      Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(display.getWidth(),display.getHeight());
      //  gifMovieView.setLayoutParams(layoutParams);
        // TODO: 18/12/18 new 1 here for reset bacground
        gifMovieView.setMovieResource(R.drawable.ic_back);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gif_image_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
