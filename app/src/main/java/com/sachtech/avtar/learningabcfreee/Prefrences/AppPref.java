package com.sachtech.avtar.learningabcfreee.Prefrences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Nardeep on 22-03-2016.
 */
public class AppPref {
    private static SharedPreferences soundPref = null;
    private static String SOUND_PREF = "SOUND_PREF";
    private static String SOUND = "SOUND";
    public static boolean SOUND_MUTE= true;
    public static boolean SOUND_UN_MUTE = false;

    private static SharedPreferences getSoundPref(Context context) {
        return soundPref = context.getSharedPreferences(SOUND_PREF, Context.MODE_PRIVATE);
    }

    public static void soundMute(Context context) {
        getSoundPref(context).edit().putBoolean(SOUND, SOUND_MUTE).commit();
    }

    public static void soundUnMute(Context context) {
        getSoundPref(context).edit().putBoolean(SOUND, SOUND_UN_MUTE).commit();
    }

    public static boolean isSoundMute(Context context) {
        return getSoundPref(context).getBoolean(SOUND, SOUND_UN_MUTE);
    }
}
