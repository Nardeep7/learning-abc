package com.sachtech.avtar.learningabcfreee.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.sachtech.avtar.learningabcfreee.Fragment.DrawLetterFragment;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nardeep Sandhu on 3/14/2016.
 */
public class DrawingView1 extends View {

    public int width;
    public  int height;
    private Bitmap mBitmap,bitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Paint mBitmapPaint,mPaint;
    Context context;
    private Paint circlePaint;
    private Path circlePath;
    private Rect m_ImageRect;
    private Rect m_TextRect;
    private List<Point> mPoints = new ArrayList<Point>();

    private int mLastPointIndex = 0;
    private int mTouchTolerance;
    int STEP_COUNT=1;
    int TOTAL_LETTTER_STEPS=0;
    List<Point>[] pointListArray = null;
    LetterCompleteListner letterCompleteListner = null;


    private boolean isPathStarted = false;
    private static final int TOUCH_TOLERANCE_DP = 24;
    public DrawingView1(Context context, int TotalLetterSteps, List<Point>[] pointListArray, int imageResourceId,DrawLetterFragment letterFragment) {
        super(context);
        this.context=context;
        letterCompleteListner = letterFragment;

        this.pointListArray = pointListArray;
        TOTAL_LETTTER_STEPS = TotalLetterSteps;
        this.pointListArray = pointListArray;

        bitmap = BitmapFactory.decodeResource(getResources(),imageResourceId );

        mPoints=pointListArray[0];

        mPath = new Path();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);

        final float scale = getResources().getDisplayMetrics().density;
        int Strokewidth= (int) (scale*25);
        mPaint.setStrokeWidth(Strokewidth);
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        mBitmapPaint.setFilterBitmap(true);
        mBitmapPaint.setAntiAlias(true);
        mTouchTolerance = dp2px(TOUCH_TOLERANCE_DP);


    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        m_ImageRect = canvas.getClipBounds();
        m_TextRect = canvas.getClipBounds();
        canvas.drawBitmap(bitmap, null, m_ImageRect, mPaint);
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.drawPath( mPath,  mPaint);
        //canvas.drawPath( circlePath,  circlePaint);
        for (Point point : mPoints) {
            mCanvas.drawPoint(point.x, point.y, mPaint);
        }
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        mPath.reset();
        Log.e("mLastPointIndex",">>"+mLastPointIndex);

    if (checkPoint(x, y, mLastPointIndex)) {

        mPath.moveTo(x, y);
        mX = x;
        mY = y;
        Log.e("Touch_Start_If", "Callled>" + mLastPointIndex);
        isPathStarted = true;
    } else {
        // user starts move from point which doen's belongs to mPinst list
        isPathStarted = false;
        Log.e("Touch_Start_If_else ", "Callled>" + mLastPointIndex);
    }

}


    private void touch_move(float x, float y) {

        Log.e("Touch_move mx", "" + mX);
        Log.e("Touch_move my", "" + mY);
        Log.e("Touch_move x",""+x);
        Log.e("Touch_move y", "" + y);

        if (isPathStarted) {

            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            Log.e("Touch_move dx", "" + dx);
            Log.e("Touch_move dy", "" + dy);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                    Log.e("Touch_move", "" + TOUCH_TOLERANCE);
                    mPath.quadTo(x, y, (x + mX) / 2, (y + mY) / 2);
                    mX = x;
                    mY = y;
                }

        }
    }

    private void touch_up(float x, float y) {

            if (checkPoint(x, y, mLastPointIndex + 1) && isPathStarted) {

                Point p = mPoints.get(mLastPointIndex);
                mPath.moveTo(p.x, p.y);
                Log.e("Touch_up", "Callled>" + mLastPointIndex);
                // end point
                p = mPoints.get(mLastPointIndex + 1);
              // mPath.lineTo(mX,mY);
                mCanvas.drawPath(mPath, mPaint);
                 mPath.reset();
                // increment point index
                ++mLastPointIndex;
                isPathStarted = false;
                Log.e("Draw done ", ">>>>>>>>>>>done >>>>>>>>>");
                if (STEP_COUNT != TOTAL_LETTTER_STEPS) {
                    init(pointListArray[STEP_COUNT]);

                    STEP_COUNT++;
                    return;
                }
                if (STEP_COUNT == TOTAL_LETTTER_STEPS) {
                    Log.e("StepCount3", ">>>>>" + STEP_COUNT);
                    Log.e("A is completed", ">>>>>com a");
                  //  Toast.makeText(context, "A is completed", Toast.LENGTH_SHORT).show();
                    letterCompleteListner.OnLetterComplete();
                    return;
                }


            } else {
                mPath.reset();
                if(isPathStarted)
                    Sound.playLetterSound(context, R.raw.tryagain);
            }

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        int bmH = mBitmap.getHeight();
        int bmW = mBitmap.getWidth();
        int x1 = (int) x;
        int y1 = (int) y;

        Log.e("DrawingView", " bmH = " + bmH + "  y1=" + y1 + "  bmW = " + bmW + "  x1 = " + x1);




        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up(x,y);
                invalidate();
                break;
        }
        return true;
    }



    public void init(List<Point> pt) {
        mLastPointIndex = 0;

        mPoints = pt;

    }

    private boolean checkPoint(float x, float y, int pointIndex) {
        Log.e("checkPoint outside ", "called");

        try {
            Point point = mPoints.get(pointIndex);
            if (x > (point.x - mTouchTolerance) && x < (point.x + mTouchTolerance)) {
                Log.e("mTouch outside ", "mTouch");
                if (y > (point.y - mTouchTolerance) && y < (point.y + mTouchTolerance)) {

                    Log.e("mTouchTolerance done ", ">>>>>>>>>>>mTouchTolerance >>>>>>>>>");
                    return true;
                }
            }
      }catch (Exception ex){
            Log.e("Exception  checkPoint", ">> "+ex);
        }

         //EDIT changed point.y to poin.x in the first if statement



        return false;
    }
    private int dp2px(int dp) {
        Resources r = getContext().getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }
}
