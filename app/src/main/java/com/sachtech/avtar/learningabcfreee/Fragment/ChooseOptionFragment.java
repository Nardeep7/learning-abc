package com.sachtech.avtar.learningabcfreee.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sachtech.avtar.learningabcfreee.Animation.ObjectAnim;
import com.sachtech.avtar.learningabcfreee.Constants.Config;
import com.sachtech.avtar.learningabcfreee.Constants.DataHolder;
import com.sachtech.avtar.learningabcfreee.PracticeActivity;
import com.sachtech.avtar.learningabcfreee.WordsActivity;
import com.sachtech.avtar.learningabcfreee.FragmentContainerActivity;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;

public class ChooseOptionFragment extends Fragment implements View.OnClickListener {

    private ImageView mABCButton, mabcButton, m123Button, mWordsButton ;

    //private MediaPlayer mediaPlayer = null;

    private LinearLayout chooseOptionLinear;

    private static ChooseOptionFragment chooseOptionActivity;

    private String TAG = this.getClass().getSimpleName();

    public static ChooseOptionFragment getInstance() {
        return chooseOptionActivity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        chooseOptionActivity = this;


        //  playMusic();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_choose_option_2, container, false);
        initView(view);
        return view;
    }

    private void playMusic() {
        //   mediaPlayer = MediaPlayer.create(getInstance(),R.raw.alphabet_song_sml);
       /* try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            L.e(TAG,"Exception at play music  == "+e);
        }*/
        // mediaPlayer.start();
    }

    private void initView(View view) {

     /*   mShapeButton = (Button) view.findViewById(R.id.choose_activiy_shape_imagebutton);*/
        mABCButton = (ImageView) view.findViewById(R.id.choose_activiy_ABC_imagebutton);
        mabcButton = (ImageView) view.findViewById(R.id.choose_activiy_abc_imagebutton);
        m123Button = (ImageView) view.findViewById(R.id.choose_activiy_numeric_imagebutton);
        mWordsButton = (ImageView) view.findViewById(R.id.choose_activiy_words_imagebutton);


        /*chooseOptionLinear = (LinearLayout) view.findViewById(R.id.choose_activiy_linear_layout);
        ObjectAnim.startAnimation(chooseOptionLinear, 2000, getActivity(), R.animator.top_to_bottom);*/

        //mShapeButton.setOnClickListener(this);
        mABCButton.setOnClickListener(this);
        mabcButton.setOnClickListener(this);
        m123Button.setOnClickListener(this);
        mWordsButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.choose_activiy_ABC_imagebutton:
                Sound.playButtonClick(getActivity());
                DataHolder.LETTER_TYPE = Config.CAPS;
                FragmentContainerActivity.getInstance().displayFrag(Config.CHOOSE_LETTER, Config.ChooseLetterFrag);
                break;

            case R.id.choose_activiy_abc_imagebutton:
                Sound.playButtonClick(getActivity());
                DataHolder.LETTER_TYPE = Config.SMALL;
                FragmentContainerActivity.getInstance().displayFrag(Config.CHOOSE_LETTER, Config.ChooseLetterFrag);
                break;

            case R.id.choose_activiy_numeric_imagebutton:
                Sound.playButtonClick(getActivity());
                DataHolder.LETTER=Config.A;
                Intent in1 =new Intent(getActivity(), PracticeActivity.class);
                startActivity(in1);
                getActivity().overridePendingTransition(0, 0);
                break;

            case R.id.choose_activiy_words_imagebutton:
                Sound.playButtonClick(getActivity());
                DataHolder.LETTER=Config.A;
                Intent in =new Intent(getActivity(), WordsActivity.class);
                startActivity(in);
                getActivity().overridePendingTransition(0, 0);
                break;

            default:
                break;
        }

    }


    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
