package com.sachtech.avtar.learningabcfreee.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.sachtech.avtar.learningabcfreee.Constants.DataHolder;
import com.sachtech.avtar.learningabcfreee.Fragment.DrawLetterFragment;
import com.sachtech.avtar.learningabc.R;
import com.sachtech.avtar.learningabcfreee.Sound.Sound;

import java.util.ArrayList;
import java.util.List;

public class PaintView2 extends View {

    private Path mPath;
    private Paint mPaint,mPaintNext;
    private Rect m_ImageRect;
    private Rect m_TextRect;
    Bitmap bitmap;
    Context context;

    int stepCount = 1;
    int TOTAL_LETTTER_STEPS = 0;
    private static final int TOUCH_TOLERANCE_DP = 24;
    private static final int TOUCH_TOLERANCE_DP_NEXT = 10;
    private static final int BACKGROUND = 0xFFDDDDDD;
    private List<Point> mPoints = new ArrayList<Point>();

    private int mLastPointIndex = 0;
    private int mTouchTolerance;
    private boolean isPathStarted = false;

    List<Point>[] pointListArray = null;
    LetterCompleteListner letterCompleteListner = null;
    private boolean tryAgainSound =true;
    private int mTouchToleranceNext;


    public PaintView2(Context context, int TotalLetterSteps, List<Point>[] pointListArray, int imageResourceId, DrawLetterFragment letterFragment) {
        super(context);
        this.context = context;
        letterCompleteListner = letterFragment;

        this.pointListArray = pointListArray;
        TOTAL_LETTTER_STEPS = TotalLetterSteps;
        this.pointListArray = pointListArray;

        mPaintNext = new Paint();
        mPaintNext.setAntiAlias(true);
        mPaintNext.setDither(true);
        mPaintNext.setColor(Color.RED);
        mPaintNext.setStyle(Paint.Style.STROKE);
        mPaintNext.setStrokeJoin(Paint.Join.ROUND);
        mPaintNext.setStrokeCap(Paint.Cap.ROUND);
        final float scale1 = getResources().getDisplayMetrics().density;
        int Strokewidth1 = (int) (scale1 * 8);
        mPaintNext.setStrokeWidth(Strokewidth1);


        mCanvas = new Canvas();
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        final float scale = getResources().getDisplayMetrics().density;
        int Strokewidth= (int) (scale*25);
        mPaint.setStrokeWidth(Strokewidth);
        mTouchTolerance = dp2px(TOUCH_TOLERANCE_DP);
        mTouchToleranceNext = dp2px(TOUCH_TOLERANCE_DP_NEXT);
        bitmap = BitmapFactory.decodeResource(getResources(), imageResourceId);
        mPoints = pointListArray[0];
        // TODO just test points
        /*
        int x1 = (int) (scale * 80);
        int y1 = (int) (scale * 30);
        int x2 = (int) (scale * 40);
        int y2 = (int) (scale * 120);
        int x3 = (int) (scale * 120);
        int y3 = (int) (scale * 120);
        int x4 = (int) (scale * 50);
        int y4 = (int) (scale * 100);
        int x5 = (int) (scale * 110);
        int y5 = (int) (scale * 100);
        Log.e("Scale", "Scale" + scale);
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Point p5 = new Point(x5, y5);
        Point p6 = new Point(400, 500);
        mPoints.add(p1);
        mPoints.add(p2);
        mPoints1.add(p1);
        mPoints1.add(p3);
        mPoints2.add(p4);
        mPoints2.add(p5);*/


        //  init();
      /*   mPoints.add(p3);
        mPoints.add(p4);
        mPoints.add(p5);
        mPoints.add(p6);*/
    }

    /*public PaintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mCanvas = new Canvas();
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
        mTouchTolerance = dp2px(TOUCH_TOLERANCE_DP);
        bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.alphabet_caps_a_dotted);
        // TODO just test points
        Point p1 = new Point(120, 20);
       *//* Point p2 = new Point(100, 100);
        Point p3 = new Point(200, 250);
        Point p4 = new Point(280, 400);
        Point p5 = new Point(350, 600);
        Point p6 = new Point(400, 500);*//*
        mPoints.add(p1);
      *//*  mPoints.add(p2);
        mPoints.add(p3);
        mPoints.add(p4);
        mPoints.add(p5);
        mPoints.add(p6);*//*
    }

    public PaintView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mCanvas = new Canvas();
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
        mTouchTolerance = dp2px(TOUCH_TOLERANCE_DP);
    }
*/
    public void init(List<Point> pt) {
        mLastPointIndex = 0;

        mPoints = pt;

    }

    Bitmap mBitmap;
    Canvas mCanvas;

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        m_ImageRect = canvas.getClipBounds();
        m_TextRect = canvas.getClipBounds();
        //it gives you an area that can draw on it,
        //the width and height of your rect depend on your screen size device
        canvas.drawBitmap(bitmap, null, m_ImageRect, mPaint);
        canvas.save();
        canvas.clipRect(m_TextRect);

        // canvas.drawText("your text", 200, 300, paint);


        canvas.drawBitmap(mBitmap, 0, 0, null);
        canvas.drawPath(mPath, mPaint);

        // TODO remove if you dont want points to be drawn
        for (Point point : mPoints) {
            canvas.drawPoint(point.x, point.y, mPaintNext);
        }
         /* for (Point point : mPoints1) {
            canvas.drawPoint(point.x, point.y, mPaint);
        }
        for (Point point : mPoints2) {
            canvas.drawPoint(point.x, point.y, mPaint);
        }*/
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up(x, y);
                invalidate();
                break;
        }

        return true;
    }

    private void touch_start(float x, float y) {

        if (checkPoint(x, y, mLastPointIndex)) {
            mPath.reset();
            // user starts from given point so path can beis started
            Log.e("Touch_Start_If ", "Callled>" + mLastPointIndex);
            isPathStarted = true;
            Point p = mPoints.get(mLastPointIndex);
            mPath.moveTo(p.x, p.y);
            tryAgainSound = true;


        } else {
            // user starts move from point which doen's belongs to mPinst list
            isPathStarted = false;

            Log.e("Touch_Start_If_else ", "Callled>" + mLastPointIndex);
        }

    }


    //ADDED WITH LAST EDIT
    private void touch_move(float x, float y) {
        // draw line with finger move
        if (isPathStarted) {
           // mPath.reset();
            Point p = mPoints.get(mLastPointIndex);
            Log.e("Touch_move", "Callled>" + mLastPointIndex);
            mPath.lineTo(x, y);
            nextPoint(x,y);
        }
    }
    private void nextPoint(float x,float y){
        if (checkNextPoint(x, y, mLastPointIndex + 1) && isPathStarted) {
            mCanvas.drawPath(mPath, mPaint);
            Point p = mPoints.get(mLastPointIndex+1);
            // mPath.moveTo(p.x,p.y);
            // mPath.reset();
            // increment point index
            mLastPointIndex++;
            tryAgainSound = false;
            if (stepCount != TOTAL_LETTTER_STEPS) {
                init(pointListArray[stepCount]);

                stepCount++;
                return;
            }
            if (stepCount == TOTAL_LETTTER_STEPS) {

                DataHolder.Word_Count++;
                //  Toast.makeText(context, "A is completed", Toast.LENGTH_SHORT).show();
                letterCompleteListner.OnLetterComplete();


                return;
            }
        }else {

        }
    }
    /**
     * Draws line.
     */
    private void touch_up(float x, float y) {

        // mPath.reset();


        if (checkPoint(x, y, mLastPointIndex ) && isPathStarted) {
            // move finished at valid point so draw whole line

            // start point
            Point p = mPoints.get(mLastPointIndex);
            // mPath.moveTo(p.x, p.y);
            Log.e("Touch_up", "Callled>" + mLastPointIndex);
            // end point
            p = mPoints.get(mLastPointIndex );
            //  mPath.lineTo(p.x, p.y);
            // mCanvas.drawPath(mPath, mPaint);
            mPath.reset();
            // increment point index
            //   ++mLastPointIndex;

           /* if (mLastPointIndex == mPoints.size()){
                Log.e("init>>>>>>>>>>>>>>>>>>",">init");
                init(mPoints1);
            }*/
            isPathStarted = false;
            tryAgainSound = true;
            Log.e("Draw done ", ">>>>>>>>>>>done >>>>>>>>>");


            /*if (STEP_COUNT != TOTAL_LETTTER_STEPS) {
                init(pointListArray[STEP_COUNT]);

                STEP_COUNT++;
                return;
            }
            if (STEP_COUNT == TOTAL_LETTTER_STEPS) {

                DataHolder.Word_Count++;
                //  Toast.makeText(context, "A is completed", Toast.LENGTH_SHORT).show();
                letterCompleteListner.OnLetterComplete();


                return;
            }*/

        } else {
            if (isPathStarted)
                if (tryAgainSound){
                    Sound.playLetterSound(context, R.raw.tryagain);
                }
            mPath.reset();
        }


        // out of bounds


    }

    /**
     * Sets paint
     *
     * @param paint
     */
    public void setPaint(Paint paint) {
        this.mPaint = paint;
    }

    /**
     * Returns image as bitmap
     *
     * @return
     */
    public Bitmap getBitmap() {
        return mBitmap;
    }

    /**
     * Clears canvas
     */
    public void clear() {
        mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        mBitmap.eraseColor(BACKGROUND);
        mCanvas.setBitmap(mBitmap);
        invalidate();
    }

    /**
     * Checks if user touch point with some tolerance
     */
    private boolean checkPoint(float x, float y, int pointIndex) {
        if (pointIndex == mPoints.size()) {
            return false;
        }
        Point point = mPoints.get(pointIndex);
        //EDIT changed point.y to poin.x in the first if statement
        if (x > (point.x - mTouchTolerance) && x < (point.x + mTouchTolerance)) {
            if (y > (point.y - mTouchTolerance) && y < (point.y + mTouchTolerance)) {

                Log.e("mTouchTolerance done ", ">>>>>>>>>>>mTouchTolerance >>>>>>>>>");
                return true;
            }
        }


        return false;
    }
    private boolean checkNextPoint(float x, float y, int pointIndex) {
        Log.e("checkPoint outside ", "called");

        try {
            Point point = mPoints.get(pointIndex);
            if (x > (point.x - mTouchToleranceNext) && x < (point.x + mTouchToleranceNext)) {
                Log.e("mTouch outside ", "mTouch");
                if (y > (point.y - mTouchToleranceNext) && y < (point.y + mTouchToleranceNext)) {

                    Log.e("mTouchTolerance done ", ">>>>>>>>>>>mTouchTolerance >>>>>>>>>");
                    return true;
                }
            }
        } catch (Exception ex) {
            Log.e("Exception  checkPoint", ">> " + ex);
        }

        //EDIT changed point.y to poin.x in the first if statement


        return false;
    }
    public List<Point> getPoints() {
        return mPoints;
    }

    public void setPoints(List<Point> points) {
        this.mPoints = points;
    }

    private int dp2px(int dp) {
        Resources r = getContext().getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }
}